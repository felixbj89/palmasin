<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content modal-border modal-color">
			<div class="modal-header flex-center">
				<b class="close" data-dismiss="modal"><i class="fa fa-times-circle close-modal" aria-hidden="true"></i></b>
				<img src="{{url('img/logo.png')}}" class="img-responsive center" width="128" height="64"/>
			</div>
			<div class="modal-body">
				<div class="row center">
					<div class="col-xs-12 col-md-12">
						<h4 id="mensajeadvertencia" class="text-center title-palmas"></h4>
						<h4 id="mesesadvertencia" class="text-center title-palmas"></h4>
						<h4 id="promoadvertencia" class="text-center title-palmas"></h4>
						<h4 id="perfiladvertencia" class="text-center title-palmas"></h4>
						<h4 id="roomsadvertencia" class="text-center title-palmas"></h4>
						<h4 id="avataradvertencia" class="text-center title-palmas"></h4>
						<h4 id="proyectosadvertencia" class="text-center title-palmas"></h4>
						<h4 id="usuariosadvertencia" class="text-center title-palmas"></h4>
						<h4 id="galeriahoteladvertencia" class="text-center title-palmas"></h4>
						<h4 id="galeriahabadvertencia" class="text-center title-palmas"></h4>
						<h4 id="listusersadvertencia" class="text-center title-palmas"></h4>
						<h4 id="comercialesadvertencia" class="text-center title-palmas"></h4>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-md-12">
						<button id="btnAcceder" class="btn btn-palmasinn center" type="button" data-dismiss="modal">
							<i class="fa fa-sign-out" aria-hidden="true"></i> ACEPTAR
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>