<div class="modal fade" id="modalEventospalmas" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="padding-bottom:0px;">
				<i id="wait" class="titulo" data-dismiss="modal">X</i>
				<p class="tituloitalic text-center">{{e($titulo_eventos)}}</p>
			</div>
			<div class="modal-body" style="padding-top:0px;">
				<div class="row">
					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								{!!$text_one!!}
							</div>
							<div class="col-xs-12 col-sm-6">
								{!!$text_two!!}
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-center">
						<button id="galevent" class="btn">Galeria</button>
						<button id="reserevet" class="btn">Reservar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
