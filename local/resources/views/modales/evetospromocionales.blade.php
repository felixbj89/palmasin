<div class="modal fade" id="modalEvepromocionalpalmas" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="padding-bottom:0px;">
				<i id="wait" class="titulo" data-dismiss="modal">X</i>
				<p class="titulo">EVENTOS PROMOCIONALES</p>
				<p>Para procesar su solicitud es necesario completar los siguientes campos</p>
			</div>
			<div class="modal-body" style="padding-top:0px;">
				<div class="row">
					<div class="col-xs-12">
						<table style="height:100%;width:100%;"><tr><td>
							<form id="form-contactusevep" method="post">
								<div class="form-group col-xs-12">
									<input type="text" class="inputpalmasPromo form-control" name="evep-nombre" id="evep-nombre" placeholder="Nombre" minlength="2"required="">
									<span class="text-danger pull-right requerido">El nombre es requerido</span>
								</div>
								<div class="form-group col-xs-12">
									<input type="text" class="inputpalmasPromo form-control" name="evep-apellido" id="evep-apellido" placeholder="Apellido" minlength="2" required="">
									<span class="text-danger pull-right requerido">El apellido es requerido</span>
								</div>
								<div class="form-group col-xs-12">
									<input type="text" class="inputpalmasPromo form-control" name="evep-telefono" id="evep-telefono" placeholder="Teléfono +00-123-1234567" minlength="16" required=""/>
									<span class="text-danger pull-right requerido">El telefono es requerido</span>
									<span class="text-danger pull-right invalido">El telefono es invalido</span>
								</div>
								<div class="form-group col-xs-12">
									<input type="email" class="inputpalmasPromo form-control" name="evep-correo" id="evep-correo" placeholder="e-mail" required="">
									<span class="text-danger pull-right requerido">El correo es requerido</span>
									<span class="text-danger pull-right invalido">El correo es invalido</span>
								</div>
								<div class="form-group col-xs-12">
									<textarea class="inputpalmasPromo form-control" name="evep-Mensaje" id="evep-Mensaje" placeholder="Solicitud o requerimiento" style="resize:none;" rows="5" minlength="20" required=""></textarea>
									<span class="text-danger pull-right requerido">El mensaje es requerido</span>
									<span class="text-danger pull-right minlength">El mensaje es muy corto</span>
								</div>
								<div class="form-group col-xs-12">
									<div id="container-btnenv">
										<button type="button" id="btn-evep" name="btn-evep" disabled>ENVIAR</button>
										<button type="button" id="btnClose-evep" data-dismiss="modal">ACEPTAR</button>
									</div>
								</div>
							</form>
						</td></tr></table>
					</div>
					<div id="retornoevep" class="form-group col-xs-12">
						<div class="row" style="margin:0px;">
							<div id="spin"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#3f7d9e;display:block;margin:0px auto;"></i><label style="display:block;text-align:center;color:#3f7d9e;font-family:Lato-Regular;">Enviando...</label></div>
							<div id="status"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>