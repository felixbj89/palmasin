<!-- Modal LightBox -->
<div id="modalGaleriapalmas" class="modal">
	<span id="closeModal" data-dismiss="modal">X</span>
	<div id="BootstrapCarouselLightBox" class="carousel slide carousel-fade" data-ride="carousel"  data-interval="false">
		<ol id="LightBox-indicators" class="carousel-indicators"></ol>
		<div id="BootstrapCarouse-innerlLightBox" class="carousel-inner" role="listbox" style="height:100%;">
			<!--<div class="item active"><div><div><img src="{{url('img/eventoscorporativos/eventoscorporativos1.jpg')}}"></div></div></div>
			<div class="item"><div><div><img src="{{url('img/eventoscorporativos/eventoscorporativos2.jpg')}}"></div></div></div>
			<div class="item"><div><div><img src="{{url('img/eventoscorporativos/eventoscorporativos3.jpg')}}"></div></div></div>-->
		</div>
		<div id="arrowLightBoxNext" class="controlLightBox controlLightBox-right" data-target="#BootstrapCarouselLightBox" data-slide="next"><object class="arrowLightBox" data="{{url('img/next.svg')}}" type="image/svg+xml" style="pointer-events:none;"></object></div>
		<div id="arrowLightBoxPrev" class="controlLightBox controlLightBox-left" data-target="#BootstrapCarouselLightBox" data-slide="prev"><object class="arrowLightBox" data="{{url('img/prev.svg')}}" type="image/svg+xml" style="pointer-events:none;"></object></div>
	</div>
</div>