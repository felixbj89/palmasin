<div class="modal fade" id="modal_clonarevento" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content modal-border modal-color">
			<div class="modal-header flex-center">
				<b class="close" data-dismiss="modal"><i class="fa fa-times-circle close-modal" aria-hidden="true"></i></b>
				<img src="{{url('img/logo.png')}}" class="img-responsive center" width="128" height="64"/>
			</div>
			<div class="modal-body">
			<input type="hidden" id="mindate" name="mindate" value="{{e($fecha_min)}}"/>
			<input type="hidden" id="evento_target" name="evento_target"/>
				<div class="row center">
					<div class="col-xs-12 col-md-12">
						<div class="row form-group">
							<div class="col-xs-12">
								<h3 class="box-title text-white pull-left">NUVA FECHA DEL EVENTO.</h3>
							</div>
							<div class="col-xs-12 - col-sm-6">
								<label class="control-label clearfix text-white">Inicio</label>
								<input type="text" id="inicio_evento" class="form-control form-border" name="inicio_evento" placeholder="*INICIO DEL EVENTO"/>
								<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
							</div>
							<div class="col-xs-12 - col-sm-6">
								<label class="control-label clearfix text-white">Fin</label>
								<input type="text" id="fin_evento" class="form-control form-border" name="fin_evento" placeholder="*FIN DEL EVENTO"/>
								<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-6">
						<button id="btnclonar" class="btn btn-palmasinn center" type="button">
							<i class="fa fa-clipboard" aria-hidden="true"></i> PEGAR
						</button>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-palmasinn center" type="button" data-dismiss="modal">
							<i class="fa fa-sign-out" aria-hidden="true"></i> CERRAR
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>