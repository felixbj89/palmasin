<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content modal-border modal-color">
			<div class="modal-header flex-center">
				<img src="{{url('img/logo.png')}}" class="img-responsive center" width="128" height="64"/>
			</div>
			<div class="modal-body">
				<div class="row center">
					<div class="col-xs-12 col-md-12">
						<h4 id="mensajeadvertencia" class="text-center title-palmas">Espere un momento</h4>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-md-12">
						<img src="{{asset('img/ring.svg')}}" class="img-responsive img-circle center"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>