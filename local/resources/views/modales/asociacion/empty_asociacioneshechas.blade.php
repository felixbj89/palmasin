<div class="modal fade" id="empty_asohechas" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content modal-border modal-color">
			<div class="modal-header flex-center">
				<img src="{{url('img/logo.png')}}" class="img-responsive center" width="128" height="64"/>
			</div>
			<div class="modal-body">
				<div class="row center">
					<div class="col-xs-12 col-md-12">
						<h4 id="mensajeadvertencia" class="text-center title-palmas">{{e($textoasochechas)}}</h4>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-md-6">
						<a href="{{url('admin/dashboard')}}" id="btnAcceder" class="btn btn-palmasinn center" type="button">
							<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
						</a>
					</div>
					<div class="col-xs-12 col-md-6">
						<a href="{{url($ruta)}}" id="btnAcceder" class="btn btn-palmasinn center" type="button">
							<i class="fa fa-save" aria-hidden="true"></i> REGISTRAR
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>