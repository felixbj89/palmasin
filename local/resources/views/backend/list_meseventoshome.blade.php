@extends("layout.dashboard")
@section("title_section")
	EVENTOS
@endsection
@section("sub_title")
	EVENTOS
@endsection
@section("title-divsection")
	Lista de meses disponibles
@endsection
@section("eventos-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/eventos.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_promo'))
		<input type="hidden" name="server" id="server" value="{{Session::get('server_promo')}}" />
	@endif
	<div class="row form-group">
		<div class="col-xs-12 col-md-12">
			<table id="event_list" class="table table-striped">
				<thead>
					<tr class="fondo-tr">
						<th>Mes</th>
						<th>Año</th>
						<th>Lanzar</th>
						<th>Eventos</th>
					</tr>
				</thead>
				<tbody>
					{!!$list_meses!!}	
				</tbody>
				<tfooter>
					<tr>
						<th>Mes</th>
						<th>Año</th>
						<th>Lanzar</th>
						<th>Eventos</th>
					</tr>
				</tfooter>
			</table>
		</div>
	</div>
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-8">
			<a href="{{url('admin/listar_eventos')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-chevron-up"></i> LISTAR
			</a>
		</div>
	</div>
@endsection
@section("scripts")
	<script src="{{asset('js/list_mesesevetoshome.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar_remover")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.alerta")
	@include("modales.empty")
@endsection
