@extends("layout.dashboard")
@section("title_section")
	<i class='fa fa-home fa-2x'></i>
@endsection
@section("sub_title")
	ADMINISTRAR HOME
@endsection
@section("title-divsection")
	HOME
@endsection
@section("galeria-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeriarestaurant_palmas.css')}}">
	<link href="{{asset('plugins/HoverEffectIdeas/css/normalize.css')}}" rel="stylesheet"/>
	<link href="{{asset('plugins/HoverEffectIdeas/css/set2.css')}}" rel="stylesheet"/>
@endsection
@section("body-section")
<div class="row">
	<div class="col-xs-12">
		<div class="row form-group">
			<div class="col-xs-12">
				<h3 class="box-title text-white pull-left">ADMINISTRAR HOME.</h3>
			</div>
			<div class="col-xs-12" id="conten_galeria">
				<div id="info" class="alert alert-info text-center">
					 <h3><strong>Para comenzar</strong> seleccione una opción que desee gestionar.</h3>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12">
				<div class="grid flex-center">
					<figure class="effect-ming fixed-border">
						<img src="{{asset('img/galeria.jpg')}}" alt="GALERÍAHCVO">
						<figcaption>
							<h2 class="text-bluedark"><i class="fa fa-cog" aria-hidden="true"></i></h2>
							<p class="text-bluedark"><strong>Administrar</strong> su galería principal</p>
							<a href="{{url('admin/gestionargaleriaprincipal')}}">View more</a>
						</figcaption>
					</figure>
					<figure class="effect-ming fixed-border">
						<img src="{{asset('img/galeria.jpg')}}" alt="GALERÍAHCVO">
						<figcaption>
							<h2 class="text-bluedark"><i class="fa fa-picture-o" aria-hidden="true"></i></h2>
							<p class="text-bluedark">Haga <strong>Click</strong> para ir a su galería secundaria</p>
							<a href="{{url('admin/listar_galerinferior')}}">View more</a>
						</figcaption>
					</figure>
				</div>
				<div class="grid flex-center">
					<figure class="effect-ming fixed-border">
						<img src="{{asset('img/galeria.jpg')}}" alt="GALERÍAHCVO">
						<figcaption>
							<h2 class="text-bluedark"><i class="fa fa-server" aria-hidden="true"></i></h2>
							<p class="text-bluedark">Haga <strong>Click</strong> para ir a su servicios</p>
							<a href="{{url('admin/crear_servicios')}}">View more</a>
						</figcaption>
					</figure>
					<figure class="effect-ming fixed-border">
						<img src="{{asset('img/galeria.jpg')}}" alt="GALERÍAHCVO">
						<figcaption>
							<h2 class="text-bluedark"><i class="fa fa-cog" aria-hidden="true"></i></h2>
							<p class="text-bluedark">Haga <strong>Click</strong> para administrar su imagen de salud</p>
							<a href="{{url('admin/gestionarimagensalud')}}">View more</a>
						</figcaption>
					</figure>
				</div>
			</div>
		</div>
	</div>
</div>
<hr/>
<div class="row form-group">
	<div class="col-xs-12 col-md-8">
		<a id="btnListar" href="{{url('admin/dashboard')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-dashboard"></i> DASHBOARD
		</a>
	</div>
</div>
@endsection
@section("scripts")
	<script src="{{asset('js/galeriarestaurant_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.restaurante.alerta")
	@include("modales.wait")
@endsection