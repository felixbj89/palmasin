@extends("layout.dashboard")
@section("title_section")
	SERVICIOS
@endsection
@section("sub_title")
	SERVICIOS
@endsection
@section("title-divsection")
	Complete la información para poder registrar.
@endsection
@section("servicios-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/promociones.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_servicios'))
		<input type="hidden" name="server_servicios" id="server_servicios" value="{{e(Session::get('server_servicios'))}}" />
	@elseif(count($errors) > 0)
		<input type="hidden" name="error_servicios" id="error_servicios" value="{{e($errors->all()[0])}}" />
	@endif
	<form id="form-servicios" action="{{url('admin/editservicioshome')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">INFORMACIÓN DE SU SERVICIO.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Título</label>
				<input type="text" id="titulo" class="form-control form-border" name="titulo" placeholder="*TÍTULO A INGRESAR" value="{{e($servicios->servicios_titulo)}}"/>
				<span class="fa fa-pencil form-control-feedback icon-space"></span>
				<hr/>
				<label class="clearfix pull-right text-white" id="cantidad-titulo">0/40</label>
			</div>
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Subtítulo</label>
				<input type="text" id="subtitulo" class="form-control form-border" name="subtitulo" placeholder="*SUBTÍTULO A INGRESAR" value="{{e($servicios->servicios_subtitulo)}}"/>
				<span class="fa fa-pencil form-control-feedback icon-space"></span>
				<hr/>
				<label class="clearfix pull-right text-white" id="cantidad-subtitulo">0/40</label>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">DESCRIPCIÓN DE SU SERVICIO.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<textarea name="descripcion_promociones" id="descripcion_promociones" class="form-border form-control summernote">
					 {{e($servicios->servicios_texto)}}
				</textarea>
				<hr/>
				<label class="clearfix pull-right text-white" id="cantidad-descripcion">0/300</label>
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnModificar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-edit"></i> MODIFICAR
				</a>
				<a href="{{url('admin/administrargaleria')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-undo"></i> REGRESAR
				</a>
			</div>
		</div>
	</form>
@endsection
@section("scripts")
	<script src="{{asset('js/servicios_home.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.servicios.alerta")
	@include("modales.wait")
@endsection
