@extends("layout.dashboard")
@section("title_section")
	<i class='fa fa-medkit fa-2x'></i>
@endsection
@section("sub_title")
	Imagen
@endsection
@section("title-divsection")
	Imagen de Salud.
@endsection
@section("galeria-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeriarestaurant_palmas.css')}}">
	<link href="{{asset('plugins/HoverEffectIdeas/css/normalize.css')}}" rel="stylesheet"/>
	<link href="{{asset('plugins/HoverEffectIdeas/css/set2.css')}}" rel="stylesheet"/>
@endsection
@section("body-section")
<div class="row">
	<div class="col-xs-12">
		<div class="row form-group">
			<div class="col-xs-12">
				<h3 class="box-title text-white pull-left">IMAGEN DE SALUD.</h3>
			</div>
			<div class="col-xs-12" id="conten_galeria">
				<div id="info" class="alert alert-info text-center">
					 <h3><strong>Para comenzar</strong> seleccione para administrar su imagen de salud.</h3>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12">
				<div class="grid flex-center">
					<figure class="effect-ming fixed-border">
						<img src="{{asset('img/galeria.jpg')}}" alt="GALERÍAHCVO">
						<figcaption>
							<h2 class="text-bluedark"><i class="fa fa-medkit" aria-hidden="true"></i></h2>
							<p class="text-bluedark">Haga <strong>Click</strong> para registrar una imágen</p>
							<a href="{{url('admin/crear_bsalud')}}">View more</a>
						</figcaption>
					</figure>
					<figure class="effect-ming fixed-border">
						<img src="{{asset('img/galeria.jpg')}}" alt="GALERÍAHCVO">
						<figcaption>
							<h2 class="text-bluedark"><i class="fa fa-book" aria-hidden="true"></i></h2>
							<p class="text-bluedark">Haga <strong>Click</strong> para visualizar su historial</p>
							<a href="{{url('admin/listar_bsalud')}}">View more</a>
						</figcaption>
					</figure>
				</div>
			</div>
		</div>
	</div>
</div>
<hr/>
<div class="row form-group">
	<div class="col-xs-12 col-md-8">
		<a id="btnListar" href="{{url('admin/administrargaleria')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-undo"></i> REGRESAR
		</a>
	</div>
</div>
@endsection
@section("scripts")
	<script src="{{asset('js/galeriarestaurant_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.restaurante.alerta")
	@include("modales.wait")
@endsection