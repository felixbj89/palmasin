@extends("layout.dashboard")
@section("title_section")
	Restaurantes / Inicio
@endsection
@section("sub_title")
	Restaurantes
@endsection
@section("title-divsection")
	Galeria.
@endsection
@section("restaurantes-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeriarestaurant_palmas.css')}}">
@endsection
@section("body-section")
<div class="row">
	<div class="col-xs-12">
		<div class="row form-group">
			<div class="col-xs-12">
				<h3 class="box-title text-white pull-left">GALERIA RESTAURANTES.</h3>
				<input type="hidden" id="idrest" name="idrest" value="{{$idrest}}"/>
			</div>
			<div class="col-xs-12" id="conten_galeria">
				<div id="info" class="alert alert-info text-center">
					 <h3><strong>Para comenzar</strong> seleccione la imagen que desea cambiar.</h3>
					 * <strong>Recuerda para crear una galeria</strong> debes cargar un minimo de cinco (5) imagenes y un maximo de ocho (8). *</br>
					 * <strong>Recuerda para crear una galeria</strong> debes seleccionar tres(4) imagenes destacadas. *
				</div>
				{!!$galeria!!}
			</div>
		</div>
	</div>
</div>
<hr/>
<div class="row form-group">
	<div class="col-xs-12 col-md-8">
		<a id="btnListar" href="{{url('admin/crear_galeriahotel')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-undo"></i> REGRESAR
		</a>
		<a id="btnListar" href="{{url('admin/listar_restaurantes')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-chevron-up"></i> REGISTRAR
		</a>
		<a id="btnListar" href="{{url('admin/listar_restfija')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-chevron-down"></i> FOTO SECUNDARIA
		</a>
	</div>
</div>
@endsection
@section("scripts")
	<script src="{{asset('js/galeriarestaurant_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.restaurante.alerta")
	@include("modales.wait")
@endsection