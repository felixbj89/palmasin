@extends("layout.dashboard")
@section("title_section")
	HABITACIONES
@endsection
@section("sub_title")
	HABITACIONES
@endsection
@section("title-divsection")
	Complete la información para poder registrar.
@endsection
@section("cuartos-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/habitad_palmas.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_promo'))
		<input type="hidden" name="server" id="server" value="{{e(Session::get('server_promo'))}}" />
	@endif
	<form id="form-habitad" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<div class="row form-group">
			<div class="col-xs-12 col-sm-6">		
				<div class="row form-group">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">INFORMACIÓN DE LA HABITACIÓN.</h3>
					</div>
					<div class="col-xs-12">
						<label class="control-label clearfix text-white">Título</label>
						<input type="text" id="titulo_habitad" class="form-control form-border" name="titulo_habitad" placeholder="*TÍTULO A INGRESAR"/>
						<span class="fa fa-pencil form-control-feedback icon-space"></span>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad-titulo">0/30</label>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-sm-6">
				<div class="row form-group">
					<div class="col-xs-12 ">
						<h3 class="box-title text-white pull-left">DESCRIPCIÓN HABITACIÓN.</h3>
					</div>
					<div class="col-xs-12">
						<div id="editor_habitad" class="summernote"></div>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad_descripcion">0/300</label>
					</div>
				</div>			
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="row form-group">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">LISTA DE CARACTERÍSTICAS.</h3>
					</div>
					<div class="col-xs-12">
						<div id="caracteristicas_habitad" class="summernote"></div>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad_caracteristicas">0/300</label>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-sm-6">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">LISTA DE COMODIDADES.</h3>
				</div>
				<div id="comodidades" class="col-xs-12">
					<label class="checkbox-inline ico ico1"><input type="checkbox" value="1" name="comodidades[]" checked><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico2"><input type="checkbox" value="2" name="comodidades[]" checked><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico3"><input type="checkbox" value="3" name="comodidades[]" checked><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico4"><input type="checkbox" value="4" name="comodidades[]" checked><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico5"><input type="checkbox" value="5" name="comodidades[]" checked><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico6"><input type="checkbox" value="6" name="comodidades[]" checked><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico7"><input type="checkbox" value="7" name="comodidades[]" checked><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico8"><input type="checkbox" value="8" name="comodidades[]" checked><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">IMAGEN PORTADA HABITACION.</h3>
				</div>				
				<div class="col-xs-12">
					<input type="hidden" id="url1" name="url1" value="{{asset('img/home/promociones/default.jpg')}}"/>
					<img src="{{asset('img/home/promociones/default.jpg')}}" id="uno_picture" class="img-responsive box-image box-promociones">
				</div>
				<div class="col-xs-12">
					<!--label class="control-label clearfix text-white">Suba su promoción</label-->
					<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
				</div>
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnRegistrar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-save"></i> REGISTRAR
				</a>
				<a id="btnListar" href="{{url('admin/listar_habitacion')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-down"></i> LISTAR
				</a>
			</div>
		</div>
	</form>
@endsection
@section("scripts")
	<script src="{{asset('js/habitad_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.habitaciones.alerta")
@endsection