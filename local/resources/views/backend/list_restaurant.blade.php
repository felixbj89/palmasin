@extends("layout.dashboard")
@section("title_section")
	RESTAURANTES
@endsection
@section("sub_title")
	RESTAURANTES
@endsection
@section("title-divsection")
	Lista de restaurantes registrados.
@endsection
@section("restaurantes-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/listrestaurant_palmas.css')}}">
@endsection
@section("body-section")
<div class="row">
	<div class="col-xs-12" id="row_edit">
		<table id="restaurant_list" class="table table-striped">
			<thead>
				<tr class="fondo-tr">
					<th>Restaurant</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($restaurat as $lista)
					<tr class="fondo-tr" data-restaurat='{"restaurat":"{{base64_encode(e($lista["id"]))}}"}'>
						<td>
							<img src="{{url($lista['restaurant_rest_file2'])}}" class="img-responsive"/>
							{{e($lista['restaurant_title'])}}
						</td>
						<td>
							<div class="row form-group center">
								<div class="col-xs-12">
									<a class="btn btn-app btn-palmasinn btnModificar center" data-restauranteid='{"restaurat":"{{base64_encode(e($lista["id"]))}}"}'>
										<i class="fa fa-pencil-square-o"></i> MODIFICAR
									</a>
								</div>
							</div>
						</td>
					</tr>				
				@endforeach
			</tbody>
			<tfooter>
				<tr>
					<th>Restaurant</th>
					<th>Opciones</th>
				</tr>
			</tfooter>
		</table>
	</div>
</div>
<hr/>
<div class="row form-group">
	<div class="col-xs-12 col-md-8">
		<a id="btnListar" href="{{url('admin/crear_restgaleria')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-chevron-down"></i> GALERÍA
		</a>
	</div>
</div>
@endsection
@section("scripts")
	<script src="{{asset('js/listrestaurant_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.restaurante.alerta")
@endsection