@extends("layout.dashboard")
@section("title_section")
	EVENTOS
@endsection
@section("sub_title")
	EVENTOS
@endsection
@section("title-divsection")
	Listar Eventos Registrados.
@endsection
@section("eventos-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/eventos.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_eventos'))
		<input type="hidden" name="server_eventos" id="server_eventos" value="{{Session::get('server_eventos')}}" />
	@endif
	<div class="row">
		<div style="max-width:1024px;margin-left:auto;margin-right:auto;" style="margin-left:0px;margin-right:0px;">
			<ul class="row list-inline list-date list-year">
				<li class="col-xs-4 text-center"><button data-year="{{e($year - 1)}}">{{e($year - 1)}}</a></li>
				<li class="col-xs-4 text-center"><button data-year="{{e($year)}}" class="active">{{e($year)}}</a></li>
				<li class="col-xs-4 text-center"><button data-year="{{e($year + 1)}}">{{e($year + 1)}}</a></li>
			</ul>
			<ul class="row list-inline list-date list-mes" style="margin-left:0px;margin-right:0px;">
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="01" @if($mes == 1)class="active"@endif>ene</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="02" @if($mes == 2)class="active"@endif>feb</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="03" @if($mes == 3)class="active"@endif>mar</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="04" @if($mes == 4)class="active"@endif>abr</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="05" @if($mes == 5)class="active"@endif>may</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="06" @if($mes == 6)class="active"@endif>jun</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="07" @if($mes == 7)class="active"@endif>jul</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="08" @if($mes == 8)class="active"@endif>ago</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="09" @if($mes == 9)class="active"@endif>sep</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="10" @if($mes == 10)class="active"@endif>otc</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="11" @if($mes == 11)class="active"@endif>nov</button></li>
				<li class="col-sm-4 col-md-1 text-center"><button data-mes="12" @if($mes == 12)class="active"@endif>dic</button></li>
			</ul>
		</div>
		<div class="col-xs-12 col-md-12">
			@if(count($list_eventos) > 0)
			<table id="event_list" class="table table-striped">
				<thead>
					<tr class="fondo-tr">
						<th>Nombre</th>
						<th>Fecha</th>
						<th>Principal</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($list_eventos as $lista)
						<tr class="fondo-tr" data-historial='{"evento":"{{base64_encode(e($lista->id))}}"}'>
							<td>
								{{e($lista->event_title)}}
							</td>
							<td>
								{{e($lista->event_dateini)}}
							</td>
							<td>
								<div class="row">
								@if($lista->event_ppal==0)<!--no es principal-->
									<div class="col-xs-12">
										<a class="btnActivar">
											<i class="fa fa-toggle-off fa-2x"></i>
										</a>
									</div>
								@else
									<div class="col-xs-12">
										<a class="btnActivar">
											<i class="fa fa-toggle-on fa-2x"></i>
										</a>
									</div>
								@endif
								</div>
							</td>	
							<td>
								<div class="row form-group center">
									<div class="col-xs-12 col-sm-12 col-md-4">
										<a class="btn btn-app btn-palmasinn btnModificar center">
											<i class="fa fa-pencil-square-o"></i> MODIFICAR
										</a>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-4">
										<a class="btn btn-app btn-palmasinn btnRemover center">
											<i class="fa fa-trash"></i> REMOVER
										</a>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-4">
										<a class="btn btn-app btn-palmasinn btnclone center">
											<i class="fa fa-clone"></i> COPIAR
										</a>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfooter>
					<tr>
						<th>Nombre</th>
						<th>Fecha</th>
						<th>Principal</th>
						<th>Opciones</th>
					</tr>
				</tfooter>
			</table>
			@else
				<label class="control-label clearfix center text-left alert-warning spacing-padding">No hay eventos registradas en el sistema.</label>
			@endif
		</div>
	</div>
	@if(count($list_eventos) > 0)
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a href="{{url('admin/crear_eventos')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-up"></i> REGISTRAR
				</a>
				<a id="btnListar" href="{{url('admin/listar_meseseventos')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-down"></i> MESES
				</a>
			</div>
		</div>
  @endif
@endsection
@section("scripts")
	<script src="{{asset('js/list_evetoshome.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar_remover")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.empty")
	@include("modales.eventos.editalerta")
	@include("modales.eventos.clonevento")
@endsection
