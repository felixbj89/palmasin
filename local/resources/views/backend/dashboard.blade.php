@extends("layout.dashboard")
@section("title_section")
	Dashboard
@endsection
@section("sub_title")
	Dashboard
@endsection
@section("dashboard-active")
	active
@endsection
@section("title-divsection")
	Resumenes Generales.
@endsection
@section("scripts")
	<script src="{{asset('js/dashboard.js')}}"></script>
@endsection
@section("body-section")
	@if(Session::has('server'))
		<input type="hidden" name="server" id="server" value="{{Session::get('server')}}" />
	@endif
	<div class="col-lg-4 col-xs-12">
		<!-- small box -->
		<div class="small-box bg-yellow">
			<div class="inner">
			<h3>{{count($rangos)}}</h3>
			<p>Promociones Activas</p>
		</div>
		<div class="icon">
			<i class="fa fa-clock-o"></i>
		</div>
			<a href="{{url('admin/listarpromohome')}}" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-4 col-xs-12">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
			<h3>{{count($vencidas)}}</h3>
			<p>Próximas Vencidas</p>
		</div>
		<div class="icon">
			<i class="fa fa-warning"></i>
		</div>
			<a href="{{url('admin/listarpromohome')}}" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-4 col-xs-12">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
			<h3>{{count($activas)}}</h3>
			<p>Próximas Promociones</p>
		</div>
		<div class="icon">
			<i class="fa fa-tags"></i>
		</div>
			<a href="{{url('admin/listarpromohome')}}" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
@endsection
@section("body-estadisticas")
	<div class="form-group">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title pull-left">Visitas Generales</h3>
			</div>
			<div class="box-body">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<!-- AREA CHART -->
					<div class="box">
						<div class="box-header with-border">
							<input type="hidden" id="meses" value="{{$meses}}"/>		
							<input type="hidden" id="visitas" value="{{$visitas}}"/>
						</div>
						<div class="box-body box-white">
							<div class="chart">
								<canvas id="areaChart" style="height:450px"></canvas>
							</div>
						</div><!-- /.box-body -->
					</div><!-- /.box -->
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title pull-left">Estadísticas registradas</h3>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="box box-default box-shape box-position">
						<div class="box-header with-border">
							<h3 class="box-title">Dispositivos</h3>
							<input type="hidden" id="dispositivosusados" value="{{e($dispositivos)}}"/>
						</div>
						<!-- /.box-header -->
						<div class="box-body box-white">
							<div class="row">
								<div class="col-md-12">
									<div class="chart-responsive">
										<canvas id="devices"></canvas>
									</div>
									<!-- ./chart-responsive -->
								</div>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.footer -->
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="box box-default box-shape box-position">
						<div class="box-header with-border">
							<h3 class="box-title">Navegadores</h3>
							<input type="hidden" id="browser" value="{{e($navegadores)}}"/>
						</div>
						<!-- /.box-header -->
						<div class="box-body box-white">
							<div class="row">
								<div class="col-md-12">
									<div class="chart-responsive">
										<canvas id="navegadores"></canvas>
									</div>
									<!-- ./chart-responsive -->
								</div>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.footer -->
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="box box-default box-shape box-position">
						<div class="box-header with-border">
							<h3 class="box-title">Secciones</h3>
							<input type="hidden" id="secciones" value="{{e($secciones)}}"/>
						</div>
						<!-- /.box-header -->
						<div class="box-body box-white">
							<div class="row">
								<div class="col-md-12">
									<div class="chart-responsive">
										<canvas id="seccionesvistas"></canvas>
									</div>
									<!-- ./chart-responsive -->
								</div>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.footer -->
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="box box-default box-shape box-position">
						<div class="box-header with-border">
							<h3 class="box-title">Países</h3>
							<input type="hidden" id="paises" value="{{e($paises)}}"/>
						</div>
						<!-- /.box-header -->
						<div class="box-body box-white">
							<div class="row">
									@if($paises[0]==0 && $paises[1]==0)
										<div class="col-md-12">
											<div class="chart-responsive box-paises">
												<img src="{{url('img/ring.svg')}}" class="img-responsive center ring-wait"/>
											</div>
											@if($paises[0]==0 && $paises[1]==0)
												<small>No sean registrado accesos</small>
											@endif
										</div>
									@else
										<div class="col-md-12">
											<div class="chart-responsive">
												<canvas id="pieChartpaises"></canvas>
											</div>
										</div>
									@endif
									<!-- ./chart-responsive -->
								<!-- /.col -->
							</div>
							<!-- /.row -->
						</div>
						<!-- /.footer -->
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section("body-resumenes")
	<div class="form-group">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title pull-right">Ultimos cambios</h3>
			</div>
			<div class="box-body">
				<div class="col-xs-12 col-md-12">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs pull-right">
							<li class="active"><a href="#activas" data-toggle="tab">Promociones Activas</a></li>
							<li class=""><a href="#avatar" data-toggle="tab">Avatares Registrados</a></li>
							<li class=""><a href="#usuarios" data-toggle="tab">Usarios Registrados</a></li>
							<li class="pull-left header"><i class="fa fa-th"></i> Registros</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="activas">
								<?php $size = count($rangos); ?>
								@if($size==0)
									<label class="control-label clearfix center text-center alert-warning spacing-padding">No hay promociones activas en el sistema.</label>
								@else
									<table id="promociondash_list" class="table table-striped table-white">
										<thead>
											<tr class="fondo-tr">
												<th>Promoción</th>
												<th>Nombre</th>
												<th>Descripción</th>
											</tr>
										</thead>
										<tbody>
											<?php for($i = 0; $i < $size; $i++){ ?>
												<tr data-promoid='{{base64_encode(e($rangos[$i]["id"]))}}'>
													<td><img id="promociones{{$rangos[$i]['id']}}" src="{{asset('img/home/promociones/'.$rangos[$i]['path'])}}" class="img-responsive img-thumbnail box-image box-list box-promociones center" width="160" height="160"></td>
													<td>{{e($rangos[$i]["title"])}}</td>
													<td>{{e(strip_tags($rangos[$i]["text"]))}}</td>
												</tr>
											<?php } ?>
										</tbody>
										<tfooter>
											<tr>
												<th>Promoción</th>
												<th>Nombre</th>
												<th>Descripción</th>
											</tr>
										</tfooter>
									</table>
								@endif
							</div>
							<div class="tab-pane" id="avatar">
								@if(count($list_avatares) > 0)
									<table id="avatares_list" class="table table-striped">
										<thead>
											<tr class="fondo-tr">
												<th>Avatares</th>
												<th>Opciones</th>
											</tr>
										</thead>
										<tbody>
											@foreach($list_avatares as $avatares)
												<tr class="fondo-tr" data-historial='{"avatar_id":"{{e(base64_encode($avatares->id))}}"}'>
													<td><img id="avatar{{$avatares->id}}" src="{{asset($avatares->avatares_path)}}" class="img-responsive img-circle box-avatares center" width="160" height="160"></td>
													<td>
														<div class="row form-group center">
															<div class="col-xs-12 col-sm-12 col-md-12">
																<a id="btnUsarAvatar" class="btn btn-app btn-palmasinn center">
																	<i class="fa fa-power-off"></i> ACTIVAR
																</a>
															</div>
														</div>
													</td>
												</tr>
											@endforeach
										</tbody>
										<tfooter>
											<tr>
												<th>Avatares</th>
												<th>Opciones</th>
											</tr>
										</tfooter>
									</table>
								@endif
							</div>
							<div class="tab-pane" id="usuarios">
								@if(count($list_usuarios) > 0)
									<table id="users_list" class="table table-striped table-white">
										<thead>
											<tr class="fondo-tr">
												<th>Avatar</th>
												<th>Nombre</th>
												<th>E-mail</th>
												<th>Rol</th>
											</tr>
										</thead>
										<tbody>
											@foreach($list_usuarios as $list)
												<tr data-historial='{"historial_id":"{{base64_encode(e($list->id))}}"}'>
													<td><img id="avatar{{$list->id}}" src="{{asset($list->user_avatar)}}" class="img-responsive img-circle box-avatares center" width="160" height="160"></td>
													<td>{{e($list->user_name)}}</td>
													<td>{{e($list->user_email)}}</td>
													<td>
														@if($list->user_role==1)
															Administrador
														@else
															Operador
														@endif
													</td>
												</tr>
											@endforeach
										</tbody>
										<tfooter>
											<tr>
												<th>Avatar</th>
												<th>Nombre</th>
												<th>E-mail</th>
												<th>Rol</th>
											</tr>
										</tfooter>
									</table>
								@else
									<label class="control-label clearfix center text-center alert-warning spacing-padding">No hay usuarios registrados en el sistema.</label>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section("notificaciones-vencidas")
	<li class="dropdown notifications-menu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-bell-o"></i>
			<span class="label label-warning">{{count($vencidas)}}</span>
		</a>
		<ul class="dropdown-menu">
			<li class="header">Promociones vencidas.</li>
			<li>
				<ul class="menu">
			<?php
				$size = count($vencidas);
				if($size==0){
			?>
					<li>
						<a href="#">
							<i class="fa fa-warning text-warning"> No hay promociones vencidas</i>
						</a>
					</li>
			<?php			
				}else{
					for($i = 0; $i < $size; $i++){
			?>

					<!-- inner menu: contains the actual data -->

					<li>
						<a href="#" data-id="{{$vencidas[$i]['id']}}" id="irpromocion">
							<i class="fa fa-warning text-warning"> {{$vencidas[$i]["title"]}}</i>
						</a>
					</li>

			<?php
					}
				}
			?>
				</ul>
			</li>
			<li class="footer"><a href="{{url('admin/listarpromohome')}}">Ver todas</a></li>
		</ul>
	</li>
@endsection
@section("notificaciones-rangos")
	<li class="dropdown notifications-menu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-clock-o"></i>
			<span class="label label-danger">{{count($rangos)}}</span>
		</a>
		<ul class="dropdown-menu">
			<li class="header">Próximas Activas.</li>
			<li>
				<ul class="menu">
			<?php
				$size = count($rangos);
				if($size==0){
			?>
					<li>
						<a href="#">
							<i class="fa fa-clock-o text-danger"> No hay promociones Activas</i>
						</a>
					</li>
			<?php
				}else{
					for($i = 0; $i < $size; $i++){
			?>

					<!-- inner menu: contains the actual data -->

					<li>
						<a href="#" data-id="{{$rangos[$i]['id']}}" id="irpromocion">
							<i class="fa fa-clock-o text-danger"> {{$rangos[$i]["title"]}}</i>
						</a>
					</li>

			<?php
					}
				}
			?>
				</ul>
			</li>
			<li class="footer"><a href="{{url('admin/listarpromohome')}}">Ver todas</a></li>
		</ul>
	</li>
@endsection
@section("notificaciones-activas")
	<li class="dropdown notifications-menu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-tags"></i>
			<span class="label label-success">{{count($activas)}}</span>
		</a>
		<ul class="dropdown-menu">
			<li class="header">Próximas promociones.</li>
			<li>
				<ul class="menu">
			<?php
				$size = count($activas);
				if($size==0){
			?>
					<li>
						<a href="#">
							<i class="fa fa-tags text-success"> No hay próximas promociones</i>
						</a>
					</li>
			<?php
				}else{
					for($i = 0; $i < $size; $i++){
			?>

					<!-- inner menu: contains the actual data -->

					<li>
						<a href="#" data-id="{{$activas[$i]['id']}}" id="irpromocion">
							<i class="fa fa-tags text-success"> {{$activas[$i]["title"]}}</i>
						</a>
					</li>

			<?php
					}
				}
			?>
				</ul>
			</li>
			<li class="footer"><a href="{{url('admin/listarpromohome')}}">Ver todas</a></li>
		</ul>
	</li>
@endsection