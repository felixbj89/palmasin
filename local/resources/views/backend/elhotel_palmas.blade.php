@extends("layout.dashboard")
@section("title_section")
	EL HOTEL, SU HISTORIA
@endsection
@section("sub_title")
	EL HOTEL, SU HISTORIA
@endsection
@section("title-divsection")
	Ingrese la información del hotel.
@endsection
@section("elhotelreal-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/elhotel.css')}}">
@endsection
@section("body-section")
<div class="row">
	<div class="col-xs-12">
		@if(Session::has('server_hotel'))
			<input type="hidden" name="server_hotel" id="server_hotel" value="{{e(Session::get('server_hotel'))}}" />
		@elseif(count($errors) > 0)
			<input type="hidden" name="error_hotel" id="error_hotel" value="{{e($errors->all()[0])}}" />
		@endif
		<form id="form-elhotel" action="{{url('admin/addelhotel')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
			<div class="row form-group">
				<div class="col-xs-12 col-md-12">
					<h3 class="box-title text-white pull-left">INFORMACIÓN DE SU HOTEL.</h3>
				</div>
			</div>
			<div class="row form-group has-feedback">
				<div class="col-xs-12 col-md-12">
					<label class="control-label clearfix text-white">Título</label>
					<input type="text" id="titulo_corporativos" class="form-control form-border" name="titulo_corporativos" placeholder="*TÍTULO A INGRESAR" value="{{$hotel->elhotel_title}}"/>
					<span class="fa fa-pencil form-control-feedback icon-space"></span>
					<hr/>
					<label class="clearfix pull-right text-white" id="cantidad-titulo">0/30</label>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12 col-md-12">
					<h3 class="box-title text-white pull-left">DESCRIPCIÓN DE SU HOTEL.</h3>
				</div>
			</div>
			<div class="row form-group has-feedback">
				<div class="col-xs-12 col-md-12">
					<textarea name="descripcion_evento1" id="descripcion_evento1" class="form-border form-control summernote">
						{{e($hotel->elhotel_texto)}}
					</textarea>
					<hr/>
					<label class="clearfix pull-right text-white" id="cantidad-descripcion1">0/898</label>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12 col-md-12">
					<h3 class="box-title text-white pull-left">SUBA LAS IMÁGENES DE SU HOTEL.</h3>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12 col-md-4 div-space">
					<label class="clearix text-white">Proyectos en curso</label>
					<input type="hidden" id="url1" name="url1" value="{{url($hotel->elhotel_picone)}}"/>
					<img src="{{url($hotel->elhotel_picone)}}" id="uno_picture" class="img-responsive box-image hotel-image box-picture">
					<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
				</div>
				<div class="col-xs-12 col-md-4 div-space">
					<label class="clearix text-white">Instalaciones</label>
					<input type="hidden" id="url2" name="url2" value="{{url($hotel->elhotel_pictwo)}}"/>
					<img src="{{url($hotel->elhotel_pictwo)}}" id="dos_picture" class="img-responsive box-image hotel-image box-picture">
					<input type="file" id="file_two" name="file_two" onchange='javascript:openFile2(event)'/>
				</div>
				<div class="col-xs-12 col-md-4 div-space">
					<label class="clearix text-white">Eventos Corporativos</label>
					<input type="hidden" id="url3" name="url3" value="{{url($hotel->elhotel_picthrid)}}"/>
					<img src="{{url($hotel->elhotel_picthrid)}}" id="tres_picture" class="img-responsive box-image hotel-image box-picture">
					<input type="file" id="file_thrid" name="file_thrid" onchange='javascript:openFile3(event)'/>
				</div>
			</div>
			<hr/>
			<div class="row form-group">
				<div class="col-xs-12 col-md-8">
					<a id="btnGuardar" class="btn btn-app btn-palmasinn">
						<i class="fa fa-edit"></i> MODIFICAR
					</a>
					<a href="{{url('admin/crear_eventoscorporativos')}}" class="btn btn-app btn-palmasinn">
						<i class="fa fa-chevron-down"></i> EVENTOS
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section("scripts")
	<script src="{{url('js/elhotelbackend.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.hotel.elhotel.alerta")
@endsection
