@extends("layout.dashboard")
@section("title_section")
	EVENTOS CORPORATIVOS
@endsection
@section("sub_title")
	EVENTOS CORPORATIVOS
@endsection
@section("title-divsection")
	Registre un evento corporativo al sistema
@endsection
@section("elhotelreal-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/eventoscorporativos.css')}}">
@endsection
@section("body-section")
<div class="row">
	<div class="col-xs-12">
		@if(Session::has('server_eventoscorporativos'))
			<input type="hidden" name="server_eventoscorporativos" id="server_eventoscorporativos" value="{{e(Session::get('server_eventoscorporativos'))}}" />
		@elseif(count($errors) > 0)
			<input type="hidden" name="error_eventoscorporativos" id="error_eventoscorporativos" value="{{e($errors->all()[0])}}" />
		@endif
		<form id="form-eventoscorporativos" action="{{url('admin/addeventoscorporativos')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
			<div class="row form-group">
				<div class="col-xs-12 col-md-12">
					<h3 class="box-title text-white pull-left">INFORMACIÓN DEL EVENTO CORPORATIVO.</h3>
				</div>
			</div>
			<div class="row form-group has-feedback">
				<div class="col-xs-12 col-md-12">
					<label class="control-label clearfix text-white">Título</label>
					<input type="text" id="titulo_corporativos" class="form-control form-border" name="titulo_corporativos" placeholder="*TÍTULO A INGRESAR" value="{{e($eventos->eventoscorporativos_name)}}"/>
					<span class="fa fa-pencil form-control-feedback icon-space"></span>
					<hr/>
					<label class="clearfix pull-right text-white" id="cantidad-titulo">0/20</label>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12 col-md-12">
					<h3 class="box-title text-white pull-left">DESCRIPCIÓN DE SU EVENTO CORPORATIVO.</h3>
				</div>
			</div>
			<div class="row form-group has-feedback">
				<div class="col-xs-12 col-md-6">
					<textarea name="descripcion_evento1" id="descripcion_evento1" class="form-border form-control summernote">
						{{e($eventos->eventoscorporativos_textone)}}
					</textarea>
					<hr/>
					<label class="clearfix pull-right text-white" id="cantidad-descripcion1">0/898</label>
				</div>
				<div class="col-xs-12 col-md-6">
					<textarea name="descripcion_evento2" id="descripcion_evento2" class="form-border form-control summernote">
						{{e($eventos->eventoscorporativos_texttwo)}}
					</textarea>
					<hr/>
					<label class="clearfix pull-right text-white" id="cantidad-descripcion2">0/898</label>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12 col-md-12">
					<h3 class="box-title text-white pull-left">GALERÍA ASOCIADA A SU EVENTO.</h3>
				</div>
			</div>
			<div class="row form-group">
				<!--div class="col-xs-12 col-sm-6 col-md-3 div-space">
					<input type="hidden" id="url1" name="url1" value="{{url($eventos->eventoscorporativos_picone)}}"/>
					<img src="{{url($eventos->eventoscorporativos_picone)}}" id="uno_picture" class="img-responsive box-image box-picture">
					<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
				</div>
				<!--div class="col-xs-12 col-sm-6 col-md-3 div-space">
					<input type="hidden" id="url2" name="url2" value="{{url($eventos->eventoscorporativos_pictwo)}}"/>
					<img src="{{url($eventos->eventoscorporativos_pictwo)}}" id="dos_picture" class="img-responsive box-image box-picture">
					<input type="file" id="file_two" name="file_two" onchange='javascript:openFile2(event)'/>
				</div>
				<!--div class="col-xs-12 col-sm-6 col-md-3 div-space">
					<input type="hidden" id="url3" name="url3" value="{{url($eventos->eventoscorporativos_picthrid)}}"/>
					<img src="{{url($eventos->eventoscorporativos_picthrid)}}" id="tres_picture" class="img-responsive box-image box-picture">
					<input type="file" id="file_thrid" name="file_thrid" onchange='javascript:openFile3(event)'/>
				</div>
				<!--div class="col-xs-12 col-sm-6 col-md-3 div-space">
					<input type="hidden" id="url4" name="url4" value="{{url($eventos->eventoscorporativos_picfour)}}"/>
					<img src="{{url($eventos->eventoscorporativos_picfour)}}" id="cuatro_picture" class="img-responsive box-image box-picture">
					<input type="file" id="file_four" name="file_four" onchange='javascript:openFile4(event)'/>
				</div-->
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="col-xs-12 box-image">
						<input type="hidden" id="url4" name="url4" value="{{url($eventos->eventoscorporativos_picone)}}"/>
						<img src="{{url($eventos->eventoscorporativos_picone)}}" id="uno_picture">
					</div>
					<div class="col-xs-12 box-file">
						<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="col-xs-12 box-image">
						<input type="hidden" id="url4" name="url4" value="{{url($eventos->eventoscorporativos_pictwo)}}"/>
						<img src="{{url($eventos->eventoscorporativos_pictwo)}}" id="dos_picture" >
					</div>
					<div class="col-xs-12 box-file">
						<input type="file" id="file_two" name="file_two" onchange='javascript:openFile2(event)'/>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="col-xs-12 box-image">
						<input type="hidden" id="url4" name="url4" value="{{url($eventos->eventoscorporativos_picthrid)}}"/>
						<img src="{{url($eventos->eventoscorporativos_picthrid)}}" id="tres_picture">
					</div>
					<div class="col-xs-12 box-file">
						<input type="file" id="file_thrid" name="file_thrid" onchange='javascript:openFile3(event)'/>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="col-xs-12 box-image">
						<input type="hidden" id="url4" name="url4" value="{{url($eventos->eventoscorporativos_picfour)}}"/>
						<img src="{{url($eventos->eventoscorporativos_picfour)}}" id="cuatro_picture">
					</div>
					<div class="col-xs-12 box-file">
						<input type="file" id="file_four" name="file_four" onchange='javascript:openFile4(event)'/>
					</div>
				</div>	
			</div>
			<hr/>
			<div class="row form-group">
				<div class="col-xs-12 col-md-8">
					<a id="btnGuardar" class="btn btn-app btn-palmasinn">
						<i class="fa fa-edit"></i> MODIFICAR
					</a>
					<a href="{{url('admin/crear_hotel')}}" class="btn btn-app btn-palmasinn">
						<i class="fa fa-chevron-up"></i> HOTEL
					</a>
					<a href="{{url('admin/crear_comerciales')}}" class="btn btn-app btn-palmasinn">
						<i class="fa fa-chevron-down"></i> COMERCIALES
					</a>
				</div>		
			</div>
		</form>
	</div>
</div>
@endsection
@section("scripts")
	<script src="{{url('js/eventoscorporativos.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.hotel.alerta")
@endsection