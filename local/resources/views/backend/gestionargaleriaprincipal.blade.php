@extends("layout.dashboard")
@section("title_section")
	<i class='fa fa-picture-o fa-2x'></i>
@endsection
@section("sub_title")
	GALERÍAS
@endsection
@section("title-divsection")
	Administar Galerías.
@endsection
@section("galeria-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeriarestaurant_palmas.css')}}">
	<link href="{{asset('plugins/HoverEffectIdeas/css/normalize.css')}}" rel="stylesheet"/>
	<link href="{{asset('plugins/HoverEffectIdeas/css/set2.css')}}" rel="stylesheet"/>
@endsection
@section("body-section")
<div class="row">
	<div class="col-xs-12">
		<div class="row form-group">
			<div class="col-xs-12">
				<h3 class="box-title text-white pull-left">ADMINISTRE SU GALERÍAS.</h3>
			</div>
			<div class="col-xs-12" id="conten_galeria">
				<div id="info" class="alert alert-info text-center">
					 <h3><strong>Para comenzar</strong> seleccione una de las opciones para administrar su galería.</h3>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12">
				<div class="grid flex-center">
					<figure class="effect-ming fixed-border">
						<img src="{{asset('img/galeria.jpg')}}" alt="GALERÍAHCVO">
						<figcaption>
							<h2 class="text-bluedark"><i class="fa fa-plus" aria-hidden="true"></i></h2>
							<p class="text-bluedark">Haga <strong>Click</strong> para registrar una imágen</p>
							<a href="{{url('admin/crear_galeria')}}">View more</a>
						</figcaption>
					</figure>
					<figure class="effect-ming fixed-border">
						<img src="{{asset('img/galeria.jpg')}}" alt="GALERÍAHCVO">
						<figcaption>
							<h2 class="text-bluedark"><i class="fa fa-cogs" aria-hidden="true"></i></h2>
							<p class="text-bluedark">Haga <strong>Click</strong> para crear una asociación</p>
							<a href="{{url('admin/listar_asociaciones')}}">View more</a>
						</figcaption>
					</figure>
				</div>
				<div class="grid flex-center">
					<figure class="effect-ming fixed-border">
						<img src="{{asset('img/galeria.jpg')}}" alt="GALERÍAHCVO">
						<figcaption>
							<h2 class="text-bluedark"><i class="fa fa-book" aria-hidden="true"></i></h2>
							<p class="text-bluedark">Haga <strong>Click</strong> para visualizar su historial</p>
							<a href="{{url('admin/listar_galeria')}}">View more</a>
						</figcaption>
					</figure>
				</div>
			</div>
		</div>
	</div>
</div>
<hr/>
<div class="row form-group">
	<div class="col-xs-12 col-md-8">
		<a id="btnListar" href="{{url('admin/administrargaleria')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-undo"></i> REGRESAR
		</a>
	</div>
</div>
@endsection
@section("scripts")
	<script src="{{asset('js/galeriarestaurant_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.restaurante.alerta")
	@include("modales.wait")
@endsection