<form id="form-restaurat" accept-charset="UTF-8" enctype="multipart/form-data">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
	<ul class="nav nav-tabs" role="tablist">
	  <li class="active"><a class="btn btn-app btn-palmasinn" href="#home" role="tab" data-toggle="tab"><i class="fa fa-cutlery"></i> REATAURANTE</a></li>
	  <li><a class="btn btn-app btn-palmasinn" href="#profile" role="tab" data-toggle="tab"><i class="fa fa-picture-o"></i> IMAGENES</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="home">
			<div id="info" class="alert alert-info text-center">
				 <h3><strong>Para comenzar</strong> edita la información del restaurante.</h3>
				 * <strong>Recuerda </strong> no puede quedar ningun campo vacio. *</br>
				 * <strong>Recuerda </strong> dar click en el boton MODIFICAR para guardar los cambios.. *
			</div>
			<div class="row form-group">
				<div class="col-xs-12 col-sm-6">		
					<div class="row form-group">
						<div class="col-xs-12">
							<h3 class="box-title text-white pull-left">INFORMACIÓN DEL RESTAURANTE.</h3>
						</div>
						<div class="col-xs-12">
							<label class="control-label clearfix text-white">Nombre</label>
							<input type="text" id="nombre_restaurant" class="form-control form-border" name="nombre_restaurant" value="{{e($restaurant['restaurant_title'])}}" placeholder="*NOMBRE DEL RESTAURANTE"/>
							<span class="fa fa-pencil form-control-feedback icon-space"></span>
							<hr/>
							<label class="clearfix pull-right text-white" id="cantidad_nombre">0/20</label>
						</div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12 col-sm-6">
					<div class="row form-group">
						<div class="col-xs-12 ">
							<h3 class="box-title text-white pull-left">DESCRIPCIÓN DE SU RESTAURANTE.</h3>
						</div>
						<div class="col-xs-12">
							<div id="editor_restaurat" class="summernote">{!!$restaurant['restaurant_descripcion']!!}</div>
							<hr/>
							<label class="clearfix pull-right text-white" id="cantidad_desrest">0/898</label>
						</div>
					</div>			
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="row form-group">
						<div class="col-xs-12">
							<h3 class="box-title text-white pull-left">CONTACTO.</h3>
						</div>
						<div class="col-xs-12">
							<label class="control-label clearfix text-white">Telefono 1:</label>
							<input type="text" id="contacto1_restaurant" class="form-control form-border" name="contacto1_restaurant" placeholder="{{e($restaurant['restaurant_contacto1'])}}" value="{{e($restaurant['restaurant_contacto1'])}}"/>
							<span class="fa fa-pencil form-control-feedback icon-space"></span>
							<hr/>
						</div>
						<div class="col-xs-12">
							<label class="control-label clearfix text-white">Telefono 2:</label>
							<input type="text" id="contacto2_restaurant" class="form-control form-border" name="contacto2_restaurant" placeholder="{{e($restaurant['restaurant_contacto2'])}}" value="{{e($restaurant['restaurant_contacto2'])}}"/>
							<span class="fa fa-pencil form-control-feedback icon-space"></span>
							<hr/>
						</div>
						<div class="col-xs-12">
							<label class="control-label clearfix text-white">Comercial:</label>
							<input type="text" id="comercal_restaurant" class="form-control form-border" name="comercal_restaurant" value="{{e($restaurant['restaurant_comercial'])}}" placeholder="*LINK VIDEO COMERCIAL"/>
							<span class="fa fa-pencil form-control-feedback icon-space"></span>
							<hr/>
							<label class="clearfix pull-right text-white" id="cantidad_comercial">0/200</label>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="profile">
		
			<div id="info" class="alert alert-info text-center">
				 <h3><strong>Para comenzar</strong> seleccione la imagen que desea cambiar.</h3>
				 * <strong>Recuerda </strong> dar click en el boton MODIFICAR para guardar los cambios. *
			</div>
		
			<div class="row form-group">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">LOGOS RESTAURANTE.</h3>
				</div>
				<div class="col-xs-12 col-sm-6">
					<label class="control-label clearfix text-white center">Logo Banner</label>		
					<div class="p_picture">
						<div class="box-image">
							<img src="{{url(e($restaurant['restaurant_rest_file1']))}}" id="rest_logo1">
						</div>
						<div class="box-file">
							<input class="file_pic" type="file" id="rest_file1" name="rest_file1" data-img="#rest_logo1"/>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<label class="control-label clearfix text-white center">Logo Hotel</label>				
					<div class="p_picture">
						<div class="box-image">
							<img src="{{url(e($restaurant['restaurant_rest_file2']))}}" id="rest_logo2">
						</div>
						<div class="box-file">
							<input class="file_pic" type="file" id="rest_file2" name="rest_file2" data-img="#rest_logo2"/>
						</div>
					</div>
				</div>	
			</div>
			<div class="row form-group">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">IMAGENES RESTAURANTE.</h3>
				</div>
				<div class="col-xs-12 col-sm-6">
					<label class="control-label clearfix text-white center">Imagen fija</label>
					<div class="p_picture">
						<div class="box-image">
							<img src="{{url(e($restaurant['restaurant_rest_file4']))}}" id="rest_fija1">
						</div>
						<div class="box-file">
							<input class="file_pic" type="file" id="rest_file4" name="rest_file4" data-img="#rest_fija1"/>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<label class="control-label clearfix text-white center">Imagen fija</label>
					<div class="p_picture">
						<div class="box-image">
							<img src="{{url(e($restaurant['restaurant_rest_file5']))}}" id="rest_fija2">
						</div>
						<div class="box-file">
							<input class="file_pic" type="file" id="rest_file5" name="fija_5" data-img="#rest_fija2"/>
						</div>
					</div>
				</div>		
			</div>
			<div class="row form-group">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">BANNER RESTAURANTE.</h3>
				</div>
				<div class="col-xs-12 col-sm-6">
					<label class="control-label clearfix text-white center">Imagen banner</label>				
					<div class="p_picture">
						<div class="box-image">
							<img src="{{url(e($restaurant['restaurant_rest_file3']))}}" id="rest_banner">
						</div>
						<div class="box-file">
							<input class="file_pic" type="file" id="rest_file3" name="rest_file3" data-img="#rest_banner"/>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-8">
			<a id="btnGuardar" class="btn btn-app btn-palmasinn" data-restaurant="{{e(base64_encode($restaurant['id']))}}">
				<i class="fa fa-edit"></i> MODIFICAR
			</a>
			<a id="btnListar" href="{{url('admin/listar_restaurantes')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-undo"></i> REGRESAR
			</a>
		</div>
	</div>
</form>
<script src="{{asset('js/listrestaurant_palmas.js')}}"></script>
<script src="{{asset('js/editrestaurant_palmas.js')}}"></script>
