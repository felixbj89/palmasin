@extends("layout.dashboard")
@section("title_section")
	Asociaciones
@endsection
@section("sub_title")
	Asociaciones
@endsection
@section("title-divsection")
	Asociaciones en el sistema.
@endsection
@section("galeria-active")
	active
@endsection
@section("body-section")
	@if(Session::has('server_asociacionlist'))
		<input type="hidden" name="server_asociacionlist" id="server_asociacionlist" value="{{Session::get('server_asociacionlist')}}" />
	@endif
	<div class="row">
		<div class="col-xs-12 col-md-12">
			@if(count($list_galeria) > 0)
				<table id="historial_list" class="table table-striped">
					<thead>
						<tr class="fondo-tr">
							<th>Imagen > {{Session::has('server_asociacionlist')}}</th>
							<th>Promoción</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($list_galeria as $lista)
							<tr class="fondo-tr" data-historial='{"historial_id":"{{e(base64_encode($lista->id))}}"}'>
								<td><img id="imagen{{$lista->id}}" src="{{asset($lista->galeria_path)}}" class="img-responsive img-thumbnail box-image box-list box-dimensiones center"></td>
								<td><img id="promo{{$lista->promo_id}}" src="{{asset('img/home/promociones/'.$lista->promo_path)}}" class="img-responsive img-thumbnail box-image box-list box-promociones center"></td>
								<td>
									<div class="row form-group center">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<a id="btnAsociadosHechas" class="btn btn-app btn-palmasinn center">
												<i class="fa fa-plus"></i> ASOCIAR
											</a>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12">
											<a id="btnRemoverAsociacionesRealizadas" class="btn btn-app btn-palmasinn center">
												<i class="fa fa-trash"></i> REMOVER
											</a>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
					<tfooter>
						<tr>
							<th>Imagen</th>
							<th>Promoción</th>
							<th>Opciones</th>
						</tr>
					</tfooter>
				</table>
			@else
				<label class="control-label clearfix center text-left alert-warning spacing-padding">No hay asociaciones registradas en el sistema.</label>
			@endif
		</div>
	</div>
	@if(count($list_galeria) > 0)
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnRegistrar" href="{{url('admin/crear_galeria')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-up"></i> REGISTRAR
				</a>
				<a id="btnListar" href="{{url('admin/gestionargaleriaprincipal')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-undo"></i> REGRESAR
				</a>
			</div>
		</div>
  @endif
@endsection
@section("scripts")
	<script src="{{asset('js/list_galeriahome.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar_remover")
	@include("modales.asociacion.listar.alerta")
	@include("modales.confirmar_removerasociacion")
	@include("modales.wait")
	@include("modales.empty")
	@include("modales.asociacion.empty_asociacioneshechas")
@endsection