<input type="hidden" id="mindate" name="mindate" value="{{e($fecha_min)}}"/>
<input type="hidden" id="maxdate" name="maxdate" value="{{e($fecha_max)}}"/>
<form id="form-promociones" action="{{url('admin/editpromohome')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
	<input type="hidden" name="posicion" id="posicion" value="{{ $promoid }}" />
	<div class="row form-group">
		<div class="col-xs-12 col-md-12">
			<h3 class="box-title text-white pull-left">INFORMACIÓN DE LA PROMOCIÓN.</h3>
		</div>
	</div>
	<div class="row form-group has-feedback">
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">Título</label>
			<input type="text" id="titulo_promociones" class="form-control form-border" name="titulo_promociones" placeholder="*TÍTULO A INGRESAR" value="{{e($promo_home['promo_title'])}}"/>
			<span class="fa fa-pencil form-control-feedback icon-space"></span>
			<hr/>
			<label class="clearfix pull-right text-white" id="cantidad-titulo">0/20</label>
		</div>
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">Estado</label>
			<select class="form-control form-border" id="estado_promociones" name="estado_promociones">
				<option value="NULL">Seleccione el estado de su agrado.</option>
				@if($promo_home['promo_status']==1)
					<option value="1" selected>Activa.</option>
					<option value="0">Inactiva.</option>
				@else
					<option value="1">Activa.</option>
					<option value="0" selected>Inactiva.</option>
				@endif
			</select>
			<span class="fa fa-power-off form-control-feedback icon-space-form"></span>
		</div>
	</div>
	<div class="row form-group has-feedback">
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">Inicio</label>
			<input type="text" id="inicio_promociones" class="form-control form-border" name="inicio_promociones" placeholder="*INICIO DE LA PROMOCIÓN" value="{{e($promo_home['promo_dateini'])}}"/>
			<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
		</div>
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">Fin</label>
			<input type="text" id="fin_promociones" class="form-control form-border" name="fin_promociones" placeholder="*FIN DE LA PROMOCIÓN" value="{{e($promo_home['promo_datefin'])}}"/>
			<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
		</div>
	</div>
	<div class="row form-group has-feedback">
		<div class="col-xs-12 col-md-12">
			<label class="control-label clearfix text-white">Suba su promoción</label>
			<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-xs-12 col-md-12">
			<h3 class="box-title text-white pull-left">DESCRIPCIÓN DE SU PROMOCIÓN.</h3>
			<h3 class="box-title text-white pull-right">VISTA PREVIA DE SU PROMOCIÓN.</h3>
		</div>
	</div>
	<div class="row form-group has-feedback">
		<div class="col-xs-12 col-md-6">
			<textarea name="descripcion_promociones" id="descripcion_promociones" class="form-border form-control summernote">
				{{e($promo_home['promo_text'])}}
			</textarea>
			<hr/>
			<label class="clearfix pull-right text-white" id="cantidad-descripcion">0/300</label>
		</div>
		<div class="col-xs-12 col-md-6">
			<input type="hidden" id="url1" name="url1" value="{{asset('img/home/promociones/'.e($promo_home['promo_path']))}}"/>
			<img src="{{asset('img/home/promociones/'.e($promo_home['promo_path']))}}" id="uno_picture" class="img-responsive box-image box-promociones">
		</div>
	</div>
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-8">
			<a id="btnModificarPromo" class="btn btn-app btn-palmasinn">
				<i class="fa fa-edit"></i> MODIFICAR
			</a>
			<a id="btnRetonar" href="{{url('admin/listar_promocion')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-undo"></i> REGRESAR
			</a>
		</div>
	</div>
</form>
<link rel="stylesheet" href="{{asset('css/promociones.css')}}">
<script src="{{asset('plugins/flatpickr-master/dist/flatpickr.js')}}"></script>
<script src="{{asset('plugins/flatpickr-master/dist/es.js')}}"></script>
<script src="{{asset('js/editarpromo_home.js')}}"></script>