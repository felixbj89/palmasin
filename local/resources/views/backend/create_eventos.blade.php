@extends("layout.dashboard")
@section("title_section")
	EVENTOS
@endsection
@section("sub_title")
	EVENTOS
@endsection
@section("title-divsection")
	Complete la información para poder registrar.
@endsection
@section("eventos-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/eventos.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_eventoshome'))
		<input type="hidden" name="server_eventoshome" id="server_eventoshome" value="{{e(Session::get('server_eventoshome'))}}" />
	@elseif(count($errors) > 0)
		<input type="hidden" name="error_eventoshome" id="error_eventoshome" value="{{e($errors->all()[0])}}" />
	@endif
	<input type="hidden" id="mindate" name="mindate" value="{{e($fecha_min)}}"/>
	<!-- action -->
	<form id="form-eventos" action="{{url('admin/addeventoshome')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<div class="row form-group">
			<div class="col-xs-12 col-md-6">
				<div class="row form-group">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">INFORMACIÓN DEL EVENTO.</h3>
					</div>
					<div class="col-xs-12">
						<label class="control-label clearfix text-white">Título</label>
						<input type="text" id="titulo_evento" class="form-control form-border" name="titulo_evento" placeholder="*TÍTULO A INGRESAR" value="{{old('titulo_evento')}}"/>
						<span class="fa fa-pencil form-control-feedback icon-space"></span>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad-titulo">0/30</label>
					</div>
					<div class="col-xs-12">
						<label class="control-label clearfix text-white">Tipo de evento</label>
						<select class="form-control form-border" name="tipoevento">
							<option value="" selected disabled> Seleccione tipo de evento</option>
							<option value="1">Corporativo</option>
							<option value="0">Promocional</option>
						</select>					
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">DESCRIPCIÓN DEL EVENTO.</h3>
					</div>
					<div class="col-xs-12">
						<textarea name="descripcion_evento" id="descripcion_evento" class="form-border form-control summernote" placeholder="* DESCRIPCIÓN DEL EVENTO">{{old('titulo_evento')}}</textarea>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad-descripcion">0/250</label>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="row form-group">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">IMAGEN PARA EL EVENTO.</h3>
					</div>				
					<div class="col-xs-12">
						<input type="hidden" id="url1" name="url1" value="{{asset('img/home/promociones/default.jpg')}}"/>
						<img src="{{asset('img/home/promociones/default.jpg')}}" id="uno_picture" class="img-responsive box-image box-promociones">
					</div>
					<div class="col-xs-12">
						<!--label class="control-label clearfix text-white">Suba su promoción</label-->
						<input class="corporativo" type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
					</div>					
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-6">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">LINK DEL EVENTO.</h3>
					</div>
					<div class="col-xs-12">
						<a href="#linkhabitaciones" class="btnlink btn btn-app btn-palmasinn" data-toggle="collapse">
							<i class="fa fa-bed"></i> HABITACIONES
						</a>
						<a href="#linkrestaurantes" class="btnlink btn btn-app btn-palmasinn" data-toggle="collapse">
							<i class="fa fa-cutlery"></i> RESTAURANTES
						</a>
						<a href="#linkexterno" class="btnlink btn btn-app btn-palmasinn" data-toggle="collapse">
							<i class="fa fa-bed"></i> EL HOTEL
						</a>
					</div>
					<div id="linkhabitaciones" class="col-xs-12 collapse">
						<label class="corporativo control-label clearfix text-white">Habitaciones</label>
						{!!$habitaciones!!}
					</div>
					<div id="linkrestaurantes" class="col-xs-12 collapse">
						<label class="control-label clearfix text-white">Restaurantes</label>
						<select class="corporativo selectlink form-control form-border" name="restaurante">
							<option value="" selected disabled> Seleccione un Restaurante</option>
							<option value="{{url('restaurantes/habneros')}}">Habaneros</option>
							<option value="{{url('restaurantes/maracuya')}}">Maracuya</option>
							<option value="{{url('restaurantes/lobbybar')}}">Lobbybar</option>
						</select>
					</div>
					<div id="linkexterno" class="col-xs-12 collapse">
						<label class="control-label clearfix text-white">Inicio</label>
						<input type="text" id="link" class="corporativo form-control form-border" name="link" placeholder="LINK EXTERNO"/>
						<span class="fa fa-external-link form-control-feedback icon-space-form"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12">
				<div class="row form-group">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">FECHA DEL EVENTO.</h3>
					</div>
					<div class="col-xs-12 - col-sm-6">
						<label class="control-label clearfix text-white">Inicio</label>
						<input type="text" id="inicio_evento" class="corporativo form-control form-border" name="inicio_evento" placeholder="*INICIO DEL EVENTO"/>
						<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
					</div>
					<div class="col-xs-12 - col-sm-6">
						<label class="control-label clearfix text-white">Fin</label>
						<input type="text" id="fin_evento" class="corporativo form-control form-border" name="fin_evento" placeholder="*FIN DEL EVENTO"/>
						<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
					</div>
				</div>			
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnRegistrar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-save"></i> REGISTRAR
				</a>
				<a id="btnListar" href="{{url('admin/listar_eventos')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-down"></i> LISTAR
				</a>
			</div>
		</div>
	</form>
@endsection
@section("scripts")
	<script src="{{asset('js/eventos_home.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.eventos.alerta")
	@include("modales.wait")
@endsection