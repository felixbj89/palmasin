<form id="form-galeriahome" action="{{url('admin/editgaleriahome')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
	<div class="row form-group">
		<div class="col-xs-12 col-sm-6 col-md-6 div-space">
			<input type="hidden" id="url1" name="url1" value="{{asset($galeria_home[0])}}"/>
			<img src="{{asset($galeria_home[0])}}" id="uno_picture" class="img-responsive box-image galeria-picture">
			<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 div-space">
		<input type="hidden" id="url2" name="url2" value="{{asset($galeria_home[1])}}"/>
			<img src="{{asset($galeria_home[1])}}" id="dos_picture" class="img-responsive box-image galeria-picture">
			<input type="file" id="file_two" name="file_two" onchange='javascript:openFile2(event)'/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-xs-12 col-sm-6 col-md-6 div-space">
			<input type="hidden" id="url3" name="url3" value="{{asset($galeria_home[2])}}"/>
			<img src="{{asset($galeria_home[2])}}" id="tres_picture" class="img-responsive box-image galeria-picture">
			<input type="file" id="file_thrid" name="file_thrid" onchange='javascript:openFile3(event)'/>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 div-space">
		<input type="hidden" id="url4" name="url4" value="{{asset($galeria_home[3])}}"/>
			<img src="{{asset($galeria_home[3])}}" id="cuatro_picture" class="img-responsive box-image galeria-picture">
			<input type="file" id="file_four" name="file_four" onchange='javascript:openFile4(event)'/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-xs-12 col-sm-6 col-md-6 div-space">
			<input type="hidden" id="url5" name="url5" value="{{asset($galeria_home[4])}}"/>
			<img src="{{asset($galeria_home[4])}}" id="cinco_picture" class="img-responsive box-image galeria-picture">
			<input type="file" id="file_five" name="file_five" onchange='javascript:openFile5(event)'/>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 div-space">
			<img src="{{asset($galeria_home[5])}}" id="seis_picture" class="img-responsive box-image galeria-picture">
				<input type="hidden" id="url6" name="url6" value="{{asset($galeria_home[5])}}"/>
			<input type="file" id="file_six" name="file_six" onchange='javascript:openFile6(event)'/>
		</div>
	</div>
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-8">
			<a id="btnRegistrar" class="btn btn-app btn-palmasinn">
				<i class="fa fa-pencil-square-o"></i> MODIFICAR
			</a>
			<a id="btnRegresar" hreF="{{url('admin/crear_galeria')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-undo"></i> REGRESAR
			</a>
			<a id="btnListar" href="{{url('admin/listar_asociaciones')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-chevron-down"></i> ASOCIACIONES
			</a>
		</div>
	</div>
</form>
<script src="{{asset('js/editgaleria_home.js')}}"></script>