<form id="form-editusers" action="{{url('admin/edit_users')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
	<input type="hidden" id="id" name="id" value="{{e($id)}}"/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-12">
			<h3 class="box-title text-white pull-left">INFORMACIÓN DEL USUARIO.</h3>
		</div>
	</div>
	<div class="row form-group has-feedback">
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">Nombre</label>
			<input type="text" id="nombreusers" class="form-control form-border" name="nombreusers" placeholder="*NOMBRE DEL USUARIO" value="{{e($usuarioID->user_name)}}" disabled/>
			<span class="fa fa-pencil form-control-feedback icon-space"></span>
			<hr/>
			<label class="clearfix pull-right text-white" id="cantidad-nombre">0/20</label>
		</div>
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">E-mail</label>
			<input type="email" id="emailusers" class="form-control form-border" name="emailusers" placeholder="*E-MAIL DEL USUARIO" value="{{e($usuarioID->user_email)}}" disabled/>
			<span class="fa fa-envelope form-control-feedback icon-space"></span>
			<hr/>
			<label class="clearfix pull-right text-white" id="cantidad-email">0/20</label>
		</div>
	</div>
	<div class="row form-group has-feedback">
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">Rol</label>
			<select id="roleusers" name="roleusers" class="form-control form-border">
				<option value="NULL">Seleccione el rol de su agrado</option>
				@if($usuarioID->user_role==1)
					<option value="1" selected>Administrador</option>
					<option value="2">Operador</option>
				@else
					<option value="1">Administrador</option>
					<option value="2" selected>Operador</option>
				@endif
			</select>
			<span class="fa fa-map-marker form-control-feedback icon-space"></span>
			<hr/>
		</div>
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">Login</label>
			<input type="text" id="loginusers" class="form-control form-border" name="loginusers" placeholder="*LOGIN DEL USUARIO" value="{{e($usuarioID->user_login)}}"/>
			<span class="fa fa-lock form-control-feedback icon-space"></span>
			<hr/>
			<label class="clearfix pull-right text-white" id="cantidad-login">0/20</label>
		</div>
	</div>
	<div class="row form-group has-feedback">
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">Password</label>
			<input type="password" id="passwordusers" class="form-control form-border" name="passwordusers" placeholder="*PASSWORD DEL USUARIO"/>
			<span class="fa fa-lock form-control-feedback icon-space"></span>
			<hr/>
			<label class="clearfix pull-right text-white" id="cantidad-password">0/20</label>
		</div>
		<div class="col-xs-12 col-md-6">
			<label class="control-label clearfix text-white">Confirmar</label>
			<input type="password" id="confirmarusers" class="form-control form-border" name="confirmarusers" placeholder="*CONFIRMAR EL PASSWORD"/>
			<span class="fa fa-lock form-control-feedback icon-space"></span>
			<hr/>
			<label class="clearfix pull-right text-white" id="cantidad-confirmar">0/20</label>
		</div>
	</div>
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-12">
			<a id="btnModificarUsers" class="btn btn-app btn-palmasinn">
				<i class="fa fa-edit"></i> MODIFICAR
			</a>
			<a id="btnVerpassword" class="btn btn-app btn-palmasinn">
				<i id="icobtn" class="fa fa-eye-slash"></i> VER CONTRASEÑA
			</a>
			<a id="" href="{{url('admin/listar_usuario')}}" class="btn btn-app btn-palmasinn">
				<i id="icobtn" class="fa fa-undo"></i> REGRESAR
			</a>
		</div>
	</div>
</form>
<script src="{{asset('js/validaciones.js')}}"></script>
<script src="{{asset('js/edituser.js')}}"></script>