@extends("layout.dashboard")
@section("title_section")
	RESTAURANTES
@endsection
@section("sub_title")
	RESTAURANTES
@endsection
@section("title-divsection")
	Registre una galería a un restaurante
@endsection
@section("restaurantes-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeriarestaurant_palmas.css')}}">
@endsection
@section("body-section")
<div class="row">
	<div class="col-xs-12">
		<div class="row form-group">
			<div class="col-xs-12">
				<h3 class="box-title text-white pull-left">GALERIA RESTAURANTES.</h3>
			</div>
			<div class="col-xs-12" id="conten_galeria">
				<div id="info" class="alert alert-info text-center">
					 <h3><strong>Para comenzar</strong> Seleccione un Bar/Restaurante para asignar una galería.</h3>
				</div>
				{!!$galeria!!}
			</div>
		</div>
	</div>
</div>
<hr/>
<div class="row form-group">
	<div class="col-xs-12 col-md-8">
		<a id="btnListar" href="{{url('admin/listar_restaurantes')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-chevron-up"></i> REGISTRAR
		</a>
		<a id="btnListar" href="{{url('admin/listar_restfija')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-chevron-down"></i> FOTO SECUNDARIA
		</a>
	</div>
</div>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.restaurante.alerta")
	@include("modales.wait")
@endsection