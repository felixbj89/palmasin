@extends("layout.dashboard")
@section("title_section")
	PROMOCIONES
@endsection
@section("sub_title")
	PROMOCIONES
@endsection
@section("title-divsection")
	Listar Promociones Registradas.
@endsection
@section("promociones-active")
	active
@endsection
@section("body-section")
	@if(Session::has('server_promo'))
		<input type="hidden" name="server_promo" id="server_promo" value="{{Session::get('server_promo')}}" />
	@endif
	<div class="row">
		<div class="col-xs-12 col-md-12">
			@if(count($list_promociones) > 0)
			<table id="promo_list" class="table table-striped">
				<thead>
					<tr class="fondo-tr">
						<th>Imagen</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($list_promociones as $lista)
						<tr class="fondo-tr" data-historial='{"historial_id":"{{base64_encode(e($lista->id))}}"}'>
							<td><img id="imagen{{$lista->id}}" src="{{asset('img/home/promociones/'.$lista->promo_path)}}" class="img-responsive img-thumbnail box-image box-list box-promociones center"></td>
							<td class="center">
								<div class="row form-group center">
									@if($lista->promo_status==0)
										<div class="col-xs-12 col-sm-12 col-md-4">
											<a id="btnActivar" class="btn btn-app btn-palmasinn">
												<i class="fa fa-power-off"></i> ACTIVAR
											</a>
										</div>
									@else
										<div class="col-xs-12 col-sm-12 col-md-4">
											<a id="btnActivar" class="btn btn-app btn-palmasinn">
												<i class="fa fa-power-off"></i> DESACTIVAR
											</a>
										</div>
									@endif
									<div class="col-xs-12 col-sm-12 col-md-4">
										<a id="btnModificar" class="btn btn-app btn-palmasinn">
											<i class="fa fa-pencil-square-o"></i> MODIFICAR
										</a>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-4">
										<a id="btnRemover" class="btn btn-app btn-palmasinn">
											<i class="fa fa-trash"></i> REMOVER
										</a>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfooter>
					<tr>
						<th>Imagen</th>
						<th>Opciones</th>
					</tr>
				</tfooter>
			</table>
			@else
				<label class="control-label clearfix center text-left alert-warning spacing-padding">No hay promociones registradas en el sistema.</label>
			@endif
		</div>
	</div>
	@if(count($list_promociones) > 0)
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a href="{{url('admin/crear_promociones')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-up"></i> REGISTRAR
				</a>
			</div>
		</div>
	@endif
@endsection
@section("scripts")
	<script src="{{asset('js/list_promohome.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar_remover")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.promociones.editalerta")
	@include("modales.empty")
@endsection
