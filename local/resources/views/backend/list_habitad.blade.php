@extends("layout.dashboard")
@section("title_section")
	HABITACIONES
@endsection
@section("sub_title")
	HABITACIONES
@endsection
@section("title-divsection")
	Lista de habitaciones  registradas.
@endsection
@section("cuartos-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/edithabitad_palmas.css')}}">
@endsection
@section("body-section")

<div class="row">
	<div class="col-xs-12" id="row_edit">
		@if(count($list_habitad) > 0)
		<table id="roomscreados_list" class="table table-striped">
			<thead>
				<tr class="fondo-tr">
					<th>Habitación</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($list_habitad as $lista)
					<tr class="fondo-tr" data-habitad='{"habitacion":"{{base64_encode(e($lista["habitacion"]))}}"}'>
						<td>
							<img src="{{url($lista['img'])}}" class="img-responsive box-image box-dimensiones center"/>
							{{e($lista['nombre'])}}
						</td>
						<td>
							<div class="row form-group center">
								<div class="col-xs-12 col-sm-12 col-md-6">
									<a class="btn btn-app btn-palmasinn btnModificar center">
										<i class="fa fa-pencil-square-o"></i> MODIFICAR
									</a>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6">
									<a class="btn btn-app btn-palmasinn btnRemover center">
										<i class="fa fa-trash"></i> REMOVER
									</a>
								</div>
							</div>
						</td>
					</tr>				
				@endforeach
			</tbody>
			<tfooter>
				<tr>
					<th>Habitación</th>
					<th>Opciones</th>
				</tr>
			</tfooter>
		</table>
		@else
			<label class="control-label clearfix center text-left alert-warning spacing-padding">No hay habitaciones registradas en el sistema.</label>
		@endif
	</div>
</div>
@if(count($list_habitad) > 0)
	<hr/>
<div class="row form-group">
	<div class="col-xs-12 col-md-8">
		<a id="btnListar" href="{{url('admin/crear_habitacion')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-chevron-up"></i> REGISTRAR
		</a>
	</div>
</div>
@endif
@endsection
@section("scripts")
	<script src="{{asset('js/edithabitad_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.confirmar_remover")
	@include("modales.alerta")
	@include("modales.wait")
	@include("modales.empty")
@endsection