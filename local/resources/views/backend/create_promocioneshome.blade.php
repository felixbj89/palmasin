@extends("layout.dashboard")
@section("title_section")
	PROMOCIONES
@endsection
@section("sub_title")
	PROMOCIONES
@endsection
@section("title-divsection")
	Complete la información para poder registrar.
@endsection
@section("promociones-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/promociones.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_promo'))
		<input type="hidden" name="server_promo" id="server_promo" value="{{e(Session::get('server_promo'))}}" />
	@elseif(count($errors) > 0)
		<input type="hidden" name="error_promo" id="error_promo" value="{{e($errors->all()[0])}}" />
	@endif
	<input type="hidden" id="mindate" name="mindate" value="{{e($fecha_min)}}"/>
	<input type="hidden" id="maxdate" name="maxdate" value="{{e($fecha_max)}}"/>
	<form id="form-promociones" action="{{url('admin/addpromocioneshome')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">INFORMACIÓN DE LA PROMOCIÓN.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Título</label>
				<input type="text" id="titulo_promociones" class="form-control form-border" name="titulo_promociones" placeholder="*TÍTULO A INGRESAR" value="{{old('titulo_promociones')}}"/>
				<span class="fa fa-pencil form-control-feedback icon-space"></span>
				<hr/>
				<label class="clearfix pull-right text-white" id="cantidad-titulo">0/30</label>
			</div>
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Estado</label>
				<select class="form-control form-border" id="estado_promociones" name="estado_promociones">
					<option value="NULL">Seleccione el estado de su agrado.</option>
					<option value="1">Activa.</option>
					<option value="0">Inactiva.</option>
				</select>
				<span class="fa fa-power-off form-control-feedback icon-space-form"></span>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">DURACIÓN DE SU PROMOCIÓN.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Inicio</label>
				<input type="text" id="inicio_promociones" class="form-control form-border" name="inicio_promociones" placeholder="*INICIO DE LA PROMOCIÓN"/>
				<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
			</div>
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Fin</label>
				<input type="text" id="fin_promociones" class="form-control form-border" name="fin_promociones" placeholder="*FIN DE LA PROMOCIÓN"/>
				<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">PROMOCIÖN A SUBIR.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<label class="control-label clearfix text-white">Suba su promoción</label>
				<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">DESCRIPCIÓN DE SU PROMOCIÓN.</h3>
				<h3 class="box-title text-white pull-right">VISTA PREVIA DE SU PROMOCIÓN.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-6">
				<textarea name="descripcion_promociones" id="descripcion_promociones" class="form-border form-control summernote">{{old('descripcion_promociones')}}</textarea>
				<hr/>
				<label class="clearfix pull-right text-white" id="cantidad-descripcion">0/300</label>
			</div>
			<div class="col-xs-12 col-md-6">
				<input type="hidden" id="url1" name="url1" value="{{asset('img/home/promociones/default.jpg')}}"/>
				<img src="{{asset('img/home/promociones/default.jpg')}}" id="uno_picture" class="img-responsive box-image box-promociones">
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnRegistrar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-save"></i> REGISTRAR
				</a>
				<a id="btnListar" href="{{url('admin/listar_promocion')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-down"></i> LISTAR
				</a>
			</div>
		</div>
	</form>
@endsection
@section("scripts")
	<script src="{{asset('js/promociones_home.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.promociones.crear.alerta")
	@include("modales.wait")
@endsection
