	@if(Session::has('server'))
		<input type="hidden" name="server" id="server" value="{{e(Session::get('server'))}}" />
	@endif
	<input type="hidden" name="urlpath" id="urlpath" value="{{ e(url('/')) }}" />
	<form id="form-asociaciones" action="{{url('admin/asociarpromogaleria')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<input type="hidden" name="asociados" id="asociados"/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">INFORMACIÓN DE LA PROMOCIÓN.</h3>
				<h3 class="box-title text-white pull-right">VISTA PREVIA DE SU PROMOCIÓN.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-6">
				<div class="row form-group has-feedback">
					<div class="col-xs-12 col-md-12">
						<label class="control-label clearfix text-white">Promociones</label>
						<select class="form-control form-border" id="promocion_seleccionada" name="promocion_seleccionada">
							<option value="NULL">Seleccione la promoción de su agrado.</option>
							<?php
								for($i = 0; $i < count($promociones_activas); $i++){
							?>
									<option value="{{e($promociones_activas[$i]->promo_path)}}">{{e($promociones_activas[$i]->promo_title)}}</option>
							<?php
								}
							?>
						</select>
						<span class="fa fa-tags form-control-feedback icon-space-form"></span>
					</div>
				</div>
				<div class="row form-group has-feedback">
					<div class="col-xs-12 col-md-12">
						<label class="control-label clearfix text-white">Imágenes de la galería</label>
						<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
							<!-- Wrapper for slides -->
							<div class="carousel-inner">
								<?php
									for($i = 0; $i < count($lista_galerias); $i++){
										if($i==0){
								?>
											<div class="item active">
												<img src="{{e(url($lista_galerias[$i]->galeria_path))}}" alt="Palmas Inn" class="img-responsive fixed-galeria-sliders">
												<div class="carousel-caption" data-chooseslider="{{e($lista_galerias[$i]->galeria_path)}}">
													<a id="btnAsociarSlider" class="btn btn-app btn-palmasinn">
														<i class="fa fa-plus"></i> ASOCIAR
													</a>
												</div>
											</div>
								<?php
										}else{
								?>
											<div class="item">
												<img src="{{e(url($lista_galerias[$i]->galeria_path))}}" alt="Palmas Inn" class="img-responsive fixed-galeria-sliders">
												<div class="carousel-caption" data-chooseslider="{{e($lista_galerias[$i]->galeria_path)}}">
													<a id="btnAsociarSlider" class="btn btn-app btn-palmasinn">
														<i class="fa fa-plus"></i> ASOCIAR
													</a>
												</div>
											</div>
								<?php
										}
									}
								?>
							</div>
							<!-- Left and right controls -->
							<a class="left carousel-control" href="#myCarousel" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#myCarousel" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<input type="hidden" id="url1" name="url1" value="{{asset('img/home/promociones/default.jpg')}}"/>
				<img src="{{asset('img/home/promociones/default.jpg')}}" id="uno_picture" class="img-responsive box-image box-promociones">
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnGuardarAsociación" class="btn btn-app btn-palmasinn">
					<i class="fa fa-save"></i> REGISTRAR
				</a>
				<a id="btnRetornar" href="{{url('admin/crear_galeria')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-undo"></i> REGRESAR
				</a>
				<a id="btnListar" href="{{url('admin/listar_galeria')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-down"></i> LISTAR
				</a>
			</div>
		</div>
	</form>
	@include("modales.asociacion.crear.alerta")
	<script src="{{asset('js/asociarpromo.js')}}"></script>
