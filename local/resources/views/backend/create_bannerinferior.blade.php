@extends("layout.dashboard")
@section("title_section")
	BLOQUE DE SALUD
@endsection
@section("sub_title")
	BLOQUE DE SALUD
@endsection
@section("title-divsection")
	Registre su Imagen de Salud.
@endsection
@section("banner-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeria.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_isalud'))
		<input type="hidden" name="server_isalud" id="server_isalud" value="{{Session::get('server_isalud')}}" />
	@elseif(count($errors) > 0)
		<input type="hidden" name="error_isalud" id="error_isalud" value="{{e($errors->all()[0])}}" />
	@endif
	<form id="form-banner" action="{{url('admin/addbannerinferior')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">INFORMACIÓN DE SU IMAGEN DE SALUD.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Título</label>
				<input type="text" id="titulo_salud" class="form-control form-border" name="titulo_salud" placeholder="*TÍTULO A INGRESAR" value="{{old('titulo_salud')}}"/>
				<span class="fa fa-pencil form-control-feedback icon-space"></span>
				<hr/>
				<label class="clearfix pull-right text-white" id="cantidad-tsalud">0/29</label>
			</div>
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Sub-Título</label>
				<input type="text" id="subtitulo_salud" class="form-control form-border" name="subtitulo_salud" placeholder="*TÍTULO A INGRESAR" value="{{old('subtitulo_salud')}}"/>
				<span class="fa fa-pencil form-control-feedback icon-space"></span>
				<hr/>
				<label class="clearfix pull-right text-white" id="cantidad-ssalud">0/80</label>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">IMAGEN DE SALUD A SUBIR.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<label class="control-label clearfix text-white">Suba su imagen</label>
				<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">VISTA PREVIA DE SU IMAGEN.</h3>
				<h3 class="box-title text-white pull-right">RESOLUCIÓN RECOMENDADA: 1368x200px.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<input type="hidden" id="url1" name="url1" value="{{asset('img/home/salud/default.jpg')}}"/>
				<img src="{{asset('img/home/salud/default.jpg')}}" id="uno_picture" class="img-responsive box-image box-salud">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">IMAGEN INTERNA A SUBIR.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<label class="control-label clearfix text-white">Suba su imagen</label>
				<input type="file" id="file_two" name="file_two" onchange='javascript:openFile2(event)'/>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">VISTA PREVIA DE SU IMAGEN.</h3>
				<h3 class="box-title text-white pull-right">RESOLUCIÓN RECOMENDADA: 500x500px.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<input type="hidden" id="url2" name="url2" value="{{asset('img/home/salud/default.jpg')}}"/>
				<img src="{{asset('img/home/salud/default.jpg')}}" id="dos_picture" class="img-responsive box-image box-interna">
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnRegistrar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-save"></i> REGISTRAR
				</a>
				<a id="btnEditar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-pencil-square-o"></i> MODIFICAR
				</a>
				<a id="btnListar" href="{{url('admin/gestionarimagensalud')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-undo"></i> REGRESAR
				</a>
			</div>
		</div>
	</form>
@endsection
@section("scripts")
	<script src="{{asset('js/imagen_inferior.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.bsalud.alerta")
	@include("modales.wait")
@endsection