@extends("layout.dashboard")
@section("title_section")
	LISTAR USUARIOS EN EL SISTEMA
@endsection
@section("sub_title")
	USUARIOS
@endsection
@section("title-divsection")
	Listando a usuarios del sistema
@endsection
@section("users-active")
	active
@endsection
@section("body-section")
	@if(Session::has('server_listusers'))
		<input type="hidden" name="server_listusers" id="server_listusers" value="{{Session::get('server_listusers')}}" />
	@elseif(count($errors) > 0)
		<input type="hidden" name="error_listusuario" id="error_listusuario" value="{{e($errors->all()[0])}}" />
	@endif
	<div class="row">
		<div class="col-xs-12 col-md-12">
			@if(count($list_usuarios) > 0)
			<table id="usersinner_list" class="table table-striped table-white">
				<thead>
					<tr class="fondo-tr">
						<th>Avatar</th>
						<th>Nombre</th>
						<th>E-mail</th>
						<th>Rol</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($list_usuarios as $list)
						<tr data-historial='{"historial_id":"{{base64_encode(e($list->id))}}"}'>
							<td><img id="avatar{{$list->id}}" src="{{asset($list->user_avatar)}}" class="img-responsive img-circle box-avatares center" width="160" height="160"></td>
							<td>{{e($list->user_name)}}</td>
							<td>{{e($list->user_email)}}</td>
							<td>
								@if($list->user_role==1)
									Administrador
								@else
									Operador
								@endif
							</td>
							<td class="center">
								<div class="row form-group center">
									<div class="col-xs-12 col-sm-12 col-md-6">
										<a id="btnModificarUsuario" class="btn btn-app btn-palmasinn center">
											<i class="fa fa-pencil-square-o"></i> MODIFICAR
										</a>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6">
										<a id="btnRemoverUsuario" class="btn btn-app btn-palmasinn center">
											<i class="fa fa-trash"></i> REMOVER
										</a>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfooter>
					<tr>
						<th>Avatar</th>
						<th>Nombre</th>
						<th>E-mail</th>
						<th>Rol</th>
						<th>Opciones</th>
					</tr>
				</tfooter>
			</table>
			@else
				<label class="control-label clearfix center text-left alert-warning spacing-padding">No hay usuarios registrados en el sistema.</label>
			@endif
		</div>
	</div>
	@if(count($list_usuarios) > 0)
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a href="{{url('admin/crear_usuario')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-up"></i> REGISTRAR
				</a>
			</div>
		</div>
	@endif
@endsection
@section("scripts")
	<script src="{{asset('js/list_users.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.confirmar_remover")
	@include("modales.usuarios.listar.alerta")
	@include("modales.empty")
	@include("modales.wait")
@endsection