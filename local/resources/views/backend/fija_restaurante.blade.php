@extends("layout.dashboard")
@section("title_section")
	RESTAURANTE
@endsection
@section("sub_title")
	RESTAURANTE
@endsection
@section("title-divsection")
	Seleccione una imagen para mostar en formulario.
@endsection
@section("restaurantes-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/fijarestaurantes_palmas.css')}}">
@endsection
@section("body-section")
<div class="row" style="margin:0;">
	<form id="form-habitad" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />	
		<div class="row form-group">
			<div class="col-xs-12">
				<h3 class="box-title text-white pull-left">FOTO SECUNDARIA.</h3>
			</div>
			<div class="col-xs-12" id="conten_galeria">
				<div id="info" class="alert alert-info text-center">
					 <h3><strong>Para comenzar</strong> selecciona una imagen.</h3>
					* <strong>Recuerda </strong> dar click en el boton MODIFICAR para guardar los cambios. *
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 text-center">
				<button type="button" data-form="1" data-rest="{{e($habaneros)}}" class="btn btn-app btn-palmasinn selres active">
					<i class=""></i> HABANEROS
				</button>
				<button type="button" data-form="2" data-rest="{{e($maracuya)}}" class="btn btn-app btn-palmasinn selres">
					<i class=""></i> MARACUYA
				</button>
				<button type="button" data-form="3" data-rest="{{e($lobbybar)}}" class="btn btn-app btn-palmasinn selres">
					<i class=""></i> LOBBYBAR
				</button>
			</div>		
		</div>
		<div class="row form-group center">
			<div class="col-xs-12 col-sm-4 col-sm-offset-4" id="cifija">
				<div class="col-xs-12 box-image">
					<img class="p_picture" src="{{url(e($habaneros))}}" id="1_picture">
				</div>
				<div class="col-xs-12 box-file">
					<input type="file" id="file_one" name="file_one"/>
				</div>
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnGuardar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-edit"></i> MODIFICAR
				</a>
				<a id="btnRemover" class="btn btn-app btn-palmasinn">
					<i class="fa fa-trash"></i> REMOVER
				</a>
				<a id="btnListar" href="{{url('admin/crear_galeriahotel')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-up"></i> GALERÍA
				</a>
			</div>		
		</div>
	</form>
</div>
@endsection
@section("scripts")
	<script src="{{asset('js/fijarestaurantes_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.restaurante.alerta")
@endsection