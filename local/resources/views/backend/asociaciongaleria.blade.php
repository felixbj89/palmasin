	@if(Session::has('server_asociacion'))
		<input type="hidden" name="server" id="server" value="{{e(Session::get('server_asociacion'))}}" />
	@endif
	<input type="hidden" name="urlpath" id="urlpath" value="{{ e(url('/')) }}" />
	<form id="form-asociaciones" action="{{url('admin/editarasociacionpg')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<input type="hidden" name="asociados" id="asociados"/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">INFORMACIÓN DE LA PROMOCIÓN.</h3>
				<h3 class="box-title text-white pull-right">VISTA PREVIA DE SU PROMOCIÓN.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-6">
				<div class="row form-group has-feedback">
					<div class="col-xs-12 col-md-12">
						<label class="control-label clearfix text-white">Promociones</label>
						<select class="form-control form-border" id="promocion_seleccionada" name="promocion_seleccionada">
							<option value="NULL">Seleccione la promoción de su agrado.</option>
							<?php
								for($i = 0; $i < count($promociones_activas); $i++){
							?>
									<option value="{{e($promociones_activas[$i]->promo_path)}}">{{e($promociones_activas[$i]->promo_title)}}</option>
							<?php
								}
							?>
						</select>
						<span class="fa fa-tags form-control-feedback icon-space-form"></span>
					</div>
				</div>
				<div class="row form-group has-feedback">
					<div class="col-xs-12 col-md-12">
						<label class="control-label clearfix text-white">Imágenes de la galería</label>
						<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
							<!-- Wrapper for slides -->
							<div class="carousel-inner">
								<div class="item active">
									<img src="{{e(url($lista_galerias['galeria_path']))}}" alt="Palmas Inn" class="img-responsive box-image">
									<div class="carousel-caption" data-chooseslider="{{e($lista_galerias['galeria_path'])}}">
										<a id="btnAsociarSlider" class="btn btn-app btn-palmasinn">
											<i class="fa fa-plus"></i> ASOCIAR
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<input type="hidden" id="url1" name="url1" value="{{asset('img/home/promociones/default.jpg')}}"/>
				<img src="{{asset('img/home/promociones/default.jpg')}}" id="uno_picture" class="img-responsive box-image box-promociones">
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnModificarGaleria" class="btn btn-app btn-palmasinn">
					<i class="fa fa-save"></i> RE-ASOCIAR
				</a>
				<a id="btnRetornar" href="{{url('admin/listar_asociaciones')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-undo"></i> REGRESAR
				</a>
				<a id="btnListar" href="{{url('admin/listar_galeria')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-down"></i> LISTAR
				</a>
			</div>
		</div>
	</form>
	<script src="{{asset('js/asociarpromo.js')}}"></script>