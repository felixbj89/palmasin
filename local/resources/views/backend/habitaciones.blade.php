@extends("layout.master")
@section('Mycss')
<link rel="stylesheet" href="{{asset('css/habitacionespalmasinn.css')}}">
@endsection
@section('navbar-palmasinn')
	@parent
@endsection
@section('habitaciones-active')
	activo
@endsection
@section('baner-page')
<div class="full-height banner_palmasinn background_img" style="background-image:url({{url($res['banner'])}});"></div>
@endsection
@section('body-page')
<!-- Habitaciones Acceso -->
<div id="accesos_habitaciones" class="row">
	<div class="col-xs-12">	
		<div id="habitypes" class="carousel fdi-Carousel carousel-fade slide" data-ride="carousel" data-interval="false">
			<div class="carousel-inner onebyone-carosel">
				{!!$res['items']!!}
			</div>
			<a class="left carousel-control" href="#habitypes" role="button" data-slide="prev"></a>
			<a class="right carousel-control" href="#habitypes" role="button" data-slide="next"></a>
			<div class="controls">
				<button class="background_img habitype_left" style="background-image:url({{asset('img/habitaciones/derecha.svg')}});"></button>
				<button  class="background_img habitype_right" style="background-image:url({{asset('img/habitaciones/izquierda.svg')}});"></button>
			</div>
		</div>
	</div>
</div>
<!-- Loading -->
<div id="loading" class="row collapse">
	<div class="col-xs-12">
		<img/>
	</div>
</div>
<!-- Habitaciones -->
<div id="habitaciones" class="row collapse">
	<div class="col-xs-12">
		<div class="row flex">
			<div class="col-xs-12 col-sm-6 col-md-5">
				<div class="row">
					<div>
						<div>
							@if(App::getLocale()=="en")
							@else
								<div class="col-xs-12">
									<p class="tituloitalic" id="nombre">{{e($res['titulo'])}}</p>
								</div>
								<div class="col-xs-12">
									<div id="descripcion">
										{!!$res['detalles']!!}
									</div>
									<div id="detalles">
										{!!$res['lista']!!}
									</div>
								</div>
								<div class="col-xs-12">	
									<ul class="row list-unstyled" id="comodidades">
										{!!$res['comodidades']!!}
									</ul>
									<ul class="list-inline">
									@if(strcmp($res['itemsgal'],"") !== 0)
										<li><button id="viewgale" class="btn" data-toggle="modal" data-target="#modalGaleriapalmas">Ver Galería</button></li><!---Faltapasar id galeria-->
									@else
										<li><button id="viewgale" class="btn" data-toggle="modal" data-target="#modalGaleriapalmas" disabled>Ver Galería</button></li><!---Faltapasar id galeria-->
									@endif
										<li><a href="{{url('booking')}}" class="btn">Reservar online</a></li>
									</ul>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-7">
				<div class="row">
					<div>			
						<div>
							@if(strcmp($res['itemsgal'],"") !== 0)
								<div class="row row_gal">
									<div class="col-xs-12" style="padding-top:66.666666%;position:relative;">
										<div id="galeriabi" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<div class="carousel-inner">
												{!!$res['itemsgal']!!}
											</div>
											<div class="controls">
												<button class="background_img galchang_up" style="background-image:url({{asset('img/habitaciones/arriba.svg')}});"></button>
												<button  class="background_img galchang_bown" style="background-image:url({{asset('img/habitaciones/abajo.svg')}});"></button>
											</div>
										</div>
									</div>
								</div>
								<div class="row row_gal" id="destacados">
									{!!$res['destacados']!!}
								</div>
							@else
								<div class="row row_gal">
									<div class="col-xs-12" style="padding-top:66.666666%;position:relative;">
										<div id="galeriabi" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<div class="carousel-inner">
												<div class="text-center item active">
													<i class="fa fa-picture-o fa-4x" aria-hidden="true"></i>
													<p class="titulo text-center vacio">NO HAY GALERIA PARA MOSTRAR</p>
												</div>
											</div>
											<div class="controls" style="display:none;">
												<button class="background_img galchang_up" style="background-image:url({{asset('img/habitaciones/arriba.svg')}});"></button>
												<button  class="background_img galchang_bown" style="background-image:url({{asset('img/habitaciones/abajo.svg')}});"></button>
											</div>
										</div>
									</div>
								</div>
								<div class="row row_gal" id="destacados">
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Banners Restaurantes -->
<div id="banner_restaurates" class="row">
	<a href="{{url('restaurantes')}}" >{!!$res['bannerestaurat']!!}</a>
</div>
@endsection
@section('Myscript')
<script src="{{asset('js/habitacionespalmasinn.js')}}"></script>
<script>
	var target = "{{e($res['id'])}}";
</script>
@include("modales.modal_imagen")
@include("modales.galeriplamas")
@endsection