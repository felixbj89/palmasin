@extends("layout.dashboard")
@section("title_section")
	Galería
@endsection
@section("sub_title")
	Galería
@endsection
@section("title-divsection")
	Historial de su galería principal
@endsection
@section("galeria-active")
	active
@endsection
@section("body-section")
	@if(Session::has('server_galeria'))
		<input type="hidden" name="server" id="server" value="{{Session::get('server_galeria')}}" />
	@endif
	<div class="row">
		<div class="col-xs-12 col-md-12">
			@if(count($list_galeria) > 0)
			<table id="historial_list" class="table table-striped">
				<thead>
					<tr class="fondo-tr">
						<th>Imagen</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($list_galeria as $lista)
						<tr class="fondo-tr" data-historial='{"historial_id":"{{e(base64_encode($lista->id))}}"}'>
							<td><img id="imagen{{$lista->id}}" src="{{asset($lista->galeria_path)}}" class="img-responsive img-thumbnail box-image box-list box-dimensiones center"></td>
							<td>
								<div class="row form-group center">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<a id="btnActivar" class="btn btn-app btn-palmasinn center">
											<i class="fa fa-power-off"></i> ACTIVAR
										</a>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12">
										<a id="btnRemover" class="btn btn-app btn-palmasinn center">
											<i class="fa fa-trash"></i> REMOVER
										</a>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfooter>
					<tr>
						<th>Imagen</th>
						<th>Opciones</th>
					</tr>
				</tfooter>
			</table>
			@else
				<label class="control-label clearfix center text-left alert-warning spacing-padding">No hay imágenes en su galería principal.</label>
			@endif
		</div>
	</div>
	@if(count($list_galeria) > 0)
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnListar" href="{{url('admin/listar_asociaciones')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-up"></i> ASOCIACIONES
				</a>
			</div>
		</div>
  @endif
@endsection
@section("scripts")
	<script src="{{asset('js/list_galeriahome.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar_remover")
	@include("modales.wait")
	@include("modales.galeria.listar.alerta")
	@include("modales.empty")
@endsection
