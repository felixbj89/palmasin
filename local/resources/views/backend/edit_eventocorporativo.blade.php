@if(Session::has('server_promo'))
	<input type="hidden" name="server" id="server" value="{{e(Session::get('server_promo'))}}">
@endif
<!-- action -->
<form id="form-eventos" action="{{url('admin/editadoeventos_corporativo')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
	<input type="hidden" id="evento" name="evento" value="{{base64_encode(e($evento->id))}}">
	<div class="row form-group">
		<div class="col-xs-12 col-md-6">
			<div class="row form-group">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">INFORMACIÓN DEL ENEVTO.</h3>
				</div>
				<div class="col-xs-12">
					<label class="control-label clearfix text-white">Título</label>
					<input type="text" id="titulo_evento" class="form-control form-border" name="titulo_evento" placeholder="*TÍTULO A INGRESAR" value="{{e($evento->event_title)}}"/>
					<span class="fa fa-pencil form-control-feedback icon-space"></span>
					<hr/>
					<label class="clearfix pull-right text-white" id="cantidad-titulo">0/30</label>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">DESCRIPCIÓN DEL EVENTO.</h3>
				</div>
				<div class="col-xs-12">
					<textarea name="descripcion_evento" id="descripcion_evento" class="form-border form-control summernote"></textarea>
					<hr/>
					<label class="clearfix pull-right text-white" id="cantidad-descripcion">0/250</label>
				</div>
			</div>
		</div>
	</div>
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-8">
			<a id="btnGuardarcorporativo" class="btn btn-app btn-palmasinn">
				<i class="fa fa-edit"></i> MODIFICAR
			</a>
			<a href="{{url('admin/listar_corporativos')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-undo"></i> REGRESAR
			</a>
		</div>
	</div>
</form>
<link rel="stylesheet" href="{{asset('css/eventos.css')}}">
<script src="{{asset('js/eventos_home.js')}}"></script>
<script>
$("textarea#descripcion_evento").val("{{e($evento->event_text)}}");
</script>