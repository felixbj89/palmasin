@extends("layout.dashboard")
@section("title_section")
	MI CUENTA
@endsection
@section("sub_title")
	MI CUENTA
@endsection
@section("title-divsection")
	Bienvenido {{$usuario->user_name}}.
@endsection
@section("cuenta-active")
	active
@endsection
@section("mi-css")
@endsection
@section("body-section")
	@if(Session::has('server_cuenta'))
		<input type="hidden" name="server" id="server" value="{{Session::get('server_cuenta')}}" />
	@elseif(Session::has('server_perfil'))
		<input type="hidden" name="server" id="server_perfil" value="{{Session::get('server_perfil')}}" />
	@endif
	<div class="col-md-12 col-xs-12">
		<!-- Custom Tabs (Pulled to the right) -->
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs pull-right">
				<li class="active"><a href="#avatar" data-toggle="tab">Avatar</a></li>
				<li><a href="#perfil" data-toggle="tab">Mi Perfil</a></li>
				<li class="pull-left header"><i class="fa fa-th"></i> Configue su cuenta</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="avatar">
					<form id="form-avatar" action="{{url('admin/addavataruser')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
						<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
						<div class="row form-group">
							<div class="col-xs-12 col-md-12">
								<h3 class="box-title text-black pull-left">SUBA UN AVATAR AL SISTEMA.</h3>
							</div>
						</div>
						<div class="row form-group has-feedback">
							<div class="col-xs-12 col-md-8">
								<label class="control-label clearfix text-white">Suba su imagen</label>
								<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
							</div>
							<div class="col-xs-12 col-md-4">
								<a id="btnAgregar" class="btn btn-app center btn-palmasinn">
									<i class="fa fa-save"></i> REGISTRAR
								</a>
							</div>
						</div>
					</form>
					@if(count($list_avatares) > 0)
						<label class="control-label clearfix center text-center text-white alert-palmas spacing-padding-inner">Resolución recomendada (160px x 160px / 256px x 256x).</label>
						<table id="historial_list" class="table table-striped">
							<thead>
								<tr class="fondo-tr">
									<th>Avatares</th>
									<th>Opciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($list_avatares as $avatares)
									<tr class="fondo-tr" data-historial='{"avatar_id":"{{e(base64_encode($avatares->id))}}"}'>
										<td><img id="avatar{{$avatares->id}}" src="{{asset($avatares->avatares_path)}}" class="img-responsive img-circle box-avatares center" width="160" height="160"></td>
										<td>
											<div class="row form-group center">
												<div class="col-xs-12 col-sm-12 col-md-6">
													<a id="btnUsarAvatar" class="btn btn-app btn-palmasinn center">
														<i class="fa fa-power-off"></i> ACTIVAR
													</a>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6">
													<a id="btnRemoverAvatar" class="btn btn-app btn-palmasinn center">
														<i class="fa fa-trash"></i> REMOVER
													</a>
												</div>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
							<tfooter>
								<tr>
									<th>Avatares</th>
									<th>Opciones</th>
								</tr>
							</tfooter>
						</table>
					@endif
				</div>
				<!-- /.tab-pane -->
				<div class="tab-pane" id="perfil">
					<div class="row form-group">
						<div class="col-xs-12 col-md-12">
							<h3 class="box-title text-black pull-right">MODIFIQUE SU PERFIL.</h3>
						</div>
					</div>
					<div clas="row form-group">
						<div clas="col-xs-12 col-md-12">
							<img id="avataractive" src="{{asset($meavatar)}}" class="img-responsive img-circle box-avatares center" width="160" height="160">
						</div>
					</div>
					<div clas="row form-group">
						<div clas="col-xs-12 col-md-12">
							<form id="form-perfil" action="{{url('admin/editperfil')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
								<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
								<div class="row form-group">
									<div class="col-xs-12 col-md-12">
										<h3 class="box-title text-black pull-left">{{e($usuario->user_name)}}.</h3>
										<h3 class="box-title text-black pull-right">{{e($usuario->user_email)}}.</h3>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-xs-12 col-md-6">
										<label class="control-label clearfix">Rol</label>
										<input type="text" id="roluser" class="form-control form-border" name="roluser" placeholder="*INGRESE UN LOGIN PARA SU USUARIO" value="{{e($whichrole)}}" disabled/>
										<span class="fa fa-map-marker form-control-feedback icon-space"></span>
										<hr/>
										<label class="clearfix pull-right" id="cantidad-role">0/20</label>
									</div>
									<div class="col-xs-12 col-md-6">
										<label class="control-label clearfix">Login</label>
										<input type="text" id="loginuser" class="form-control form-border" name="loginuser" placeholder="*INGRESE UN LOGIN PARA SU USUARIO" value="{{e($usuario->user_login)}}"/>
										<span class="fa fa-user form-control-feedback icon-space"></span>
										<hr/>
										<label class="clearfix pull-right" id="cantidad-login">0/20</label>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-xs-12 col-md-6">
										<label class="control-label clearfix">Password</label>
										<input type="text" id="passworduser" class="form-control form-border" name="passworduser" placeholder="*INGRESE EL PASSWORD PARA SU USUARIO"/>
										<span class="fa fa-lock form-control-feedback icon-space"></span>
										<hr/>
										<label class="clearfix pull-right" id="cantidad-password">0/20</label>
									</div>
									<div class="col-xs-12 col-md-6">
										<label class="control-label clearfix">Confirmar</label>
										<input type="text" id="confirmaruser" class="form-control form-border" name="confirmaruser" placeholder="*CONFIRME EL PASSWORD PARA SU USUARIO"/>
										<span class="fa fa-lock form-control-feedback icon-space"></span>
										<hr/>
										<label class="clearfix pull-right" id="cantidad-confirmar">0/20</label>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-xs-12 col-md-8">
										<a id="btnModificarPerfil" class="btn btn-app btn-palmasinn">
											<i class="fa fa-edit"></i> MODIFICAR
										</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
	</div>
@endsection
@section("scripts")
	<script src="{{asset('js/list_avatares.js')}}"></script>
	<script src="{{asset('js/validaciones.js')}}"></script>
	<script src="{{asset('js/meaccount.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar_remover")
	@include("modales.wait")
	@include("modales.alerta")
	@include("modales.avatar.alerta_avatar")
	@include("modales.empty")
@endsection
