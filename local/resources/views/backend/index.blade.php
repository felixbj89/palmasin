<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<title>{{e(Lang::get('message.title_bui'))}}</title>
			<!-- Tell the browser to be responsive to screen width -->
			<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
			<link rel="shortcut icon" type="image/png" href="{{asset('img/fav.png')}}"/>
			<!-- Bootstrap 3.3.7 -->
			<link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
			<!-- Font Awesome -->
			<link rel="stylesheet" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
			<!-- Ionicons -->
			<link rel="stylesheet" href="{{asset('fonts/ionicons-2.0.1/css/ionicons.min.css')}}">
			<!-- STYLE DASHBOARD -->
			<link rel="stylesheet" href="{{asset('css/master.css')}}">
			<link rel="stylesheet" href="{{asset('css/backend.css')}}">
			<link rel="stylesheet" href="{{asset('css/modales.css')}}">
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>
		<body class="hold-transition login-page box-login">
			<div class="login-box login-div">
				<!-- /.login-logo -->
				<div class="login-box-body border-div">
					<form id="form-login" action="{{url('admin/dashboard')}}" method="post">
						<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}"/>
						<div class="row form-group has-feedback">
							<img src="{{asset('img/logo_verde.png')}}" class="img-responsive center">
						</div>
						<div class="row form-group has-feedback">
							<p class="login-box-msg">{{e(Lang::get('message.subtitle'))}}</p>
						</div>
						<div class="row form-group has-feedback">
							<input type="text" class="form-control" placeholder="Login" id="loginuser" name="loginuser" value="{{old('loginuser')}}">
							<span class="fa fa-lock form-control-feedback"></span>
						</div>
						<div class="row form-group has-feedback">
							<input type="password" class="form-control" placeholder="Password" id="passworduser" name="passworduser" value="{{old('passworduser')}}">
							<span class="fa fa-lock form-control-feedback"></span>
						</div>
						<div class="row form-group">
							<div class="col-xs-4">
								<button id="btnAcceder" type="button" class="btn btn-default btn-block btn-palmasinn">
									<i class="fa fa-sign-in" aria-hidden="true"></i> ACCEDER
								</button>
							</div>
							<!-- /.col -->
						</div>
						<div class="row form-group">
							<div class="col-xs-12">
								<small><a href="{{url('/')}}" class="pull-right">Ir al Sitio</a></small>
							</div>
						</div>
					</form>
				</div>
				<!-- /.login-box-body -->
			</div>
			@include("modales.alerta")
			@include("errores.inputs")
			<!-- /.login-box -->
			<!-- jQuery 2.2.3 -->
			<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
			<!-- Bootstrap 3.3.7 -->
			<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
			<!-- JS -->
			<script src="{{asset('js/validaciones.js')}}"></script>
			<script src="{{asset('js/login.js')}}"></script>
		</body>
	</html>
