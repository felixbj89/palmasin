@extends("layout.dashboard")
@section("title_section")
	HABITACIONES
@endsection
@section("sub_title")
	HABITACIONES
@endsection
@section("title-divsection")
	Seleccione una imagen para mostar como Foto principal
@endsection
@section("habitaciones-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/bannertophabitad_palmas.css')}}">
@endsection
@section("body-section")
	<form id="form-habitad" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />	
		<div class="row form-group">
			<div class="col-xs-12">
				<h3 class="box-title text-white pull-left">FOTO PRINCIPAL.</h3>
			</div>
			<div class="col-xs-12" id="conten_galeria">
				<div id="info" class="alert alert-info text-center">
					 <h3><strong>Para comenzar</strong> selecciona una imagen.</h3>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12">
				<input type="hidden" id="url1" name="url1" value="{{url(e($fija))}}"/>
				<img src="{{url(e($fija))}}" id="banner_bottom" class="img-responsive box-image box-picture">
				<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnGuardar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-edit"></i> MODIFICAR
				</a>
				<a id="" href="{{url('admin/crear_fotosecundariaroom')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-down"></i> FOTO SECUNDARIA
				</a>
			</div>		
		</div>
	</form>			
@endsection
@section("scripts")
	<script src="{{asset('js/bannertophabitad_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.habitaciones.alerta")
@endsection