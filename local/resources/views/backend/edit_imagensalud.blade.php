<form id="form-banner" action="{{url('admin/editbannerinferior')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">INFORMACIÓN DE SU IMAGEN DE SALUD.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Título</label>
				<input type="text" id="titulo_salud" class="form-control form-border" name="titulo_salud" placeholder="*TÍTULO A INGRESAR" value="{{e($imagen_salud[0]->salud_titulo)}}"/>
				<span class="fa fa-pencil form-control-feedback icon-space"></span>
				<hr/>
				<label class="clearfix pull-right text-white" id="cantidad-tsalud">0/20</label>
			</div>
			<div class="col-xs-12 col-md-6">
				<label class="control-label clearfix text-white">Sub-Título</label>
				<input type="text" id="subtitulo_salud" class="form-control form-border" name="subtitulo_salud" placeholder="*TÍTULO A INGRESAR" value="{{e($imagen_salud[0]->salud_situlo)}}"/>
				<span class="fa fa-pencil form-control-feedback icon-space"></span>
				<hr/>
				<label class="clearfix pull-right text-white" id="cantidad-ssalud">0/20</label>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">IMAGEN DE SALUD A SUBIR.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<label class="control-label clearfix text-white">Suba su imagen</label>
				<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">VISTA PREVIA DE SU IMAGEN.</h3>
				<h3 class="box-title text-white pull-right">RESOLUCIÓN RECOMENDADA: 1368x200px.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<input type="hidden" id="url1" name="url1" value="{{asset($imagen_salud[0]->salud_path)}}"/>
				<img src="{{asset($imagen_salud[0]->salud_path)}}" id="uno_picture" class="img-responsive box-image box-salud">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">IMAGEN INTERNA A SUBIR</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<label class="control-label clearfix text-white">Suba su imagen</label>
				<input type="file" id="file_two" name="file_two" onchange='javascript:openFile2(event)'/>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">VISTA PREVIA DE SU IMAGEN.</h3>
				<h3 class="box-title text-white pull-right">RESOLUCIÓN RECOMENDADA: 500x500px.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-12">
				<input type="hidden" id="url2" name="url2" value="{{asset($imagen_salud[0]->salud_pathinterna)}}"/>
				<img src="{{asset($imagen_salud[0]->salud_pathinterna)}}" id="dos_picture" class="img-responsive box-image box-interna">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnEditarImagenSalud" class="btn btn-app btn-palmasinn">
					<i class="fa fa-pencil-square-o"></i> MODIFICAR
				</a>
				<a id="btnRegresar" hreF="{{url('admin/crear_bsalud')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-undo"></i> REGRESAR
				</a>
			</div>
		</div>
	</form>
	<script src="{{asset('js/imagen_inferior.js')}}"></script>