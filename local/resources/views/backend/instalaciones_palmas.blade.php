@extends("layout.dashboard")
@section("title_section")
	INSTALACIONES
@endsection
@section("sub_title")
	INSTALACIONES
@endsection
@section("title-divsection")
	Su galería de contar con (4) imágenes mínimas
@endsection
@section("elhotel-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeriarestaurant_palmas.css')}}">
@endsection
@section("body-section")
<div class="row">
	<div class="col-xs-12">
		<div class="row form-group">
			<div class="col-xs-12">
				<h3 class="box-title text-white pull-left">GALERIA INTALACIONES.</h3>
			</div>
			<div class="col-xs-12" id="conten_galeria">
				<div id="info" class="alert alert-info text-center">
					 <h3><strong>Para comenzar</strong> seleccione la imagen que desea cambiar.</h3>
					 * <strong>Recuerda para crear una galeria</strong> debes cargar un minimo de cuatro (4) imagenes y un maximo de ocho (8). *</br>
				</div>
				{!!$galeria!!}
			</div>
		</div>
	</div>
</div>
<hr/>
<div class="row form-group">
	<div class="col-xs-12 col-md-8">
		<a href="{{url('admin/crear_fotoprincipalhotel')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-chevron-up"></i> FOTO PRINCIPAL
		</a>
		<a href="{{url('admin/crear_proyectos')}}" class="btn btn-app btn-palmasinn">
			<i class="fa fa-chevron-down"></i> PROYECTOS
		</a>
	</div>		
</div>
@endsection
@section("scripts")
	<script src="{{asset('js/galeriainstalaciones_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.hotel.alerta")
@endsection