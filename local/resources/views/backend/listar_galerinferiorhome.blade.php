@extends("layout.dashboard")
@section("title_section")
	GALERIA HOME
@endsection
@section("sub_title")
	GALERIA HOME
@endsection
@section("title-divsection")
	Complete la información para poder registrar cada una por separado.
@endsection
@section("inferior-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeriasecundaria_home.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_galeriasecu'))
		<input type="hidden" id="server_galeriasecu" value="{{Session::get('server_galeriasecu')}}" />
	@endif
	<div id="row_edit" class="row form-group">
		<div class="col-xs-12 col-md-12">
			@if(count($list_imagen) > 0)
			<table id="galeria_list" class="table table-striped">
				<thead>
					<tr class="fondo-tr">
						<th>Posición</th>
						<th>Imagem</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($list_imagen as $lista)
						<tr class="fondo-tr" data-historial='{"evento":"{{base64_encode(e($lista->id))}}"}'>
							<td>
								{{e($lista->bannergaleri_id)}}
							</td>
							<td>
								<img src="{{url($lista->bannergaleri_rutaimg)}}" class="img-responsive img-thumbnail box-list box-galeriainferior center">
							</td>
							<td>
								<div class="row form-group center">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<a class="btn btn-app btn-palmasinn btnModificar center">
											<i class="fa fa-pencil-square-o"></i> MODIFICAR
										</a>
									</div>
								</div>
								<div class="row form-group center">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<a class="btn btn-app btn-palmasinn btnRemover center">
											<i class="fa fa-trash"></i> REMOVER
										</a>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfooter>
					<tr>
						<th>Posición</th>
						<th>Imagem</th>
						<th>Opciones</th>
					</tr>
				</tfooter>
			</table>
			@else
				<label class="control-label clearfix center text-left alert-warning spacing-padding">No hay galeria.</label>
			@endif
		</div>
	</div>
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-8">
			<a id="btnListar" href="{{url('admin/administrargaleria')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-undo"></i> REGRESAR
			</a>
		</div>
	</div>
@endsection
@section("scripts")
<script src="{{asset('js/galeriasecundaria_home.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.confirmar_remover")
	@include("modales.eventos.editalerta")
	@include("modales.wait")
@endsection
