	<form id="form-habitad" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<div class="row form-group">
			<div class="col-xs-12 col-sm-6">		
				<div class="row form-group">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">INFORMACIÓN DE LA HABITACION.</h3>
					</div>
					<div class="col-xs-12">
						<label class="control-label clearfix text-white">Título</label>
						<input type="text" id="titulo_habitad" class="form-control form-border" name="titulo_habitad" placeholder="*TÍTULO A INGRESAR" value="{{e($habitad->nombre)}}"/>
						<span class="fa fa-pencil form-control-feedback icon-space"></span>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad-titulo">0/30</label>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-sm-6">
				<div class="row form-group">
					<div class="col-xs-12 ">
						<h3 class="box-title text-white pull-left">DESCRIPCIÓN HABITACION.</h3>
					</div>
					<div class="col-xs-12">
						<div id="editor_habitad" class="summernote">{!!$habitad->descripcion!!}</div>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad_descripcion">0/330</label>
					</div>
				</div>			
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="row form-group">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">LISTA DE CARACTERISTICAS.</h3>
					</div>
					<div class="col-xs-12">
						<div id="caracteristicas_habitad" class="summernote">{!!$habitad->detalles!!}</div>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad_caracteristicas">0/330</label>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-sm-6">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">LISTA DE COMODIDADES.</h3>
				</div>
				<div id="comodidades" class="col-xs-12">
				
				<?php 
					$comodidades = explode(',',$habitad->list_comodidades);
				?>
					<label class="checkbox-inline ico ico1"><input type="checkbox" value="1" name="comodidades[]" @if(in_array("1", $comodidades))checked @endif><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico2"><input type="checkbox" value="2" name="comodidades[]" @if(in_array("2", $comodidades))checked @endif><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico3"><input type="checkbox" value="3" name="comodidades[]" @if(in_array("3", $comodidades))checked @endif><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico4"><input type="checkbox" value="4" name="comodidades[]" @if(in_array("4", $comodidades))checked @endif><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico5"><input type="checkbox" value="5" name="comodidades[]" @if(in_array("5", $comodidades))checked @endif><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico6"><input type="checkbox" value="6" name="comodidades[]" @if(in_array("6", $comodidades))checked @endif><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico7"><input type="checkbox" value="7" name="comodidades[]" @if(in_array("7", $comodidades))checked @endif><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
					<label class="checkbox-inline ico ico8"><input type="checkbox" value="8" name="comodidades[]" @if(in_array("8", $comodidades))checked @endif><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i></label>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">IMAGEN PORTADA HABITACION.</h3>
				</div>				
				<div class="col-xs-12">
					<input type="hidden" id="url1" name="url1" value="{{asset('img/home/promociones/default.jpg')}}"/>
					<img src="{{url(e($habitad->img))}}" id="uno_picture" class="img-responsive box-image box-promociones">
				</div>
				<div class="col-xs-12">
					<!--label class="control-label clearfix text-white">Suba su promoción</label-->
					<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
				</div>
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnGuardar" class="btn btn-app btn-palmasinn" data-habitad="{{e(base64_encode($habitad->habitacion))}}">
					<i class="fa fa-save"></i> REGISTRAR
				</a>
				<a id="btnListar" href="{{url('admin/listar_habitacion')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-undo"></i> REGRESAR
				</a>
			</div>
		</div>
	</form>
	<link rel="stylesheet" href="{{asset('css/habitad_palmas.css')}}">
	<script src="{{asset('js/habitadedit_palmas.js')}}"></script>