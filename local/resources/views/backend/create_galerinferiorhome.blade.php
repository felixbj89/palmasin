<form id="form-galeri" method="post" action="{{url('admin/savegaleriahome')}}" accept-charset="UTF-8" enctype="multipart/form-data">
	<div class="form-group">
		<div class="col-xs-12">		
			<div class="row form-group">
				<div class="col-xs-12 col-sm-6">
					<div class="row form-group">
						<div class="col-xs-12">
							<h3 class="box-title text-white pull-left">DESCRIPCIÓN GALERIA.</h3>
						</div>
						<div class="col-xs-12">
							<textarea id="editor" name="text" class="summernote">{{e($galeria->bannergaleri_texto)}}</textarea>
							<hr/>
							<label class="clearfix pull-right text-white" id="cantidad_descripcion">0/330</label>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12">
							<div class="row">
								<div class="col-xs-12">
									<h3 class="box-title text-white pull-left">LINK GALERIA</h3>
								</div>
								<div class="col-xs-12">
									<a href="#linkhabitaciones" class="btnlink btn btn-app btn-palmasinn" data-toggle="collapse">
										<i class="fa fa-bed"></i> HABITACIONES
									</a>
									<a href="#linkrestaurantes" class="btnlink btn btn-app btn-palmasinn" data-toggle="collapse">
										<i class="fa fa-cutlery"></i> RESTAURANTES
									</a>
									<a id="elhotel" href="#linkexterno" class="btnlink btn btn-app btn-palmasinn" data-toggle="collapse">
										<i class="fa fa fa-bed"></i> HOTEL
									</a>
								</div>
								<div id="linkhabitaciones" class="col-xs-12 collapse">
									<label class="corporativo control-label clearfix text-white">Habitaciones</label>
									{!!$habitaciones!!}
								</div>
								<div id="linkrestaurantes" class="col-xs-12 collapse">
									<label class="control-label clearfix text-white">Restaurantes</label>
									<select class="corporativo selectlink form-control form-border" name="restaurante">
										<option value="" selected disabled> Seleccione un Restaurante</option>
										<option value="restaurantes/habneros">Habaneros</option>
										<option value="restaurantes/maracuya">Maracuya</option>
										<option value="restaurantes/lobbybar">Lobbybar</option>
									</select>
								</div>
								<div id="linkexterno" class="col-xs-12 collapse">
									<label class="control-label clearfix text-white">Inicio</label>
									<input type="text" id="link" class="corporativo form-control form-border" name="link" placeholder="LINK EXTERNO"/>
									<span class="fa fa-external-link form-control-feedback icon-space-form"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="col-xs-12">
						<h3 class="box-title text-white pull-left">IMAGEN GALERIA</h3>
					</div>
					<div class="col-xs-12">
						<div class="p_picture">
							<div class="box-image">
								<img src="{{url($galeria->bannergaleri_rutaimg)}}" id="galeri_1">
							</div>
							<div class="box-file">
								<input data-status="{{e($galeria->bannergaleri_destacar)}}" class="file_pic" type="file" id="galeri_file1" name="galeri_file1" data-img="#galeri_1"/>
							</div>
						</div>
					</div>
				</div>					
			</div>
		</div>
		<div class="col-xs-12">
			<hr/>
			<input type="hidden" name="galeria" value="{{base64_encode($galeria->id)}}"/>
			{{ csrf_field() }}
			<button type="button" id="btn-guardar" class="btn btn-app btn-palmasinn">
				<i class="fa fa-save"></i> GUARDAR
			</button>
			<a href="{{url('admin/listar_galerinferior')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-chevron-down"></i> LISTAR
			</a>
		</div>
	</div>
</form>	
<script>

</script>
<script src="{{asset('js/galeriasecundaria_home.js')}}"></script>		