@if(Session::has('server_promo'))
	<input type="hidden" name="server" id="server" value="{{e(Session::get('server_promo'))}}">
@endif
<input type="hidden" id="mindate" name="mindate" value="{{e($fecha_min)}}">
<!-- action -->
<form id="form-eventos" action="{{url('admin/editadoeventos_home')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
	<input type="hidden" id="evento" name="evento" value="{{base64_encode(e($evento->id))}}">
	<div class="row form-group">
		<div class="col-xs-12 col-md-6">
			<div class="row form-group">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">INFORMACIÓN DEL ENEVTO.</h3>
				</div>
				<div class="col-xs-12">
					<label class="control-label clearfix text-white">Título</label>
					<input type="text" id="titulo_evento" class="form-control form-border" name="titulo_evento" placeholder="*TÍTULO A INGRESAR"/>
					<span class="fa fa-pencil form-control-feedback icon-space"></span>
					<hr/>
					<label class="clearfix pull-right text-white" id="cantidad-titulo">0/20</label>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">DESCRIPCIÓN DEL EVENTO.</h3>
				</div>
				<div class="col-xs-12">
					<textarea name="descripcion_evento" id="descripcion_evento" class="form-border form-control summernote"></textarea>
					<hr/>
					<label class="clearfix pull-right text-white" id="cantidad-descripcion">0/250</label>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="row form-group">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">IMAGEN PARA EL EVENTO.</h3>
				</div>				
				<div class="col-xs-12">
					<input type="hidden" id="url1" name="url1" value="{{url('img/home/eventos')}}/{{e($evento->event_path)}}"/>
					<img src="{{url('img/home/eventos')}}/{{e($evento->event_path)}}" id="uno_picture" class="img-responsive box-image box-promociones">
				</div>
				<div class="col-xs-12">
					<!--label class="control-label clearfix text-white">Suba su promoción</label-->
					<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
				</div>					
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-xs-6">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">LINK DEL EVENTO.</h3>
				</div>
				<div class="col-xs-12">
					<a href="#linkhabitaciones" class="btnlink btn btn-app btn-palmasinn" data-toggle="collapse">
						<i class="fa fa-bed"></i> HABITACIONES
					</a>
					<a href="#linkrestaurantes" class="btnlink btn btn-app btn-palmasinn" data-toggle="collapse">
						<i class="fa fa-cutlery"></i> RESTAURANTES
					</a>
					<a href="#linkexterno" class="btnlink btn btn-app btn-palmasinn" data-toggle="collapse">
						<i class="fa fa-external-link"></i> EXTERNO
					</a>
				</div>
				<div id="linkhabitaciones" class="col-xs-12 collapse">
					<label class="control-label clearfix text-white">Habitaciones</label>
					{!!$habitaciones!!}
				</div>
				<div id="linkrestaurantes" class="col-xs-12 collapse">
					<label class="control-label clearfix text-white">Restaurantes</label>
					<select class="form-control form-border" name="restaurante">
						<option value="" selected disabled> Seleccione un Restaurante</option>
						<option value="{{url('restaurantes/habneros')}}">Habaneros</option>
						<option value="{{url('restaurantes/maracuya')}}">Maracuya</option>
						<option value="{{url('restaurantes/lobbybar')}}">Lobbybar</option>
					</select>
				</div>
				<div id="linkexterno" class="col-xs-12 collapse">
					<label class="control-label clearfix text-white">Inicio</label>
					<input type="text" id="link" class="form-control form-border" name="link" placeholder="LINK EXTERNO"/>
					<span class="fa fa-external-link form-control-feedback icon-space-form"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-xs-12">
			<div class="row form-group">
				<div class="col-xs-12">
					<h3 class="box-title text-white pull-left">FECHA DEL EVENTO.</h3>
				</div>
				<div class="col-xs-12 - col-sm-6">
					<label class="control-label clearfix text-white">Inicio</label>
					<input type="text" id="inicio_evento" class="form-control form-border" name="inicio_evento" placeholder="*INICIO DEL EVENTO"/>
					<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
				</div>
				<div class="col-xs-12 - col-sm-6">
					<label class="control-label clearfix text-white">Fin</label>
					<input type="text" id="fin_evento" class="form-control form-border" name="fin_evento" placeholder="*FIN DEL EVENTO"/>
					<span class="fa fa-calendar form-control-feedback icon-space-form"></span>
				</div>
			</div>			
		</div>
	</div>
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-8">
			<a id="btnGuardar" class="btn btn-app btn-palmasinn">
				<i class="fa fa-edit"></i> MODIFICAR
			</a>
			<a href="{{url('admin/listar_eventos')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-undo"></i> REGRESAR
			</a>
		</div>
	</div>
</form>
<link rel="stylesheet" href="{{asset('css/eventos.css')}}">
<script src="{{asset('js/eventos_home.js')}}"></script>
<script src="{{asset('js/editadoeventos_home.js')}}"></script>
<script>
$(document).ready(function(){
	$('#titulo_evento').val('{{e($evento->event_title)}}');
	$('#cantidad-titulo').text(''+($('#titulo_evento').val().length)+'/20');
	$('#descripcion_evento').val('{{e($evento->event_text)}}');
	$('#cantidad-descripcion').text(''+($('#descripcion_evento').val().length)+'/300');
	$('#inicio_evento').val('{{e($evento->event_dateini)}}');
	$('#fin_evento').val('{{e($evento->event_datefin)}}');
});
</script>
