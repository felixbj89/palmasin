@extends("layout.dashboard")
@section("title_section")
	EVENTOS CORPORATIVOS
@endsection
@section("sub_title")
	EVENTOS CORPORATIVOS
@endsection
@section("title-divsection")
	Listar Eventos Corporativos Registrados.
@endsection
@section("eventos-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/eventos.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_eventos'))
		<input type="hidden" name="server_eventos" id="server_eventos" value="{{Session::get('server_eventos')}}" />
	@endif
	<div class="row">
		<div class="col-xs-12 col-md-12">
			@if(count($list_eventos) > 0)
			<table id="event_list" class="table table-striped">
				<thead>
					<tr class="fondo-tr">
						<th>Nombre</th>
						<th>Descrpción</th>
						<th>Estado</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($list_eventos as $lista)
						<tr class="fondo-tr" data-historial='{"evento":"{{base64_encode(e($lista->id))}}"}'>
							<td>
								{{e($lista->event_title)}}
							</td>
							<td>
								{{substr($lista->event_text,0,50)}}...
							</td><td>
								<div class="row">
								@if($lista->event_ppal==0)<!--no sta ativo-->
									<div class="col-xs-12">
										<a class="btnActivarcorporativo">
											<i class="fa fa-toggle-off fa-2x"></i>
										</a>
									</div>
								@else
									<div class="col-xs-12">
										<a class="btnActivarcorporativo">
											<i class="fa fa-toggle-on fa-2x"></i>
										</a>
									</div>
								@endif
								</div>
							</td>
							<td>
								<div class="row form-group center">
									<div class="col-xs-12 col-sm-6">
										<a class="btn btn-app btn-palmasinn btnModificarCorporativos center">
											<i class="fa fa-pencil-square-o"></i> MODIFICAR
										</a>
									</div>
									<div class="col-xs-12 col-sm-6">
										<a class="btn btn-app btn-palmasinn btnRemover center">
											<i class="fa fa-trash"></i> REMOVER
										</a>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfooter>
					<tr>
						<th>Nombre</th>
						<th>Descrpción</th>
						<th>Estado</th>
						<th>Opciones</th>
					</tr>
				</tfooter>
			</table>
			@else
				<label class="control-label clearfix center text-left alert-warning spacing-padding">No hay eventos registradas en el sistema.</label>
			@endif
		</div>
	</div>
	@if(count($list_eventos) > 0)
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a href="{{url('admin/crear_eventos')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-up"></i> REGISTRAR
				</a>
			</div>
		</div>
  @endif
@endsection
@section("scripts")
	<script src="{{asset('js/list_evetoshome.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar_remover")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.empty")
	@include("modales.eventos.editalerta")
@endsection