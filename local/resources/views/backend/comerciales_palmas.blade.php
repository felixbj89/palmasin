@extends("layout.dashboard")
@section("title_section")
	COMERCIALES
@endsection
@section("sub_title")
	COMERCIALES
@endsection
@section("title-divsection")
	COMERCIALES
@endsection
@section("elhotelreal-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/eventoscorporativos.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_comerciales'))
		<input type="hidden" name="server_comerciales" id="server_comerciales" value="{{Session::get('server_comerciales')}}" />
	@endif

	<form id="form-comerciales" action="{{url('admin/editcomercialeshome')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">COMERCIAL #1.</h3>
			</div>
		</div>
		<div class="row form-group has-feedback">
			<div class="col-xs-12 col-md-8">
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<label class="control-label clearfix text-white">Título</label>
						<input type="text" id="titulo_comercialone" class="form-control form-border" name="titulo_comercialone" placeholder="*TÍTULO A INGRESAR" value="{{e($comerciales->comerciales_textoone)}}"/>
						<span class="fa fa-pencil form-control-feedback icon-space"></span>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad-titulo">0/34</label>
					</div>
					<div class="col-xs-12 col-md-12">
						<label class="control-label clearfix text-white">Link</label>
						<input type="url" id="link_comercialone" class="form-control form-border" name="link_comercialone" placeholder="*LINK A INGRESAR"  value="{{e($comerciales->comerciales_linkone)}}"/>
						<span class="fa fa-link form-control-feedback icon-space"></span>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad-linksone">0/200</label>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="col-xs-12 box-image">
					<input type="hidden" id="url1" name="url1" value="{{url($comerciales->comerciales_pictureone)}}"/>
					<img src="{{url($comerciales->comerciales_pictureone)}}" id="uno_picture">
				</div>
				<div class="col-xs-12 box-file">
					<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">COMERCIAL #2.</h3>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<label class="control-label clearfix text-white">Título</label>
						<input type="text" id="titulo_comercialtwo" class="form-control form-border" name="titulo_comercialtwo" placeholder="*TÍTULO A INGRESAR" value="{{e($comerciales->comerciales_textotwo)}}"/>
						<span class="fa fa-pencil form-control-feedback icon-space"></span>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad-titulotwo">0/34</label>
					</div>
					<div class="col-xs-12 col-md-12">
						<label class="control-label clearfix text-white">Link</label>
						<input type="url" id="link_comercialdos" class="form-control form-border" name="link_comercialdos" placeholder="*LINK A INGRESAR" value="{{e($comerciales->comerciales_linktwo)}}"/>
						<span class="fa fa-link form-control-feedback icon-space"></span>
						<hr/>
						<label class="clearfix pull-right text-white" id="cantidad-linkdos">0/200</label>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="col-xs-12 box-image">
					<input type="hidden" id="url2" name="url2" value="{{url($comerciales->comerciales_picturetwo)}}"/>
					<img src="{{url($comerciales->comerciales_picturetwo)}}" id="dos_picture">
				</div>
				<div class="col-xs-12 box-file">
					<input type="file" id="file_two" name="file_two" onchange='javascript:openFile2(event)'/>
				</div>
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnModificar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-edit"></i> MODIFICAR
				</a>
				<a href="{{url('admin/crear_eventoscorporativos')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-up"></i> EVENTOS
				</a>
			</div>
		</div>
	</form>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.hotel.comerciales.alerta")
@endsection
@section("scripts")
	<script src="{{asset('js/comerciales_hotel.js')}}"></script>
@endsection