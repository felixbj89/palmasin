@extends("layout.dashboard")
@section("title_section")
	BLOQUE DE SALUD
@endsection
@section("sub_title")
	BLOQUE DE SALUD
@endsection
@section("title-divsection")
	Listar Imágenes de Salud
@endsection
@section("banner-active")
	active
@endsection
@section("body-section")
	@if(Session::has('server_bannerinferior'))
		<input type="hidden" name="server" id="server" value="{{Session::get('server_bannerinferior')}}" />
	@endif
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<label class="control-label clearfix center text-center text-white alert-palmas spacing-padding-inner">Si su imagen está activa y en historial, no podrá ser activada.</label>
			@if(count($list_bannerinferior) > 0)
				<table id="historial_list" class="table table-striped">
					<thead>
						<tr class="fondo-tr">
							<th>Imagen Externa</th>
							<th>Imagen Interna</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($list_bannerinferior as $lista)
							<tr class="fondo-tr" data-historial='{"historial_id":"{{e(base64_encode($lista->id))}}"}'>
								<td><img id="imagenexterna{{$lista->id}}" src="{{asset($lista->salud_path)}}" class="img-responsive img-thumbnail box-image box-list box-bannersalud center"></td>
								<td><img id="imageninterna{{$lista->id}}" src="{{asset($lista->salud_pathinterna)}}" class="img-responsive img-thumbnail box-image box-list box-bsaludpromo center"></td>
								<td>
									<div class="row form-group position-right">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<a id="btnUsarSalud" class="btn btn-app btn-palmasinn">
												<i class="fa fa-power-off"></i> ACTIVAR
											</a>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12">
											<a id="btnRemoverSalud" class="btn btn-app btn-palmasinn">
												<i class="fa fa-trash"></i> REMOVER
											</a>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
					<tfooter>
						<tr>
							<th>Imagen Externa</th>
							<th>Imagen Interna</th>
							<th>Opciones</th>
						</tr>
					</tfooter>
				</table>
			@else
				<label class="control-label clearfix center text-left alert-warning spacing-padding">No hay bloques de salud registrados en el sistema.</label>
			@endif
		</div>
	</div>
	@if(count($list_bannerinferior) > 0)
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-8">
			<a id="btnRegistrar" href="{{url('admin/gestionarimagensalud')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-undo"></i> REGRESAR
			</a>
		</div>
	</div>
	@endif
@endsection
@section("scripts")
	<script src="{{asset('js/list_bannerinferior.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar_remover")
	@include("modales.wait")
	@include("modales.empty")
	@include("modales.bsalud.alerta")
@endsection