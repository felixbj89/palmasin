@extends("layout.dashboard")
@section("title_section")
	GALERÍA DE HABITACIONES
@endsection
@section("sub_title")
	GALERÍA DE HABITACIONES
@endsection
@section("title-divsection")
	Seleccione uana habitación y complete la información para poder registrar.
@endsection
@section("habitaciones-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeriahabitad_palmas.css')}}">
@endsection
@section("body-section")
	@if(Session::has('server_promo'))
		<input type="hidden" name="server" id="server" value="{{e(Session::get('server_promo'))}}" />
	@endif
	<div class="row form-group">
		<form id="form-habitad" accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />	
			<div class="form-group">
				<div class="col-xs-12 col-sm-6">		
					<div class="row form-group">
						<div class="col-xs-12">
							<h3 class="box-title text-white pull-left">NOMBRE HABITACIÓN.</h3>
						</div>
						<div class="col-xs-12">
							<select class="form-control" id="sel_habitad">
								<option selected value="NULL">Escoge una habitación</option>
								@foreach($listhabitad AS $clave => $valor)
									<option value="{{e($valor['habitad_value'])}}">{{e($valor['habitad_name'])}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12">		
					<div class="row form-group">
						<div class="col-xs-12">
							<h3 class="box-title text-white pull-left">GALERIA HABITACIÓN.</h3>
						</div>
						<div class="col-xs-12" id="conten_galeria">
							<div id="info" class="alert alert-info text-center">
								 <h3><strong>Para comenzar</strong> selecciona una habitación.</h3>
								 * <strong>Recuerda para crear una galeria</strong> debes cargar un minimo de cuatro (4) imagenes y un maximo de ocho (8). *</br>
								 * <strong>Recuerda para crear una galeria</strong> debes seleccionar tres(3) imagenes destacadas. *
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>	
	</div>
	<hr/>
	<div class="row form-group">
		<div class="col-xs-12 col-md-8">
			<a id="btnListar" href="{{url('admin/crear_fotosecundariaroom')}}" class="btn btn-app btn-palmasinn">
				<i class="fa fa-chevron-up"></i> FOTO SECUNDARIA
			</a>
		</div>
	</div>
@endsection
@section("scripts")
	<script src="{{asset('js/galerihabitad_palmas.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.empty")
	@include("modales.alerta")
	@include("modales.wait")
@endsection