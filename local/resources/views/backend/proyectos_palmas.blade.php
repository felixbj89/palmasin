@extends("layout.dashboard")
@section("title_section")
	PROYECTOS
@endsection
@section("sub_title")
	PROYECTOS
@endsection
@section("title-divsection")
	Complete la información para poder registrar
@endsection
@section("elhotel-active")
	active
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeria.css')}}">
@endsection
@section("body-section")
@if(Session::has('server_proyectos'))
		<input type="hidden" name="server_proyectos" id="server_proyectos" value="{{Session::get('server_proyectos')}}" />
	@endif

	<form id="form-proyectoshome" action="{{url('admin/addproyectoshome')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">INGRESE SU PROYECTO EN CURSO.</h3>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<label class="control-label clearfix text-white">Proyecto a subir</label>
				<input type="file" id="proyecto_curso" name="proyecto_curso" onchange='javascript:openPROYECTOS(event)'/>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<h3 class="box-title text-white pull-left">PROYECTOS REGISTRADOS EN EL SISTEMA.</h3>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-sm-6 col-md-6 div-space">
				<input type="hidden" id="url1" name="url1" value="{{asset($proyectos->proyectos_picone)}}"/>
				<img src="{{asset($proyectos->proyectos_picone)}}" id="uno_picture" class="img-responsive box-image proyectos-box-picture">
				<input type="file" id="file_one" name="file_one" onchange='javascript:openFile1(event)'/>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 div-space">
			<input type="hidden" id="url2" name="url2" value="{{asset($proyectos->proyectos_pictwo)}}"/>
				<img src="{{asset($proyectos->proyectos_pictwo)}}" id="dos_picture" class="img-responsive box-image proyectos-box-picture">
				<input type="file" id="file_two" name="file_two" onchange='javascript:openFile2(event)'/>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-sm-6 col-md-6 div-space">
				<input type="hidden" id="url3" name="url3" value="{{asset($proyectos->proyectos_picthrid)}}"/>
				<img src="{{asset($proyectos->proyectos_picthrid)}}" id="tres_picture" class="img-responsive box-image proyectos-box-picture">
				<input type="file" id="file_thrid" name="file_thrid" onchange='javascript:openFile3(event)'/>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 div-space">
				<input type="hidden" id="url4" name="url4" value="{{asset($proyectos->proyectos_picfour)}}"/>
				<img src="{{asset($proyectos->proyectos_picfour)}}" id="cuatro_picture" class="img-responsive box-image proyectos-box-picture">
				<input type="file" id="file_four" name="file_four" onchange='javascript:openFile4(event)'/>
			</div>
		</div>
		<hr/>
		<div class="row form-group">
			<div class="col-xs-12 col-md-8">
				<a id="btnRegistrar" class="btn btn-app btn-palmasinn">
					<i class="fa fa-edit"></i> MODIFICAR
				</a>
				<a href="{{url('admin/crear_instalaciones')}}" class="btn btn-app btn-palmasinn">
					<i class="fa fa-chevron-up"></i> INSTALACIONES
				</a>
			</div>
		</div>
	</form>
@endsection
@section("scripts")
	<script src="{{asset('js/proyectos.js')}}"></script>
@endsection
@section("modales")
	@include("modales.confirmar")
	@include("modales.wait")
	@include("modales.hotel.alerta")
@endsection