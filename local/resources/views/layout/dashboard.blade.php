<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta name="csrf-token" content="{{ csrf_token() }}">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<title>{{e(Lang::get('message.title_dui'))}}</title>
			<!-- Tell the browser to be responsive to screen width -->
			<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
			<link rel="shortcut icon" type="image/png" href="{{asset('img/fav.png')}}"/>
			<!-- Bootstrap 3.3.7 -->
			<link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
			<!-- Font Awesome -->
			<link rel="stylesheet" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
			<!-- Ionicons -->
			<link rel="stylesheet" href="{{asset('fonts/ionicons-2.0.1/css/ionicons.min.css')}}">
			<!-- STYLE DASHBOARD -->
			<link href="{{asset('plugins/jQuery.filer-1.3.0/css/jquery.filer.css')}}" media="all" rel="stylesheet">
			<link href="{{asset('plugins/fileuploader-1.0.0.4/src/jquery.fileuploader.css')}}" media="all" rel="stylesheet">
			<link href="{{asset('plugins/DataTables-1.10.13/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.dataTables.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/DataTables-1.10.13/extensions/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/flatpickr-master/dist/flatpickr.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/summernote-master/dist/summernote.css')}}" rel="stylesheet">
			<link rel="stylesheet" href="{{asset('css/master.css')}}">
			@yield("mi-css")
			<link rel="stylesheet" href="{{asset('css/backend.css')}}">
			<link rel="stylesheet" href="{{asset('css/modales.css')}}">
			<link rel="stylesheet" href="{{asset('css/style.css')}}">
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>
		<body class="hold-transition fixed skin-blue sidebar-collapse sidebar-mini">
			<!-- Site wrapper -->
			<div class="wrapper">
				<header class="main-header">
					<!-- Logo -->
					<a href="{{url('/')}}" class="logo">
					  <!-- mini logo for sidebar mini 50x50 pixels -->
					  <span class="logo-mini"><b>I</b>NN</span>
					  <!-- logo for regular state and mobile devices -->
					  <span class="logo-lg"><b>PALMAS</b>INN</span>
					</a>
					<nav class="navbar navbar-static-top">
						<!-- Sidebar toggle button-->
						<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">
							<!-- Notifications: style can be found in dropdown.less -->
								@yield("notificaciones-vencidas")
								@yield("notificaciones-rangos")
								@yield("notificaciones-activas")
								<!-- User Account: style can be found in dropdown.less -->
								<li class="dropdown user user-menu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<img id="useravatar" src="{{asset($usuario->user_avatar)}}" class="user-image" alt="User Image">
										<span class="hidden-xs">{{e($usuario->user_name)}}</span>
									</a>
									<ul class="dropdown-menu">
										<!-- User image -->
										<li class="user-header">
											<img id="usersubavatar" src="{{asset($usuario->user_avatar)}}" class="img-circle" alt="User Image">
											<p>
												{{e($usuario->user_name)}}
												<small>{{e($whichrole)}}</small>
											</p>
										</li>
										<!-- Menu Body -->
										<li class="user-body">
											<div class="row">
												<div class="col-xs-4 text-center">
													<a href="{{url('admin/mi_cuenta')}}"><i class="fa fa-user-circle"></i></a>
												</div>
												<div class="col-xs-4 text-center">
													<a href="{{url('admin/salir')}}"><i class="fa fa-sign-out"></i></a>
												</div>
												<div class="col-xs-4 text-center">
													<a href="{{url('admin/bloquear')}}"><i class="fa fa-lock"></i></a>
												</div>
											</div>
											<!-- /.row -->
										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-left">
												<a href="#" class="btn btn-default btn-flat">AVATAR</a>
											</div>
											<div class="pull-right">
												<a href="{{url('admin/salir')}}" class="btn btn-default btn-flat">SALIR</a>
											</div>
										</li>
									</ul>
								</li>
									<!-- Control Sidebar Toggle Button -->
								<li><a href="{{url('/')}}" target="_blank"><i class="fa fa-link"></i></a></li>
								<li><a href="{{url('admin/bloquear')}}"><i class="fa fa-lock"></i></a></li>
							</ul>
						</div>
					</nav>
				</header>
				<!-- Header Navbar: style can be found in header.less -->
				<!-- Left side column. contains the sidebar -->
				<aside class="main-sidebar">
					<section class="sidebar">
						<div class="user-panel">
							<div class="pull-left image">
								<img id="sidebaravatar" src="{{asset($usuario->user_avatar)}}" class="img-circle" alt="User Image">
							</div>
							<div class="pull-left info">
								<p>{{e($usuario->user_name)}}</p>
								<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
							</div>
						</div>
						<ul class="sidebar-menu">
							<li class="treeview @yield('dashboard-active')">
								<a href="{{url('admin/dashboard')}}">
									<i class="fa fa-dashboard"></i> <span>Dashboard</span>
									<span class="pull-right-container"></span>
								</a>
							</li>
							<li class="header title_sidebar">Usuarios</li>
							<li class="treeview @yield('cuenta-active')">
								<a href="{{url('admin/mi_cuenta')}}">
									<i class="fa fa-user-circle"></i><span>Mi Cuenta</span>
									<span class="pull-right-container"></span>
								</a>
							</li>
							@if($usuario->user_role==0 || $usuario->user_role==1)
								<li class="treeview @yield('users-active')">
									<a href="#">
										<i class="fa fa-users"></i><span>Usuarios</span>
										<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
									</a>
									<ul class="treeview-menu">
										<li><a href="{{url('admin/crear_usuario')}}"><i class="fa fa-circle-o"></i> Registrar</a></li>
										<li><a href="{{url('admin/listar_usuario')}}"><i class="fa fa-circle-o"></i> Listar</a></li>
									</ul>
								</li>
							@endif
							<li class="header title_sidebar">Home</li>
							<li class="treeview @yield('galeria-active')">
								<a href="{{url('admin/administrargaleria')}}">
									<i class="fa fa-home"></i> <span>Administrar</span>
								</a>
							</li>
							<li class="treeview @yield('promociones-active')">
								<a href="#">
									<i class="fa fa-tags "></i> <span>Promociones</span>
									<span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
									</span>
								</a>
								<ul class="treeview-menu">
									<li><a href="{{url('admin/crear_promociones')}}"><i class="fa fa-circle-o"></i> Registrar</a></li>
									<li><a href="{{url('admin/listar_promocion')}}"><i class="fa fa-circle-o"></i> Historial</a></li>
								</ul>
							</li>
							@if($usuario->user_role==0 || $usuario->user_role==1)
								<li class="treeview @yield('eventos-active')">
									<a href="#">
										<i class="fa fa-calendar-o"></i> <span>Eventos</span>
										<span class="pull-right-container">
											<i class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
									<ul class="treeview-menu">
										<li><a href="{{url('admin/crear_eventos')}}"><i class="fa fa-circle-o"></i> Registrar</a></li>
										<li><a href="{{url('admin/listar_meseseventos')}}"><i class="fa fa-circle-o"></i> Lanzar Mes</a></li>
										<li><a href="{{url('admin/listar_eventos')}}"><i class="fa fa-circle-o"></i> Eventos Promocionales</a></li>
										<li><a href="{{url('admin/listar_corporativos')}}"><i class="fa fa-circle-o"></i> Eventos Corporativos</a></li>
									</ul>
								</li>
							@endif
							<li class="header title_sidebar">El Hotel</li>
							@if($usuario->user_role==0 || $usuario->user_role==1)
								<li class="treeview @yield('elhotel-active')">
									<a href="#">
										<i class="fa fa-file-picture-o"></i><span>Imágenes</span>
										<span class="pull-right-container">
											<i class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
									<ul class="treeview-menu">
											<li><a href="{{url('admin/crear_fotoprincipalhotel')}}"><i class="fa fa-circle-o"></i> Foto Principal</a></li>
											<li><a href="{{url('admin/crear_instalaciones')}}"><i class="fa fa-circle-o"></i> Instalaciones</a></li>
											<li><a href="{{url('admin/crear_proyectos')}}"><i class="fa fa-circle-o"></i> Proyectos</a></li>
									</ul>
								</li>
							@endif
							<li class="treeview @yield('elhotelreal-active')">
								<a href="#">
									<i class="fa fa-building"></i> <span>El Hotel</span>
									<span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
									</span>
								</a>
								<ul class="treeview-menu">
									@if($usuario->user_role==0 || $usuario->user_role==1)
										<li><a href="{{url('admin/crear_hotel')}}"><i class="fa fa-circle-o"></i> El Hotel</a></li>
										<li><a href="{{url('admin/crear_eventoscorporativos')}}"><i class="fa fa-circle-o"></i> Eventos Corporativos</a></li>
									@endif
									<li><a href="{{url('admin/crear_comerciales')}}"><i class="fa fa-circle-o"></i> Comerciales</a></li>
								</ul>
							</li>
							<li class="header title_sidebar">Habitaciones</li>
							@if($usuario->user_role==0 || $usuario->user_role==1)
								<li class="treeview @yield('habitaciones-active')">
									<a href="#">
										<i class="fa fa-file-picture-o"></i><span>Imágenes</span>
										<span class="pull-right-container">
											<i class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
									<ul class="treeview-menu">
											<li><a href="{{url('admin/crear_fotoprincipalroom')}}"><i class="fa fa-circle-o"></i> Foto Principal</a></li>
											<li><a href="{{url('admin/crear_fotosecundariaroom')}}"><i class="fa fa-circle-o"></i> Foto Secundaria</a></li>
											<li><a href="{{url('admin/crear_galeriaroom')}}"><i class="fa fa-circle-o"></i> Galeria</a></li>
									</ul>
								</li>
							@endif
							<li class="treeview @yield('cuartos-active')">
								<a href="#">
									<i class="fa fa-bed"></i> <span>Habitaciones</span>
									<span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
									</span>
								</a>
								<ul class="treeview-menu">
									<li><a href="{{url('admin/crear_habitacion')}}"><i class="fa fa-circle-o"></i> Registrar</a></li>
									<li><a href="{{url('admin/listar_habitacion')}}"><i class="fa fa-circle-o"></i> Mis Habitaciones</a></li>
								</ul>
							</li>
							<li class="header title_sidebar">Restaurante</li>
							<li class="treeview @yield('restaurantes-active')">
								<a href="#">
									<i class="fa fa-cutlery"></i> <span>Restaurante</span>
									<span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
									</span>
								</a>
								<ul class="treeview-menu">
									<li><a href="{{url('admin/listar_restaurantes')}}"><i class="fa fa-circle-o"></i> Mis Restaurantes</a></li>
									<li><a href="{{url('admin/crear_galeriahotel')}}"><i class="fa fa-circle-o"></i> Galeria</a></li>
									<li><a href="{{url('admin/listar_restfija')}}"><i class="fa fa-circle-o"></i> Formulario Imagen</a></li>
								</ul>
							</li>
						</ul>
					</section>
				</aside>
				<div class="content-wrapper">
					<!-- Content Header (Page header) -->
					<section class="content-header">
						<h1>
							@yield("title_section")
						</h1>
						<ol class="breadcrumb">
							<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
							<li class="active">@yield("sub_title")</li>
						</ol>
					</section>
					<!-- Main content -->
					<section class="content">
						<!-- Default box -->
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">@yield("title-divsection")</h3>
							</div>
							<div id="row_edit" class="box-body">
								@yield("body-section")
							</div>
							<div class="box-body">
								@yield("body-estadisticas")
							</div>
							<div class="box-body">
								@yield("body-resumenes")
							</div>
						</div>
					</section>
				</div>
				<footer class="main-footer">
					<div class="pull-right hidden-xs">
						<b>Version</b> 0.1.5
					</div>
					<strong>Copyright &copy; 2017 | Powered By <a href="#">Hacienda Creativa C.A</a>.</strong>
				</footer>
			</div>
			<!-- jQuery 2.2.3 -->
			<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
			<!-- Bootstrap 3.3.7 -->
			<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
			<!-- SlimScroll -->
			<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
			<!-- FastClick -->
			<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
			<script src="{{asset('plugins/jQuery.filer-1.3.0/js/jquery.filer.min.js')}}" type="text/javascript"></script>
			<script src="{{asset('plugins/fileuploader-1.0.0.4/src/jquery.fileuploader.min.js')}}" type="text/javascript"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/media/js/jquery.dataTables.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/media/js/dataTables.bootstrap.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.bootstrap.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.print.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.flash.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
			<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
			<script src="{{asset('plugins/flatpickr-master/dist/flatpickr.js')}}"></script>
			<script src="{{asset('plugins/flatpickr-master/dist/es.js')}}"></script>
			<script src="{{asset('plugins/summernote-master/dist/summernote.min.js')}}"></script>
			<script src="{{asset('js/summernote-es-ES.js')}}"></script>
			<!-- AdminLTE App -->
			<script src="{{asset('plugins/chartjs/Chart.min.js')}}"></script>
			<script src="{{asset('dist/js/app.min.js')}}"></script>
			<script src="{{asset('js/validaciones.js')}}"></script>
			<script src="{{asset('plugins/maskphone/maskphone.js')}}" type="text/javascript"></script>
			@yield("scripts")
			@include("modales.avatar.alerta_avatar")
			@yield("modales")
		</body>
	</html>
