<!DOCTYPE HTML> 
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="shortcut icon" type="image/png" href="{{asset('img/fav.png')}}"/>
		<!-- Bootstrap -->
		<link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="{{asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">
		<!-- flatpickr CSS -->
		<link href="{{asset('plugins/flatpickr-master/dist/flatpickr.min.css')}}" rel="stylesheet">
		<!-- CUSTOM CSS -->
		<link rel="stylesheet" href="{{asset('css/booking.css')}}">
		<link rel="stylesheet" href="{{asset('css/master.css')}}">
		<link rel="stylesheet" href="{{asset('css/masterpalmasinn.css')}}">
		@yield("Mycss")
		<title>Hotel Las Palmas Inn</title>
	</head>
	<body>
		<div class="container-palmasinn"><!-- Continer -->
			<div id="lhc_chatbox_container">
				<a id="chatbox-icon" class="status-icon" target="_blank" href="https://web.whatsapp.com/send?phone=584146994100&text=%C2%A1Hola!%20Quisiera%20hablar%20con%20un%20asesor%20de%20reservaciones,%20lo%20he%20contactado%20desde%20el%20enlace%20💻%20web.%20"></a>
			</div>
			@section('navbar-palmasinn')
				@include("frontend.components.navbar")
			@show<!-- navbar-palmasinn -->
			@section('booking-palmasinn')
				@include("frontend.components.booking")
			@show<!-- booking-palmasinn -->
			<div class="row"><!-- body-page -->
				@yield("baner-page")
				<!-- Boton Reserva -->
				<div id="BotonRes" class="">
					<div class="shadown">
						<div id="keys" class="background_img"></div>
						<a href="{{url('booking')}}">{{Lang::get("home.reservaonline")}}</a>
					</div>
				</div>
				<!-- Boton whatsapp -->
				<div id="Botonwhat" class="">
					<div class="shadown">
						<div id="what" class="background_img"></div>
						<a id="icon-w1" target="_blank" href="">{{Lang::get("home.whatsapponline")}}</a>
					</div>
				</div>
				@yield("body-page")
				<!--div id="lhc_status_container_page" ></div-->
			</div>
		</div>
		@section('footer-palmasinn')
			@include("frontend.components.footer")
		@show<!-- footer-palmasinn -->
		<!-- Modal -->
		<div class="modal fade" id="modal_load" role="dialog" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header flex-center reverserow">
						<b class="close" data-dismiss="modal"><i class="fa fa-times-circle close-modal" aria-hidden="true"></i></b>
						<img src="{{url('img/logolaspalmas_mobile.png')}}" class="img-responsive center" width="128" height="64"/>
					</div>
					<div class="modal-body">
						<div class="row center">
							<div class="col-xs-12 col-md-12">
								<div class="row m-0 d-flex">
									<div class="col-xs-12 col-sm-6 d-center">
										<div class="row m-0">
											<div class="col-xs-12">
												<p class="tituloitalic text-center" id="nombre">{{Lang::get("home.welcome")}}</p>
											</div>
											<div class="col-xs-12">
												<div class="media">
													<div class="media-left media-middle">
														<a href="tel:+582699620000">
															<img class="media-object" src="{{url('img/phone-call.svg')}}" >
														</a>
													</div>
													<div class="media-body">
														<h4 class="media-heading">MASTER</h4>
														<h4><a href="tel:+582699620000">+58 269 962 0000</a><br/><a href="tel:+582694204163">+58 269 420 4163</a></h4>
													</div>
												</div>
												<div class="media">
													<div class="media-left media-middle">
														<a href="tel:+584146994100">
															<img class="media-object" src="{{url('img/smartphone.svg')}}" >
														</a>
													</div>
													<div class="media-body">
														<h4 class="media-heading">MOVIL</h4>
														<h4><a href="tel:+584146994100">+58 414 699 4100</a></h4>
													</div>
												</div>
												<div class="media">
													<div class="media-left media-middle">
														<a id="icon-w2" target="_blacnk" href="https://web.whatsapp.com/send?phone=584146994100&text=%C2%A1Hola!%20Quisiera%20hablar%20con%20un%20asesor%20de%20reservaciones,%20lo%20he%20contactado%20desde%20el%20enlace%20💻%20web.%20">
															<img class="media-object" src="{{url('img/whatspegajoso.svg')}}" >
														</a>
													</div>
													<div class="media-body">
														<h4 class="media-heading">WHATSAPP</h4>
														<h4><a target="_blacnk" id="icon-w3"  href="https://web.whatsapp.com/send?phone=584146994100&text=%C2%A1Hola!%20Quisiera%20hablar%20con%20un%20asesor%20de%20reservaciones,%20lo%20he%20contactado%20desde%20el%20enlace%20💻%20web.%20">+58 414 699 4100</a></h4>
													</div>
												</div>
												<div class="media">
													<div class="media-left media-middle">
														<a target="_blacnk" href="https://www.google.co.ve/maps/place/Hotel+Las+Palmas+Inn/@11.7059306,-70.1895368,17z/data=!3m1!4b1!4m5!3m4!1s0x8e85ecdea826b53d:0x5d0f204fa01b2743!8m2!3d11.7059306!4d-70.1873481">
															<img class="media-object" src="{{url('img/ubicacionheader_over.svg')}}" >
														</a>
													</div>
													<div class="media-body">
														<h4 class="media-heading"><a href="https://www.google.co.ve/maps/place/Hotel+Las+Palmas+Inn/@11.7059306,-70.1895368,17z/data=!3m1!4b1!4m5!3m4!1s0x8e85ecdea826b53d:0x5d0f204fa01b2743!8m2!3d11.7059306!4d-70.1873481">{{Lang::get("home.mubicacion")}}</a></h4>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 d-xs-none modal-img">
										<div>	
											<img src="{{url('img/aviso.jpg')}}"/>
										</div>									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</body>
	<!-- jQuery 3.1.1 -->
	<script src="{{asset('plugins/jQuery/jquery-3.1.1.min.js')}}"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	<!-- flatpickr -->
	<script src="{{asset('plugins/flatpickr-master/dist/flatpickr.js')}}"></script>
	<script src="{{asset('plugins/flatpickr-master/dist/es.js')}}"></script>
	<script type="text/javascript">
		$('#keys').on('click',function(){
			window.location.href = "{{url('booking')}}";
		});
	</script>
	
	@yield("modales")
	@yield("Myscript")
<script>
jQuery(function ($) {
	
	$(document).ready(function (){
		var modal_load = sessionStorage.getItem('load');
		if(!modal_load){
			$('#modal_load').modal('show');
			sessionStorage.setItem('load', '1');
		}
	});

	var height;
	var maxH = 640;
    var winH = $(window).height();
    $(window).on("scroll", function () {
		if(maxH > winH){
			height = maxH;
		}else{
			height = winH;
		}
		if(450 > $(window).width()){
			height = 320;
		}
        if($(this).scrollTop() > height ){
			$('#BotonRes').addClass('fixed');
        }else{
            $('#BotonRes').removeClass('fixed');
		}
    }).on("resize", function(){
       winH = $(this).height();
    });	
	//booking	
	$("#inicio_").flatpickr({
		minDate: "today",
		onChange:function(selectedDates, dateStr, instance){
			$("#fin_").flatpickr({
				minDate: dateStr,
			});
		}
	});	
	$("#fin_").flatpickr({
		minDate: "today",
		onChange:function(selectedDates, dateStr, instance){
			$("#inicio_").flatpickr({
				minDate: dateStr,
			});
		}			
	});
	
	$("#form_bookingpalmasin").submit(function( event ) {
		//se puede incluir validaciones de num,ero de personas conrespecto a habitaciones si es que pasan esa info 
		if (($("input[type='text']").val().length != 0)&&($("#habitacion").val().length != 0)&&($("#adulto").val().length != 0)) {
			event.preventDefault();

		// DATOS CAPTURADOS DE LA RESERVACION
		ini    = $('#inicio_').val();
        out    = $('#fin_').val();
        hab    = $('select[name=habt]').val();
        adult  = $('select[name=adult]').val();
        minor  = $('select[name=minor]').val();
        infant = $('select[name=infant]').val();
		//FIN DATOS CAPTURADOS DE LA RESERVACION
		datos = "?fechain="+ini+"&fechaout="+out+"&hab="+hab+"&adult="+adult+"&minor="+minor+"&infant="+infant;
		document.cookie = "booking="+encodeURIComponent(datos); +"max-age=3600; path=/";

		location.href = ('booking');

		}else{
			alert('por favor introduzca una fecha valida, con minimo 1 habitacion y 1 adulto');
		}
		event.preventDefault();
	});	

	if( !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ) {//nomovil
		$('#chatbox-icon, #icon-w1, #icon-w2, #icon-w3').attr("href", "https://web.whatsapp.com/send?phone=584146994100&text=%C2%A1Hola!%20Quisiera%20hablar%20con%20un%20asesor%20de%20reservaciones,%20lo%20he%20contactado%20desde%20el%20enlace%20💻%20web.%20");
	}else{//movil
		$('#chatbox-icon, #icon-w1, #icon-w2, #icon-w3').attr("href", "https://wa.me/584146994100?text=%C2%A1Hola!%20Quisiera%20hablar%20con%20un%20asesor%20de%20reservaciones,%20lo%20he%20contactado%20desde%20el%20enlace%20movil📱.%20");
	}
	
});
</script>	
</html>