 <div id="bookingpalmasin">
	<form id="form_bookingpalmasin">
	  <div id="bookingin" class="">
		<div class="form-checkinout">
		  <div class="input-group">
			<div class="input-group-addon calendar"></div>
			<input type="text" class="form-control bpalmas" id="inicio_" placeholder="Check in">
		  </div>
		</div>
		<div class="form-checkinout">
		  <div class="input-group">
			<div class="input-group-addon calendar"></div>
			<input type="text" class="form-control bpalmas" id="fin_" placeholder="Check out">
		  </div>
		</div>
		<div class="form-select"><!--sel-habi-->
		  <div class="input-group">
			<div class="input-group-addon habitad"></div>
			  <select class="custom-select bpalmas" name=habt id="habitacion">
				<option value="" selected>Hab.</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="3">4</option>
				<option value="3">5</option>
			  </select>
		  </div>
		</div>
		<div class="form-select">
		  <div class="input-group">
			<div class="input-group-addon adult"></div>
			  <select class="custom-select bpalmas" name=adult id="adulto">
				<option value="0" selected>{{Lang::get("home.adulto")}}.</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="3">4</option>
				<option value="3">5</option>
			  </select>
		  </div>
		</div>
		<div class="form-select">
		  <div class="input-group">
			<div class="input-group-addon kid"></div>
			  <select class="custom-select bpalmas" name=minor id="menor">
				<option value="0"selected>{{Lang::get("home.minor")}} (+3).</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="3">4</option>
				<option value="3">5</option>
			  </select>
		  </div>
		</div>
		<div class="form-select">
		  <div class="input-group">
			<div class="input-group-addon baby"></div>
			  <select class="custom-select bpalmas" name=infant id="infante">
				<option value="0"selected>{{Lang::get("home.infante")}} (-3).</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="3">4</option>
				<option value="3">5</option>
			  </select>
		  </div>
		</div>&nbsp;
		<div class="form-btnb">
			<button type="submit" class="btn btn-buscar" >{{Lang::get("home.buscar")}}</button>
			<img src="{{url('img/logovisa.svg')}}">
			<img src="{{url('img/logomaster.svg')}}">
		</div>	
	  </div>
	</form>
</div>