<div id="movilnavbarpalmasin">
	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
			<a class="navbar-brand" href="{{url('/')}}"></a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#movilNavbar">
			</button>
			</div>
			<div id="headerbtns">
				<a target="_blacnk" href="https://wa.me/584146994100?text=%C2%A1Hola!%20Quisiera%20hablar%20con%20un%20asesor%20de%20reservaciones,%20lo%20he%20contactado%20desde%20el%20enlace%20movil📱.%20"></a>
				<a target="_blacnk" href="https://www.facebook.com/laspalmsinn/"></a>
				<a target="_blacnk" href="https://www.instagram.com/laspalmasinn/"></a>
				<a target="_blacnk" href="https://twitter.com/laspalmasinn"></a>
				<a target="_blacnk" href="https://www.tripadvisor.com.ve/Hotel_Review-g316095-d1899060-Reviews-Hotel_Las_Palmas_Inn-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html"></a>
				<a target="_blacnk" href="https://www.google.co.ve/maps/place/Hotel+Las+Palmas+Inn/@11.7059306,-70.1895368,17z/data=!3m1!4b1!4m5!3m4!1s0x8e85ecdea826b53d:0x5d0f204fa01b2743!8m2!3d11.7059306!4d-70.1873481"></a>
			</div>
			<div class="collapse navbar-collapse" id="movilNavbar">
				<ul class="nav navbar-nav">
					<li><a href="{{url('/')}}" class="@yield('home-active')">HOME</a></li>
					<li><a href="{{url('elhotel')}}"  class="@yield('elhotel-active')">{{Lang::get("home.hotel")}}</a></li>
					<li><a href="{{url('habitaciones')}}"  class="@yield('habitaciones-active')">{{Lang::get('home.habitacionesnav')}}</a></li>
					<li><a href="{{url('restaurantes')}}"  class="@yield('restaurantes-active')">{{Lang::get("home.restaurantenav")}}</a></li>
					<li><a href="{{e($res['promociones'])}}" class="btn-scroll">PROMOCIONES</a></li>
					<li><a href="{{e($res['eventos'])}}" class="btn-scroll">{{Lang::get("home.eventosnav")}}</a></li>
				</ul>
			</div>
			<div id="movilreserva">
			<a href="{{url('booking')}}">{{Lang::get("home.reserva")}}</a>
			</div>
		</div>
	</nav> 
</div>
<div id="navbarpalmasin">
	<div>
		<ul class="list-inline">
			@foreach (Config::get('languages') as $lang => $language)
				@if(App::getLocale()=="es" && $language=="Spanish")
					<li class="active" style="margin-left:11px !important;height:36px;border:none;">
						<a href="{{ route('lang.switch', $lang) }}" class="esp"></a>
					</li>
					<li class="active" style="margin-left:11px !important;height:36px;border:none;">
						<a href="{{ route('lang.switch', 'en') }}" class="en"></a>
					</li>
				@elseif(App::getLocale()=="en" && $language=="English")
					<li class="active" style="margin-left:11px !important;height:36px;border:none;">
						<a href="{{ route('lang.switch', 'es') }}" class="esp"></a>
					</li>
					<li class="active" style="margin-left:11px !important;height:36px;border:none;">
						<a href="{{ route('lang.switch', $lang) }}" class="en"></a>
					</li>
				@else
					<!--<li class="active" style="margin-left:11px !important;height:36px;border:none;">
						<a href="{{ route('lang.switch', $lang) }}" class="en"></a>
					</li>
					<li style="margin-left:11px !important;height:36px;border:none;">
						<a href="{{ route('lang.switch', $lang) }}" class="esp"></a>
					</li>-->
				@endif
			@endforeach
		</ul>
		<ul class="list-inline">
			<li><a target="_blacnk" href="https://www.tripadvisor.com.ve/Hotel_Review-g316095-d1899060-Reviews-Hotel_Las_Palmas_Inn-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html" class="tripavisor"></a></li>
			<li><a target="_blacnk" href="https://www.facebook.com/laspalmsinn/" class="facebook"></a></li>
			<li><a target="_blacnk" href="https://www.instagram.com/laspalmasinn/" class="instagram"></a></li>
			<li><a target="_blacnk" href="https://twitter.com/laspalmasinn" class="twitter"></a></li>
		</ul>
		<label class="tlf" style="display: flex;"><i class="whatsapp"><a target="_blank" href="https://web.whatsapp.com/send?phone=584146994100&text=%C2%A1Hola!%20Quisiera%20hablar%20con%20un%20asesor%20de%20reservaciones,%20lo%20he%20contactado%20desde%20el%20enlace%20💻%20web.%20"></a></i>+58 <a href="tel:04146994100">414 699 4100</a></label>
		<label class="tlf" >{{Lang::get("home.phone")}}:+58 <a href="tel:2699620000">269 962 0000</a></label>
		<a target="_blacnk" href="https://www.google.co.ve/maps/place/Hotel+Las+Palmas+Inn/@11.7059306,-70.1895368,17z/data=!3m1!4b1!4m5!3m4!1s0x8e85ecdea826b53d:0x5d0f204fa01b2743!8m2!3d11.7059306!4d-70.1873481" class="ubicator"></a>
	</div>
</div>
<nav id="navbarpalmasinn" class="navbar" role="navigation">
	<div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarpalmasinn-collapse">
				<i class="fa fa-bars fa-2x" aria-hidden="true"></i>
			</button>
			<a href="{{url('/')}}" style="display:block;"><div id="logopalmasinn"></div></a>
		</div>
		<div id="navbarpalmasinn-collapse" class="collapse navbar-collapse">
			<form class="navbar-form navbar-right" role="reservar" action="booking">
				<button type="submit" class="btn btn-reserva">{{Lang::get("home.reserva")}}</button>
			</form>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('/')}}" class="@yield('home-active')">HOME</a></li>
				<li><a href="{{url('elhotel')}}"  class="@yield('elhotel-active')">{{Lang::get("home.hotel")}}</a></li>
				<li><a href="{{url('habitaciones')}}"  class="@yield('habitaciones-active')">{{Lang::get('home.habitacionesnav')}}</a></li>
				<li><a href="{{url('restaurantes')}}"  class="@yield('restaurantes-active')">{{Lang::get("home.restaurantenav")}}</a></li>
				<li><a href="{{e($res['promociones'])}}" class="btn-scroll">PROMOCIONES</a></li>
				<li><a href="{{e($res['eventos'])}}" class="btn-scroll">{{Lang::get("home.eventosnav")}}</a></li>
			</ul>
		</div>
	</div>
</nav>