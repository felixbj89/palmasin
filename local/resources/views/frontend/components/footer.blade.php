<div id="footerpalmasinn">
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<div class="row">
				<div class="col-xs-3 background_img" style="background-image: url({{asset('img/footer/logo.png')}});"></div>
				<div class="col-xs-3 background_img" style="background-image: url({{asset('img/footer/Habanerosblanco.png')}});"></div>
				<div class="col-xs-3 background_img" style="background-image: url({{asset('img/footer/maracuyablanco.png')}});"></div>
				<div class="col-xs-3 background_img" style="background-image: url({{asset('img/footer/lobbybarblanco.png')}});"></div>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<div class="row">
				<ul class="list-inline mapasitio pull-left">
					<li><a href="#mapasitio" data-toggle="collapse">{{e(Lang::get('home.mapa'))}}</a></li>
					<li><a href="#">{{e(Lang::get('home.condiciones'))}}</a></li>
				</ul>
				<ul class="list-inline socialfooter pull-right">
					<li><a href="https://www.facebook.com/laspalmsinn/" class="facebook"></a></li>
					<li><a href="https://www.instagram.com/laspalmasinn/" class="instagram"></a></li>
					<li><a href="https://twitter.com/laspalmasinn" class="twitter"></a></li>
					<li><a href="https://www.tripadvisor.com.ve/Hotel_Review-g316095-d1899060-Reviews-Hotel_Las_Palmas_Inn-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html" class="tripavisor"></a></li>
					<li><a href="#" class="googlemaps"></a></li>
				</ul>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-md-offset-6 collapse" id="mapasitio">
			<ul class="col-xs-12 col-sm-3">
				<li class="mp-titulo">Home</li>
				<li><a href="{{url('habitaciones')}}">Habitaciones</a></li>
				<li><a href="{{url('elhotel/instalaciones')}}">Instalaciones</a></li>
				<li><a href="{{url('restaurantes')}}">Restaurantes</a></li>
				<li><a href="{{$res['turismo']}}" class="btn-scroll">Turismo de Salud</a></li>
				<li><a href="{{e($res['promociones'])}}">Promociones</a></li>
				<li><a href="{{e($res['eventos'])}}">Eventos</a></li>
				<li><a href="{{url('booking')}}">Reservar</a></li>
			</ul>
			<ul class="col-xs-12 col-sm-3">
				<li class="mp-titulo">El Hotel</li>
				<li><a href="{{url('elhotel')}}">El Hotel</a></li>
				<li><a href="{{url('elhotel/instalaciones')}}">Instalaciones</a></li>
				<li><a href="{{url('elhotel/eventoscorporativos')}}">Eventos Corporativos</a></li>
				<li><a href="{{url('elhotel/proyectos')}}">Proyectos</a></li>
				<li><a href="{{url('elhotel/comerciales')}}">Comerciales</a></li>
				<li><a href="{{url('elhotel/centroempresarial')}}">Centro Empresarial</a></li>
				<li><a href="{{url('elhotel/ubicacion')}}">Ubicación</a></li>
				<li><a href="{{url('elhotel/contacto')}}">Contacto</a></li>
			</ul>
			<ul class="col-xs-12 col-sm-3">
				<li class="mp-titulo">Habitaciones</li>
				{!!$listahabitaciones!!}
				<li><a href="{{url('habitaciones/banner_restaurates')}}">Restaurantes</a></li>
			</ul>
			<ul class="col-xs-12 col-sm-3">
				<li class="mp-titulo">Restaurantes</li>
				<li><a href="{{url('restaurantes/habneros')}}">Habaneros</a></li>
				<li><a href="{{url('restaurantes/maracuya')}}">Maracuyá</a></li>
				<li><a href="{{url('restaurantes/lobbybar')}}">Lobbybar</a></li>
				<li><a href="{{url('restaurantes/contacto')}}">Contacto Restaurante</a></li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<p>Powered by <a href="http://haciendacreativa.com.ve/" target="_blank">HACIENDA CREATIVA C.A</a></p>
			<p>Todos los derechos reservados</p>
		</div>
	</div>
</div>