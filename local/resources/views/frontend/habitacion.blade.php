@extends("layout.master")
@section('Mycss')
<link rel="stylesheet" href="{{asset('css/habitacionespalmasinn.css')}}">
<link href="{{ asset('plugins/light-gallery/css/lightgallery.min.css') }}" rel="stylesheet">
@endsection
@section('navbar-palmasinn')
	@parent
@endsection
@section('habitaciones-active')
	activo
@endsection
@section('baner-page')
	<div class="full-height banner_palmasinn background_img" style="background-image:url({{url($res['banner'])}});"></div>
@endsection
@section('body-page')
	<div id="gridhabitacion" class="row">
		<div class="col-xs-11 col-md-10">
			<div class="row" style="margin:0;">
				<div class="col-xs-12 col-sm-10 col-md-5">
					<div class="row">
						<div>
							<div class="col-xs-12">
								<p class="tituloitalic" id="nombre">{{e($res['habitacion']['nombre'])}}</p>
							</div>
							<div class="col-xs-12">
								<div id="descripcion">
									{!!$res['habitacion']['descripcion']!!}
								</div>
								<div id="detalles">
									{!!$res['habitacion']['detalles']!!}
								</div>
							</div>
							<div id="comodidades" class="col-xs-12">
								<ul>
									{!!$res['habitacion']['comodidades']!!}
								</ul>
							</div>
							<div id="bth" class="col-xs-12">	
								<ul class="list-inline">
									<li><a href="javascript:vgaleria(0)" class="btn btn-light">{{Lang::get("home.boton1hotel")}}</a></li>
									<li><a href="{{url('booking')}}" class="btn btn-bold">{{Lang::get("home.boton2hotel")}}</a></li>
								</ul>
							</div>
							<div id="back" class="col-xs-12">
								<a href="{{url('habitaciones')}}" class="btn-back"><i></i>{{Lang::get("home.boton3hotel")}}</a>
							</div>
					</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-7">

					<div class="row" id="galeria">
						<div class="col-xs-12">
							<div id="galeriabi" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
								<div class="carousel-inner">
										<?php 
											$destacados =[];
										?>
									@foreach ($res['galeria'] as $i => $item)
										<div class="item @if($i == 0) active @endif">
											<img src="{{url($item['bannergaleri_rutaimg'])}}">
										</div>
										@if ($item['bannergaleri_destacar'] == '1')
											<?php 
												array_push($destacados, [$i,$item['bannergaleri_rutaimg']]);
											?>
										@endif
									@endforeach
								</div>
								<div class="controls">
									<button class="background_img galchang_up" style="background-image:url({{asset('img/habitaciones/arriba.svg')}});"></button>
									<button  class="background_img galchang_bown" style="background-image:url({{asset('img/habitaciones/abajo.svg')}});"></button>
								</div>
							</div>
						</div>
					</div>
					<div class="row" id="destacados">
						<div class="col-xs-12 col-sm-4">
							@if (isset($destacados[0]))
								<a href="javascript:vgaleria('{{$destacados[0][0]}}')" style="background-image:url({{url($destacados[0][1])}});"></a>
							@else
								<a href="javascript:void('0')"></a>
							@endif
						</div>
						<div class="col-xs-12 col-sm-4">
							@if (isset($destacados[1]))
								<a href="javascript:vgaleria('{{$destacados[1][0]}}')" style="background-image:url({{url($destacados[1][1])}});"></a>
							@elseif (isset($destacados[0]))
								<a href="javascript:vgaleria('{{$destacados[0][0]}}')" style="background-image:url({{url($destacados[0][1])}});"></a>
							@else
								<a href="javascript:void('0')"></a>
							@endif
						</div>
						<div class="col-xs-12 col-sm-4">
							@if (isset($destacados[2]))
								<a href="javascript:vgaleria('{{$destacados[2][0]}}')" style="background-image:url({{url($destacados[2][1])}});"></a>
							@elseif (isset($destacados[0]))
								<a href="javascript:vgaleria('{{$destacados[0][0]}}')" style="background-image:url({{url($destacados[0][1])}});"></a>
							@else
								<a href="javascript:void('0')"></a>
							@endif
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>



<div id="thumbnials">
	@foreach ($res['galeria'] as $i => $item)
		<a id="child-{{e($i)}}"href="{{url($item['bannergaleri_rutaimg'])}}">
			<img src="{{url($item['bannergaleri_rutaimg'])}}" />
		</a>
	@endforeach
</div>
	  
	  

	<!-- Banners Restaurantes -->
	<div id="banner_restaurates" class="row" style="margin-left:0;margin-right:0;">
		<a href="{{url('restaurantes')}}" >{!!$res['bannerestaurat']!!}</a>
	</div>
@endsection
@section('Myscript')
<script src="{{ asset('plugins/light-gallery/js/lightgallery-all.js') }}"></script>
<!-- habitaciones -->
<script src="{{asset('js/habitacionespalmasinn.js')}}"></script>

<script>
	var target = "{{ e($res['id']) }}";
    $('#thumbnials').lightGallery({
        thumbnail:true,
        animateThumb: false,
        showThumbByDefault: false
	}); 
	function vgaleria(i){
		$('#child-'+i).click();
	}
</script>
@include("modales.modal_imagen")
@include("modales.galeriplamas")
@endsection