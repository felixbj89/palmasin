@extends("layout.master")
@section('Mycss')
<link rel="stylesheet" href="{{asset('css/habitacionespalmasinn.css')}}">
@endsection
@section('navbar-palmasinn')
	@parent
@endsection
@section('habitaciones-active')
	activo
@endsection
@section('baner-page')
	<div class="full-height banner_palmasinn background_img" style="background-image:url({{url($res['banner'])}});"></div>
@endsection
@section('body-page')
	<div id="gridhabitaciones" class="row">
		<div class="col-xs-11 col-md-10">
			<div>
				@foreach ($res['habitaciones'] as $item)
					<div class="item" style="background-image:url({{ asset($item['img']) }});">
						<div class="text-center w-100">
							<p>{{$item['nombre']}}</p>
							<a href="{{url("habitaciones/".base64_encode($item['habitacion']))}}">Ver Habitación</a>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

	<!-- Banners Restaurantes -->
	<div id="banner_restaurates" class="row" style="margin-left:0;margin-right:0;">
		<a href="{{url('restaurantes')}}" >{!!$res['bannerestaurat']!!}</a>
	</div>
@endsection
@section('Myscript')

<!-- habitaciones -->
<script src="{{asset('js/habitacionespalmasinn.js')}}"></script>

<script>
	var target = "{{ e($res['id']) }}";

</script>
@include("modales.modal_imagen")
@include("modales.galeriplamas")
@endsection