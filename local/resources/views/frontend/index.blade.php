@extends("layout.master")
@section('Mycss')
<link rel="stylesheet" href="{{asset('css/indexpalmasinn.css')}}">
<link href="{{asset('plugins/shadowbox-3.0.3/shadowbox.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('navbar-palmasinn')
	@parent
@endsection
@section('home-active')
	activo
@endsection
@section('baner-page')
<div class="full-height banner_palmasinn">
	<div id="Carousel_palmasinn" class="carousel slide carousel-fade" data-ride="carousel" data-interval="7000">
		<ol class="carousel-indicators"><!-- Indicators -->
		@if(strcmp($res['imets_ppal'], "") !== 0)
			{!!$res['indicators']!!}
		@endif
		</ol>
		<div class="carousel-inner background-banner"><!-- Slides --><!--falta btn-promo-->
		@if(strcmp($res['imets_ppal'], "") !== 0)
			{!!$res['imets_ppal']!!}
		@endif
		</div>
		<div class="controls">
			<a  class="background_img galchang_left" href="#Carousel_palmasinn"  data-slide="next"></a>
			<a class="background_img galchang_right" href="#Carousel_palmasinn"  data-slide="prev"></a>
		</div>		
	</div>
</div>
@endsection
@section('body-page')
<!-- Accesos Directos -->
<div id="accesososdirectos" class="row">
	<div class="col-xs-12 col-sm-10 col-sm-offset-1">
		<div class="row row-flex">
			<div id="palmasinnHabitaciones" class="col-xs-12 col-sm-4">
				<a href="{{url('habitaciones')}}" class="esp_ad background_img" style="background-image:url({{url('img/home_habitaciones.jpg')}});"><!--ruta img-->
					<div class="name_ad">
						<a href="{{url('habitaciones')}}" style="display:block;">{{Lang::get("home.habitaciones")}}</a><!--link-->
					</div>
				</a>
			</div>
			<div id="palmasinnInstalaciones" class="col-xs-12 col-sm-4">
				<a href="{{url('elhotel')}}" class="esp_ad background_img" style="background-image:url({{url('img/home_instalacones.jpg')}});"><!--ruta img-->
					<div class="name_ad">
						<a href="{{url('elhotel')}}" style="display:block;">{{Lang::get("home.instalaciones")}}</a><!--link-->
					</div>
				</a>
			</div>
			<div id="palmasinnRestaurantes" class="col-xs-12 col-sm-4">
				<a href="{{url('restaurantes')}}" class="esp_ad background_img" style="background-image:url({{url('img/home_restaurantes.jpg')}});"><!--ruta img-->
					<div class="name_ad">
						<a href="{{url('restaurantes')}}" style="display:block;">{{Lang::get("home.restauranten")}}</a><!--link-->
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
<!-- Servicios -->
<div id="servicios" class="row">
	<div class="col-xs-12 col-sm-10 col-sm-offset-1">
		<div class="row flex">
			<div class="col-sm-12 col-md-7">
				<p class="titulo">
					@if(App::getLocale()=="es")
						{{e($servicios->servicios_titulo)}}
					@else
						{!!Lang::get('home.titulo_welcome')!!}
					@endif
				</p>
				<p class="tituloitalic">
				
					@if(App::getLocale()=="es")
						{{e($servicios->servicios_subtitulo)}}
					@else
						{!!Lang::get('home.subtitulo_welcome')!!}
					@endif
				</p>
				<div class="parrafo">
					<table><tr><td>
					{!!Lang::get('home.welcome_1')!!}
					</br>
					{!!Lang::get('home.welcome_2')!!}
					
					<!--{!!$servicios->servicios_texto!!}-->
					</td></tr></table>
				</div>
				<div class="row">
					<a href="#" class="btn btn-reserva" style="margin-top:10px;">{{Lang::get("home.verhotel")}}</a>
					<a href="https://www.youtube.com/watch?v=n0QG1kN6P_o" class="btn btn-comerciales" style="margin-top:10px;">{{Lang::get("home.vercomerciales")}}</a>
				</div>
			</div>
			<div class="col-sm-12 col-md-5">
			<table><tr><td>
				<div class="row rowservi">
					<div class="col-xs-12 col-sm-6 itemservi">
						<div>
							<div class="icoservi" id="seguridad_ico"></div>
							<small>{{Lang::get("home.sistema")}}.</small>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 itemservi">
						<div>
							<div class="icoservi" id="comodidades_icon"></div>
							<small>{{Lang::get("home.comodidades")}}.</small>
						</div>
					</div>
				</div>
				<div class="row rowservi">
					<div class="col-xs-12 col-sm-6 itemservi">
						<div>
							<div class="icoservi" id="ubicacion_ico"></div>
							@if(App::getLocale()=="en")
							    <small>Easy access and comfortable location.</small>
							@else
							    <small>Fácil acceso y cómoda ubicación.</small>
							@endif
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 itemservi">
						<div>
							<div class="icoservi" id="roomservices_ico"></div>
							<small>{{Lang::get("home.atencion")}}.</small>	
						</div>
					</div>				
				</div>
			</td></tr></table>
			</div>
		</div>
	</div>
</div>
<!-- Turismo Salud -->
{!!$bloque_salud["bloque_salud"]!!}
<!-- Promociones -->
@if(strcmp($res['promos'], "") !== 0)
<div id="promociones" class="row">
	<table><tr><td>
		<div class="col-md-12 col-lg-10 col-lg-offset-1">
			<div class="row">
				<div id="carouselPromo" class="col-xs-12 carousel fdi-Carousel carousel-fade slide">
					<div class="row carousel-inner onebyone-carosel">
						{!!$res['promos']!!}	
					</div>
					<div class="ocultarpromo left">
						<a href="#carouselPromo" data-slide="prev"></a>
					</div>
					<div class="ocultarpromo right">
						<a href="#carouselPromo" data-slide="next"></a>
					</div>
				</div>
			</div>
		</div>
	</td></tr></table>
</div>
@endif
<!-- Eventos -->
@if(strcmp($res['meses'], "") !== 0)
<div id="eventos" class="row">
	<div class="col-xs-12">
		<p class="tituloitalic text-center">Nuestros eventos</p>
	</div>
	<div class="col-xs-12">
		<ul class="list-inline">
		@if(strcmp($res['meses'], "") !== 0)
			{!!$res['meses']!!}			
		@endif
		</ul>
	</div>	
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-7"><!-- mes seleccionado -->
				<ul class="list-unstyled" id="li_eventos">
<!-- item evento mes -->				
		@if(strcmp($res['li_eventos'], "") !== 0)
			{!!$res['li_eventos']!!}
		@endif
<!-- item evento mes -->
				</ul>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-5"><!-- fijos -->
				<div class="row">
					<div class="col-xs-12" id="eve_ppal">
<!-- principal mes seleccionado -->
		@if(strcmp($res['eve_ppal'], "") !== 0)
			{!!$res['eve_ppal']!!}
		@endif
<!-- principal mes seleccionado -->	
					</div>
					<div class="col-xs-6" id="cuadro1"><!-- fijo seleccionado-->
<!-- principal otro seleccionado -->
		@if(strcmp($res['cuadro1'], "") !== 0)
			{!!$res['cuadro1']!!}
		@endif						
<!-- principal otro seleccionado -->						
					</div>
					<div class="col-xs-6" id="cuadro2"><!-- fijo seleccionado-->
<!-- principal otro seleccionado -->
		@if(strcmp($res['cuadro2'], "") !== 0)
			{!!$res['cuadro2']!!}
		@endif						
<!-- principal otro seleccionado -->	
					</div>					
				</div>
			</div>
		</div>
		<div id="loading">
		
			<div>
				<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
				<label>Cargando...</label>
			</div>
		
		</div>
	</div>
</div>
@endif
<!-- Galeria -->
@if(strcmp($res['secundaria'], "") !== 0)
<div id="galeria" class="row">
	<div class="col-xs-12">
		<div>
			<div id="carouselGaleria" class="carousel fdi-Carousel carousel-fade slide">
				<div class="row carousel-inner onebyone-carosel">
					{!!$res['secundaria']!!}
				</div>
				<div class="controlgaleria left">
					<a href="#carouselGaleria" data-slide="prev"></a>
				</div>
				<div class="controlgaleria right">
					<a href="#carouselGaleria" data-slide="next"></a>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<div class="row">
	<br/>
</div>
@endif
@include("modales.modal_imagen")
@include("modales.promocionespalmas")
@include("modales.evetospromocionales")
@endsection
@section('Myscript')
<script src="{{asset('js/indexpalmasinn.js')}}"></script>
<script src="{{asset('plugins/MaskedInput/jquery.maskedinput.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/shadowbox-3.0.3/shadowbox.js')}}"></script>
<script type="text/javascripT">	Shadowbox.init({ language: "es", players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv'] });</script>
<script>
	var target = "{{e($res['id'])}}";
	var modal_load = "{{e($modal_load)}}";
	var ruta = "{{e($ruta)}}";
</script>
@endsection