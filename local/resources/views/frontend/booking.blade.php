@extends("layout.master")
@section('Mycss')
<link rel="stylesheet" href="{{asset('css/elhotelpalmasinn.css')}}">
<style>
#BotonRes{display:none!important}
#bookingpalmasin{
    display:none;
}
</style>
@endsection
@section('navbar-palmasinn')
    @parent
@endsection
@section('baner-page')
<div class="full-height banner_palmasinn background_img" style="background-image:url(img/booking.jpg);">
</div>
@endsection
@section('body-page')
<!-- Historia -->
<style>
    .width-booking{
        width: 1280px !important;
    }
</style>
<div id="historia" class="row fixed-contenedor width-booking">
    <div class="col-xs-12">
        <p class="tituloitalic text-center">Reserva Online</p>
    </div>
    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
        <div class="row">
            <div class="col-xs-12">
                <a id="regersar" href="{{url('/')}}" class="text-left">Regresar</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 fixed-booking">
                @if(!isset($_REQUEST['d']))
                   <iframe src="{{$url_booking}}" width="100%" height="800px" style="border:0"></iframe>
                @else
                   <?php
                        //codigo de seguridad. Novared lo provee
                        $secureKey = "697fc3746b5fa7c63df77cc87a756bbc";
                        if(isset($_REQUEST['d'])){
                          $datab64 = $_REQUEST['d'];
                          $chk = strtolower($_REQUEST['chk']);
                          $chkCalc = md5($secureKey.$datab64);

                          //Chequeamos que los datos no han sido modificados
                          if($chk == $chkCalc)
                          {
                            $json = base64_decode($datab64);
                            $data = json_decode($json, true);

                            //datos de la transaccion

                            if($data['cod'] == 0){//codigo de aprobacion. La transaccion es aprobado si este valor es 0
                              echo '<div class="text-center alert alert-success col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4" role="success">
                                        <span class="glyphicon glyphicon-check text-center" aria-hidden="true"></span>Transaccion Aprobada
                                    </div>';
                            }else{
                              echo '<div class="text-center alert alert-success col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4" role="success">
                                        <span class="glyphicon glyphicon-check text-center" aria-hidden="true"></span>Transaccion no Existosa
                                    </div>';
                            }

                            $reservData = $data['reserv'];
                            echo "<div class='row'>
                                    <p class=\"col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3\" style=\"padding-top:15px;font-size:14px;color:black\">
                                        Cliente: ".$reservData['clte']['nombre'].' '.$reservData['clte']['apellido'].'
                                    </p>
                                </div><br/>'; //nombre y apellido del cliente
                            echo "<div class=\"row\">
                                    <p style=\"margin-left:15px;padding-top:15px;font-size:14px;color:black\">
                                        <strong>
                                            VER COMPROBANTE <a href='".$data['rec']."'>Ver
                                            <span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span></a>
                                        </strong>
                                    </p></div>"; //URL del recibo
                            echo "<div class='row'>
                                    <div class='col-md-6'>
                                        <p style='font-size:14px;padding-top:15px;color:black' class='centra'>
                                            DETALLES DE LA RESERVACI&Oacute;N <span class='fa fa-search' aria-hidden='true'></span>
                                        </p>
                                        <p style='padding-left:20px;color:black' class='centra'>
                                            N&uacute;mero de la Reservaci&oacute;n <strong>'". $reservData['cod']."'</strong>
                                        </p><br/>
                                    </div>
                                    <div class='col-md-6'></div>
                                </div>";
                            //datos de la reservacion
                            echo "<div class='row'>
                                    <div class='col-md-6'>
                                        <p style='padding-left:20px;color:black' class='text-center'>
                                            Fecha de Entrada <strong>". $reservData['desde'].'</strong>
                                        </p>
                                    </div>
                                    <div class=\'col-md-6\'>
                                        <p style=\'padding-left:20px;color:black\' class=\'text-center\'>
                                            Fecha de Salida <strong>'.$reservData['hasta'].'</strong>
                                        </p>
                                    </div>
                                </div><br/>';

                            //detalles de la reservacion
                            $montoTotal = 0;

                            echo "<div class=\"row\">
                                    <div class=\"table-responsive\" style=\"padding-left:15px;padding-right:15px;color:black;\">
                                        <table class=\"table\" style=\"padding-top:15px;\">
                                            <thead><tr><th>COD HAB</th><th>NOMBRE</th><th>CNTD HAB</th><th>CNTD PERSONAS</th>".
                                            "<th>CNTD NI&Ntilde;OS</th><th>MONTO</th></tr></thead><tbody>";

                            foreach($reservData['det'] as $detalle)
                            {
                                echo "<tr><td>".$detalle['ref'].'</td>'; //codigo de habitacion
                                echo "<td>".$detalle['nom'].'</td>'; //nombre de habitacion
                                echo "<td>".$detalle['cant'].'</td>'; //cantidad de habitaciones rentadas
                                echo "<td>".$detalle['per'].'</td>'; //cantidad de personas para esta habitacion
                                echo "<td>".$detalle['nin'].'</td>'; //cantidad de niños para esta habitacion
                                echo '<td>'.$detalle['monto'].'</td></tr>';
                              $montoTotal += $detalle['monto']; //monto de la reservacion de la habitacion actual
                            }

                            echo
                              " </tbody></table>
                            </div></div>";
                            echo "</br>
                                <div class='row'>
                                    <div class='col-md-8'></div>
                                    <div class='col-md-4'>
                                        <p style='padding-top:15px;padding-bottom:15px;font-size:14px;color:black;text-right'>
                                            CONSUMO TOTAL: <strong>".$montoTotal."</strong>
                                        </p>
                                    </div>
                                </div></a>"; //URL del recibo
                          }
                          else
                          {
                            //error en el calculo de integridad de los datos
                            echo 'Error: datos invalidos';
                          }
                        }
                    ?>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
