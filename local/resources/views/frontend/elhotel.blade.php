@extends("layout.master")
@section('Mycss')
<link rel="stylesheet" href="{{asset('css/elhotelpalmasinn.css')}}">
@endsection
@section('navbar-palmasinn')
	@parent
@endsection
@section('elhotel-active')
	activo
@endsection
@section('baner-page')
<div class="full-height banner_palmasinn background_img" style="background-image:url({{url($res['banner'])}});">
	<a href="#" class="background_img"></a>
</div>
@endsection
@section('body-page')
<!-- Historia -->
<div id="historia" class="row">
	<div class="col-xs-12">
		<p class="tituloitalic text-center">{{Lang::get('elhotel.titulo_welcome')}}</p>
	</div>
	<div class="col-xs-12 col-sm-10 col-sm-offset-1">
		<p class="text-center">{{Lang::get('elhotel.subtitulo_hotel')}}</p>
	</div>
	<div class="col-xs-12 col-sm-10 col-sm-offset-1">
		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-6">
				<div>
					<div>
						<p class="tituloitalic text-center">
							@if(App::getLocale()=="es")
								{{e($elhotel->elhotel_title)}}
							@else
								{{Lang::get('elhotel.innertitle_hotel')}}
							@endif
						</p><!--MAX24 -->
						<div>
							<div class="contentparrafo">
							@if(App::getLocale()=="en")
									{!!Lang::get('elhotel.parrafo_hotel')!!}
								@else
									{!!$elhotel->elhotel_texto!!}
								@endif
							</div>
						</div>
					</div>
				</div>
				<div id="proyectos" class="background_img galeriahistoria_" style="background-image:url({{url($elhotel->elhotel_picone)}});">
					<div class="galeriahistoria_over">
						<div>
							<div>
								<p class="tituloitalic text-center">{{Lang::get("elhotel.proyectos")}}</p>
								<ul class="list-inline text-center">
									<li><a href='{{url($proyectos->proyectos_pdf)}}' class="btn fixed-btn" target="_blank">{{Lang::get("elhotel.info")}}</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-6">
				<div id="instalaciones" class="background_img galeriahistoria_" style="background-image:url({{url($elhotel->elhotel_pictwo)}});">
					<div class="galeriahistoria_over">
						<div>
							<div>
								<p class="tituloitalic text-center">{{Lang::get("home.instalaciones")}}</p>
								<ul class="list-inline text-center">
									@if($size_inst > 0)
									<li><button type="button" class="btn" id="galinstalaciones">{{Lang::get("elhotel.galeria")}}</button></li>
									@endif
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div id="eventoscorporativos" class="background_img galeriahistoria_" style="background-image:url({{url($elhotel->elhotel_picthrid)}});">
					<div class="galeriahistoria_over">
						<div>
							<div>
								<p class="tituloitalic text-center">{{Lang::get("elhotel.eventos")}}</p>
								<ul class="list-inline text-center">
									<li><button type="button" class="btn" data-toggle="modal" data-target="#modalEventospalmas">{{Lang::get("elhotel.info")}}</button></li>
									<li><a class="btn btn-scroll" href="#contacto">{{Lang::get("elhotel.reserva")}}</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Comerciales -->
<div id="comerciales" class="row">
	@if(App::getLocale()=="en")
		<div class="col-xs-12 col-sm-12 col-md-6 background_img" style="background-image:url({{url($res['comerciales']->comerciales_pictureone)}});">
			<div class="col-xs-12">
				<a href="{{e($res['comerciales']->comerciales_linkone)}}"class="btn-comerciales titulo text-center pull-right fixed-titlecomerciales">{{Lang::get("elhotel.enjoy")}}</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 background_img" style="background-image:url({{url($res['comerciales']->comerciales_picturetwo)}});">
			<div class="col-xs-12">
				<a href="{{e($res['comerciales']->comerciales_linktwo)}}"class="btn-comerciales titulo text-center pull-right fixed-titlecomerciales">{{Lang::get("elhotel.family")}}</a>
			</div>
		</div>
	@else
		<div class="col-xs-12 col-sm-12 col-md-6 background_img" style="background-image:url({{url($res['comerciales']->comerciales_pictureone)}});">
			<div class="col-xs-12">
				<a href="{{e($res['comerciales']->comerciales_linkone)}}"class="btn-comerciales titulo text-center pull-right fixed-titlecomerciales">{{e($res['comerciales']->comerciales_textoone)}}</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 background_img" style="background-image:url({{url($res['comerciales']->comerciales_picturetwo)}});">
			<div class="col-xs-12">
				<a href="{{e($res['comerciales']->comerciales_linktwo)}}"class="btn-comerciales titulo text-center pull-right fixed-titlecomerciales">{{e($res['comerciales']->comerciales_textotwo)}}</a>
			</div>
		</div>
	@endif
</div>
<!-- Galeria -->
<div id="galeria" class="row">
	<div class="col-xs-12" id="centroempresarial">
		<div>
			<div style="width:100%"> <!-- wrap @img width -->
			  <div id="carousel-example-generic" class="carousel fdi-Carousel carousel-fade slide" data-ride="carousel" data-interval="false">
				<!-- Wrapper for slides -->
				<div class="carousel-inner vertical onebyone-carosel" role="listbox">
				  <div class="item active">
					<a href='{{url($proyectos->proyectos_pdf)}}' target="_blank">
						<img src="{{url($proyectos->proyectos_picone)}}">
					</a>
				  </div>
				  <div class="item">
					<a href='{{url($proyectos->proyectos_pdf)}}' target="_blank">
						<img src="{{url($proyectos->proyectos_pictwo)}}">
					</a>
				  </div>
				  <div class="item">
					<a href='{{url($proyectos->proyectos_pdf)}}' target="_blank">
						<img src="{{url($proyectos->proyectos_picthrid)}}">
					</a>
				  </div>
				  <div class="item">
					<a href='{{url($proyectos->proyectos_pdf)}}' target="_blank">
						<img src="{{url($proyectos->proyectos_picfour)}}">
					</a>
				  </div>
				</div>
				<!-- Controls -->

				<div class="controls">
					<a class="background_img chang_right" href="#carousel-example-generic" data-slide="next"></a>
					<a class="background_img chang_left " href="#carousel-example-generic" data-slide="prev"></a>
				</div>

			  </div>
			</div>
		</div>
	</div>
</div>
<!-- ubicacion -->
<div id="ubicacion" class="row">
	<div class="col-xs-12 background_img" style="background-image:url({{url('/')}}/img/elhotel/ubicacion.png);">
		<div>
			<div>
				<a href="" class="background_img" style="background-image:url({{url('/')}}/img/elhotel/ubicacion.svg);"></a>
				<p class="text-center">Av. Principal Bella Vista, Punto Fijo, Península de Paraguaná, Edo. Falcón.</p>
				<p class="text-center">{{Lang::get('elhotel.phonecard')}}: +58 0414-6994100 / 0269-3914068</p>
			</div>
		</div>
	</div>
</div>
<!-- contacto -->
<div id="contacto" class="row">
	<div class="col-xs-12">
		<p class="tituloitalic text-center">{{Lang::get("elhotel.contacto")}}</p>
	</div>
	<div class="col-xs-10 col-xs-offset-1">
		<form class="row" id="contactoelhotel" name="contactoelhotel">
			<div id="eventos-radio" class="form-group col-xs-12">
				@if(count($corporativos) > 0)
					@foreach($corporativos AS $event)
						<label class="radio-inline">
							<input type="radio" name="everadio" id="{{e($event->evento)}}" value="{{e($event->nombre)}}" data-nombre="{{e($event->nombre)}}" data-texto="{{e($event->descripcion)}}">
							<i class="fa fa-square-o"></i>
							<i class="fa fa-square"></i>{{e($event->nombre)}}
						</label>
					@endforeach
				@else
					<label class="radio-inline">
						<input type="radio" name="everadio" id="Corporativo" value="Corporativo" data-texto="Evento default Corporativo">
						<i class="fa fa-square-o"></i>
						<i class="fa fa-square"></i>{{Lang::get('elhotel.coporativo')}}
					</label>
				@endif
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<input type="text" class="form-control inputpalmasinn" name="nombre" id="nombre" placeholder="{{Lang::get('elhotel.nombre')}}">
				<span class="text-danger pull-right requerido">El nombre es requerido</span>
			</div>
			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<input type="text" class="form-control inputpalmasinn" name="apellido" id="apellido" placeholder="{{Lang::get('elhotel.apellido')}}">
				<span class="text-danger pull-right requerido">El apellido es requerido</span>
			</div>
			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<input type="text" class="form-control inputpalmasinn" name="telefono" id="telefono" placeholder="{{Lang::get('elhotel.phone')}}">
				<span class="text-danger pull-right requerido">El telefono es requerido</span>
				<span class="text-danger pull-right invalido">El telefono es invalido</span>
			</div>
			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<input type="email" class="form-control inputpalmasinn" name="email" id="email" placeholder="E-mail">
				<span class="text-danger pull-right requerido">El correo es requerido</span>
				<span class="text-danger pull-right invalido">El correo es invalido</span>
			</div>
			<div class="from-group col-xs-12">
				<textarea class="form-control inputpalmasinn" name="mensaje" id="mensaje" rows="4" placeholder="{{Lang::get('elhotel.solicitud')}}" style="resize:none;" minlength="20" required=""></textarea>
				<span class="text-danger pull-right requerido">El mensaje es requerido</span>
				<span class="text-danger pull-right minlength">El mensaje es muy corto</span>
			</div>
			<div class="from-group col-xs-12">
				<button type="submit" class="btn" id="btn-enviar" disabled>{{Lang::get('elhotel.send')}}</button>
			</div>
		</form>
	</div>
</div>
@include("modales.modal_imagen")
@include("modales.eventoscorporativos")
@include("modales.eventosinfpalmas")
@include("modales.galeriplamas")
@endsection
@section('Myscript')
<script src="{{asset('js/elhotelpalmasinn.js')}}"></script>
<script src="{{asset('plugins/MaskedInput/jquery.maskedinput.min.js')}}"></script>
<script>
	var target = "{{e($res['id'])}}";
</script>
@endsection
