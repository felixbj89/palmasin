@extends("layout.master")
@section('Mycss')
<link rel="stylesheet" href="{{asset('css/restauratespalmasinn.css')}}">
@endsection
@section('navbar-palmasinn')
	@parent
@endsection
@section('restaurantes-active')
	activo
@endsection
@section('baner-page')
<div class="full-height banner_palmasinn background_img">
	<div id="restbarCarousel" class="carousel fdi-Carousel slide" data-ride="carousel" data-interval="false">
		<div class="carousel-inner onebyone-carosel background_img">
			{!!$res['itembanner']!!}
		</div>
		<a class="background_img conrtosBanner conrtosBanner-left" href="#restbarCarousel" data-slide="prev"></a>
		<a class="background_img conrtosBanner conrtosBanner-right" href="#restbarCarousel" data-slide="next"></a>
	</div>
</div>
@endsection
@section('body-page')
<!-- collapse -->
<div id="collapse" class="row">
	{!!$res['itemrestau']!!}
</div>

<!-- Reservaviones -->
<div class="row reservaciones" id="contacto">
	<div class="col-xs-12">
		<div>
			<div class="row">
				<div id="img_form" class="background_img" style="background-image:url({{$res['habaneros_form']}});"></div>
				<div class="col-xs-12 col-sm-9">
					<div>
						<div>
							<form class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<div class="form-group col-xs-12">
										<p class="tituloitalic text-center" style="color:#FFF;text-transform:uppercase;">{{Lang::get("home.reservaciones")}}</p>
									</div>
									<div class="form-group col-xs-12 col-sm-6">
										<input type="text" class="form-control inputpalmasinn" name="nombre" id="nombre" placeholder="{{Lang::get('elhotel.nombre')}}">
									</div>
									<div class="form-group col-xs-12 col-sm-6">
										<input type="text" class="form-control inputpalmasinn" name="apellido" id="apellido" placeholder="{{Lang::get('elhotel.apellido')}}">
									</div>
									<div class="form-group col-xs-12 col-sm-6">
										<input type="text" class="form-control inputpalmasinn" name="telefono" id="telefono" placeholder="{{Lang::get('elhotel.phone')}}">
									</div>
									<div class="form-group col-xs-12 col-sm-6">
										<input type="email" class="form-control inputpalmasinn" name="email" id="email" placeholder="E-mail">
									</div>
									<div class="from-group col-xs-12">
										<textarea class="form-control inputpalmasinn" name="mensaje" id="mensaje" rows="4" placeholder="{{Lang::get('elhotel.solicitud')}}" style="resize:none;" minlength="20" required=""></textarea>
									</div>
									<div class="from-group col-xs-12">
										<button type="submit" class="btn" id="btn-enviar" disabled>{{Lang::get('elhotel.send')}}</button>
									</div>
								</div>
							</form>								
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('Myscript')
<script src="{{asset('js/restaurantespalmasinn.js')}}"></script>
@include("modales.modal_imagen")
@include("modales.galeriplamas")
<script>
	var target = "{{e($res['id'])}}";
</script>
@endsection