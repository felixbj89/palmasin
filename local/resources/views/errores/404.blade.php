<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<title>404 | No existe su solicitud</title>
			<!-- Tell the browser to be responsive to screen width -->
			<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
			<link rel="shortcut icon" type="image/png" href="{{asset('img/fav.png')}}"/>
			<!-- Bootstrap 3.3.7 -->
			<link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
			<!-- Font Awesome -->
			<link rel="stylesheet" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
			<!-- Ionicons -->
			<link rel="stylesheet" href="{{asset('fonts/ionicons-2.0.1/css/ionicons.min.css')}}">
			<!-- STYLE DASHBOARD -->
			<link rel="stylesheet" href="{{asset('css/master.css')}}">
			<link rel="stylesheet" href="{{asset('css/backend.css')}}">
			<link rel="stylesheet" href="{{asset('css/modales.css')}}">
			<link rel="stylesheet" href="{{asset('css/erros.css')}}">
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>
		<body class="hold-transition login-page box-login">
			<!--div class="login-box login-div">
				<div class="login-box-body border-div">
					<div class="row form-group">
						<div class="col-xs-12">
							<small><a href="{{url('/')}}" class="pull-right">Ir al Sitio</a></small>
						</div>
					</div>
				</div>
			</div-->
			<div class="row">
				<div class="col-xs-10 text-right">
					<a href="{{url('/')}}"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Ir al Sitio</a>
				</div>
			</div>
			<!-- jQuery 2.2.3 -->
			<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
			<!-- Bootstrap 3.3.7 -->
			<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
		</body>
	</html>
