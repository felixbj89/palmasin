<input type="hidden" id="camposvacios" value="{{e(Lang::get('errores.emptyinput'))}}"/>
<input type="hidden" id="loginvacio" value="{{e(Lang::get('errores.logininput'))}}"/>
<input type="hidden" id="passwordvacio" value="{{e(Lang::get('errores.passwordinput'))}}"/>
@if(count($errors) > 0)
	<input type="hidden" id="server_error" value="{{e($errors->all()[0])}}"/>
@endif