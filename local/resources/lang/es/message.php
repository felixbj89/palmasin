<?php

return [

	'title_bui' => 'Palmas Inn | Acceso',
	'title_dui' => 'Palmas Inn | Dashboard',
	"subtitle" => 'Ingreses sus credenciales.',
	"yes_register" => "Los cambios han sido guardados satisfactoriamente.",
	"login_repetido" => "El login a usar ya se encuentra en uso",
	"email_repetido" => "El e-mail a usar ya se encuentra en uso",
	"is_unique" => "La título de su promoción ya está en uso.",
	"yes_modify" => "La modificación fue satisfactoria.",
	"yes_asociado" => "Sea asociado su promoción satisfactoriamente.",
	"not_register" => "No habido cambios en la galería.",
	"not_users" => "No pudo ser creado su usuario.",
	"not_modify" => "No habido cambios aparentes en su cuenta.",
	"not_size" => "Solo pueden existir 3 promociones activas a la vez.",
	"not_change" => "No habido cambios en su perfil.",
	"not_changeusers" => "No habido cambios en el perfi del usuario.",
	"not_asociado" => "Solo sea podido llevar una asociación por imagen.",
	"evet_not_reg" => "Algo salio mal no se pudo reguistrar el Evento."
];
