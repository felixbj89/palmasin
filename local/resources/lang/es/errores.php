<?php

return [

    'emptyinput' => 'Los campos son obligatorios.',
	'loginvacio' => 'El campo login es obligatorio.',
	'passwordvacio' => 'El campo password es obligatorio.',
	'credenciales_incorrectas' => "Las credenciales ingresadas son incorrectas."
];
