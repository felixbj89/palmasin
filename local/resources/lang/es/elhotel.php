<?php

return [
	"titulo_welcome" => "Un lugar para recordar",
	"subtitulo_hotel" => "Las Palmas Inn es conocido como el  lugar donde no pasa el tiempo, un lugar para recordar y vivir experiencias mágicas, conoce el confort de sus 122 habitaciones y 4 suites.",
	"galeria" => "Galeria",
	"eventos" => "Eventos Corporativos",
	"info" => "Información",
	"reserva" => "Reservar",
	"proyectos" => "Proyectos en curso",
	"enjoy" => "Disfruta de nuestras instalaciones",
	"family" => "El mejor lugar para vacacionar en familia",
	"contacto" => "Contacto",
	"nombre" => "Nombre",
	"apellido" => "Apellido",
	"phone" => "Teléfono",
	"solicitud" => "Solicitud o Requerimiento",
	"send" => "Enviar",
	"coporativo" => "Corporativo",
	"phonecard" => "Telf."
];

?>