<?php

return [
	"welcome_1" => "<p>Las Palmas Inn es un hotel situado en la ciudad de mayor actividad comercial del estado Falcón, en él encontraras innovación, calidad y servicio como tú lo mereces; un lugar que te va a cautivar con todas sus comodidades, un lugar lleno de entretenimiento que incluyen piscina rodeada de palmas, amplio lobby, restaurant, gimnasio, etc.</p>",
	"welcome_2" => "<p>Las Palmas Inn es confort, elegancia, servicio y calidad con un aire clásico sin dejar a un lado lo actual y moderno dentro de sus ambientes sutilmente decorados.</p>",
	"habitaciones" => "Habitaciones",
	"adulto" => "Adulto",
	"minor" => "Menor",
	"infante" => "Infante",
	"buscar" => "BUSCAR",
	"phone" => "Tlfs",
	"hotel" => "EL HOTEL",
	"restauranten" => "Restaurants",
	"restaurantenav" => "RESTAURANTES",
	"habitacionesnav" => "HABITACIONES",
	"eventosnav" => "EVENTOS",
	"reserva" => "RESERVA",
	"instalaciones" => "Instalaciones",
	"reservaonline" => "Reserva Online",
	"sistema" => "Sistema de seguridad, cámaras y monitoreo las 24 hrs",
	"comodidades" => "Las mejores comodidades",
	"atencion" => "Atención personalizada",
	"verhotel" => "Ver Hotel",
	"vercomerciales" => "Ver Comerciales",
	"seemore" => "Ver más",
	"galeria" => "Gallery",
	"eventos" => "Corporate Events",
	"info" => "Information",
	"proyectos" => "Projects in progress",
	"enjoy" => "Enjoy our facilities",
	"family" => "The best place to vacation with the family",
	"reservaciones" => "Reservaciones",
	"mapa" => "Mapa del Sitio",
	"condiciones" => "Terminos y Condiciones",
	"welcome" => "¡Bienvenidos!",
	"mubicacion" => "UBICACIÓN",
	"whatsapponline" => "Activo / On Line",
	"boton1hotel" => "Ver Galería",
	"boton2hotel" => "Reserva Online",
	"boton3hotel" => "Atrás"
];

?>