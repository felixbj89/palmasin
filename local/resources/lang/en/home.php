<?php

return [
	"titulo_welcome" => "Welcome Hotel Las Palmas Inn",
	"subtitulo_welcome" => "a place to remember",
	"welcome_1" => "<p>Las Palmas Inn is a hotel located in the city of greater commercial activity in the state of Falcon, where you will find innovation, quality and service as you deserve it; A place that will captivate you with all its amenities, a place full of entertainment that include a pool surrounded by palms, great lobby, restaurant, gym, etc.</p>",
	"welcome_2" => "<p>Las Palmas Inn is comfort, elegance, service and quality with a classic air without leaving aside the current and modern within its subtly decorated environments.</p>",
	"short_lobby" => "It offers the exclusive night atmosphere, ideal for dancing and sharing pleasant and unforgettable moments.",
	"short_habaneros" => "In the best Tex - mex style we offer you a varied Menu in Our modern and comfortable spaces with a unique atmosphere in the city where you can experience the sensation of being somewhere else.",
	"short_maracuya" => "It’s an outdoor environment, a place positioned as one of the best environments to share and enjoy with good music, diverse menu, with great variety in drinks and famous for its Mojito Cubano to the best chillout style.",
	"short_breakfast" => "we take care of offering you a complete buffet for breakfast with a great variety of dishes and homemade pastry.",
	"short_dj" => "On Thursdays, Saturdays and Sundays you can enjoy our invited Dj’s and spend a different time with the most varied mixes that only they can offer you.",
	"short_talento" => "With the intention of offering you a pleasant entertainment experience, we have incorporated live show nights from Wednesday to Sunday.",
	"habitaciones" => "Rooms",
	"habitacionesnav" => "ROOMS",
	"adulto" => "Adult",
	"minor" => "Minor",
	"infante" => "Children",
	"buscar" => "SEARCH",
	"phone" => "Phones",
	"hotel" => "THE HOTEL",
	"restauranten" => "Restaurants",
	"restaurantenav" => "RESTAURANTS",
	"eventosnav" => "EVENTS",
	"reserva" => "RESERVE",
	"instalaciones" => "Facilities",
	"reservaonline" => "Online Booking.",
	"sistema" => "Security system, cameras and 24-hour monitoring.",
	"comodidades" => "The best amenities",
	"atencion" => "Personal attention",
	"verhotel" => "See Hotel",
	"vercomerciales" => "See commercials",
	"seemore" => "See more",
	"reservaciones" => "Reservations",
	"mapa" => "Sitemap",
	"condiciones" => "Terms and Conditions",
	"welcome" => "welcome!",
	"mubicacion" => "LOCATION",
	"whatsapponline" => "Active / Online",
	"boton1hotel" => "See Gallery",
	"boton2hotel" => "Online Booking",
	"boton3hotel" => "Back"
];

?>