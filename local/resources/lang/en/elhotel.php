<?php

return [
	"titulo_welcome" => "A place to remember",
	"subtitulo_hotel" => "Las Palmas Inn is known as the place where time does not pass, a place to remember and live magical experiences, know the comfort of its 122 rooms and 4 suites.",
	"innertitle_hotel" => "12 years of exclusivity",
	"parrafo_hotel" => "<p>The Las Palmas Inn hotel is strategically located in the area of ​​greater financial, commercial, cultural and tourist flow of Paraguaná, land of great contrasts, historical and tourist landscapes, the best beaches, delicious gastronomy, the excellence in care and service that characterizes the region, in it you will find innovation and quality as you deserve it, in addition you will find the attention of a highly qualified personnel with vocation of service.</p></br><p>It has corporate rates for businessmen who constantly make business trips to the city, has a corporate and yet familiar approach, which has led to position as the best hotel in the city, We are fully involved in offering you our best values ​​and Services to each of our guests, you have to live the experience.</p></br><p>Come and enjoy Paraguaná at your Hotel Las Palmas Inn.</p>",
	"galeria" => "Gallery",
	"eventos" => "Corporate Events",
	"info" => "Information",
	"reserva" => "Reserve",
	"proyectos" => "Projects in progress",
	"enjoy" => "Enjoy our facilities",
	"family" => "The best place to vacation with the family",
	"contacto" => "Contact",
	"nombre" => "Name",
	"apellido" => "Last Name",
	"phone" => "Phone",
	"solicitud" => "Request or Requirement",
	"send" => "Send",
	"coporativo" => "Corporative",
	"phonecard" => "Phone"
];

?>
