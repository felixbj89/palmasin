<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Comerciales;

class CreateTableComerciales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comerciales', function (Blueprint $table) {
            $table->increments('id');
			$table->text('comerciales_pictureone');
			$table->text('comerciales_picturetwo');
			$table->string('comerciales_textoone');
			$table->string('comerciales_textotwo');
			$table->text('comerciales_linkone');
			$table->text('comerciales_linktwo');
            $table->timestamps();
        });
		
		Comerciales::create([
			"comerciales_pictureone" => "img/elhotel/comerciales/hotel_comerciales1.jpg",
			"comerciales_picturetwo" => "img/elhotel/comerciales/hotel_comerciales2.jpg",
			"comerciales_textoone" => "Disfruta de nuestras instalaciones",
			"comerciales_textotwo" => "El mejor lugar para vacacionar en familia",
			"comerciales_linkone" => "http://",
			"comerciales_linktwo" => "http://",
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('comerciales');
    }
}
