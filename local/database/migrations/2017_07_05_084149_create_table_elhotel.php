<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ElHotel;

class CreateTableElhotel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('elhotel', function (Blueprint $table) {
            $table->increments('id');
			$table->string('elhotel_title');
			$table->text('elhotel_texto');
			$table->text('elhotel_picone');
			$table->text('elhotel_pictwo');
			$table->text('elhotel_picthrid');
			$table->timestamps();
        });
		
		ElHotel::create([
			"elhotel_title" => "12 años de exclusividad",
			"elhotel_texto" => "<p>El hotel Las Palmas Inn, cuenta con una ubicación estratégica en la zona de mayor afluencia financiera, comercial, cultural y turística de Paraguaná, tierra de grandes contrastes, paisajes históricos y turísticos, las mejores playas, deliciosa gastronomía,  sumado a la excelencia en atención y servicio que caracteriza a la región, en él encontraras innovación y calidad como tú lo mereces, además hallarás la atención de un personal altamente calificado con vocación de servicio.</p><p>Cuenta con tarifas corporativas para los empresarios que constantemente realizan viajes de negocios a la ciudad, tiene un enfoque corporativo y a la vez familiar, lo que lo ha llevado a posicionarse como el mejor hotel de la zona, Estamos completamente involucrados en ofrecerle nuestros mejores valores y servicios a cada uno de nuestros huéspedes, tienes que vivir la experiencia. Ven y disfruta de Paraguaná en tu Hotel Las Palmas Inn.</p>",
			"elhotel_picone" => "img/elhotel/historia/hotel_proyectos.jpg",
			"elhotel_pictwo" => "img/elhotel/historia/hotel_galeriainstalaciones.jpg",
			"elhotel_picthrid" => "img/elhotel/historia/hotel_galeriaeventos.jpg"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("elhotel");
    }
}
