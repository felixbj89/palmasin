<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Avatares;

class CreateTableAvatares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avatares', function (Blueprint $table) {
          $table->increments('id');
          $table->text('avatares_path');
          $table->timestamps();
        });
		
		Schema::table('avatares', function (Blueprint $table) {
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
		});
		
		Avatares::create([
			"avatares_path" => "img/avatares/user2-160x160.jpg",
			"user_id" => "1"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avatares');
    }
}
