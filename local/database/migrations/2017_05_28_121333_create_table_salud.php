<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ISalud;

class CreateTableSalud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('salud', function (Blueprint $table) {
			$table->increments('id');
			$table->string('salud_titulo');
			$table->string('salud_situlo');
			$table->text('salud_path');
			$table->text('salud_pathinterna');
			$table->enum('salud_estado',["0","1","2"]);
			$table->timestamps();
		});
		
		Schema::table('salud', function (Blueprint $table) {
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
		});
		
		ISalud::create([
			"salud_titulo" => "Turismo de salud en Paraguaná",
			"salud_situlo" => "estamos cerca de las principales clínicas para su comodidad",
			"salud_path" => "img/home/salud/default.jpg",
			"salud_pathinterna" =>  "img/home/salud/default.jpg",
			"salud_estado" => "0",
			"user_id" => "1"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salud');
    }
}
