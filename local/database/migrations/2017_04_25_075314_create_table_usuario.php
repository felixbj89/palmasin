<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Usuario;
use App\Promociones;
use App\Galeria;
use App\Servicios;

class CreateTableUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('user_name');
          $table->string('user_email')->unique();
          $table->text('user_login');
          $table->string('user_password');
		  $table->string('user_avatar');
          $table->enum('user_role',['0','1','2']);
          $table->enum('user_status',['0','1']);
          $table->timestamps();
        });
		
		Schema::table('promociones', function (Blueprint $table) {
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
        });
		
		Schema::table('galeria', function (Blueprint $table) {
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
		});
							
        Usuario::create([
			"user_name" => "palmasinn",
			"user_email" => "haciendacreativa@gmail.com",
			"user_login" => "palmasinn",
			"user_password" => hash('ripemd320', "palmasinn"),
			"user_avatar" => "img/avatares/user2-160x160.jpg",
			"user_role" => "0",
			"user_status" => "1",
		]);
		
		Promociones::create([
			"promo_path" => "img/home/promociones/default.jpg",
			"promo_title" => "TITLE",
			"promo_text" => "TEXT",
			"promo_status" => "1",
			"promo_dateini" => "0000-00-00 00:00:00",
			"promo_datefin" => "0000-00-00 00:00:00",
			"promo_used" => "1",
			"user_id" => "1"
		]);
				
		Galeria::create([
			"galeria_pos" => "1",
			"galeria_path" => "img/home/galeria/default.jpg",
			"promo_id" => "1",
			"user_id" => "1"
		]);
		
		Galeria::create([
			"galeria_pos" => "2",
			"galeria_path" => "img/home/galeria/default.jpg",
			"promo_id" => "1",
			"user_id" => "1"
		]);
		
		Galeria::create([
			"galeria_pos" => "3",
			"galeria_path" => "img/home/galeria/default.jpg",
			"promo_id" => "1",
			"user_id" => "1"
		]);
		
		Galeria::create([
			"galeria_pos" => "4",
			"galeria_path" => "img/home/galeria/default.jpg",
			"promo_id" => "1",
			"user_id" => "1"
		]);
		
		Galeria::create([
			"galeria_pos" => "5",
			"galeria_path" => "img/home/galeria/default.jpg",
			"promo_id" => "1",
			"user_id" => "1"
		]);
		
		Galeria::create([
			"galeria_pos" => "6",
			"galeria_path" => "img/home/galeria/default.jpg",
			"promo_id" => "1",
			"user_id" => "1"
		]);
    }

    public function down()
    {
        Schema::drop('users');
    }
}
