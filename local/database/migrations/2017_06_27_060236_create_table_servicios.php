<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Servicios;

class CreateTableServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
			$table->string('servicios_titulo');
			$table->string('servicios_subtitulo');
			$table->text('servicios_texto');
            $table->timestamps();
        });
		
		Servicios::create([
			"servicios_titulo" => "Bienvenido a Hotel Las Palmas Inn",
			"servicios_subtitulo" => "un lugar para recordar",
			"servicios_texto" => "<p>Las Palmas Inn es un hotel situado en la ciudad de mayor actividad comercial del estado Falcón, en él encontraras innovación, calidad y servicio como tú lo mereces; un lugar que te va a cautivar con todas sus comodidades, un lugar lleno de entretenimiento que incluyen piscina rodeada de palmas, amplio lobby, restaurant, gimnasio, etc.</p><p>Las Palmas Inn es confort, elegancia, servicio y calidad con un aire clásico sin dejar a un lado lo actual y moderno dentro de sus ambientes sutilmente decorados.</p>",
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('servicios');
    }
}
