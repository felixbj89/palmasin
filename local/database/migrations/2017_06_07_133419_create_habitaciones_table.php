<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Habitacion;

class CreateHabitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitaciones', function (Blueprint $table) {
            $table->increments('id');
			$table->string('room_title');
			$table->text('room_text');
			$table->text('room_listdetails');
			$table->text('room_comodidades');
			$table->string('room_comodidades_list');
			$table->string('room_imgportada');
            $table->timestamps();
        });
		/*
		Habitacion::create([
			"room_title" => "Matrimonial Ejecutiva",
			"room_text" => "<p>Un lugar que te va ha encantar con su amplia variedad de servicios, que incluyen piscina rodeada de palmas, áreas comunes, áreas verdes, gimnasio,  etc. Las Palmas Inn es confort, elegancia, servicio, calidad con un aire contemporáneo sin dejar de lado lo actual y moderno dentro de sus ambientes exclusivamente decorados.</p>",
			"room_listdetails" => "<li>1 Cama King Size</li><li>Desayuno Incluido ( hasta 2 pax max )</li><li>personas adicionales tendrán cargos extras</li><li>Capacidad de 1 a 2 pax Máximo</li>",
			"room_comodidades" => "<li class=\"col-xs-4 col-sm-3 background_img ico ico1\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico2\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico3\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico4\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico5\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico6\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico7\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico8\"></li>",
			"room_imgportada" => "img/habitaciones/hab_1.jpg",
		]);
		
		Habitacion::create([
			"room_title" => "Matrimonial Estándar",
			"room_text" => "<p>Un lugar que te va ha encantar con su amplia variedad de servicios, que incluyen piscina rodeada de palmas, áreas comunes, áreas verdes, gimnasio,  etc. Las Palmas Inn es confort, elegancia, servicio, calidad con un aire contemporáneo sin dejar de lado lo actual y moderno dentro de sus ambientes exclusivamente decorados.</p>",
			"room_listdetails" => "<li>1 Cama King Size</li><li>Desayuno Incluido ( hasta 2 pax max )</li><li>personas adicionales tendrán cargos extras</li><li>Capacidad de 1 a 2 pax Máximo</li>",
			"room_comodidades" => "<li class=\"col-xs-4 col-sm-3 background_img ico ico1\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico2\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico3\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico4\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico5\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico6\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico7\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico8\"></li>",
			"room_imgportada" => "img/habitaciones/hab_2.jpg",
		]);
		
		Habitacion::create([
			"room_title" => "Doble Estándar",
			"room_text" => "<p>Un lugar que te va ha encantar con su amplia variedad de servicios, que incluyen piscina rodeada de palmas, áreas comunes, áreas verdes, gimnasio,  etc. Las Palmas Inn es confort, elegancia, servicio, calidad con un aire contemporáneo sin dejar de lado lo actual y moderno dentro de sus ambientes exclusivamente decorados.</p>",
			"room_listdetails" => "<li>1 Cama King Size</li><li>Desayuno Incluido ( hasta 2 pax max )</li><li>personas adicionales tendrán cargos extras</li><li>Capacidad de 1 a 2 pax Máximo</li>",
			"room_comodidades" => "<li class=\"col-xs-4 col-sm-3 background_img ico ico1\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico2\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico3\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico4\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico5\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico6\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico7\"></li><li class=\"col-xs-4 col-sm-3 background_img ico ico8\"></li>",
			"room_imgportada" => "img/habitaciones/hab_3.jpg",
		]);*/
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitaciones');
    }
}
