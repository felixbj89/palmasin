<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePromociones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promociones', function (Blueprint $table) {
          $table->increments('id');
          $table->text('promo_path');
          $table->string('promo_title')->unique();
		  $table->text('promo_text');
		  $table->enum('promo_status',['0','1']);
		  $table->enum('promo_used',['0','1']);
		  $table->text('promo_dateini')->nullable();
		  $table->text('promo_datefin')->nullable();
          $table->timestamps();
        });
		
		Schema::table('galeria', function (Blueprint $table) {
			$table->integer('promo_id')->unsigned();
			$table->foreign('promo_id')->references('id')->on('promociones');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promociones');
    }
}
