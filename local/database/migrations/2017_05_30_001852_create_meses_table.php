<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Meses;

class CreateMesesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meses', function (Blueprint $table) {
            $table->increments('id');
			$table->string('mes_nombre')->unique();
			$table->text('mes_titulo');
			$table->text('mes_year');
			$table->enum('mes_status',['0','1']);			
            $table->timestamps();
        });
		
		Meses::create([
			"mes_nombre" => "ene",
			"mes_titulo" => "Enero",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "feb",
			"mes_titulo" => "Febrero",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "mar",
			"mes_titulo" => "Marzo",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "abr",
			"mes_titulo" => "Abril",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "may",
			"mes_titulo" => "Mayo",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "jun",
			"mes_titulo" => "Junio",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "jul",
			"mes_titulo" => "Julio",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "ago",
			"mes_titulo" => "Agosto",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "sep",
			"mes_titulo" => "Septiembre",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "opt",
			"mes_titulo" => "Optubre",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "nov",
			"mes_titulo" => "Noviembre",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
		Meses::create([
			"mes_nombre" => "dic",
			"mes_titulo" => "Diciembre",
			"mes_year" => "0",
			"mes_status" => "0"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meses');
    }
}
