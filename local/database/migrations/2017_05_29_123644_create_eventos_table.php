<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
			$table->string('event_title');
			$table->text('event_text');
			$table->enum('event_ppal',['0','1']);
			$table->enum('event_type',['0','1']);//0 -> Promocional 1 -> Corporativo
			$table->text('event_dateini')->nullable();
			$table->text('event_horaini')->nullable();
			$table->text('event_datefin')->nullable();
			$table->text('event_href')->nullable();
			$table->text('event_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
