<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Restaurant;

class CreateRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant', function (Blueprint $table) {
            $table->increments('id');
			$table->text('restaurant_descripcion');
			$table->string('restaurant_title');
			$table->string('restaurant_contacto1')->nullable();
			$table->string('restaurant_contacto2');
			$table->string('restaurant_comercial');
			$table->string('restaurant_rest_file1');
			$table->string('restaurant_rest_file2');
			$table->string('restaurant_rest_file3');
			$table->string('restaurant_rest_file4');
			$table->string('restaurant_rest_file5');
			$table->string('restaurant_color');
            $table->timestamps();
        });

		Restaurant::create([//HABANEROS
			"restaurant_title" => "HABANEROS",
			"restaurant_descripcion" => "<p>Es un hotel desarrollado a través de una visión familiar, el hotel abrió sus puertas al publico el viernes 25 de febrero de 2005 con 12 habitaciones, gracias a la gran receptividad que tuvo el hotel que actualmente cuenta con 126 habitaciones, situado en la ciudad de mayor actividad comercial del estado Falcón. Tierra de grandes contrastes, atractivos colores, diversas culturas, diversos paisajes históricos y turísticos, las mejores playas.</p>",
			"restaurant_contacto1" => "000 0000000",
			"restaurant_contacto2" => "000 0000000",
			"restaurant_comercial" => "https://www.google.co.ve/",
			"restaurant_rest_file1" => "img/restaurantes/banner/Habanerosblanco.png",
			"restaurant_rest_file2" => "img/restaurantes/restaurantes/habaneros.png",
			"restaurant_rest_file3" => "img/default.jpg",
			"restaurant_rest_file4" => "img/default.jpg",
			"restaurant_rest_file5" => "img/default.jpg",
			"restaurant_color" => "#D03327"
		]);	

		Restaurant::create([//MARACUYA
			"restaurant_title" => "MARACUYA",
			"restaurant_descripcion" => "<p>Es un hotel desarrollado a través de una visión familiar, el hotel abrió sus puertas al publico el viernes 25 de febrero de 2005 con 12 habitaciones, gracias a la gran receptividad que tuvo el hotel que actualmente cuenta con 126 habitaciones, situado en la ciudad de mayor actividad comercial del estado Falcón. Tierra de grandes contrastes, atractivos colores, diversas culturas, diversos paisajes históricos y turísticos, las mejores playas.</p><p>Es un hotel desarrollado a través de una visión familiar, el hotel abrió sus puertas al publico el viernes 25 de febrero de 2005 con 12 habitaciones, gracias a la gran receptividad que tuvo el hotel que actualmente cuenta con 126 habitaciones, situado en la ciudad de mayor actividad comercial del estado Falcón. Tierra de grandes contrastes, atractivos colores, diversas culturas, diversos paisajes históricos y turísticos, las mejores playas.</p>",
			"restaurant_contacto1" => "111 1111111",
			"restaurant_contacto2" => "111 1111111",
			"restaurant_comercial" => "https://www.google.co.ve/",
			"restaurant_rest_file1" => "img/restaurantes/banner/maracuyablanco.png",
			"restaurant_rest_file2" => "img/restaurantes/restaurantes/maracuya.png",
			"restaurant_rest_file3" => "img/default.jpg",
			"restaurant_rest_file4" => "img/default.jpg",
			"restaurant_rest_file5" => "img/default.jpg",
			"restaurant_color" => "#222042"
		]);	
		
		Restaurant::create([//LOBBYBAR
			"restaurant_title" => "LOBBYBAR",
			"restaurant_descripcion" => "<p>Es un hotel desarrollado a través de una visión familiar, el hotel abrió sus puertas al publico el viernes 25 de febrero de 2005 con 12 habitaciones, gracias a la gran receptividad que tuvo el hotel que actualmente cuenta con 126 habitaciones, situado en la ciudad de mayor actividad comercial del estado Falcón. Tierra de grandes contrastes, atractivos colores, diversas culturas, diversos paisajes históricos y turísticos, las mejores playas.</p><p>Es un hotel desarrollado a través de una visión familiar, el hotel abrió sus puertas al publico el viernes 25 de febrero de 2005 con 12 habitaciones, gracias a la gran receptividad que tuvo el hotel que actualmente cuenta con 126 habitaciones, situado en la ciudad de mayor actividad comercial del estado Falcón. Tierra de grandes contrastes, atractivos colores, diversas culturas, diversos paisajes históricos y turísticos, las mejores playas.</p>",
			"restaurant_contacto1" => "222 2222222",
			"restaurant_contacto2" => "222 2222222",
			"restaurant_comercial" => "https://www.google.co.ve/",
			"restaurant_rest_file1" => "img/restaurantes/banner/lobbybarblanco.png",
			"restaurant_rest_file2" => "img/restaurantes/restaurantes/lobbybar.png",
			"restaurant_rest_file3" => "img/default.jpg",
			"restaurant_rest_file4" => "img/default.jpg",
			"restaurant_rest_file5" => "img/default.jpg",
			"restaurant_color" => "#E46225"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant');
    }
}
