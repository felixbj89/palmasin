<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Bannergaleri;

class CreateBannergaleriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bannergaleri', function (Blueprint $table) {
            $table->increments('id');
			$table->enum('bannergaleri_section',["0","1","2","3"]);//home,elhotel,habitaciones,restaurantres
			$table->integer('bannergaleri_id');
			$table->enum('bannergaleri_destacar',["0","1","2"]);//nodestacar,destacar,imgen-fija-seccion
			$table->string('bannergaleri_rutaimg');
			$table->string('bannergaleri_link')->nullable();
			$table->text('bannergaleri_texto')->nullable();
            $table->timestamps();
        });
		//--------//Galeria Restaurante //"bannergaleri_section" => "3","bannergaleri_id" => "1"
		//3
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "3",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "3",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "3",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "3",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "3",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "3",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "3",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "3",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		
		//2
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "2",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "2",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "2",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "2",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "2",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "2",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "2",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "2",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		
		//1
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "3",//restaurantes
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		//--------//Galeria El hotel instalaciones //"bannergaleri_section" => "3","bannergaleri_id" => "1"
		Bannergaleri::create([
			"bannergaleri_section" => "1",//Elhotel
			"bannergaleri_id" => "1",//galeria instalaciones
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "1",//Elhotel
			"bannergaleri_id" => "1",//galeria instalaciones
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "1",//Elhotel
			"bannergaleri_id" => "1",//galeria instalaciones
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "1",//Elhotel
			"bannergaleri_id" => "1",//galeria instalaciones
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "1",//Elhotel
			"bannergaleri_id" => "1",//galeria instalaciones
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "1",//Elhotel
			"bannergaleri_id" => "1",//galeria instalaciones
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "1",//Elhotel
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "1",//Elhotel
			"bannergaleri_id" => "1",//galeria restaurante
			"bannergaleri_destacar" => "0",//nodestacado
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		//--------//Galeria Secundaria Home //"bannergaleri_section" => "0","bannergaleri_id" => "1-6" bannergaleri_destacar => 1-0
		Bannergaleri::create([
			"bannergaleri_section" => "0",//home
			"bannergaleri_id" => "1",//id item
			"bannergaleri_destacar" => "0",//no esta
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "0",//home
			"bannergaleri_id" => "2",//id item
			"bannergaleri_destacar" => "0",//no esta
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "0",//home
			"bannergaleri_id" => "3",//id item
			"bannergaleri_destacar" => "0",//no esta
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "0",//home
			"bannergaleri_id" => "4",//id item
			"bannergaleri_destacar" => "0",//no esta
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "0",//home
			"bannergaleri_id" => "5",//id item
			"bannergaleri_destacar" => "0",//no esta
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
		Bannergaleri::create([
			"bannergaleri_section" => "0",//home
			"bannergaleri_id" => "6",//id item
			"bannergaleri_destacar" => "0",//no esta
			"bannergaleri_rutaimg" => "img/default.jpg"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bannergaleri');
    }
}
