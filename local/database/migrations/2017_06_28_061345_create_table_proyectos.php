<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Proyectos;

class CreateTableProyectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->increments('id');
			$table->text('proyectos_picone');
			$table->text('proyectos_pictwo');
			$table->string('proyectos_picthrid');
			$table->string('proyectos_picfour');
			$table->text('proyectos_pdf');
            $table->timestamps();
        });
		
		Proyectos::create([
			"proyectos_picone" => "img/elhotel/centroempresarial/centroempresarial_1.jpg",
			"proyectos_pictwo" => "img/elhotel/centroempresarial/centroempresarial_2.jpg",
			"proyectos_picthrid" => "img/elhotel/centroempresarial/centroempresarial_3.jpg",
			"proyectos_picfour" => "img/elhotel/centroempresarial/centroempresarial_4.jpg",
			"proyectos_pdf" => "defualt.pdf",
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
