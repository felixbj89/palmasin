<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//GET-FRONTEND
/*Route::get('/', function () {
    return view('welcome');
});*/
Route::group(['middleware' => ['web']], function () {
	Route::get('traducir/{lang}', ['as'=>'lang.switch', 'uses'=>'FrontController@switchLang']);
	Route::get('/','FrontController@index');


	Route::get('booking','FrontController@booking');


	Route::get('home/{target?}', 'FrontController@index');
	Route::get('elhotel/{target?}','FrontController@elhotel');
	Route::get('habitaciones','FrontController@habitaciones');
	Route::get('habitaciones/{target}','FrontController@habitacion');
	Route::get('restaurantes/{target?}','FrontController@restaurantes');
	//GET-VIEW
	Route::get('view/gallery/{codigo}/{section}', ['uses' => 'FrontController@view_gallery', 'as' => 'view/gallery/{codigo}/{section}']);
	//POST-FRONTEN
	Route::post('index_eventos','FrontController@index_eventos');
	Route::post('habitaciones/habitajax','FrontController@habitajax');
	Route::post('elhotel/corporativos','FrontController@eventosgalerisajax');
	Route::post('elhotel/instalaciones','FrontController@instalacionesgalerisajax');
	//DESCARGA
	Route::get('descarga','FrontController@descarga');
});

//BACKEND
Route::group(['prefix' => 'admin'], function () {
	Route::get('backend', ['uses' => 'LoginController@backend', 'as' => 'admin/backend']);
	Route::get('salir', ['uses' => 'DashboardController@salir', 'as' => 'admin/salir']);
	Route::get('dashboard', ['uses' => 'DashboardController@dashboard', 'as' => 'admin/dashboard']);
	Route::get('mi_cuenta', ['uses' => 'DashboardController@account', 'as' => 'admin/mi_cuenta']);
	Route::get('bloquear', ['uses' => 'DashboardController@bloquear', 'as' => 'admin/bloquear']);
	Route::group(['middleware' => 'chequear_cuenta'], function(){
		Route::get('bloquear_cuenta', ['uses' => 'DashboardController@bloquear_cuenta', 'as' => 'admin/backend']);
	});
	
	//GET-ADMINISTRAR
	Route::get('administrargaleria', ['uses' => 'DashboardController@administrargaleria', 'as' => 'admin/administrargaleria']);
	//GET-GESTIONAR
	Route::get('gestionargaleriaprincipal', ['uses' => 'DashboardController@gestionargaleriaprincipal', 'as' => 'admin/gestionargaleriaprincipal']);
	Route::get('gestionarimagensalud', ['uses' => 'DashboardController@gestionarimagensalud', 'as' => 'admin/gestionarimagensalud']);
	//GET-CREATE
	Route::get('crear_galeria', ['uses' => 'DashboardController@crear_galeria', 'as' => 'admin/crear_galeria']);
	Route::get('crear_promociones', ['uses' => 'DashboardController@crear_promociones', 'as' => 'admin/crear_promociones']);
	Route::get('crear_bsalud', ['uses' => 'DashboardController@crear_bsalud', 'as' => 'admin/crear_bsalud']);
	Route::get('crear_eventos', ['uses' => 'DashboardController@crear_eventos', 'as' => 'admin/crear_eventos']);
	Route::get('crear_habitacion', ['uses' => 'DashboardController@crear_habitacion', 'as' => 'admin/crear_habitacion']);
	Route::get('crear_galeriaroom', ['uses' => 'DashboardController@crear_galeriaroom', 'as' => 'admin/crear_galeriaroom']);
	Route::get('crear_fotosecundariaroom', ['uses' => 'DashboardController@crear_fotosecundariaroom', 'as' => 'admin/crear_fotosecundariaroom']);
	Route::get('crear_fotoprincipalroom', ['uses' => 'DashboardController@crear_fotoprincipalroom', 'as' => 'admin/crear_fotoprincipalroom']);
	Route::get('crear_restgaleria/{opc}', ['uses' => 'DashboardController@crear_restgaleria', 'as' => 'admin/crear_restgaleria/{opc}']);
	Route::get('crear_fotoprincipalhotel', ['uses' => 'DashboardController@crear_fotoprincipalhotel', 'as' => 'admin/crear_fotoprincipalhotel']);
	Route::get('crear_instalaciones', ['uses' => 'DashboardController@crear_instalaciones', 'as' => 'admin/crear_instalaciones']);
	Route::get('crear_comerciales', ['uses' => 'DashboardController@crear_comerciales', 'as' => 'admin/crear_comerciales']);
	Route::get('crear_hotel', ['uses' => 'DashboardController@crear_hotel', 'as' => 'admin/crear_hotel']);
	Route::get('crear_eventoscorporativos', ['uses' => 'DashboardController@crear_eventoscorporativos', 'as' => 'admin/crear_eventoscorporativos']);
	Route::get('crear_proyectos', ['uses' => 'DashboardController@crear_proyectos', 'as' => 'admin/crear_proyectos']);
	Route::get('crear_usuario', ['uses' => 'DashboardController@crear_usuario', 'as' => 'admin/crear_usuario']);
	Route::get('crear_servicios', ['uses' => 'DashboardController@crear_servicios', 'as' => 'admin/crear_servicios']);
	Route::get('crear_galeriahotel', ['uses' => 'DashboardController@crear_galeriahotel', 'as' => 'admin/crear_galeriahotel']);
	//GET-LIST
	Route::get('listar_galeria', ['uses' => 'DashboardController@listar_galeria', 'as' => 'admin/listar_galeria']);
	Route::get('listar_promocion', ['uses' => 'DashboardController@listar_promocion', 'as' => 'admin/listar_promocion']);
	Route::get('listar_asociaciones', ['uses' => 'DashboardController@listar_asociaciones', 'as' => 'admin/listar_asociaciones']);
	Route::get('listar_eventos', ['uses' => 'DashboardController@listar_eventos', 'as' => 'admin/listar_eventos']);
	Route::get('listar_restfija', ['uses' => 'DashboardController@listar_restfija', 'as' => 'admin/listar_restfija']);
	Route::post('viewasociacion', ['uses' => 'DashboardController@viewasociacion', 'as' => 'admin/viewasociacion']);
	Route::post('listmesyeareventohome', ['uses' => 'EventosController@listmesyeareventohome', 'as' => 'admin/listmesyeareventohome']);
	Route::get('listar_meseseventos', ['uses' => 'DashboardController@listar_meseseventos', 'as' => 'admin/listar_meseseventos']);
	Route::get('listar_habitacion', ['uses' => 'DashboardController@listar_habitacion', 'as' => 'admin/listar_habitacion']);
	Route::get('listar_restaurantes', ['uses' => 'DashboardController@listar_restaurantes', 'as' => 'admin/listar_restaurantes']);
	Route::get('listar_usuario', ['uses' => 'DashboardController@listar_usuario', 'as' => 'admin/listar_usuario']);
	Route::get('listar_bsalud', ['uses' => 'DashboardController@listar_bsalud', 'as' => 'admin/listar_bsalud']);
	Route::get('listar_corporativos', ['uses' => 'DashboardController@listar_corporativos', 'as' => 'admin/listar_corporativos']);
	Route::get('listar_galerinferior', ['uses' => 'DashboardController@listar_galerinferior', 'as' => 'admin/listar_galerinferior']);
	//POST-CREATE
	Route::post('addgaleriahome', ['uses' => 'GaleriaController@addgaleriahome', 'as' => 'admin/addgaleriahome']);
	Route::post('addpromocioneshome', ['uses' => 'PromocionesController@addpromocioneshome', 'as' => 'admin/addpromocioneshome']);
	Route::post('addbannerinferior', ['uses' => 'GaleriaController@addbannerinferior', 'as' => 'admin/addbannerinferior']);
	Route::post('addeventoshome', ['uses' => 'EventosController@addeventoshome', 'as' => 'admin/addeventoshome']);
	Route::post('addavataruser', ['uses' => 'UsersController@addavataruser', 'as' => 'admin/addavataruser']);
	Route::post('addhabitad', ['uses' => 'HabitacionController@addhabitad', 'as' => 'admin/addhabitad']);
	Route::post('galerihabitad', ['uses' => 'BannergaleriController@galerihabitad', 'as' => 'admin/galerihabitad']);
	Route::post('imagesfija', ['uses' => 'BannergaleriController@imagesfija', 'as' => 'admin/imagesfija']);
	Route::post('bannersection', ['uses' => 'BannergaleriController@bannersection', 'as' => 'admin/bannersection']);
	Route::post('addusers', ['uses' => 'UsersController@addusers', 'as' => 'admin/addusers']);
	Route::post('addelhotel', ['uses' => 'HotelController@addelhotel', 'as' => 'admin/addelhotel']);
	Route::post('addproyectoshome', ['uses' => 'GaleriaController@addproyectoshome', 'as' => 'admin/addproyectoshome']);
	Route::post('addeventoscorporativos', ['uses' => 'EventosController@addeventoscorporativos', 'as' => 'admin/addeventoscorporativos']);
	Route::post('cloneventohome', ['uses' => 'EventosController@cloneventohome', 'as' => 'admin/cloneventohome']);
	//POST-EDIT
	Route::post('viewgaleriahome', ['uses' => 'DashboardController@viewgaleriahome', 'as' => 'admin/viewgaleriahome']);
	Route::post('viewimagensalud', ['uses' => 'DashboardController@viewimagensalud', 'as' => 'admin/viewimagensalud']);
	Route::post('viewpromocioneshome', ['uses' => 'DashboardController@viewpromocioneshome', 'as' => 'admin/viewpromocioneshome']);
	Route::post('asociargaleriahome', ['uses' => 'DashboardController@asociargaleriahome', 'as' => 'admin/asociargaleriahome']);
	Route::post('editgaleriahome', ['uses' => 'GaleriaController@editgaleriahome', 'as' => 'admin/editgaleriahome']);
	Route::post('editpromohome', ['uses' => 'PromocionesController@editpromohome', 'as' => 'admin/editpromohome']);
	Route::post('editarasociacionpg', ['uses' => 'GaleriaController@editarasociacionpg', 'as' => 'admin/editarasociacionpg']);
	Route::post('editbannerinferior', ['uses' => 'GaleriaController@editbannerinferior', 'as' => 'admin/editbannerinferior']);
	Route::post('ppalevento', ['uses' => 'EventosController@ppalevento', 'as' => 'admin/ppalevento']);
	Route::post('editadoeventohome', ['uses' => 'DashboardController@editadoeventohome', 'as' => 'admin/editadoeventohome']);
	Route::post('editadoeventos_home', ['uses' => 'EventosController@editadoeventos_home', 'as' => 'admin/editadoeventos_home']);
	Route::post('editperfil', ['uses' => 'UsersController@editperfil', 'as' => 'admin/editperfil']);
	Route::post('editusers', ['uses' => 'DashboardController@editusers', 'as' => 'admin/editusers']);
	Route::post('edit_users', ['uses' => 'UsersController@edit_users', 'as' => 'admin/edit_users']);
	Route::post('editgalerihabitad', ['uses' => 'BannergaleriController@editgalerihabitad', 'as' => 'admin/editgalerihabitad']);
	Route::post('edithabitad', ['uses' => 'DashboardController@edithabitad', 'as' => 'admin/edithabitad']);
	Route::post('edit_habitad', ['uses' => 'HabitacionController@edit_habitad', 'as' => 'admin/edit_habitad']);
	Route::post('editrestaurante', ['uses' => 'DashboardController@editrestaurante', 'as' => 'admin/editrestaurante']);
	Route::post('edit_restaurat', ['uses' => 'RestauranteController@edit_restaurat', 'as' => 'admin/edit_restaurat']);
	Route::post('editgalerirestaurat', ['uses' => 'BannergaleriController@editgalerirestaurat', 'as' => 'admin/editgalerirestaurat']);
	Route::post('editgalerinstalaciones', ['uses' => 'BannergaleriController@editgalerinstalaciones', 'as' => 'admin/editgalerinstalaciones']);
	Route::post('editservicioshome', ['uses' => 'ServiciosController@editservicioshome', 'as' => 'admin/editservicioshome']);
	Route::post('editcomercialeshome', ['uses' => 'GaleriaController@editcomercialeshome', 'as' => 'admin/editcomercialeshome']);
	Route::post('editadoeventocorporativo', ['uses' => 'DashboardController@editadoeventocorporativo', 'as' => 'admin/editadoeventocorporativo']);
	Route::post('editadoeventos_corporativo', ['uses' => 'EventosController@editadoeventos_corporativo', 'as' => 'admin/editadoeventos_corporativo']);
	Route::post('actv_corporativo', ['uses' => 'EventosController@actv_corporativo', 'as' => 'admin/actv_corporativo']);	
	Route::post('editadogaleriahome', ['uses' => 'DashboardController@editadogaleriahome', 'as' => 'admin/editadogaleriahome']);
	Route::post('savegaleriahome', ['uses' => 'BannergaleriController@savegaleriahome', 'as' => 'admin/savegaleriahome']);
	//POST-LIST
	Route::post('viewasociacion', ['uses' => 'DashboardController@viewasociacion', 'as' => 'admin/viewasociacion']);
	Route::post('listmesyeareventohome', ['uses' => 'EventosController@listmesyeareventohome', 'as' => 'admin/listmesyeareventohome']);
	//POST-STATE
	Route::post('estadogaleriahome', ['uses' => 'GaleriaController@estadogaleriahome', 'as' => 'admin/estadogaleriahome']);
	Route::post('estadopromocioneshome', ['uses' => 'PromocionesController@estadopromocioneshome', 'as' => 'admin/estadopromocioneshome']);
	Route::post('estadobannerinferior', ['uses' => 'GaleriaController@estadobannerinferior', 'as' => 'admin/estadobannerinferior']);
	Route::post('asociarpromogaleria', ['uses' => 'GaleriaController@asociarpromogaleria', 'as' => 'admin/asociarpromogaleria']);
	Route::post('lanzarmesevetoshome', ['uses' => 'MesesController@lanzarmesevetoshome', 'as' => 'admin/lanzarmesevetoshome']);
	Route::post('estadoavatares', ['uses' => 'UsersController@estadoavatares', 'as' => 'admin/estadoavatares']);
	//POST-DELETE
	Route::post('delete_galeria', ['uses' => 'GaleriaController@delete_galeria', 'as' => 'admin/delete_galeria']);
	Route::post('delete_promo', ['uses' => 'PromocionesController@delete_promo', 'as' => 'admin/delete_promo']);
	Route::post('delete_asociacion', ['uses' => 'GaleriaController@delete_asociacion', 'as' => 'admin/delete_asociacion']);
	Route::post('delete_bsalud', ['uses' => 'GaleriaController@delete_bsalud', 'as' => 'admin/delete_bsalud']);
	Route::post('delete_aremover', ['uses' => 'UsersController@delete_aremover', 'as' => 'admin/delete_aremover']);
	Route::post('eraseventohome', ['uses' => 'EventosController@eraseventohome', 'as' => 'admin/eraseventohome']);
	Route::post('erasegalerihabitad', ['uses' => 'BannergaleriController@erasegalerihabitad', 'as' => 'admin/erasegalerihabitad']);
	Route::post('erasehabitad', ['uses' => 'HabitacionController@erasehabitad', 'as' => 'admin/erasehabitad']);
	Route::post('erasefijasection', ['uses' => 'BannergaleriController@erasefijasection', 'as' => 'admin/erasefijasection']);
	Route::post('erasegalerirestaurante', ['uses' => 'BannergaleriController@erasegalerirestaurante', 'as' => 'admin/erasegalerirestaurante']);
	Route::post('erasegalerinstalaciones', ['uses' => 'BannergaleriController@erasegalerinstalaciones', 'as' => 'admin/erasegalerinstalaciones']);
	Route::post('delete_users', ['uses' => 'UsersController@delete_users', 'as' => 'admin/delete_users']);
	Route::post('erasegalesecun', ['uses' => 'BannergaleriController@erasegalesecun', 'as' => 'admin/erasegalesecun']);
	//AUTENTIFICAR
	Route::post('dashboard', ['uses' => 'LoginController@dashboard', 'as' => 'admin/dashboard']);
});
