<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Proyectos extends Model
{
    protected $table = "proyectos";
	
	protected $fillable = [
		"proyectos_picone",
		"proyectos_pictwo",
		"proyectos_picthrid",
		"proyectos_picfour",
		"proyectos_pdf",
	];

	protected $hidden = [
      'remember_token',
	];
					
	public static function addproyectoshome($inputs){
		$finder = Proyectos::where("id","1")->first();
		if(count($finder)==1){
			$pdf = ""; $uno = ""; $dos = ""; $tres = ""; $cuatro = "";
			$date = Carbon::now(-4);
			
			if(array_key_exists("proyecto_curso",$inputs) && $inputs["proyecto_curso"]!=""){//PDF
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["proyecto_curso"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$pdf = (str_replace($search, $replace, $name));
				\Storage::disk('proyectos')->put($pdf,  \File::get($inputs["proyecto_curso"]));
				\Storage::disk('proyectos')->delete($finder->proyectos_pdf);
				$pdf = ('img/elhotel/centroempresarial/'.$pdf);
			}else{
				$pdf = $finder->proyectos_pdf;
			}
			
			if(array_key_exists("file_one",$inputs) && $inputs["file_one"]!=""){//1
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$uno = (str_replace($search, $replace, $name));
				\Storage::disk('proyectos')->put($uno,  \File::get($inputs["file_one"]));
				\Storage::disk('proyectos')->delete($finder->proyectos_picone);
				$uno = ('img/elhotel/centroempresarial/'.$uno);
			}else{
				$uno = $finder->proyectos_picone;
			}
			
			if(array_key_exists("file_two",$inputs) && $inputs["file_two"]!=""){//2
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_two"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$dos = (str_replace($search, $replace, $name));
				\Storage::disk('proyectos')->put($dos,  \File::get($inputs["file_two"]));
				\Storage::disk('proyectos')->delete($finder->proyectos_pictwo);
				$dos = ('img/elhotel/centroempresarial/'.$dos);
			}else{
				$dos = $finder->proyectos_pictwo;
			}
			
			if(array_key_exists("file_thrid",$inputs) && $inputs["file_thrid"]!=""){//3
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_thrid"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$tres = (str_replace($search, $replace, $name));
				\Storage::disk('proyectos')->put($tres,  \File::get($inputs["file_thrid"]));
				\Storage::disk('proyectos')->delete($finder->proyectos_picthrid);
				$tres = ('img/elhotel/centroempresarial/'.$tres);
			}else{
				$tres = $finder->proyectos_picthrid;
			}
			
			if(array_key_exists("file_four",$inputs) && $inputs["file_four"]!=""){//4
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_four"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$cuatro = (str_replace($search, $replace, $name));
				\Storage::disk('proyectos')->put($cuatro,  \File::get($inputs["file_four"]));
				\Storage::disk('proyectos')->delete($finder->proyectos_picfour);
				$cuatro = ('img/elhotel/centroempresarial/'.$cuatro);
			}else{
				$cuatro = $finder->proyectos_picfour;
			}
			
			$finder->update([
				"proyectos_picone" => $uno,
				"proyectos_pictwo" => $dos,
				"proyectos_picthrid" => $tres,
				"proyectos_picfour" => $cuatro,
				"proyectos_pdf" => $pdf,
			]);
			
			return 1;
		}return 0;
	}
	
	public static function obtenerProyectos(){
		return Proyectos::where("id","1")->first()->toJson();
	}
}
