<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Bannergaleri extends Model
{
    protected $table = "bannergaleri";

	protected $fillable = [
		"bannergaleri_section",
		"bannergaleri_id",
		"bannergaleri_destacar",
		"bannergaleri_rutaimg",
		"bannergaleri_link",
		"bannergaleri_texto"
	];

	//OBTENER GALERÍA SEGUN ID
	public static function getGaleriaRestID($section,$id){
		return Bannergaleri::where("bannergaleri_section",$section)->where("bannergaleri_id",$id)->wherein("bannergaleri_destacar",["0","1"])->get()->toJson();
	}

	//OBTENER TODAS GALERIA HABITACION 
	public static function getGaleriahabitacionall($data){//->groupBy('bannergaleri_id')
		return Bannergaleri::where('bannergaleri_section',$data['seccion'])->wherein('bannergaleri_destacar',["0","1"])->get()->toJson();
	}

	//OBTENER GALERIA HABITACION ID
	public static function getGaleriahabitacion($data){
		return Bannergaleri::where('bannergaleri_section',$data['seccion'])->where('bannergaleri_id',base64_decode($data['galeria']))->wherein('bannergaleri_destacar',["0","1"])->get();
	}
	//OBTENER FIJAS REDTAURANTE ID
	public static function getFijasrestaurante($data){
		return Bannergaleri::select('bannergaleri_rutaimg AS img')->where('bannergaleri_section',"3")->where('bannergaleri_id',base64_decode($data['galeria']))->where('bannergaleri_destacar',"2")->get();
	}
	//OBTENER GALERIA RESTAURANT
	public static function getGalerirest(){
		return Bannergaleri::where('bannergaleri_section',"3")->where('bannergaleri_id',"1")->wherein('bannergaleri_destacar',["0","1"])->get();
	}
	//OBTENER GALERIA INSTALACIONES
	public static function getGaleriinsta(){
		return Bannergaleri::where('bannergaleri_section',"1")->where('bannergaleri_id',"1")->where('bannergaleri_destacar',"0")->get();
	}
	//OBTENER FIJA SECCION
	public static function getFijaseccion($data){
		return Bannergaleri::select('bannergaleri_rutaimg AS img')->where('bannergaleri_section',$data['seccion'])->where('bannergaleri_id',"0")->where('bannergaleri_destacar',"2")->get();
	}
	//CREAR GALAERIA HABITACION STORE
	public static function addGalerihabitad($request){
		if(array_key_exists("seccion",$request) && array_key_exists("habitad",$request)){
			Bannergaleri::create([
				"bannergaleri_section" => $request['seccion'],//seccion
				"bannergaleri_id" => $request['habitad'],//id habitacion
				"bannergaleri_destacar" => "0",
				"bannergaleri_rutaimg" => "img/default.jpg"
			]);
			return 1;
		}else{
			return 0;
		}
	}
	//STORE FILE GALAERIA HABITACION
	public static function updateFilehabitad($habitad,$file,$f_name,$destacado,$clave){
		$date = Carbon::now(-4);
		$habitacion =  Bannergaleri::where('bannergaleri_section','2')->where('bannergaleri_id',base64_decode($habitad))->where('bannergaleri_rutaimg',$f_name)->wherein('bannergaleri_destacar',["0","1"])->first();
		if(count($habitacion)==1){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$clave.$file->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			\Storage::disk('habitadgaleri')->put($one,  \File::get($file));
			$del_file = explode('/',$habitacion->bannergaleri_rutaimg);
			if (strcmp($habitacion->bannergaleri_rutaimg,"img/default.jpg") !== 0) {
				\Storage::disk('habitadgaleri')->delete($del_file[3]);//borro imagen vieja
			}
			$habitacion->update([
				"bannergaleri_rutaimg" => "img/habitaciones/galeria/".$one,
				"bannergaleri_destacar" => $destacado
			]);
			return 1;
		}
		return 0;
	}
	//EDIT DESTACAR GALAERIA HABITACION
	public static function updateDestacarhabitad($habitad,$f_name,$destacado){
		$habitacion =  Bannergaleri::where('bannergaleri_section','2')->where('bannergaleri_id',base64_decode($habitad))->where('bannergaleri_rutaimg',$f_name)->wherein('bannergaleri_destacar',["0","1"])->first();
		if(count($habitacion)==1){
			$habitacion->update([
				"bannergaleri_destacar" => $destacado
			]);
			return 1;
		}else{
			return 0;
		}
	}
	//RESET IMG GALERIA HABITACION BORRAR
	public static function erasegalerihabitad($data){
		$habitacion =  Bannergaleri::where('bannergaleri_section','2')->where('bannergaleri_id',base64_decode($data['delgaleria']))->where('bannergaleri_rutaimg',$data['delimage'])->wherein('bannergaleri_destacar',["0"])->first();
		if(count($habitacion)==1){
			$habitacion->update([
				"bannergaleri_rutaimg" => "img/default.jpg"
			]);
			if (strcmp($data['delimage'],"img/default.jpg") !== 0) {
				$del_file = explode('/',$data['delimage']);
				\Storage::disk('habitadgaleri')->delete($del_file[3]);//borro imagen vieja
			}
			return 1;
		}else{
			return 0;
		}
	}
	//ERASE TODA LA GALERIA HABITACION
	public static function eraseallgalerihabitad($data){
		$galeria = Bannergaleri::where('bannergaleri_section','2')->where('bannergaleri_id',base64_decode($data['target']))->wherein('bannergaleri_destacar',["0","1"])->get();
		foreach($galeria AS $gale){
			if (strcmp($gale['bannergaleri_rutaimg'],"img/default.jpg") !== 0) {
				$del_file = explode('/',$gale['bannergaleri_rutaimg']);
				\Storage::disk('habitadgaleri')->delete($del_file[3]);//borro imagen vieja
			}
		}
		Bannergaleri::where('bannergaleri_section','2')->where('bannergaleri_id',base64_decode($data['target']))->wherein('bannergaleri_destacar',["0","1"])->delete();
		return 1;
	}
	//CREO ACTUALIZO IMAGEN FIJA EN UNA SECCION
	public static function imagesfija($data){
		$date = Carbon::now(-4);
		$id_ = "0";
		if($data['section'] == '2'){
			$section = 'habitad';
			$dir = "habitaciones";
		}else if($data['section'] == '3'){
			$section = 'restaurante';
			$dir = "restaurantes";
		}
		if(array_key_exists("id_",$data)){
			$id_ = $data['id_'];
		}
		$fija = Bannergaleri::where('bannergaleri_section',$data['section'])->where('bannergaleri_id',$id_)->where('bannergaleri_destacar','2')->first();
		if(count($fija)==1){//actualizo
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$data['file']->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			\Storage::disk($section)->put($one,  \File::get($data['file']));
			$del_file = explode('/',$fija->bannergaleri_rutaimg);
			\Storage::disk($section)->delete($del_file[2]);//borro imagen vieja
			$fija->update([
				"bannergaleri_rutaimg" => "img/".$dir."/".$one
			]);
			return 1;
		}else if(count($fija)==0){//creo
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$data['file']->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			\Storage::disk($section)->put($one,  \File::get($data['file']));
			\Storage::disk($section)->put($one,  \File::get($data['file']));
			Bannergaleri::create([
				"bannergaleri_section" => $data['section'],//seccion
				"bannergaleri_id" => $id_,
				"bannergaleri_destacar" => "2",
				"bannergaleri_rutaimg" => "img/".$dir."/".$one
			]);
			return 1;
		}
		return 0;
	}
	//CREO ACTUALIZO BANNER EN UNA SECCION
	public static function bannersection($data){
		$date = Carbon::now(-4);
		$baner = Bannergaleri::where('bannergaleri_section',$data['section'])->where('bannergaleri_id','0')->where('bannergaleri_destacar','0')->first();		
		if($data['section'] == '2'){
			$section = 'habitad';
			$dir = "habitaciones";
		}else if($data['section'] == '1'){
			$section = 'elhotel';
			$dir = "elhotel";
		}
		if(count($baner)==1){//actualizo
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$data['file']->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			\Storage::disk($section)->put($one,  \File::get($data['file']));
			$del_file = explode('/',$baner->bannergaleri_rutaimg);
			\Storage::disk($section)->delete($del_file[2]);//borro imagen vieja
			$baner->update([
				"bannergaleri_rutaimg" => "img/".$dir."/".$one
			]);
			return 1;
		}else if(count($baner)==0){//creo
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$data['file']->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			\Storage::disk($section)->put($one,  \File::get($data['file']));
			\Storage::disk($section)->put($one,  \File::get($data['file']));
			Bannergaleri::create([
				"bannergaleri_section" => $data['section'],//seccion
				"bannergaleri_id" => "0",
				"bannergaleri_destacar" => "0",
				"bannergaleri_rutaimg" => "img/".$dir."/".$one
			]);
			return 1;
		}
		return 0;
	}
	//RETORNAR IMAGEN FIJA
	public static function getfijasection($section){
		$fija = Bannergaleri::where('bannergaleri_section',$section)->where('bannergaleri_id','0')->where('bannergaleri_destacar','2')->first();
		if(count($fija) != 1){//noesta creada
			$fija = "img/default.jpg";
			return $fija;
		}
		return $fija->bannergaleri_rutaimg;
	}
	//RETORNAR IMAGEN BANNER
	public static function getbannersection($section){
		$fija = Bannergaleri::where('bannergaleri_section',$section)->where('bannergaleri_id','0')->where('bannergaleri_destacar','0')->first();
		if(count($fija) != 1){//noesta creada
			$fija = "img/default.jpg";
			return $fija;
		}
		return $fija->bannergaleri_rutaimg;
	}
	//ERASE IMAGEN FIJA
	public static function erasefijasection($section){
		$id_ = "0";
		if(array_key_exists("id_",$section)){
			$id_ = $section['id_'];
		}
		$fija = Bannergaleri::where('bannergaleri_section',$section['section'])->where('bannergaleri_id',$id_)->where('bannergaleri_destacar','2')->first();
		if(count($fija) == 1){
			$del_file = explode('/',$fija->bannergaleri_rutaimg);
			\Storage::disk('habitad')->delete($del_file[2]);//borro imagen vieja
			$fija ->delete();
			return 1;
		}
		return 0;
	}

	//STORE FILE GALAERIA RESTAURANTE
	public static function updateFilerestaurante($file,$f_name,$destacado,$clave,$id){
		$date = Carbon::now(-4);
		$restaurante =  Bannergaleri::where('bannergaleri_section','3')->where('bannergaleri_id',(int)$id)->where('bannergaleri_rutaimg',$f_name)->wherein('bannergaleri_destacar',["0","1"])->first();
		if(count($restaurante)==1){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$clave.$file->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			\Storage::disk('restauratgaleri')->put($one,  \File::get($file));
			$del_file = explode('/',$restaurante->bannergaleri_rutaimg);
			if (strcmp($restaurante->bannergaleri_rutaimg,"img/default.jpg") !== 0) {
				\Storage::disk('restauratgaleri')->delete($del_file[3]);//borro imagen vieja
			}
			$restaurante->update([
				"bannergaleri_rutaimg" => "img/restaurantes/galeria/".$one,
				"bannergaleri_destacar" => $destacado
			]);
			return 1;
		}
		return 0;
	}
	//EDIT DESTACAR GALAERIA RESTAURANTE
	public static function updateDestacarrestaurante($f_name,$destacado,$id){
		$restaurante =  Bannergaleri::where('bannergaleri_section','3')->where('bannergaleri_id',(int)$id)->where('bannergaleri_rutaimg',$f_name)->wherein('bannergaleri_destacar',["0","1"])->first();
		if(count($restaurante)==1){
			$restaurante->update([
				"bannergaleri_destacar" => $destacado
			]);
			return 1;
		}else{
			return 0;
		}
	}
	//RESET IMG GALERIA RESTAURANTE BORRAR
	public static function erasegalerirestaurante($data){
		$habitacion =  Bannergaleri::where('bannergaleri_section','3')->where('bannergaleri_id','1')->where('bannergaleri_rutaimg',$data['delimage'])->wherein('bannergaleri_destacar',["0"])->first();
		if(count($habitacion)==1){
			$habitacion->update([
				"bannergaleri_rutaimg" => "img/default.jpg"
			]);
			if (strcmp($data['delimage'],"img/default.jpg") !== 0) {
				$del_file = explode('/',$data['delimage']);
				\Storage::disk('habitadgaleri')->delete($del_file[3]);//borro imagen vieja
			}
			return 1;
		}else{
			return 0;
		}
	}
	//STORE FILE GALERIA INSTALACIONES
	public static function editgalerinstalaciones ($file,$item,$clave){
		$date = Carbon::now(-4);
		$instalacion =  Bannergaleri::where('bannergaleri_section','1')->where('bannergaleri_id','1')->where('id',$item)->where('bannergaleri_destacar',"0")->first();
		if(count($instalacion)==1){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$clave.$file->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			\Storage::disk('instalaciones')->put($one,  \File::get($file));
			$del_file = explode('/',$instalacion->bannergaleri_rutaimg);
			if (strcmp($instalacion->bannergaleri_rutaimg,"img/default.jpg") !== 0) {
				\Storage::disk('instalaciones')->delete($del_file[3]);//borro imagen vieja
			}
			$instalacion->update([
				"bannergaleri_rutaimg" => "img/elhotel/instalaciones/".$one
			]);
			return 1;
		}
		return 0;
	}
	//RESET IMG GALERIA RESTAURANTE BORRAR
	public static function erasegalerinstalaciones($data){
		$instalacion =  Bannergaleri::where('bannergaleri_section','1')->where('bannergaleri_id','1')->where('id',base64_decode($data['delgaleria']))->where('bannergaleri_destacar',"0")->first();
		if(count($instalacion)==1){
			$instalacion->update([
				"bannergaleri_rutaimg" => "img/default.jpg"
			]);
			if (strcmp($data['delimage'],"img/default.jpg") !== 0) {
				$del_file = explode('/',$data['delimage']);
				\Storage::disk('instalaciones')->delete($del_file[3]);//borro imagen vieja
			}
			return 1;
		}else{
			return 0;
		}
	}
	//EDIT ITEM GALSECU HOME
	public static function savegaleriahome($request){
		$date = Carbon::now(-4);
		$link = $texto = NULL;
		if(array_key_exists("galeria",$request) && array_key_exists("galeri_file1",$request)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["galeri_file1"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			$item = Bannergaleri::where('id',base64_decode($request["galeria"]))->first();
			if(count($item) == 1){
				$link = $item->bannergaleri_link;
				$texto = $item->bannergaleri_texto;

				if(array_key_exists("link",$request) &&  !is_null($request["link"])){
					$link = $request["link"];
				}else if(array_key_exists("restaurante",$request) &&  !is_null($request["restaurante"])){
					$link = $request["restaurante"];
				}else if(array_key_exists("habitaciones",$request) &&  !is_null($request["habitaciones"])){
					$link = $request["habitaciones"];
				}
				if(array_key_exists("text",$request) &&  strlen($request["text"]) >= 20){
					$texto = $request["text"];
				}else{
					$link = $texto = NULL;
				}

				\Storage::disk('secundaria')->put($one,  \File::get($request["galeri_file1"]));

				$del_file = explode('/',$item->bannergaleri_rutaimg);
				if (strcmp($item->bannergaleri_rutaimg,"img/default.jpg") !== 0) {
					\Storage::disk('secundaria')->delete($del_file[3]);//borro imagen vieja
				}
				$item->update([
					"bannergaleri_destacar" => '1',
					"bannergaleri_rutaimg" => "img/home/secundaria/".$one,
					"bannergaleri_link" => $link,
					"bannergaleri_texto" => $texto
				]);
				return 1;
			}
			return 0;
		}else{
			$item = Bannergaleri::where('id',base64_decode($request["galeria"]))->first();
			if(count($item) == 1){
				$link = $item->bannergaleri_link;
				$texto = $item->bannergaleri_texto;

				if(array_key_exists("link",$request) &&  !is_null($request["link"])){
					$link = $request["link"];
				}else if(array_key_exists("restaurante",$request) &&  !is_null($request["restaurante"])){
					$link = $request["restaurante"];
				}else if(array_key_exists("habitaciones",$request) &&  !is_null($request["habitaciones"])){
					$link = $request["habitaciones"];
				}
				if(array_key_exists("text",$request) && strlen($request["text"]) >= 20){
					$texto = $request["text"];
				}else{
					$link = $texto = NULL;
				}
				$item->update([
					"bannergaleri_destacar" => '1',
					"bannergaleri_link" => $link,
					"bannergaleri_texto" => $texto
				]);
				return 1;
			}
			return 0;
		}
	}
	//EDIT ELIMINTAR ITEM GALSECU HOME
	public static function erasegalesecun($request){

		$item = Bannergaleri::where('id',base64_decode($request["galeria"]))->first();

		if(count($item) == 1){
			$del_file = explode('/',$item->bannergaleri_rutaimg);
			if (strcmp($item->bannergaleri_rutaimg,"img/default.jpg") !== 0) {
				\Storage::disk('secundaria')->delete($del_file[3]);//borro imagen vieja
			}
			$item->update([
				"bannergaleri_destacar" => "0",
				"bannergaleri_rutaimg" => "img/default.jpg",
				"bannergaleri_link" => NULL,
				"bannergaleri_texto" => NULL
			]);
			return 1;
		}
		return 0;
	}
}
