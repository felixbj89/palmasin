<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Historial;
use App\Promociones;

class Galeria extends Model
{
    protected $table = "galeria";

	protected $fillable = [
		"galeria_pos",
		"galeria_path",
		'galeria_status',
		"promo_id",
		"user_id"
	];

	protected $hidden = [
      'remember_token',
	];

	public function promociones(){
        return $this->hasMany('App\Promociones',"id","promo_id");
    }

	public function usuario(){
        return $this->hasMany('App\Usuario',"id","user_id");
    }

	//ADD-GALERIAHOME
	public static function addgaleriahome($inputs){
		$usuario = \Cache::store('database')->get("usuario");
		
		$galeria = Galeria::all();
		$date = Carbon::now(-4);
		$uno = ""; $dos = ""; $tres = ""; $cuatro = ""; $cinco = ""; $seis = "";
		if(array_key_exists("file_one",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$uno = (str_replace($search, $replace, $name));
			if($galeria[0]->galeria_path=="img/home/galeria/default.jpg"){//SIN IMÁGENES
				\Storage::disk('galeriahome')->put($uno,  \File::get($inputs["file_one"]));
				$uno = ("img/home/galeria/".$uno);
			}else{//EXISTE UNA IMAGEN YA PARA ESTA POSICIÓN
				$rutaone = "img/historial/galeriahome/";
				if(file_exists($galeria[0]->galeria_path)){
					$imagen = substr($galeria[0]->galeria_path,strripos($galeria[0]->galeria_path,"/")+1);
					$rutaone .= $imagen;
					if(copy($galeria[0]->galeria_path,$rutaone)){
						unlink($galeria[0]->galeria_path);
					}
				}
				
				Galeria::create([
					"galeria_pos" => "1",
					"galeria_path" => $rutaone,
					"galeria_status" => "1",
					"promo_id" => $galeria[0]->promo_id,
					"user_id" => $usuario->id
				]);
			
				\Storage::disk('galeriahome')->put($uno,  \File::get($inputs["file_one"]));
				$uno = ("img/home/galeria/".$uno);
			}
			
			Galeria::where("id",$galeria[0]->id)->update([
				"galeria_path" => $uno,
				"galeria_status" => "0",
				"user_id" => $usuario->id
			]);

			$uno = "1";
		}

		if(array_key_exists("file_two",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_two"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$dos = (str_replace($search, $replace, $name));
			if($galeria[1]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($dos,  \File::get($inputs["file_two"]));
				$dos = ("img/home/galeria/".$dos);
			}else{
				$rutatwo = "img/historial/galeriahome/";
				if(file_exists($galeria[1]->galeria_path)){
					$imagen = substr($galeria[1]->galeria_path,strripos($galeria[1]->galeria_path,"/")+1);
					$rutatwo .= $imagen;
					if(copy($galeria[1]->galeria_path,$rutatwo)){
						unlink($galeria[1]->galeria_path);
					}
				}

				Galeria::create([
					"galeria_pos" => "2",
					"galeria_path" => $rutatwo,
					"galeria_status" => "1",
					"promo_id" => $galeria[1]->promo_id,
					"user_id" => $usuario->id
				]);
				
				\Storage::disk('galeriahome')->put($dos,  \File::get($inputs["file_two"]));
				$dos = ("img/home/galeria/".$dos);
			}

			Galeria::where("id",$galeria[1]->id)->update([
				"galeria_path" => $dos,
				"galeria_status" => "0",
				"user_id" => $usuario->id
			]);

			$dos = "1";
		}

		if(array_key_exists("file_thrid",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_thrid"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$tres = (str_replace($search, $replace, $name));
			if($galeria[2]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($tres,  \File::get($inputs["file_thrid"]));
				$tres = ("img/home/galeria/".$tres);
			}else{
				$rutathrid = "img/historial/galeriahome/";
				if(file_exists($galeria[2]->galeria_path)){
					$imagen = substr($galeria[2]->galeria_path,strripos($galeria[2]->galeria_path,"/")+1);
					$rutathrid .= $imagen;
					if(copy($galeria[2]->galeria_path,$rutathrid)){
						unlink($galeria[2]->galeria_path);
					}
				}

				Galeria::create([
					"galeria_pos" => "3",
					"galeria_path" => $rutathrid,
					"galeria_status" => "1",
					"promo_id" => $galeria[2]->promo_id,
					"user_id" => $usuario->id
				]);
				
				\Storage::disk('galeriahome')->put($tres,  \File::get($inputs["file_thrid"]));
				$tres = ("img/home/galeria/".$tres);
			}

			Galeria::where("id",$galeria[2]->id)->update([
				"galeria_path" => $tres
			]);

			$tres = "1";
		}

		if(array_key_exists("file_four",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_four"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$cuatro = (str_replace($search, $replace, $name));
			if($galeria[3]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($cuatro,  \File::get($inputs["file_four"]));
				$cuatro = ("img/home/galeria/".$cuatro);
			}else{
				$rutafour = "img/historial/galeriahome/";
				if(file_exists($galeria[3]->galeria_path)){
					$imagen = substr($galeria[3]->galeria_path,strripos($galeria[3]->galeria_path,"/")+1);
					$rutafour .= $imagen;
					if(copy($galeria[3]->galeria_path,$rutafour)){
						unlink($galeria[3]->galeria_path);
					}
				}

				Galeria::create([
					"galeria_pos" => "4",
					"galeria_path" => $rutafour,
					"galeria_status" => "1",
					"promo_id" => $galeria[3]->promo_id,
					"user_id" => $usuario->id
				]);
				
				\Storage::disk('galeriahome')->put($cuatro,  \File::get($inputs["file_four"]));
				$cuatro = ("img/home/galeria/".$cuatro);
			}

			Galeria::where("id",$galeria[3]->id)->update([
				"galeria_path" => $cuatro
			]);

			$cuatro = "1";
		}

		if(array_key_exists("file_five",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_five"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$cinco = (str_replace($search, $replace, $name));
			if($galeria[4]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($cinco,  \File::get($inputs["file_five"]));
				$cinco = ("img/home/galeria/".$cinco);
			}else{
				$rutafive = "img/historial/galeriahome/";
				if(file_exists($galeria[4]->galeria_path)){
					$imagen = substr($galeria[4]->galeria_path,strripos($galeria[4]->galeria_path,"/")+1);
					$rutafive .= $imagen;
					if(copy($galeria[4]->galeria_path,$rutafive)){
						unlink($galeria[4]->galeria_path);
					}
				}

				Galeria::create([
					"galeria_pos" => "5",
					"galeria_path" => $rutafive,
					"galeria_status" => "1",
					"promo_id" => $galeria[4]->promo_id,
					"user_id" => $usuario->id
				]);
				
				\Storage::disk('galeriahome')->put($cinco,  \File::get($inputs["file_five"]));
				$cinco = ("img/home/galeria/".$cinco);
			}

			Galeria::where("id",$galeria[4]->id)->update([
				"galeria_path" => $cinco
			]);

			$cinco = "1";
		}

		if(array_key_exists("file_six",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_six"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$seis = (str_replace($search, $replace, $name));
			if($galeria[5]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($seis,  \File::get($inputs["file_six"]));
				$seis = ("img/home/galeria/".$seis);
			}else{
				$rutasix = "img/historial/galeriahome/";
				if(file_exists($galeria[5]->galeria_path)){
					$imagen = substr($galeria[5]->galeria_path,strripos($galeria[5]->galeria_path,"/")+1);
					$rutasix .= $imagen;
					if(copy($galeria[5]->galeria_path,$rutasix)){
						unlink($galeria[5]->galeria_path);
					}
				}

				Galeria::create([
					"galeria_pos" => "6",
					"galeria_path" => $rutasix,
					"galeria_status" => "1",
					"promo_id" => $galeria[5]->promo_id,
					"user_id" => $usuario->id
				]);
				
				\Storage::disk('galeriahome')->put($seis,  \File::get($inputs["file_six"]));
				$seis = ("img/home/galeria/".$seis);
			}

			Galeria::where("id",$galeria[5]->id)->update([
				"galeria_path" => $seis
			]);

			$seis = "1";
		}

		if($uno=="1" || $dos=="1" || $tres=="1" || $cuatro=="1" || $cinco=="1" || $seis=="1"){
			return 1;
		}

		return 0;
	}

	//EDIT-GALERIAHOME
	public static function editgaleriahome($inputs){
		$usuario = \Cache::store('database')->get("usuario");
		$galeria = Galeria::all();
		$date = Carbon::now(-4);
		$uno = ""; $dos = ""; $tres = ""; $cuatro = ""; $cinco = ""; $seis = "";
		if(array_key_exists("file_one",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$uno = (str_replace($search, $replace, $name));
			if($galeria[0]->galeria_path=="img/home/galeria/default.jpg"){//SIN IMÁGENES
				\Storage::disk('galeriahome')->put($uno,  \File::get($inputs["file_one"]));
				$uno = ("img/home/galeria/".$uno);
			}else{//EXISTE UNA IMAGEN YA PARA ESTA POSICIÓN
				if(file_exists($galeria[0]->galeria_path)){
					unlink($galeria[0]->galeria_path);
				}
				
				\Storage::disk('galeriahome')->put($uno,  \File::get($inputs["file_one"]));
				$uno = ("img/home/galeria/".$uno);
			}
			
			Galeria::where("id",$galeria[0]->id)->update([
				"galeria_path" => $uno,
				"galeria_status" => "0",
				"user_id" => $usuario->id
			]);

			$uno = "1";
		}

		if(array_key_exists("file_two",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_two"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$dos = (str_replace($search, $replace, $name));
			if($galeria[1]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($dos,  \File::get($inputs["file_two"]));
				$dos = ("img/home/galeria/".$dos);
			}else{
				if(file_exists($galeria[1]->galeria_path)){
					unlink($galeria[1]->galeria_path);
				}
				
				\Storage::disk('galeriahome')->put($dos,  \File::get($inputs["file_two"]));
				$dos = ("img/home/galeria/".$dos);
			}

			Galeria::where("id",$galeria[1]->id)->update([
				"galeria_path" => $dos,
				"galeria_status" => "0",
				"user_id" => $usuario->id
			]);

			$dos = "1";
		}

		if(array_key_exists("file_thrid",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_thrid"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$tres = (str_replace($search, $replace, $name));
			if($galeria[2]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($tres,  \File::get($inputs["file_thrid"]));
				$tres = ("img/home/galeria/".$tres);
			}else{
				if(file_exists($galeria[2]->galeria_path)){
					unlink($galeria[2]->galeria_path);
				}

				\Storage::disk('galeriahome')->put($tres,  \File::get($inputs["file_thrid"]));
				$tres = ("img/home/galeria/".$tres);
			}

			Galeria::where("id",$galeria[2]->id)->update([
				"galeria_path" => $tres
			]);

			$tres = "1";
		}

		if(array_key_exists("file_four",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_four"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$cuatro = (str_replace($search, $replace, $name));
			if($galeria[3]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($cuatro,  \File::get($inputs["file_four"]));
				$cuatro = ("img/home/galeria/".$cuatro);
			}else{
				if(file_exists($galeria[3]->galeria_path)){
					unlink($galeria[3]->galeria_path);
				}
				
				\Storage::disk('galeriahome')->put($cuatro,  \File::get($inputs["file_four"]));
				$cuatro = ("img/home/galeria/".$cuatro);
			}

			Galeria::where("id",$galeria[3]->id)->update([
				"galeria_path" => $cuatro
			]);

			$cuatro = "1";
		}

		if(array_key_exists("file_five",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_five"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$cinco = (str_replace($search, $replace, $name));
			if($galeria[4]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($cinco,  \File::get($inputs["file_five"]));
				$cinco = ("img/home/galeria/".$cinco);
			}else{
				$rutafive = "img/historial/galeriahome/";
				if(file_exists($galeria[4]->galeria_path)){
					unlink($galeria[4]->galeria_path);
				}
				
				\Storage::disk('galeriahome')->put($cinco,  \File::get($inputs["file_five"]));
				$cinco = ("img/home/galeria/".$cinco);
			}

			Galeria::where("id",$galeria[4]->id)->update([
				"galeria_path" => $cinco
			]);

			$cinco = "1";
		}

		if(array_key_exists("file_six",$inputs)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_six"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$seis = (str_replace($search, $replace, $name));
			if($galeria[5]->galeria_path=="img/home/galeria/default.jpg"){
				\Storage::disk('galeriahome')->put($seis,  \File::get($inputs["file_six"]));
				$seis = ("img/home/galeria/".$seis);
			}else{
				$rutasix = "img/historial/galeriahome/";
				if(file_exists($galeria[5]->galeria_path)){
					unlink($galeria[5]->galeria_path);
				}
				
				\Storage::disk('galeriahome')->put($seis,  \File::get($inputs["file_six"]));
				$seis = ("img/home/galeria/".$seis);
			}

			Galeria::where("id",$galeria[5]->id)->update([
				"galeria_path" => $seis
			]);

			$seis = "1";
		}

		if($uno=="1" || $dos=="1" || $tres=="1" || $cuatro=="1" || $cinco=="1" || $seis=="1"){
			return 1;
		}return 0;
	}

	//STATE-GALERIAHOME
	public static function estadogaleriahome($id){
		$finder = Galeria::getGaleriaID($id);
		if(count($finder)==1){
			$ruta_activa = ""; $ruta_historial = "";
			$imagen_actual = Galeria::where("galeria_pos",$finder->galeria_pos)->where("galeria_status","0")->first();
			if(file_exists($imagen_actual->galeria_path)){//EXISTE MI GALERIA ACTUAL.
				$imagen = substr($imagen_actual->galeria_path,strripos($imagen_actual->galeria_path,"/")+1);
				$ruta_historial = "img/historial/galeriahome/".$imagen;
				if($imagen_actual->galeria_path!="img/home/galeria/default.jpg" && copy($imagen_actual->galeria_path,$ruta_historial)){
					unlink($imagen_actual->galeria_path);
				}
			}

			if(file_exists($finder->galeria_path)){//EXISTE MI GALERIA EN EL HISTORIAL.
				$imagen = substr($finder->galeria_path,strripos($finder->galeria_path,"/")+1);
				$ruta_activa = "img/home/galeria/".$imagen;
				if(copy($finder->galeria_path,$ruta_activa)){
					unlink($finder->galeria_path);
				}
			}

			$finder->update([
				"galeria_path" => $ruta_historial
			]);

			$imagen_actual->update([
				"galeria_path" => $ruta_activa
			]);

			return 1;
		}return 0;
	}

	//DELETE-GALERIAHOME
	public static function deletegaleria($inputs){
		$finder = Galeria::getGaleriaID(base64_decode($inputs["historial"]));
		if(count($finder)==1){
			if(file_exists($finder->galeria_path)){//EXISTE MI GALERIA EN EL HISTORIAL.
				$imagen = substr($finder->galeria_path,strripos($finder->galeria_path,"/")+1);
				unlink($finder->galeria_path);
				$finder->delete();
				return 1;
			}
		}return 0;
	}

	//DELETE-ASOCIACION
	public static function deleteasociacion($inputs){
		$finder = Galeria::where("id",base64_decode($inputs["historial"]))->first();
		if(count($finder)==1){
			$default = Promociones::where("promo_path","img/home/promociones/default.jpg")->value("id");
			$promocion = Promociones::where("id",$finder->promo_id)->where("promo_status","0")->first();
			if(count($promocion)==1){
				$finder->update([
					"promo_id" => $default
				]);
				
				return 1;
			}return 2;
		}return 0;
	}
	
	//OBTENGO LA GALERÍA
	public static function obtenerGaleria(){
		$inserciones = Galeria::all();
		return [
			$inserciones[0]->galeria_path,
			$inserciones[1]->galeria_path,
			$inserciones[2]->galeria_path,
			$inserciones[3]->galeria_path,
			$inserciones[4]->galeria_path,
			$inserciones[5]->galeria_path
		];
	}

	public static function obtenerGaleriaDisponible(){
		$inserciones = Galeria::join("promociones","promociones.id","galeria.promo_id")->where("promociones.promo_path","img/home/promociones/default.jpg")->select("galeria.galeria_path")->get()->toJson();
		return $inserciones;
	}

	public static function getGaleriaAsociada(){
		return Galeria::join("promociones","promociones.id","galeria.promo_id")->select("galeria.id","galeria.promo_id","galeria.galeria_path","promociones.promo_path")->whereNotIn("promociones.promo_path",["img/home/promociones/default.jpg"])->get();
	}
	
	public static function get_historial(){
		return Galeria::where("galeria_status","1")->get()->toJson();
	}
	
	public static function getGaleriaID($id){
		return Galeria::where("id",$id)->first();
	}
	
	public static function asociarPromo($imagen, $path){
		$id_promo = Promociones::where("promo_path",$path)->value("id");//ID DE LA PROMOCIÓN
		$galeria = Galeria::where("galeria_path",$imagen)->select("id","promo_id")->first();//ID Y PROMO_ID DE LA PROMOCIÖN
		$default = Promociones::get_promoDefault();//OBTENGO EL ID DE LA PROMO POR DEFAULT
		
		if($galeria->promo_id==$default->id && $id_promo!=$galeria->promo_id){//IMAGEN SIN PROMOCIÓN ASOCIADA
		  Galeria::where("id",$galeria->id)->update([
			"promo_id" => $id_promo
		  ]);
		  return 1;
		}else if($galeria->promo_id!=$default->id && $id_promo!=$galeria->promo_id){
			Galeria::where("id",$galeria->id)->update([
			"promo_id" => $id_promo
		  ]);
		  return 1;
		}else{//INTENT ASOCIAR A UNAP ROMOCIÓN CON LA MISMA PROMO
		  return 0;
		}
	}

	public static function size(){
		return count(Galeria::whereNotIn("galeria_path",["img/home/galeria/default.jpg"])->get());
	}
	
	//OBTENER SLIDER HOME
	public static function getAllslider($estado){
		return Galeria::where("galeria_status",$estado)->where("galeria_path","<>","img/home/galeria/default.jpg")->select("galeria_path AS img","promo_id AS promo")->orderBy('galeria_pos', 'asc')->get()->toJson();
	}
}
