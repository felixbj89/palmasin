<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Galeria;
use Carbon\Carbon;

class Promociones extends Model
{
    protected $table = "promociones";

	protected $fillable = [
		"promo_path",
		"promo_title",
		"promo_text",
		"promo_status",
		"promo_used",
		"promo_dateini",
		"promo_datefin",
		"user_id",
	];

	protected $hidden = [
      'remember_token',
	];

	public function galeria(){
      return $this->belongsTo('App\Galeria', 'user_id');
	}

	public function usuario(){
        return $this->hasMany('App\Usuario',"id","user_id");
    }

	private static function transformarTexto($descripcion){//QUITAR FORMATO AL TEXTO INGRESADO
		$buscar=array("\r\n", "\n", "\r");//LIMPIO DE CUALQUIER CARACTER O SECUENCIA DE ESCAPE QUE EXISTA
		$reemplazar=array("", "", "");
		$cadena=str_ireplace($buscar,$reemplazar,$descripcion);
		$cadena = (str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$cadena));
		$cadena = (str_replace("\r\n",'', $cadena));
		$buscar = "\"";
		$reemplazar   = '\\"';
		$cadena = (str_replace($buscar,$reemplazar,$cadena));
		return $cadena;
	}
	
	public static function promocionesrango(){
		$actual = Carbon::now("America/Asuncion",-4);
		$promociones = json_decode(Promociones::getPromos("1","0"));
		$rangos = [];
		for($i = 0, $j = 0; $i < count($promociones); $i++){
			$ini = $promociones[$i]->promo_dateini;
			$date_ini = explode("-",explode(" ",$ini)[0]);
			$hora_ini = explode(":",explode(" ",$ini)[1]);
			$init_ini = Carbon::create($actual->year, $actual->month, $actual->day, $actual->hour, $actual->minute, $actual->second, 'America/Asuncion');
			$end_ini = Carbon::create($date_ini[0], $date_ini[1], $date_ini[2], $hora_ini[0], $hora_ini[1], $hora_ini[2], 'America/Asuncion');

			$fin = $promociones[$i]->promo_datefin;
			$date_fin = explode("-",explode(" ",$fin)[0]);
			$hora_fin = explode(":",explode(" ",$fin)[1]);
			$end_fin = Carbon::create($date_fin[0], $date_fin[1], $date_fin[2], $hora_fin[0], $hora_fin[1], $hora_fin[2], 'America/Asuncion');

			if($init_ini->gte($end_ini) && $init_ini->lte($end_fin)){
				$rangos[$j] = [
					"id" => base64_encode ( $promociones[$i]->id ),
					"title" => $promociones[$i]->promo_title,
					"text" => $promociones[$i]->promo_text,
					"path" => $promociones[$i]->promo_path,
					"rango" => ($init_ini->gte($end_ini) && $init_ini->lte($end_fin)),
					"ini" => explode(" ",$promociones[$i]->promo_dateini)[0],
					"fin" => explode(" ",$promociones[$i]->promo_datefin)[0]
				]; $j++;

				Promociones::activar( $promociones[$i]->id );
			}
		}

		return $rangos;
	}
	
	public static function getPromos($status, $used){//PROMOCIONES ACTIVAS Y EN USO Ó VICEVERSA
		return Promociones::where("promo_status",$status)->where("promo_used",$used)->whereNotIn("promo_path",["img/home/promociones/default.jpg"])->get()->toJson();
	}

	public static function get_promosAll(){//OBTENGO TODAS LAS PROMOCIONES
		return Promociones::whereNotIn("promo_path",["img/home/promociones/default.jpg"])->get();
	}

	public static function get_promosActivas(){//OBTENGO TODAS LAS PROMOCIONES
		return Promociones::whereNotIn("promo_path",["img/home/promociones/default.jpg"])->where("promo_status","1")->where("promo_used","0")->get();
	}

	public static function get_promoDefault(){
		return Promociones::where("promo_path","img/home/promociones/default.jpg")->where("promo_status","1")->where("promo_used","1")->first();
	}
	
	public static function getPromocionesNotID($id){//PROMOOCIONES MENOS YO
		return Promociones::whereNotIn("promo_path",["img/home/promociones/default.jpg"])->whereNotIn("id",[$id])->get();
	}
	
	public static function activar($id){
		Promociones::where("id",$id)->update([
			"promo_status" => "1"
		]);
	}

	public static function desactivar($id){
		Promociones::where("id",$id)->update([
			"promo_status" => "0"
		]);
	}

	public static function obtenerPromo($id){//OBTENGO LA PROMO SEGÚN MI ID.
		return Promociones::where("id",base64_decode($id))->first();
	}

	public static function whichstatus($id){//OBTENGO EL ESTADO ACTUAL DE UNA PROMO
		return Promociones::where("id",$id)->select("promo_status")->first();
	}

	public static function delete_promo($inputs){//ELIMINO LA PROMOCIÓN SI ESTÁ DESACTIVADA
		$finder = Promociones::where("id",base64_decode($inputs["promo_id"]))->first();
		if(count($finder)==1 && $finder->promo_status==0){
			$usando_galeria = Galeria::where("promo_id",$finder->id)->first();
			if(count($usando_galeria)==0){
				$imagenEliminar = "img/home/promociones/".$finder->promo_path;
				if($finder->promo_path!="img/home/promociones/default.jpg" && file_exists($imagenEliminar)){
					unlink($imagenEliminar);
				}

				$finder->delete();
				return 1;
			}else{
				return 3;
			}
		}else{
			return 2;
		}return 0;
	}

	public static function estadopromocioneshome($inputs){
		$size = count(Promociones::where("promo_status","1")->get());
		$finder = Promociones::where("id",base64_decode($inputs["promo_id"]))->first();
		if(count($finder)==1){
			if($finder->promo_status==1){
				$finder->update([
					"promo_status" => "0",
					"promo_used" => "1"
				]);
			}else{
				if($size <= 3){
					$finder->update([
						"promo_status" => "1",
						"promo_used" => "0"
					]);
				}else{
					return 2;
				}
			}

			return 1;
		}return 0;
	}

	public static function editpromohome($inputs){
		$finder = Promociones::where("id",base64_decode($inputs["posicion"]))->first();
		$date = Carbon::now(-4);
		if(count($finder)==1){
			$titulo = $finder->promo_title; $estado = $finder->promo_status; $descripcion = $finder->promo_text; $one = $finder->promo_path;
			if(array_key_exists("titulo_promociones",$inputs)){
				if(count(Promociones::whereNotIn("promo_title",[$inputs["titulo_promociones"]])->first())==0)return 2;
				$titulo = $inputs["titulo_promociones"];
			}//NOMBRE

			if(array_key_exists("estado_promociones",$inputs)){
				$estado = $inputs["estado_promociones"];
			}//ESTADO

			if(array_key_exists("descripcion_promociones",$inputs)){
				$descripcion = Promociones::transformarTexto($inputs["descripcion_promociones"]);
			}//DESCRIPCIÓN

			if(array_key_exists("file_one",$inputs)){
				$rutaone = "img/historial/promocioneshome/";
				if($finder->promo_path!="img/home/promociones/default.jpg" && file_exists($finder->promo_path)){
					$imagen = substr($finder->promo_path,strripos($finder->promo_path,"/")+1);
					$rutaone .= $imagen;
					if(copy($finder->promo_path,$rutaone)){
						unlink($finder->promo_path);
					}

					Historial::store_historial($rutaone,"1");
				}

				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$one = (str_replace($search, $replace, $name));
				\Storage::disk('promohome')->put($one,  \File::get($inputs["file_one"]));
			}

			$finder->update([
				"promo_path" => $one,
				"promo_title" => $titulo,
				"promo_text" => $descripcion,
				"promo_status" => $estado,
				"promo_dateini" => $inputs["inicio_promociones"],
				"promo_datefin" => $inputs["fin_promociones"],
				"promo_used" => "0",
				"user_id" => "1",
			]);

			return 1;
		}return 0;
	}

	public static function addpromocioneshome($inputs){
		$finder = Promociones::where("promo_path","img/home/promociones/default.jpg")->first();
		$vestado = count(Promociones::where("promo_status","1")->get());
		$date = Carbon::now(-4);
		if($vestado <= 3){
			$vtitle = Promociones::where("promo_title",$inputs["titulo_promociones"])->first();
			if(count($vtitle)==0){
				if(count($finder)==1){
					$titulo = $finder->promo_title; $estado = $finder->promo_status; $descripcion = $finder->promo_text; $one = $finder->promo_path;
					if(array_key_exists("titulo_promociones",$inputs)){
						$titulo = $inputs["titulo_promociones"];
					}//NOMBRE

					if(array_key_exists("estado_promociones",$inputs)){
						$estado = $inputs["estado_promociones"];
					}//ESTADO

					if(array_key_exists("descripcion_promociones",$inputs)){
						$descripcion = Promociones::transformarTexto($inputs["descripcion_promociones"]);
					}//DESCRIPCIÓN

					if(array_key_exists("file_one",$inputs)){
						$rutaone = "img/historial/promocioneshome/";
						if($finder->promo_path!="img/home/promociones/default.jpg" && file_exists($finder->promo_path)){
							$imagen = substr($finder->promo_path,strripos($finder->promo_path,"/")+1);
							$rutaone .= $imagen;
							if(copy($finder->promo_path,$rutaone)){
								unlink($finder->promo_path);
							}
						}

						$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
						$search  = array(' ', '(', ')','ñ');
						$replace = array('', '', '','n');
						$one = (str_replace($search, $replace, $name));
						\Storage::disk('promohome')->put($one,  \File::get($inputs["file_one"]));
					}

					$finder->update([
						"promo_path" => $one,
						"promo_title" => $titulo,
						"promo_text" => $descripcion,
						"promo_status" => $estado,
						"promo_dateini" => $inputs["inicio_promociones"],
						"promo_datefin" => $inputs["fin_promociones"],
						"promo_used" => "0",
						"user_id" => "1",
					]);

					Promociones::create([
						"promo_path" => "img/home/promociones/default.jpg",
						"promo_title" => "TITLE",
						"promo_text" => "TEXT",
						"promo_status" => "1",
						"promo_dateini" => "0000-00-00 00:00:00",
						"promo_datefin" => "0000-00-00 00:00:00",
						"promo_used" => "1",
						"promo_status" => "1",
						"user_id" => "1",
					]);

					$id = $finder->id;
					$finder = Promociones::where("promo_path","img/home/promociones/default.jpg")->first();
					Galeria::where("promo_id",$id)->update([
						"promo_id" => $finder->id
					]);

					return 1;
				}
			}return 2;
		}return 0;
	}
	
	//OBTENER PROMOS ACTIVAS
	public static function getPromosactives(){
		return Promociones::promocionesrango();
	}
}
