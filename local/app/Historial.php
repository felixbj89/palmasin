<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    protected $table = "historial";
	
	protected $fillable = [
		"historial_path",
		"historial_status",
	];

	protected $hidden = [
      'remember_token',
	];
	
	public static function store_historial($ruta, $estado){
		Historial::create([
			"historial_path" => $ruta,
			"historial_status" => $estado,
		]);
	}
	
	public static function get_historial($type){
		return Historial::where("historial_status",$type)->get();
	}

	public static function get_historialwithid($id){
		return Historial::where("historial_status","0")->where("id",$id)->first();
	}
}
