<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Usuario;

class Avatares extends Model
{
    protected $table = "avatares";
	
	protected $fillable = [
		"avatares_path",
		"user_id",
	];

	protected $hidden = [
      'remember_token',
	];
	
	public function usuario(){
        return $this->hasMany('App\Usuario',"id","user_id");
    }
	
	//ELIMINO UN AVATAR DEL HISTORIAL
	public static function deletearemover($inputs){
		$finder = Avatares::where("id",base64_decode($inputs["historial"]))->first();
		if(count($finder)==1){
			$activo = Usuario::where("user_avatar",$finder->avatares_path)->first();
			if(count($activo)==1){
				return 2;
			}else{			
				if(file_exists($finder->avatares_path)){
					unlink($finder->avatares_path);
					$finder->delete();
					
					return 1;
				}
			}
		}return 0;
	}
	
	public static function addavataruser($inputs){
		$date = Carbon::now(-4);
		if(array_key_exists("file_one",$inputs)){
			$name = str_random(4).$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$uno = (str_replace($search, $replace, $name));
			\Storage::disk('avatarhome')->put($uno,  \File::get($inputs["file_one"]));
			$uno = ("img/avatares/".$uno);

			Avatares::create([
				"avatares_path" => $uno,
				"user_id" => "1"
			]);
			
			return 1;
		}
		
		return 0;
	}
}
