<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException as HttpException;
use \Illuminate\Auth\Access\AuthorizationException as AuthorizationException;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use DB;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
		
		if($e instanceof AuthorizationException){
			DB::table("cache")->where("key","laravelusuario")->delete(); //ELIMINO EL CACHE DE LA BD
			\Cache::flush();//LIMPIO EL CACHE GENERADO
		
			return redirect()->to("admin/backend")->withErrors(["sinacceso" => "Lo sentimos, no posee los permisos para acceder"]);
		}else if($e instanceof TokenMismatchException){
			DB::table("cache")->where("key","laravelusuario")->delete(); //ELIMINO EL CACHE DE LA BD
			\Cache::flush();//LIMPIO EL CACHE GENERADO
		
			return redirect()->to("admin/backend")->withErrors(["sinacceso" => "Lo sentimos, su tiempo de sesión ha terminado"]);
		}else if($e instanceof NotFoundHttpException){
			return response(view('errores.404',["aviso" => 1]),404);
		}else{
			return parent::render($request, $e);
		}
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
