<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicios extends Model
{
    protected $table = "servicios";
	
	protected $fillable = [
		"servicios_titulo",
		"servicios_subtitulo",
		"servicios_texto",
	];

	protected $hidden = [
      'remember_token',
	];
	
	private static function transformarTexto($descripcion){//QUITAR FORMATO AL TEXTO INGRESADO
		$buscar=array("\r\n", "\n", "\r");//LIMPIO DE CUALQUIER CARACTER O SECUENCIA DE ESCAPE QUE EXISTA
		$reemplazar=array("", "", "");
		$cadena=str_ireplace($buscar,$reemplazar,$descripcion);
		$cadena = (str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$cadena));
		$cadena = (str_replace("\r\n",'', $cadena));
		$buscar = "\"";
		$reemplazar   = '\\"';
		$cadena = (str_replace($buscar,$reemplazar,$cadena));
		return $cadena;
	}
	
	public static function editservicioshome($inputs){
		if(array_key_exists("titulo",$inputs) && array_key_exists("subtitulo",$inputs) && array_key_exists("descripcion_promociones",$inputs)){
			
			Servicios::where("id","1")->update([
				"servicios_titulo" => $inputs["titulo"],
				"servicios_subtitulo" => $inputs["subtitulo"],
				"servicios_texto" => Servicios::transformarTexto($inputs["descripcion_promociones"]),
			]);
			
			return 1;
		}
		
		return 0;
	}
	
	public static function obtenerServicios(){
		return Servicios::first()->toJson();
	}
}
