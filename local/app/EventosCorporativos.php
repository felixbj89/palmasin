<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EventosCorporativos extends Model
{
    protected $table = "eventoscorporativos";
	
	protected $fillable = [
		'eventoscorporativos_name',
		'eventoscorporativos_textone',
		'eventoscorporativos_texttwo',
		'eventoscorporativos_picone',
		'eventoscorporativos_pictwo',
		'eventoscorporativos_picthrid',
		'eventoscorporativos_picfour'
	];

	protected $hidden = [
      'remember_token',
	];
	
	private static function transformarTexto($descripcion){//QUITAR FORMATO AL TEXTO INGRESADO
		$buscar=array("\r\n", "\n", "\r");//LIMPIO DE CUALQUIER CARACTER O SECUENCIA DE ESCAPE QUE EXISTA
		$reemplazar=array("", "", "");
		$cadena=str_ireplace($buscar,$reemplazar,$descripcion);
		$cadena = (str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$cadena));
		$cadena = (str_replace("\r\n",'', $cadena));
		$buscar = "\"";
		$reemplazar   = '\\"';
		$cadena = (str_replace($buscar,$reemplazar,$cadena));
		return $cadena;
	}
	
	public static function addeventoscorporativos($inputs){
		$finder = EventosCorporativos::where("id","1")->first();
		$date = Carbon::now(-4);
		if(count($finder)==1){
			$titulo = ""; $d1 = ""; $d2 = ""; $uno = ""; $dos = ""; $tres = ""; $cuatro = "";
			if(array_key_exists("titulo_corporativos",$inputs) && $inputs["titulo_corporativos"]!=""){
				$titulo = $inputs["titulo_corporativos"];
			}else{
				$titulo = $finder->eventoscorporativos_name;
			}
			
			if(array_key_exists("descripcion_evento1",$inputs) && $inputs["descripcion_evento1"]!=""){
				$d1 = EventosCorporativos::transformarTexto($inputs["descripcion_evento1"]);
			}else{
				$d1 = $finder->eventoscorporativos_textone;
			}
			
			if(array_key_exists("descripcion_evento2",$inputs) && $inputs["descripcion_evento2"]!=""){
				$d2 = EventosCorporativos::transformarTexto($inputs["descripcion_evento2"]);
			}else{
				$d2 = $finder->eventoscorporativos_texttwo;
			}
						
			if(array_key_exists("file_one",$inputs) && $inputs["file_one"]!=""){//1
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$uno = (str_replace($search, $replace, $name));
				if($finder->eventoscorporativos_picone!="img/eventoscorporativos/default.jpg")\Storage::disk('eventoscorporativos')->delete($finder->eventoscorporativos_picone);
				\Storage::disk('eventoscorporativos')->put($uno,  \File::get($inputs["file_one"]));
				$uno = ('img/eventoscorporativos/'.$uno);
			}else{
				$uno = $finder->eventoscorporativos_picone;
			}
			
			if(array_key_exists("file_two",$inputs) && $inputs["file_two"]!=""){//2
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_two"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$dos = (str_replace($search, $replace, $name));
				if($finder->eventoscorporativos_pictwo!="img/eventoscorporativos/default.jpg")\Storage::disk('eventoscorporativos')->delete($finder->eventoscorporativos_pictwo);
				\Storage::disk('eventoscorporativos')->put($dos,  \File::get($inputs["file_two"]));
				$dos = ('img/eventoscorporativos/'.$dos);
			}else{
				$dos = $finder->eventoscorporativos_pictwo;
			}
			
			if(array_key_exists("file_thrid",$inputs) && $inputs["file_thrid"]!=""){//3
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_thrid"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$tres = (str_replace($search, $replace, $name));
				if($finder->eventoscorporativos_picthrid!="img/eventoscorporativos/default.jpg")\Storage::disk('eventoscorporativos')->delete($finder->eventoscorporativos_picthrid);
				\Storage::disk('eventoscorporativos')->put($tres,  \File::get($inputs["file_thrid"]));
				$tres = ('img/eventoscorporativos/'.$tres);
			}else{
				$tres = $finder->eventoscorporativos_picthrid;
			}
			
			if(array_key_exists("file_four",$inputs) && $inputs["file_four"]!=""){//4
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_four"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$cuatro = (str_replace($search, $replace, $name));
				if($finder->eventoscorporativos_picfour!="img/eventoscorporativos/default.jpg")\Storage::disk('eventoscorporativos')->delete($finder->eventoscorporativos_picfour);
				\Storage::disk('eventoscorporativos')->put($cuatro,  \File::get($inputs["file_four"]));
				$cuatro = ('img/eventoscorporativos/'.$cuatro);
			}else{
				$cuatro = $finder->eventoscorporativos_picfour;
			}
			
			$finder->update([
				'eventoscorporativos_name' => $titulo,
				'eventoscorporativos_textone' => $d1,
				'eventoscorporativos_texttwo' => $d2,
				'eventoscorporativos_picone' => $uno,
				'eventoscorporativos_pictwo' => $dos,
				'eventoscorporativos_picthrid' => $tres,
				'eventoscorporativos_picfour' => $cuatro
			]);
			
			return 1;
		}return 0;
	}
	
	public static function obtenerEventos(){
		return EventosCorporativos::first()->toJson();
	}
}