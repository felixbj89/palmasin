<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Comerciales extends Model
{
    protected $table = "comerciales";
	
	protected $fillable = [
		"comerciales_pictureone",
		"comerciales_picturetwo",
		"comerciales_textoone",
		"comerciales_textotwo",
		"comerciales_linkone",
		"comerciales_linktwo",
	];

	protected $hidden = [
      'remember_token',
	];
	
	public static function editcomercialeshome($inputs){
		$finder = Comerciales::where("id","1")->first();
		$date = Carbon::now(-4);
		
		$textone = "";
		if(array_key_exists("titulo_comercialone",$inputs) && $inputs["titulo_comercialone"]!=""){
			$textone = $inputs["titulo_comercialone"];
		}else{
			$textone = $finder->comerciales_textoone;
		}
		
		$linksone = "";
		if(array_key_exists("link_comercialone",$inputs) && $inputs["link_comercialone"]!=""){
			$linksone = $inputs["link_comercialone"];
		}else{
			$linksone = $finder->comerciales_linkone;
		}
		
		$texttwo = "";
		if(array_key_exists("titulo_comercialtwo",$inputs) && $inputs["titulo_comercialtwo"]!=""){
			$texttwo = $inputs["titulo_comercialtwo"];
		}else{
			$texttwo = $finder->comerciales_textotwo;
		}
		
		$linksdos = "";
		if(array_key_exists("link_comercialdos",$inputs) && $inputs["link_comercialdos"]!=""){
			$linksdos = $inputs["link_comercialdos"];
		}else{
			$linksdos = $finder->comerciales_linktwo;
		}
		
		$rutaone = "";
		if(array_key_exists("file_one",$inputs) && $inputs["file_one"]!=""){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			\Storage::disk('comercialhome')->put($one,  \File::get($inputs["file_one"]));//nueva imagen
			\Storage::disk('comercialhome')->delete($finder->comerciales_pictureone);//borro imagen vieja
			$rutaone = ("img/elhotel/comerciales/".$one);
		}else{
			$rutaone = $finder->comerciales_pictureone;
		}
		
		$rutatwo = "";
		if(array_key_exists("file_two",$inputs) && $inputs["file_two"]!=""){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_two"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$two = (str_replace($search, $replace, $name));
			\Storage::disk('comercialhome')->put($two,  \File::get($inputs["file_two"]));//nueva imagen
			\Storage::disk('comercialhome')->delete($finder->comerciales_picturetwo);//borro imagen vieja
			$rutatwo = ("img/elhotel/comerciales/".$two);
		}else{
			$rutatwo = $finder->comerciales_picturetwo;
		}
		
		Comerciales::where("id","1")->update([
			"comerciales_pictureone" => $rutaone,
			"comerciales_picturetwo" => $rutatwo,
			"comerciales_textoone" => $textone,
			"comerciales_textotwo" => $texttwo,
			"comerciales_linkone" => $linksone,
			"comerciales_linktwo" => $linksdos,
		]);
		
		return 1;
	}
	
	public static function obtenerComerciales(){
		return Comerciales::where("id","1")->first()->toJson();
	}
}
