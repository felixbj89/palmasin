<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Avatares;

class Usuario extends Model
{
    protected $table = "users";

	protected $fillable = [
		"user_name",
		"user_email",
		"user_login",
		"user_password",
		"user_avatar",
		"user_role",
		"user_status"
	];

	protected $hidden = [
      'remember_token',
	];

	public function promociones(){
      return $this->belongsTo('App\Promociones', 'promo_id');
	}

	public function galeria(){
      return $this->belongsTo('App\Galeria', 'galeria_id');
	}

	public function isalud(){
      return $this->belongsTo('App\ISalud', 'salud_id');
	}

	public function avatares(){
      return $this->belongsTo('App\Avatares', 'avatares_id');
	}

	//ELIMINAR UN USUARIO
	public static function deleteusers($inputs){
		$finder = Usuario::where("id",base64_decode($inputs["user_id"]))->first();
		if(count($finder)==1){
			$finder->delete();

			return 1;
		}return 0;
	}

	//OBTENGO A TODOS LOS USUARIOS
	public static function obtenerUsuarios(){
		return Usuario::where("user_role",">","0")->get();
	}

	//OBTENER UN USUARIO POR ID
	public static function obtenerUsuarioID($id){
		return Usuario::where("id",$id)->first();
	}

	//CUAL ES EL ROLE
	public static function whichRole($role){
		$roles = [
			0 => "Super Admin",
			1 => "Admin",
			2 => "Operador"
		];

		return $roles[$role];
	}

	//COMPRUEBO SI LAS CREDENCIALES ENVIADAS SON CORRECTAS Y RETORNO EL USUARIO
	public static function check_user($inputs){
		return Usuario::where("user_login",$inputs["loginuser"])->
		where("user_password",hash("ripemd320",$inputs["passworduser"]))->
		where("user_status","1")->first();
	}

	//CREO UN NUEVO USUARIO
	public static function addusers($inputs){
		$finder = Usuario::where("user_login",$inputs["loginusers"])->first();
		if(count($finder)==0){
			$finder = Usuario::where("user_email",$inputs["emailusers"])->first();
			if(count($finder)==0){
				Usuario::create([
					"user_name" => $inputs["nombreusers"],
					"user_email" => $inputs["emailusers"],
					"user_login" => $inputs["loginusers"],
					"user_password" => hash("ripemd320",$inputs["passwordusers"]),
					"user_avatar" => "img/avatares/user2-160x160.jpg",
					"user_role" => $inputs["roleusers"],
					"user_status" => "1"
				]);

				return 1;
			}return 3;
		}else{
			return 2;
		}return 0;
	}

	//EDITAR EL PERFIL DE UN USUARIO
	public static function editperfil($inputs, $id){
		$finder = Usuario::where("id",$id)->first();
		if(($inputs["loginuser"]!="" && $finder->user_login==$inputs["loginuser"]) && (array_key_exists("passworduser",$inputs) && $inputs["passworduser"]=="") && (array_key_exists("confirmaruser",$inputs) && $inputs["confirmaruser"]=="")){
			return 2;
		}else{
			if(count($finder)==1){
				$login = "";
				$password = "";

				if(array_key_exists("loginuser",$inputs) && $inputs["loginuser"]!=""){
					$login = $inputs["loginuser"];
				}else{
					$login = $finder->user_login;
				}

				if(array_key_exists("passworduser",$inputs) && $inputs["passworduser"]!=""){
					$password = hash('ripemd320', $inputs["passworduser"]);
				}else{
					$password = $finder->user_password;
				}

				$finder->update([
					"user_login" => $login,
					"user_password" => $password
				]);

				\Cache::store('database')->put('usuario', $finder, 480);
				return 1;
			}
		}return 0;
	}

	//EDITO A UN USARIO CREADO
	public static function edit_users($inputs){
		$finder = Usuario::where("id",base64_decode($inputs["id"]))->first();
		if(count($finder)==1){
			$login = "";
			$password = "";
			$role = "";

			if(array_key_exists("loginusers",$inputs) && $inputs["loginusers"]!=""){
				$login = $inputs["loginusers"];
			}else{
				$login = $finder->user_login;
			}

			if(array_key_exists("passwordusers",$inputs) && $inputs["passwordusers"]!=""){
				$password = hash("ripemd320",$inputs["passwordusers"]);
			}else{
				$password = $finder->user_password;
			}

			if(array_key_exists("roleusers",$inputs) && $inputs["roleusers"]!=""){
				$role = $inputs["roleusers"];
			}else{
				$role = $finder->user_role;
			}

			$finder->update([
				"user_login" => $login,
				"user_password" => $password,
				"user_role" => $role,
			]);


			return 1;
		}return 0;
	}

	//CAMBIA EL AVATRAR
	public static function estadoavatar($inputs, $id){
		$finder = Usuario::where("id",$id)->first();
		if(count($finder)==1){
			$avatar = Avatares::where("id",base64_decode($inputs["historial"]))->first();
			if($finder->user_avatar!=$avatar->avatares_path){
				$finder->update([
					"user_avatar" => $avatar->avatares_path
				]);

				\Cache::store('database')->put('usuario', $finder, 480);
				return 1;
			}return 2;
		}return 0;
	}
}
