<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Meses;

class MesesController extends Controller
{
    //ACTIVAR MES
	public function lanzarmesevetoshome(Request $request){
		$res = "";
		$m = new Meses();
		$estado = $m->lanzarmesevetoshome($request->all());
		$res = array('estado'=> $estado);
		return $res;
	}	
}
