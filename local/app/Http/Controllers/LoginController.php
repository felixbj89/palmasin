<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Lang;

class LoginController extends Controller
{
	//ACCEDO A LA INTERFAZ DE ACCESO AL BACKEND
    public function backend(){
		return view("backend.index");
	}

	//INGRESO AL DASHBOARD
    public function dashboard(Request $request){
		$usuario = Usuario::check_user($request->all());
		if(count($usuario)==1){
			\Cache::store('database')->put('usuario', $usuario, 960);
			return redirect()->action("DashboardController@dashboard");
		}return redirect()->back()->withInput()->withErrors(Lang::get('errores.credenciales_incorrectas'));
	}
}
