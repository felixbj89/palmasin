<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AccessAdmin;
use App\Http\Requests\AccessOperator;

use App\Usuario;
use App\Galeria;
use App\Promociones;
use App\ISalud;
use App\Historial;
use App\Comerciales;
use App\Avatares;
use App\Eventos;
use App\Meses;
use App\EventosCorporativos;
use App\Proyectos;
use App\Habitacion;
use App\ElHotel;
use App\Restaurant;
use App\Servicios;
use App\Bannergaleri;
use Carbon\Carbon;
use Lang;
use DB;
use Crypt;

class DashboardController extends Controller
{
	//OBTENGO LA ESTADÍSTICA PARA LOS DISPOSITIVOS USADOS
	private function getDispositivos($dispositivos){
		return [
			count(DB::table("tracker_devices")->whereIn("kind",[$dispositivos["0"]])->select("kind")->get()),
			count(DB::table("tracker_devices")->whereIn("kind",[$dispositivos["1"]])->select("kind")->get()),
			count(DB::table("tracker_devices")->whereIn("kind",[$dispositivos["2"]])->select("kind")->get()),
		];
	}

	//OBTENGO LA ESTADÍSTICA PARA LOS NAVEGADORES EMPLEADOS
	private function getNavegadores($navegadores){
		return [
			count(DB::table("tracker_agents")->whereIn("browser",[$navegadores["0"]])->select("browser")->get()),
			count(DB::table("tracker_agents")->whereIn("browser",[$navegadores["1"]])->select("browser")->get()),
			count(DB::table("tracker_agents")->whereIn("browser",[$navegadores["2"]])->select("browser")->get()),
			count(DB::table("tracker_agents")->whereIn("browser",[$navegadores["3"]])->select("browser")->get())
		];
	}

	//OBTENGO LA ESTADÍSTICA DE LAS SECCIONES VISTAS
	private function getSecciones($secciones){
		return [
			count(DB::table("tracker_route_paths")->whereIn("path",[$secciones["0"]])->select("path")->get()),
			count(DB::table("tracker_route_paths")->whereIn("path",[$secciones["1"]])->select("path")->get()),
			count(DB::table("tracker_route_paths")->whereIn("path",[$secciones["2"]])->select("path")->get()),
			count(DB::table("tracker_route_paths")->whereIn("path",[$secciones["3"]])->select("path")->get()),
			count(DB::table("tracker_route_paths")->whereIn("path",[$secciones["4"]])->select("path")->get())
		];
	}

	//OBTENGO LASESTADÍSTICA DE LOS PAÍSES
	private function getPaises($paises){
		return [
			count(DB::table("tracker_geoip")->whereIn("country_name",[$paises["0"]])->select("country_name")->get()),
			count(DB::table("tracker_geoip")->whereIn("country_name",[$paises["1"]])->select("country_name")->get()),
			count(DB::table("tracker_geoip")->whereIn("country_name",[$paises["2"]])->select("country_name")->get()),
		];
	}

	//GENERO LOS 6 MESES DE LA VISITA
	private function getMonths(){
		$date = Carbon::now("America/Asuncion",-4);
		$mes_actual = 12;
		$init = 0;
		$meses = [
			0 => "Enero",
			1 => "Febrero",
			2 => "Marzo",
			3 => "Abril",
			4 => "Mayo",
			5 => "Junio",
			6 => "Julio",
			7 => "Agosto",
			8 => "Septiembre",
			9 => "Octubre",
			10 => "Noviembre",
			11 => "Diciembre",
		];


		$generar_meses = [];
		for($i = $init, $j = 0; $i < $mes_actual; $i++){
			$generar_meses[$j++] = $meses[$i];
		}

		return $generar_meses;
	}

	//COUNT SESSION
	private function countSession($mes){
		$visitas = DB::table("tracker_sessions")->select("created_at")->get();
		$size = 0;
		for($i = 0; $i < count($visitas); $i++){
			$fmes = explode("-",explode(" ",$visitas[$i]->created_at)[0])[1];
			if($mes==$fmes){
				$size++;
			}
		}

		return $size;
	}

	//CUENTO Y GENERO EL TOTAL DE VISITAS REALIZADAS
	private function totalsessiones(){
		$date = Carbon::now("America/Asuncion",-4);
		$mes_actual = $date->month;
		$init = ($mes_actual>9?($mes_actual-9):($mes_actual-$mes_actual));
		$visitas = [];
		for($i = $init; $i < $mes_actual; $i++){
			$visitas[$i] = $this->countSession(($i+1));
		}return $visitas;
	}

	private function promosactivar(){
		$actual = Carbon::now("America/Asuncion",-4);
		$promociones = json_decode(Promociones::getPromos("1","0"));
		$activas = [];
		for($i = 0, $j = 0; $i < count($promociones); $i++){
			$ini = $promociones[$i]->promo_dateini;
			$date_ini = explode("-",explode(" ",$ini)[0]);
			$hora_ini = explode(":",explode(" ",$ini)[1]);
			$init = Carbon::create($actual->year, $actual->month, $actual->day, $actual->hour, $actual->minute, $actual->second, 'America/Asuncion');
			$end = Carbon::create($date_ini[0], $date_ini[1], $date_ini[2], $hora_ini[0], $hora_ini[1], $hora_ini[2], 'America/Asuncion');
			if($init->lte($end)){
				$activas[$j] = [
					"id" => base64_encode ( $promociones[$i]->id ),
					"title" => $promociones[$i]->promo_title,
					"vencida" => $init->lte($end)
				]; $j++;
			}
		}

		return $activas;
	}

	private function promovencidas(){
		$actual = Carbon::now("America/Asuncion",-4);
		$promociones = json_decode(Promociones::getPromos("1","0"));
		$vencidas = [];
		for($i = 0, $j = 0; $i < count($promociones); $i++){
			$fin = $promociones[$i]->promo_datefin;
			$date_fin = explode("-",explode(" ",$fin)[0]);
			$hora_fin = explode(":",explode(" ",$fin)[1]);
			$init = Carbon::create($actual->year, $actual->month, $actual->day, $actual->hour, $actual->minute, $actual->second, 'America/Asuncion');
			$end = Carbon::create($date_fin[0], $date_fin[1], $date_fin[2], $hora_fin[0], $hora_fin[1], $hora_fin[2], 'America/Asuncion');
			if($init->gte($end)){
				$vencidas[$j] = [
					"id" => base64_encode ( $promociones[$i]->id ),
					"title" => $promociones[$i]->promo_title,
					"vencida" => $init->gte($end)
				]; $j++;

				Promociones::desactivar( $promociones[$i]->id );
			}
		}

		return $vencidas;
	}

	//INGRESO AL DASHBOARD
    public function dashboard(){
		//NAVEGADORES
		$dispositivos = [
			"0" => "Computer",
			"1" => "Phone",
			"2" => "Tablet"
		];

		$estadistica_dispositivos = $this->getDispositivos($dispositivos);

		//NAVEGADORES
		$navegadores = [
			"0" => "Firefox",
			"1" => "Chrome",
			"2" => "IE",
			"3" => "Safari",
			"4" => "Edge"
		];

		$estadistica_navegadores = $this->getNavegadores($navegadores);

		//SECCIONES
		$secciones = [
			"0" => "/",
			"1" => "habitaciones",
			"2" => "elhotel",
			"3" => "restaurantes",
			"4" => "booking"
		];

		$estadistica_secciones = $this->getSecciones($secciones);

		$paises = [
			"0" => "Venezuela",
			"1" => "Germany",
			"2" => "United States"
		];
		$estadistica_paises = $this->getPaises($paises);


		$this->meseslanzados();

		//VISITAS GENERALES
		$meses = $this->getMonths();
		$visitas = $this->totalsessiones();

		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.dashboard",[
			"usuario" => $usuario,
			"vencidas" => $this->promovencidas(),
			"activas" => $this->promosactivar(),
			"rangos" => Promociones::promocionesrango(),
			"list_avatares" => Avatares::all(),
			"list_usuarios" => Usuario::obtenerUsuarios(),
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"dispositivos" => json_encode($estadistica_dispositivos),
			"navegadores" => json_encode($estadistica_navegadores),
			"secciones" => json_encode($estadistica_secciones),
			"meses" => json_encode($meses),
			"visitas" => json_encode($visitas),
			"paises" => json_encode($estadistica_paises),
		]);
	}

	//INGRESO A MI CUENTA
	public function account(){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.me_account",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_avatares" => Avatares::all(),
			"meavatar" => url(Usuario::where("id",$usuario->id)->value("user_avatar")),
			"texto" => "No hay avatares disponibles a utilizar.",
			"ruta" => url('admin/account')
		]);
	}

	//LOCK
	public function bloquear(){
		DB::table("cache")->where("key","laravelusuario")->delete(); //ELIMINO EL CACHE DE LA BD
		\Cache::flush();//LIMPIO EL CACHE GENERADO
		return redirect()->to("admin/backend");//REGRESO AL INDEX
	}

	//LIBERO EL CACHE DE LA APP
	public function salir(){
		DB::table("cache")->where("key","laravelusuario")->delete(); //ELIMINO EL CACHE DE LA BD
		\Cache::flush();//LIMPIO EL CACHE GENERADO
		return redirect()->to("admin/backend");//REGRESO AL INDEX
	}
	
	//ADMINISTRAR
	public function administrargaleria(){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.administrargalerias",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
		]);
	}
	
	//GESTIONAR
	public function gestionargaleriaprincipal(){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.gestionargaleriaprincipal",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
		]);
	}
	
	public function gestionarimagensalud(){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.gestionarimagensalud",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
		]);
	}
	
	//CREAR
	public function crear_servicios(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$servicios = Servicios::obtenerServicios();

		return view("backend.create_servicioshome",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"servicios" => json_decode($servicios)
		]);
	}

	public function crear_galeria(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.create_galeriahome",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"galeria_home" => Galeria::obtenerGaleria(),
			"size_galeria" => Galeria::size(),
			"promos_activas" => Promociones::get_promosActivas()
		]);
	}

	public function crear_bsalud(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.create_bannerinferior",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role)
		]);
	}

	public function crear_promociones(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$fecha = explode(" ",$fecha_actual)[0];
		$partes = explode("-",$fecha);
		$mes = "";
		if(($partes[1]+3) < 10){
			$mes = "0".($partes[1]+3);
		}else if(($partes[1]+3) >= 10 && ($partes[1]+3) <= 12){
			$mes = $partes[1]+3;
		}else if($partes[1] == 11){
			$mes = "01";
		}else if($partes[1] == 12){
			$mes = "03";
		}
		
		$fecha_fin = $partes[0]."-".$mes."-".$partes[2];
		return view("backend.create_promocioneshome",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"fecha_min" => $fecha,
			"fecha_max" => $fecha_fin,
		]);
	}

	public function crear_eventos(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$fecha = explode(" ",$fecha_actual)[0];
		$habitaciones = "<select class=\"corporativo selectlink form-control form-border\"  name=\"habitaciones\"><option  value=\"\" selected disabled> Seleccione una habitacion</option>";
		foreach(json_decode(Habitacion::getAllhabitacionesname(),true) as $clave => $habitacion){
			$habitaciones .= "<option value=\"habitaciones/".base64_encode($habitacion['habitacion'])."\">".$habitacion['nombre']."</option>";
		}
		$habitaciones .= "</select>";
		return view("backend.create_eventos",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"fecha_min" => $fecha,
			"habitaciones" => $habitaciones
		]);
	}

	public function crear_habitacion(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");

		return view("backend.create_habitacion",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
		]);
	}

	public function crear_galeriaroom(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");

		$h = new Habitacion();
		$b = new Bannergaleri();
		$listhabitad = array();
		$data = array();
		$habitaciones = $h->getAllhabitaciones();
		foreach(json_decode($habitaciones,true) AS $clave => $valor){
			$data = array_add($data,'habitad_value',base64_encode($valor['habitacion']));
			$data = array_add($data,'habitad_name',$valor['nombre']);
			$listhabitad = array_add($listhabitad,$clave,$data);
			$data = array();
		}
		
		$galeria = array('1' => url('img/default.jpg'));
		$galeria = array_add($galeria,'2',url('img/default.jpg'));
		$galeria = array_add($galeria,'3',url('img/default.jpg'));
		$galeria = array_add($galeria,'4',url('img/default.jpg'));
		$galeria = array_add($galeria,'5',url('img/default.jpg'));
		$galeria = array_add($galeria,'6',url('img/default.jpg'));
		$galeria = array_add($galeria,'7',url('img/default.jpg'));
		$galeria = array_add($galeria,'8',url('img/default.jpg'));
		
		return view("backend.galeria_habitacion",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"listhabitad" => $listhabitad,
			"galeria" => $galeria,
			"texto" => "No hay habitaciones registradas en el sistema..",
			"ruta" => url('admin/crear_habitacion')
		]);
	}

	public function crear_galeriahotel(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		$galeria = "";
		$logos = json_decode(Restaurant::getLogos());
		for($i = 0; $i < 3; $i++){
			$galeria .= "<div class=\"col-xs-12 col-sm-12 col-md-4\">
			<a href='".url('admin/crear_restgaleria/'.str_random(5).($i+1))."' class='text-white text-center center'><div class=\"col-xs-12 box-image box-hover-border\">
				<img class=\"p_picture\" src=\"".url($logos[$i]->restaurant_rest_file2)."\" id=\"".($i)."_picture\">
			</div></a>
			</div>";
		}

		return view("backend.create_addgaleriahotel",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"galeria" => $galeria
		]);
	}

	public function crear_fotosecundariaroom(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$b = new Bannergaleri();
		$fija = $b->getfijasection('2');
		return view("backend.bannerinf_habitad",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"fija" => $fija
		]);
	}

	public function crear_fotoprincipalroom(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$b = new Bannergaleri();
		$fija = $b->getbannersection('2');
		return view("backend.bannertop_habitad",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"fija" => $fija
		]);
	}

	public function crear_fotoprincipalhotel(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$b = new Bannergaleri();
		$fija = $b->getbannersection('1');
		return view("backend.banner_hotel",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"fija" => $fija
		]);
	}

	public function crear_restgaleria(AccessOperator $operator, $opc){
		$usuario = \Cache::store('database')->get("usuario");
		$identificador = substr($opc,strlen($opc)-1,strlen($opc));
		$b = new Bannergaleri();
		$images = json_decode($b->getGaleriaRestID("3",$identificador),true);
		$galeria = "";
		foreach($images AS $clave => $valor){
			if( $valor['bannergaleri_destacar'] == '1'){
				$label = "<label id=\"".($clave + 1)."_label\" class=\"checkbox-inline\">
					<input checked type=\"checkbox\" value=\"1\" id=\"destacados_".($clave + 1)."\" name=\"destacados_".($clave + 1)."\" class=\"checkboxgroup\" data-image=\"#".($clave + 1)."_picture\"><i class=\"fa fa-square-o fa-3x\"></i><i class=\"fa fa-check-square-o fa-3x\"></i>
					<button class=\"delmy\" type=\"button\" data-file=\"#file_".($clave + 1)."\" data-image='{\"delgaleria\":\"".base64_encode($valor['id'])."\",\"delimage\":\"".$valor['bannergaleri_rutaimg']."\"}' data-label=\"#".($clave + 1)."_label\" data-picture=\"#".($clave + 1)."_picture\"><i class=\"fa fa-trash-o fa-2x\" aria-hidden=\"true\"></i></button>
				</label>";
			}else{
				$label = "<label id=\"".($clave + 1)."_label\" class=\"checkbox-inline\">
					<input type=\"checkbox\" value=\"1\" id=\"destacados_".($clave + 1)."\" name=\"destacados_".($clave + 1)."\" class=\"checkboxgroup\" data-image=\"#".($clave + 1)."_picture\"><i class=\"fa fa-square-o fa-3x\"></i><i class=\"fa fa-check-square-o fa-3x\"></i>
					<button class=\"delmy\" type=\"button\" data-file=\"#file_".($clave + 1)."\" data-image='{\"delgaleria\":\"".base64_encode($valor['id'])."\",\"delimage\":\"".$valor['bannergaleri_rutaimg']."\"}' data-label=\"#".($clave + 1)."_label\" data-picture=\"#".($clave + 1)."_picture\"><i class=\"fa fa-trash-o fa-2x\" aria-hidden=\"true\"></i></button>
				</label>";
			}
			$galeria .= "<div class=\"col-xs-12 col-sm-6 col-md-3\">
			<div class=\"col-xs-12 box-image\">
				".$label."
				<img class=\"p_picture\" src=\"".url($valor['bannergaleri_rutaimg'])."\" id=\"".($clave + 1)."_picture\">
			</div>
			<div class=\"col-xs-12 box-file\">
				<input type=\"file\" id=\"file_".($clave + 1)."\" name=\"file_".($clave + 1)."\" class=\"file_img\" data-image=\"#".($clave + 1)."_picture\" data-label=\"#".($clave + 1)."_label\" />
			</div>
			</div>";
		}

		$galeria .= "<div class=\"col-xs-12\">
			<a id=\"btnRegistrar\" class=\"btn btn-app btn-palmasinn\">
				<i class=\"fa fa-edit\"></i> MODIFICAR
			</a>
		</div>";

		return view("backend.galeria_restaurante",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"galeria" => $galeria,
			"idrest" => $opc
		]);
	}

	public function crear_instalaciones(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$b = new Bannergaleri();
		$images = json_decode($b->getGaleriinsta()->toJson(),true);
		$galeria = "";
		foreach($images AS $clave => $valor){
			$galeria .= "<div class=\"col-xs-12 col-sm-6 col-md-3\">
			<div class=\"col-xs-12 box-image\">
				<div class=\"delimage\">
					<button class=\"delmy\" type=\"button\" data-file=\"#file_".($clave + 1)."\" data-image='{\"delgaleria\":\"".base64_encode($valor['id'])."\",\"delimage\":\"".$valor['bannergaleri_rutaimg']."\"}' data-label=\"#file_".($clave + 1)."\" data-picture=\"#".($clave + 1)."_picture\"><i class=\"fa fa-trash-o fa-2x\" aria-hidden=\"true\"></i></button>
				</div>
				<img class=\"p_picture\" src=\"".url($valor['bannergaleri_rutaimg'])."\" id=\"".($clave + 1)."_picture\">
			</div>
			<div class=\"col-xs-12 box-file\">
				<input type=\"file\" id=\"file_".($clave + 1)."\" name=\"file_".($clave + 1)."\" class=\"file_img\" data-image=\"#".($clave + 1)."_picture\" data-label=\"#".($clave + 1)."_label\"/>
			</div>
			</div>";
		}
		$galeria .= "<div class=\"col-xs-12\">
			<a id=\"btnRegistrar\" class=\"btn btn-app btn-palmasinn\">
				<i class=\"fa fa-edit\"></i> MODIFICAR
			</a>
		</div>";
		return view("backend.instalaciones_palmas",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"galeria" => $galeria
		]);
	}

	public function crear_comerciales(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.comerciales_palmas",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"comerciales" => json_decode(Comerciales::obtenerComerciales())
		]);
	}

	public function crear_hotel(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$hotel = json_decode(ElHotel::obtenerHistoria());
		return view("backend.elhotel_palmas",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"hotel" => $hotel
		]);
	}

	public function crear_eventoscorporativos(AccessAdmin $operator){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.evecorporativos_palmas",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"eventos" => json_decode(EventosCorporativos::obtenerEventos())
		]);
	}

	public function crear_proyectos(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.proyectos_palmas",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"proyectos" => json_decode(Proyectos::obtenerProyectos())
		]);
	}

	public function crear_usuario(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.create_users",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
		]);
	}

	//LIST
	public function listar_galeria(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.list_galeriahome",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_galeria" => json_decode(Galeria::get_historial("1")),
			"texto" => "No hay elementos registrados en el historial.",
			"ruta" => url('admin/crear_galeria')
		]);
	}

	public function listar_restfija(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		$b = new Bannergaleri();
		//$fija = $b->getfijasection('3');
		$habaneros = Bannergaleri::where('bannergaleri_section','3')->where('bannergaleri_destacar','2')->where('bannergaleri_id','1')->select('bannergaleri_rutaimg')->first();
		if(is_null($habaneros)){
			$habaneros = "img/default.jpg";
		}else{
			$habaneros = $habaneros->bannergaleri_rutaimg;
		}
		$maracuya = Bannergaleri::where('bannergaleri_section','3')->where('bannergaleri_destacar','2')->where('bannergaleri_id','2')->select('bannergaleri_rutaimg')->first();
		if(is_null($maracuya)){
			$maracuya = "img/default.jpg";
		}else{
			$maracuya = $maracuya->bannergaleri_rutaimg;
		}
		$lobbybar = Bannergaleri::where('bannergaleri_section','3')->where('bannergaleri_destacar','2')->where('bannergaleri_id','3')->select('bannergaleri_rutaimg')->first();
		if(is_null($lobbybar)){
			$lobbybar = "img/default.jpg";
		}else{
			$lobbybar = $lobbybar->bannergaleri_rutaimg;
		}
		return view("backend.fija_restaurante",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"habaneros" => $habaneros,
			"maracuya" => $maracuya,
			"lobbybar" => $lobbybar
		]);
	}

	public function listar_asociaciones(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.list_asociaciones",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_galeria" => Galeria::getGaleriaAsociada(),
			"texto" => "No hay asociaciones presentes.",
			"textoasochechas" => "No hay otras promociones registradas.",
			"ruta" => url('admin/crear_promociones')
		]);
	}

	public function listar_promocion(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.list_promohome",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_promociones" => Promociones::get_promosAll(),
			"texto" => "No hay elementos registrados en el historial.",
			"ruta" => url('admin/crear_promociones')
		]);
	}

	public function listar_eventos(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$mes = explode(" ",$fecha_actual)[0];
		$year = explode("-",$mes)[0];//año actual
		$mes = explode("-",$mes)[1];//mes actual
		$date = array('mes'=> $mes);
		$date = array_add($date,'year',$year);

		$falg = true;
		$eventos = Eventos::get_eventosAll($date);
		while(count($eventos) == 0 && $falg){
			if(12 > $date['mes']){
				$date['mes']+=1;
				if($date['mes'] == $mes){
					$date['year'] = $year;//retomo año actual
					$falg = false;//para salirdelwhile si no hay nada
				}
			}else{
				$date['mes'] = 1;
				$date['year']+=1;
			}
			$eventos = Eventos::get_eventosAll($date);
		}
		$fecha = explode(" ",$fecha_actual)[0];
		return view("backend.list_eventoshome",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_eventos" => $eventos,
			"texto" => "No hay elementos registrados en el historial.",
			"ruta" => url('admin/crear_eventos'),
			"mes" => $date['mes'],
			"year" => $date['year'],
			"fecha_min" => $fecha
		]);
	}

	public function listar_meseseventos(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$mes = explode(" ",$fecha_actual)[0];
		$year = explode("-",$mes)[0];//año actual
		$mes = explode("-",$mes)[1];//mes actual
		$m = new Meses();
		$this->meseslanzados();
		$list_meses = $m->get_Allmesescounteventos();
		$tabla = "";
		foreach($list_meses as $clave => $lista){
			if($lista['mes_year'] == 0){
				if($mes > $lista['id']){
					$table_year = $year + 1;
				}else{
					$table_year = $year;
				}
			}else{
				$table_year = $lista['mes_year'];
			}
			$tabla .= "<tr class=\"fondo-tr\" data-historial='{\"evento\":\"".base64_encode($lista['id'])."\"}' data-evemes=\"".$lista['counteventos']."\"><td>".$lista['mes_titulo']."</td><td>".$table_year."</td><td><div class=\"row\">";
			if($lista['mes_status'] == 0){
				$tabla .= "<div class=\"col-xs-12\"><a class=\"btnActivar\"><i class=\"fa fa-toggle-off fa-2x\"></i></a></div>";
			}else{
				$tabla .= "<div class=\"col-xs-12\"><a class=\"btnActivar\"><i class=\"fa fa-toggle-on fa-2x\"></i></a></div>";
			}
			$tabla .= "</div></td><td><span class=\"badge\">".$lista['counteventos']."</span></td></tr>";
		}
		return view("backend.list_meseventoshome",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_meses" => $tabla,
			"texto" => "No hay elementos registrados en el historial.",
			"ruta" => url('admin/crear_eventos'),
			"mes" => $mes,
			"year" => $year
		]);
	}

	public function listar_habitacion(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		$h = new Habitacion();
		return view("backend.list_habitad",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_habitad" => json_decode($h->getAllhabitaciones(),true),
			"texto" => "No hay habitaciones registradas en el sistema..",
			"ruta" => url('admin/crear_habitacion')
		]);
	}

	public function listar_restaurantes(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		$r = new Restaurant();
		return view("backend.list_restaurant",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"restaurat" => $r->getAllrestaurants()
		]);
	}

	public function listar_usuario(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.list_users",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_usuarios" => Usuario::obtenerUsuarios(),
			"texto" => "No hay usuarios registrados en el sistema.",
			"ruta" => url('admin/crear_usuario')
		]);
	}

	public function listar_bsalud(AccessOperator $operator){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.list_bannerinferior",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"texto" => "No hay elementos registrados en el historial.",
			"ruta" => url('admin/crear_bsalud'),
			"list_bannerinferior" => json_decode(ISalud::obtenerBannerSalud("1")),

		]);
	}

	public function listar_corporativos(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		$eventos = Eventos::get_corporativosAll();
		return view("backend.list_corporativos",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_eventos" => $eventos,
			"texto" => "No hay elementos registrados en el historial.",
			"ruta" => url('admin/crear_eventos')
		]);
	}

	public function listar_galerinferior(AccessAdmin $admin){
		$usuario = \Cache::store('database')->get("usuario");
		return view("backend.listar_galerinferiorhome",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"list_imagen" => Bannergaleri::where('bannergaleri_section','0')->whereIn('bannergaleri_id',['1','2','3','4','5','6'])->get()
		]);
	}
		
	//EDIT
	public function viewgaleriahome(Request $request){
		return view("backend.edit_galeriahome",[
			"galeria_home" => Galeria::obtenerGaleria()
		]);
	}

	public function viewimagensalud(Request $request){
		return view("backend.edit_imagensalud",[
			"imagen_salud" => json_decode(ISalud::obtenerBannerSalud("0"))
		]);
	}

	public function viewasociacion(Request $request){
		$usuario = \Cache::store('database')->get("usuario");
		$list_galeria = Galeria::getGaleriaID(base64_decode($request->input("historial")));
		return view("backend.asociaciongaleria",[
			"usuario" => $usuario,
			"whichrole" => Usuario::whichRole($usuario->user_role),
			"lista_galerias" => $list_galeria,
			"promociones_activas" => Promociones::getPromocionesNotID($list_galeria->promo_id),
			"ruta" => url('admin/listasociaciones')
		]);
	}

	public function asociargaleriahome(Request $request){
		$promociones_activas = json_decode(base64_decode($request->input("promociones")));
		return view("backend.asociargaleriapromo",[
			"promociones_activas" => $promociones_activas,
			"lista_galerias" => json_decode(Galeria::obtenerGaleriaDisponible())
		]);
	}

	public function viewpromocioneshome(Request $request){
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$fecha = explode(" ",$fecha_actual)[0];
		$partes = explode("-",$fecha);
		$mes = "";
		if(($partes[1]+3) < 10){
			$mes = "0".($partes[1]+3);
		}else if($partes[1]+3 >= 10 && $partes[1]+3 <= 12){
			$mes = $partes[1]+3;
		}else if($partes[1] == 11){
			$mes = "01";
		}else if($partes[1] == 12){
			$mes = "03";
		}

		$fecha_fin = $partes[0]."-".$mes."-".$partes[2];
		return view("backend.edit_promohome",[
			"promo_home" => Promociones::obtenerPromo($request->input("promociones")),
			"fecha_min" => $fecha,
			"fecha_max" => $fecha_fin,
			"promoid" => $request->input("promociones"),
		]);
	}

	public function editusers(Request $request){
		$usuario = Usuario::obtenerUsuarioID(base64_decode($request->input("user_id")));
		return view("backend.edit_users",[
			"usuarioID" => $usuario,
			"id" => $request->input("user_id")
		]);
	}

	public function editadoeventohome(Request $request){
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$fecha = explode(" ",$fecha_actual)[0];
		$e = new Eventos();
		$habitaciones = "<select class=\"corporativo form-control form-border\"  name=\"habitaciones\"><option  value=\"\" selected disabled> Seleccione una habitacion</option>";
		foreach(json_decode(Habitacion::getAllhabitacionesname(),true) as $clave => $habitacion){
			$habitaciones .= "<option value=\"habitaciones/".base64_encode($habitacion['habitacion'])."\">".$habitacion['nombre']."</option>";
		}
		$habitaciones .= "</select>";
		return view("backend.edit_eventohome",[
			"fecha_min" => $fecha,
			"evento" => $e->get_Evento($request->all()),
			"habitaciones" => $habitaciones,
		]);
	}

	public function edithabitad(Request $request){
		$h = new Habitacion();
		return view("backend.edit_habitad",[
			"habitad" => $h->getHabitacion($request->all())
		]);
	}

	public function editrestaurante(Request $request){
		$r = new Restaurant();
		return view("backend.edit_restaurant",[
			"restaurant" => json_decode($r->getRestaurant($request->all())->toJson(),true)
		]);
	}

	public function editadoeventocorporativo(Request $request){
		return view("backend.edit_eventocorporativo",[
			"evento" => Eventos::get_Evento($request->all())
		]);
	}

	public function meseslanzados(){
		$clave_mes = "";
		$array = array();
		foreach(json_decode(Meses::getMesactivos()->toJson(),true) AS $clave => $mes){
			if($mes['id'] < Carbon::now(-4)->month && $mes['mes_year'] == Carbon::now(-4)->year){
				$clave_mes = $mes['id'];
			}
		}
		$activo = Meses::where('id',$clave_mes)->first();
		if(count($activo) == 1){
			$activo->update([
				"mes_status" => "0",
				"mes_year" => "0"
			]);
		}
	}

	public function editadogaleriahome (Request $request){
		$habitaciones = "<select class=\"corporativo form-control form-border\"  name=\"habitaciones\"><option  value=\"\" selected disabled> Seleccione una habitacion</option>";
		foreach(json_decode(Habitacion::getAllhabitacionesname(),true) as $clave => $habitacion){
			$habitaciones .= "<option value=\"habitaciones/".base64_encode($habitacion['habitacion'])."\">".$habitacion['nombre']."</option>";
		}
		$habitaciones .= "</select>";
		return view("backend.create_galerinferiorhome",[
			"habitaciones" => $habitaciones,
			"galeria" => Bannergaleri::where('id',base64_decode($request->galeria))->first()
		]);
	}
}
