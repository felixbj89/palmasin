<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Habitacion;

class HabitacionController extends Controller
{
	//ADD
    public function addhabitad(Request $request){
		$h = new Habitacion();
		$estado = $h->addhabitad($request->all());
		return $estado;
	}

	//EDIT
    public function edit_habitad(Request $request){
		$h = new Habitacion();
		$estado = $h->edit_habitad($request->all());
		return $estado;
	}
	
	//ERASE
    public function erasehabitad(Request $request){
		$res = "";
		$h = new Habitacion();
		$estado = $h->erasehabitad($request->all());
		$res = array('estado' => $estado);
		return $res;
	}
}
