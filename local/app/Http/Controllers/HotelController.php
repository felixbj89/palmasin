<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ElHotel;
use Lang;

class HotelController extends Controller
{
	//ADD-HISTOROÍA
    public function addelhotel(Request $request){
		$estado = ElHotel::addelhotel($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_hotel" => Lang::get("message.yes_modify")]);
		}else{
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.evet_not_reg"));
		}
	}
}
