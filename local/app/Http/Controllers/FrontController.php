<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ISalud;
use App\Galeria;
use App\Promociones;
use App\Servicios;
use App\Meses;
use App\Eventos;
use App\EventosCorporativos;
use App\Habitacion;
use App\ElHotel;
use App\Bannergaleri;
use App\Proyectos;
use App\Restaurant;
use App\Comerciales;
use Carbon\Carbon;
use Session;
use Config;
use Redirect;
use Lang;
use App;

class FrontController extends Controller
{
	private function bloquesalud(){
		return json_decode(ISalud::obtenerBannerSalud("0"));
	}
	
	public function switchLang($lang){
		if (array_key_exists($lang, Config::get('languages'))) {
			Session::put('applocale', $lang);
		}
		
		return Redirect::back();
    }

	private function cuadro($date){//eventos cuadros peque
		$cuadro = $eventos = $evento_ppal = "";
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$m = new Meses();
		$eventos = json_decode(Eventos::get_eventosAll($date)->toJson(),true);
		$datevens = Carbon::parse($date['year']."-".$date['mes']."-01");
		if(count($eventos) > '0'){//existen eventos en el mes
			if($datevens->gt($fecha_actual)){//si son eventos futuros
				$ppal = Eventos::where("event_type","0")->whereMonth('event_dateini',$date['mes'])->whereYear('event_dateini',$date['year'])->where('event_ppal','1')->first();
				if(count($ppal) == '1'){
					foreach(Eventos::get_eventosAll($date) AS $clave => $valor){
						if($valor->event_ppal == '1'){
							$evento_ppal = $clave;
							break;
						}
					}
				}else{//no hayppal el primero
					$evento_ppal = 0;
				}
			}else{//si son eventos pasados el ultimo
				$evento_ppal = count($eventos) - 1;
			}
		}else{//no hay eventos en el mes
			//No se cumple no se puede lanzar un mes si eventos
		}
		$cuadro .= "<div class=\"fijos background_img\" style=\"background-image:url(".url('img/home/eventos')."/".$eventos[$evento_ppal]['event_path'].");\"><button class=\"datevent\" data-mes=\"".(int)explode("-",$eventos[$evento_ppal]['event_dateini'])[1]."\">".explode("-",explode(" ",$eventos[$evento_ppal]['event_dateini'])[0])[2]."</br>".$m->get_mes(explode('-',$eventos[$evento_ppal]['event_dateini'])[1])->mes_nombre."</button><!-- dia mes url--></div>";
		return $cuadro;
	}

	private function cuadro_rand($date){//eventos cuadros peque randon
		$cuadro = $eventos = $max = $rand = "";
		$e = new Eventos();
		$m = new Meses();
		$eventos = json_decode($e->get_eventosAll($date)->toJson(),true);
		$max = count($eventos) - 1;
		$rand = 0;
		if(count($eventos) > 1){
			do{
				$rand = rand( 0,$max);
			}while ($eventos[$rand]['event_ppal'] == '1');
		}else if(count($eventos) == 1){
			$rand = 0;
		}
		if(count($eventos) > 0){
			if(Carbon::now(-4)->between(Carbon::parse($eventos[$rand]['event_dateini']),Carbon::parse($eventos[$rand]['event_datefin']))){
				//fecha actual
				$cuadro .= "<div class=\"fijos background_img\" style=\"background-image:url(".url('img/home/eventos')."/".$eventos[$rand]['event_path'].");\"><button class=\"datevent\" data-mes=\"".Carbon::now(-4)->month."\">".Carbon::now(-4)->day."</br>".$m->get_mes(Carbon::now(-4)->month)->mes_nombre."</button><!-- dia mes url--></div>";
			}else if(Carbon::parse($eventos[$rand]['event_dateini'])->gte(Carbon::now(-4))){
				//fecha ini
				$cuadro .= "<div class=\"fijos background_img\" style=\"background-image:url(".url('img/home/eventos')."/".$eventos[$rand]['event_path'].");\"><button class=\"datevent\" data-mes=\"".(int)explode("-",$eventos[$rand]['event_dateini'])[1]."\">".explode("-",explode(" ",$eventos[$rand]['event_dateini'])[0])[2]."</br>".$m->get_mes(explode("-",$eventos[$rand]['event_dateini'])[1])->mes_nombre."</button><!-- dia mes url--></div>";
			}else{
				//fecha fin
				$cuadro .= "<div class=\"fijos background_img\" style=\"background-image:url(".url('img/home/eventos')."/".$eventos[$rand]['event_path'].");\"><button class=\"datevent\" data-mes=\"".(int)explode("-",$eventos[$rand]['event_datefin'])[1]."\">".explode("-",explode(" ",$eventos[$rand]['event_datefin'])[0])[2]."</br>".$m->get_mes(explode("-",$eventos[$rand]['event_datefin'])[1])->mes_nombre."</button><!-- dia mes url--></div>";
			}
		}else{
			$cuadro .= "<div class=\"fijos background_img\" style=\"background-image:url(".url('img')."/default.jpg);\"></div>";
		}
		return $cuadro;
	}

	private function cuadro_principal($date){//evento principal
		$eve_ppal = $day = "";
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$evento_ppal = -1;
		$m = new Meses();
		$eventos = json_decode(Eventos::get_eventosAll($date)->toJson(),true);
		$ppal = Eventos::where("event_type","0")->whereMonth('event_dateini',$date['mes'])->whereYear('event_dateini',$date['year'])->where('event_ppal','1')->first();
		if(count($ppal) == '1'){
			$dateven = Carbon::parse($ppal['event_datefin']);
			if($dateven->gt($fecha_actual)){//si la fecha del principal es mas grande bien
				foreach(Eventos::get_eventosAll($date) AS $clave => $valor){
					if($valor->event_ppal == '1'){
						$evento_ppal = $clave;
						break;
					}
				}
			}else{//no lo muestro la fecha ya paso
				foreach(Eventos::get_eventosAll($date) AS $clave => $valor){
					$dateven = Carbon::parse($valor->event_dateini);
					if($dateven->gte($fecha_actual)){//si la fhecha es mayor o igual a la actual
						$evento_ppal = $clave;
						break;
					}
				}
			}
		}else{//no hay evento ppal
			foreach(Eventos::get_eventosAll($date) AS $clave => $valor){
				if(Carbon::now(-4)->between(Carbon::parse($valor->event_dateini),Carbon::parse($valor->event_datefin))){
					$evento_ppal = $clave;
					break;
				}else if(Carbon::parse($valor->event_dateini)->gte(Carbon::now(-4))){
					$evento_ppal = $clave;
					break;
				}
			}
			if($evento_ppal == -1){
				$evento_ppal = count($eventos) - 1;
			}
		}
		if(Carbon::now(-4)->between(Carbon::parse($eventos[$evento_ppal]['event_dateini']),Carbon::parse($eventos[$evento_ppal]['event_datefin']))){
			$day = $fecha_actual->day;
		}else{
			if(Carbon::parse($eventos[$evento_ppal]['event_dateini'])->gte($fecha_actual)){
				$day = Carbon::parse($eventos[$evento_ppal]['event_dateini'])->day;
			}else if($fecha_actual->gte($eventos[$evento_ppal]['event_datefin'])){
				$day = Carbon::parse($eventos[$evento_ppal]['event_datefin'])->day;
			}
		}
		if($evento_ppal != -1){//existen eventos en el mes
			if(strlen($eventos[$evento_ppal]['event_href']) > 0){
				$url = "<a href=\"".$eventos[$evento_ppal]['event_href']."\" class=\"view\">ver>></a>";
				$pop = "<li><button class=\"compartir\" data-url=\"".$eventos[$evento_ppal]['event_href']."\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></button></li>";
			}else{
				$url = "";
				$pop = "";
			}
			$eve_ppal .= "<div class=\"fijos background_img\" style=\"background-image:url(".url('img/home/eventos')."/".$eventos[$evento_ppal]['event_path'].");\">";
			$eve_ppal .= "<div><div class=\"row\"><div class=\"col-xs-12\">";
			$eve_ppal .= "<p class=\"titiloseven\">".$eventos[$evento_ppal]['event_title']."</p><!-- titulo -->";
			$eve_ppal .= "<p style=\"max-height:145px;overflow:hidden;\">".$eventos[$evento_ppal]['event_text']."</p><!--truncar parrafo-->";
			$eve_ppal .= "</div></div><div class=\"row\"><div class=\"col-xs-12\" style=\"height:95px;position:absolute;bottom:15px;\">";
			$eve_ppal .= "<p class=\"horario\" style=\"width:calc(100% - 85px);\">Horario: ".$eventos[$evento_ppal]['event_horaini']." ".$url."</p><!-- titulo -->";
			if(Carbon::parse($eventos[$evento_ppal]['event_datefin'])->gt($fecha_actual)){
				$eve_ppal .= "<ul class=\"list-inline\"><li><a data-horain=\"".$eventos[$evento_ppal]['event_horaini']."\" data-timein=\"".$eventos[$evento_ppal]['event_dateini']."\" data-timefin=\"".$eventos[$evento_ppal]['event_datefin']."\" data-nombre=\"".$eventos[$evento_ppal]['event_title']."\" data-texto=\"".$eventos[$evento_ppal]['event_text']."\" data-toggle=\"modal\" href=\"#modalEvepromocionalpalmas\" class=\"btn btn-reserevent\">Reservar</a></li>".$pop."</ul>";//<-------pop compartir
			}else{
				$eve_ppal .= "<ul class=\"list-inline\"><li>Evento Finalizado</li></ul>";
			}
			$eve_ppal .= "<button class=\"datevent\" style=\"margin:initial;left:initial;\">".$day."</br>".$m->get_mes(explode("-",explode(" ",$eventos[$evento_ppal]['event_dateini'])[0])[1])->mes_nombre."</button><!-- dia mes url-->";
			$eve_ppal .= "</div></div></div></div>";
			$res_ppal = array('ppal' => $evento_ppal);
			$res_ppal = array_add($res_ppal,'eve_ppal',$eve_ppal);
		}else{//en el mes no hay eventos
			$eve_ppal = "<div class=\"fijos background_img\" style=\"background-image:url(".url('img')."/default.jpg);\"></div>";
			$res_ppal = array('ppal' => $evento_ppal);
			$res_ppal = array_add($res_ppal,'eve_ppal',$eve_ppal);
		}
		return $res_ppal;
	}

	private function lista_meses($mesesact){
		$res_meses = $key = $meses = $sigyear = "";
		$flag = false;
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$fecha = explode(" ",$fecha_actual)[0];
		$mes_actual = explode("-",$fecha)[1];//mes actual
		$dia_actual = explode("-",$fecha)[2];//dia actual
		$year_actual = explode("-",$fecha)[0];//año actual
		foreach($mesesact as $clave => $valor){
			$encontrado = array_filter($valor,function($v, $k)use($mes_actual){return $k == 'id' && $v == $mes_actual;},ARRAY_FILTER_USE_BOTH);
			if(count($encontrado) == 1){
				$key = $clave;
			}
		}
		if (strcmp($key, "") !== 0){//mes actual esta en mess lanzados
			foreach($mesesact as $clave => $mes){
				if($mes['id'] >= Carbon::now(-4)->month && $mes['mes_year'] >= Carbon::now(-4)->year){
					if($clave == $key){//mes actualmes activo
						$meses .= "<li><button class=\"btn-mesactivo active\" data-mes=\"".$mes['id']."\" data-year=\"".$mes['mes_year']."\" data-key=\"".$clave."\">".$mes['mes_nombre']."</button></li>";
					}else{
						$meses .= "<li><button class=\"btn-mesactivo\" data-mes=\"".$mes['id']."\" data-year=\"".$mes['mes_year']."\" data-key=\"".$clave."\">".$mes['mes_nombre']."</button></li>";
					}
				}else{
					if($mes['mes_year'] >= Carbon::now(-4)->year){
						$sigyear .= "<li><button class=\"btn-mesactivo\" href=\"\" data-mes=\"".$mes['id']."\" data-year=\"".$mes['mes_year']."\" data-key=\"".$clave."\">".$mes['mes_nombre']."</button></li>";
					}
				}
			}
		}else{//mes actual no esta en mess lanzados
			foreach($mesesact as $clave => $valor){
				$encontrado = array_filter($valor,function($v, $k)use($mes_actual){return $k == 'id' && $v > $mes_actual;},ARRAY_FILTER_USE_BOTH);
				if(count($encontrado) == 1 && !$flag){
					$key = $clave;//activo el mes mas siguiente al actual
					$flag = true;
				}
			}
			if (strcmp($key, "") == 0){//no hay mes sigiente activo el primero mes del año siguiente
				$key = 0;
			}
			foreach($mesesact as $clave => $mes){
				if($mes['id'] >= Carbon::now(-4)->month && $mes['mes_year'] >= Carbon::now(-4)->year){
					if($clave == $key){
						$meses .= "<li><button href=\"\" class=\"btn-mesactivo active\" data-mes=\"".$mes['id']."\" data-year=\"".$mes['mes_year']."\" data-key=\"".$clave."\">".$mes['mes_nombre']."</button></li>";
					}else{
						$meses .= "<li><button class=\"btn-mesactivo\" href=\"\" data-mes=\"".$mes['id']."\" data-year=\"".$mes['mes_year']."\" data-key=\"".$clave."\">".$mes['mes_nombre']."</button></li>";
					}
				}else{
					if($mes['mes_year'] >= Carbon::now(-4)->year){
						$sigyear .= "<li><button class=\"btn-mesactivo\" href=\"\" data-mes=\"".$mes['id']."\" data-year=\"".$mes['mes_year']."\" data-key=\"".$clave."\">".$mes['mes_nombre']."</button></li>";
					}
				}
			}
		}
		if(strcmp($sigyear, "") !== 0){
			$meses .= $sigyear;
		}
		$res_meses = array('key' => $key);
		$res_meses = array_add($res_meses,'meses',$meses);
		return $res_meses;
	}

	private function lista_eventos($date,$ppal){
		$li_eventos = "";
		$e = new Eventos();
		$m = new Meses();
		$eventos = json_decode($e->get_eventosAll($date)->toJson(),true);
		$mes_nombre = $m->get_mes(explode("-",explode(" ",$eventos[$ppal]['event_dateini'])[0])[1])->mes_nombre;
		foreach($eventos as $clave => $valor){
			if(($valor["event_title"]=="DJ" || $valor["event_title"]=="dj") && App::getLocale()=="en"){
				$valor["event_text"] = Lang::get("home.short_dj");
			}else if(($valor["event_title"]=="Talento en vivo" || $valor["event_title"]=="TALENTO EN VIVO") && App::getLocale()=="en"){
				$valor["event_title"] = "Live Talent";
				$valor["event_text"] = Lang::get("home.short_talento");
			}
			
			if(strlen($valor['event_href']) > 0){
				$url = "<a href=\"".$valor['event_href']."\" class=\"view\">ver>></a>";
				$pop = "<li><button class=\"compartir\" data-url=\"".$valor['event_href']."\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></button></li>";
			}else{
				$url = "";
				$pop = "";
			}
			$fecha_actual = Carbon::now(-4,"America/Asuncion");
			$dateven = Carbon::parse($valor['event_datefin']);
			//if($ppal != $clave && $dateven->gt($fecha_actual)){
			if($ppal != $clave){
				$day = "";
				if(Carbon::now(-4)->between(Carbon::parse($valor['event_dateini']),Carbon::parse($valor['event_datefin']))){
					$day = $fecha_actual->day;
				}else{
					if(Carbon::parse($valor['event_dateini'])->gte($fecha_actual)){
						$day = Carbon::parse($valor['event_dateini'])->day;
					}else if($fecha_actual->gte(Carbon::parse($valor['event_datefin']))){
						$day = Carbon::parse($valor['event_datefin'])->day;
					}
				}
				$li_eventos .= "<li><div class=\"col-md-12 col-lg-4\">";
				$li_eventos .= "<div class=\"fijos background_img\" style=\"background-image:url(".url('img/home/eventos')."/".$valor['event_path'].");\">";
				$li_eventos .= "<button class=\"datevent\">".$day."</br>".$mes_nombre."</button><!-- dia mes url-->";
				$li_eventos .= "</div></div><div class=\"col-md-12 col-lg-8\"><!-- descripcion evento --><div class=\"row\"><div class=\"col-xs-12\"><table><tr><td>";
				$li_eventos .= "<p class=\"titiloseven\">".$valor['event_title']."</p><!-- titulo -->";
				$li_eventos .= "<p style=\"max-height:145px;overflow:hidden;\">".$valor['event_text']."</p><!--truncar parrafo; href -->";//<a href=\"#\" class=\"view\"> ver>></a>
				$li_eventos .= "</td></tr></table></div></div><div class=\"row\"><div class=\"col-xs-12\">";
				$li_eventos .= "<p class=\"horario\" style=\"width:calc(100% - 85px);\">Horario: ".$valor['event_horaini']." ".$url."</p>";
				if($dateven->gt($fecha_actual)){
					$li_eventos .= "<ul class=\"list-inline\"><li><a data-horain=\"".$valor['event_horaini']."\" data-timein=\"".$valor['event_dateini']."\"  data-timefin=\"".$valor['event_datefin']."\" data-nombre=\"".$valor['event_title']."\" data-texto=\"".$valor['event_text']."\" data-toggle=\"modal\" href=\"#modalEvepromocionalpalmas\" class=\"btn btn-reserevent\">Reservar</a></li>".$pop."</ul>";//<-------pop compartir
				}else{
					$li_eventos .= "<ul class=\"list-inline\"><li>Evento Finalizado</li></ul>";
				}
				$li_eventos .= "</div></div></div></li>";
			}
		}

		if(strcmp($li_eventos, "") == 0){
			$li_eventos .= "<li><div class=\"col-md-12 col-lg-4\"><div class=\"fijos background_img text-center\" style=\"color:#72a84f;display:table;\"><i style=\"display: table-cell;vertical-align: middle;\" class=\"fa fa-calendar-times-o fa-5x\" aria-hidden=\"true\"></i></div></div><div class=\"col-md-12 col-lg-8\"><!-- descripcion evento --><div class=\"row\"><div class=\"col-xs-12\" style=\"height:270px;\"><table><tbody><tr><td><p class=\"titulo\" style=\"color:#72a84f;\">No hay mas eventos registrados este mes.</p></td></tr></tbody></table></div></div></div></li>";
		}
		return $li_eventos;
	}

	private function generateSlider($indicators, $imets){
		$g = new Galeria();
		$promociones = Promociones::getPromosactives();
		foreach(json_decode($g->getAllslider("0"),true) as $clave => $slide){
			if($clave == 0){
				$promo = Promociones::where('id',$slide['promo'])->first();
				$pp = -1;
				foreach($promociones AS $key => $promos){
					if(strcmp(base64_decode($promos['id']),$promo['id']) == 0){
						$pp = $key; //muestro el boton
						break;
					}
				}
				if($pp == -1){
					$imets .= "<div class=\"item active\" style=\"background-image:url(".url($slide['img']).");\"></div>";
				}else{
					$imets .= "<div class=\"item active\" style=\"background-image:url(".url($slide['img']).");\">
					<a class=\"p_slider btn-scroll btn btn-reserva\" href=\"#carouselPromo\" data-slide-to=\"1\">".$promociones[$pp]['title']."</a>
					</div>";
				}
				$indicators .= "<li data-target=\"#Carousel_palmasinn\" data-slide-to=\"".$clave."\" class=\"active\"></li>";
			}else{
				$promo = Promociones::where('id',$slide['promo'])->first();
				$pp = -1;
				foreach($promociones AS $key => $promos){
					if(strcmp(base64_decode($promos['id']),$promo['id']) == 0){
						$pp = $key; //muestro el boton
						break;
					}
				}
				if($pp == -1){
					$imets .= "<div class=\"item\" style=\"background-image:url(".url($slide['img']).");\"></div>";
				}else{
					$imets .= "<div class=\"item\" style=\"background-image:url(".url($slide['img']).");\">
					<a class=\"p_slider btn-scroll btn btn-reserva\" href=\"#carouselPromo\" data-slide-to=\"".($pp - 1)."\">".$promociones[$pp]['title']."</a>
					</div>";
				}
				$indicators .= "<li data-target=\"#Carousel_palmasinn\" data-slide-to=\"".$clave."\"></li>";
			}
		}
		if(strcmp($imets,"") == 0){
			$imets .= "<div class=\"item active\" style=\"background-image:url(".url('img/home/galeria/default.jpg').");\"></div>";
			$indicators .= "<li data-target=\"#Carousel_palmasinn\" data-slide-to=\"0\" class=\"active\"></li>";
		}
		return [$indicators, $imets];
	}

	public function booking(Request $request){

		$dats_reserve="";
		
		if(isset($_COOKIE['booking'])){

			$dats_reserve = $_COOKIE['booking'];
		}

		if($dats_reserve != null){
			/*lhBp6*/
			$url ='https://postheocert.novared.net.ve/HotelWEB/hotel/lhBp6'.$dats_reserve;
			$url_booking = urldecode($url);
			//dd($url_booking);


		}else{
			$url_booking ='https://postheocert.novared.net.ve/HotelWEB/hotel/lhBp6';
		}

		$indicators = $imets = "";
		$galeria = [];
		$galeria = $this->generateSlider($indicators,$imets);
		$promociones =  url('/')."/home/promociones";
		$eventos = url('/')."/home/eventos";
		$res = array('indicators' => $galeria[0]);
		$res = array_add($res,'imets_ppal',$galeria[1]);
		$res = array_add($res,'promociones',$promociones);
		$res = array_add($res,'eventos',$eventos);
		$res = array_add($res,'turismo',url('/')."/home/turismosalud");
		return view("frontend.booking",
			["res" => $res,
			 "listahabitaciones" => $this->listahabitaciones(), 
			 "url_booking" => $url_booking,
			]
		);
	}

	public function index_eventos(Request $request){
		$m = new Meses();
		$respuesta = "";
		$key = $request->all()['key'];

		$date = array('mes'=> $request->all()['mes']);//mes activo
		$date = array_add($date,'year',$request->all()['year']);

		$res_ppal = ($this->cuadro_principal($date));
		$ppal = $res_ppal['ppal'];//clave del evento principal
		$eve_ppal = $res_ppal['eve_ppal'];//evento principal
		$li_eventos = ($this->lista_eventos($date,$ppal));//lista de eventos

		$mesesact = json_decode($m->getMesactivos()->toJson(),true);

		//meses lanzados
		$mesesact = json_decode($m->getMesactivos()->toJson(),true);
		$clave_mes = "";
		$primero = '0';
		foreach($mesesact as $clave => $mes){
			if($mes['id'] < Carbon::now(-4)->month && $mes['mes_year'] == Carbon::now(-4)->year){
				$clave_mes = $clave;
				$primero = '1';
			}
		}
		unset($mesesact[$clave_mes]);
		//dd($mesesact);

		$othermeses = array_filter($mesesact,function($k)use($key){return $k != $key;},ARRAY_FILTER_USE_KEY);

		if(count($othermeses) == 0){//si solo se lanzo un mes
			$date = array('mes'=> $mesesact[$primero]['id']);
			$date = array_add($date,'year',$mesesact[$primero]['mes_year']);//date  para buscar evento
			$cuadro1 = ($this->cuadro_rand($date));
			$cuadro2 = ($this->cuadro_rand($date));
		}
		if(count($othermeses) == 1){//se lanzaron dos meses
			$keys = array_keys($othermeses);
			//primer cuadro primer mes
			$date = array('mes'=> $othermeses[$keys[0]]['id']);
			$date = array_add($date,'year',$othermeses[$keys[0]]['mes_year']);//date  para buscar evento
			$cuadro1 = ($this->cuadro($date));
			$date = array('mes'=> $mesesact[$key]['id']);
			$date = array_add($date,'year',$mesesact[$key]['mes_year']);
			$cuadro2 = ($this->cuadro_rand($date));
		}
		if(count($othermeses) == 2){//se lanzaron tres meses
			$keys = array_keys($othermeses);
			//primer cuadro primer mes
			$date = array('mes'=> $othermeses[$keys[0]]['id']);
			$date = array_add($date,'year',$othermeses[$keys[0]]['mes_year']);//date  para buscar evento
			$cuadro1 = ($this->cuadro($date));
			//segundo cuadro segundo mes
			$date = array('mes'=> $othermeses[$keys[1]]['id']);
			$date = array_add($date,'year',$othermeses[$keys[1]]['mes_year']);//date  para buscar evento
			$cuadro2 = ($this->cuadro($date));
		}
		$respuesta = array('eve_ppal' => $eve_ppal);
		$respuesta = array_add($respuesta,'li_eventos',$li_eventos);
		$respuesta = array_add($respuesta,'cuadro1',$cuadro1);
		$respuesta = array_add($respuesta,'cuadro2',$cuadro2);
		return $respuesta;
	}

	public function habitajax(Request $request){
		$res = $data = "";
		$itemsgal = $destacados = "";
		$h = new Habitacion();
		$b = new Bannergaleri();
		$habitacion = json_decode($h->getHabitacion($request),true);
		$data = array('seccion' => '2');//elhotel
		$data = array_add($data,'galeria',$request->target);
		$galeria = json_decode($b->getGaleriahabitacion($data),true);
		if(count($galeria)>0){
			$count = 0;
			foreach($galeria as $clave => $gal){
				if (strcmp($gal['bannergaleri_rutaimg'],"img/default.jpg") !== 0){
					if($count == 0){
						$itemsgal .= "<div class=\"item active\">";
					}else{
						$itemsgal .= "<div class=\"item\">";
					}
					$itemsgal .= "<div>
							<div>
								<div style=\"background-image:url(".url($gal['bannergaleri_rutaimg']).");\" class=\"background_img\"></div>
							</div>
						</div>
					</div>";
					if($gal['bannergaleri_destacar'] == '1'){
						$destacados.= "<div class=\"col-xs-4 p3\">
							<div>
								<div class=\"background_img\" style=\"background-image:url(".url($gal['bannergaleri_rutaimg']).");\"></div>
							</div>
						</div>";
					}
				}$count++;
			}
		}else{
			$itemsgal .= "<div class=\"item active\">
				<div>
					<div>
						<div class=\"background_img\" style=\"background-image:url(img/default.jpg);\"></div>
					</div>
				</div>
			</div>";
			$destacados.= "<div class=\"col-xs-4 p3\">
				<div>
					<div class=\"background_img\" style=\"background-image:url(img/default.jpg);\"></div>
				</div>
			</div>
			<div class=\"col-xs-4 p3\">
				<div>
					<div class=\"background_img\" style=\"background-image:url(img/default.jpg);\"></div>
				</div>
			</div>
			<div class=\"col-xs-4 p3\">
				<div>
					<div class=\"background_img\" style=\"background-image:url(img/default.jpg);\"></div>
				</div>
			</div>";
		}

		if($habitacion == null){
			$res = array('estado' => 0);
			$res = array_add($res,'itemsgal',$itemsgal);
			$res = array_add($res,'destacados',$destacados);
		}else{
			$res = array('estado' => 1);
			$res = array_add($res,'habitacion',$habitacion);
			$res = array_add($res,"locale",App::getLocale());
			$res = array_add($res,'itemsgal',$itemsgal);
			$res = array_add($res,'destacados',$destacados);
			$res = array_add($res,'modal',$this->modatgalerihabitad($data));
		}
		return $res;
	}

	private function listahabitaciones(){
		$h = new Habitacion();
		$lista = "";
		foreach(json_decode($h->getAllhabitacionesname(),true) as $clave => $habitacion){
			$lista .= "<li><a href=\"".url('habitaciones')."/".base64_encode($habitacion['habitacion'])."\">".$habitacion['nombre']."</a></li>";
		}
		return $lista;
	}

	public function eventosgalerisajax	(){
		$indicators = $items = "";
		$count = 0;
		$e = json_decode(EventosCorporativos::obtenerEventos());
		if (strcmp($e->eventoscorporativos_picone,"img/eventoscorporativos/default.jpg") !== 0) {
			if($count == 0){
				$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails active\" style=\"background-image:url(".url($e->eventoscorporativos_picone).")\"></li>";
				$items .= "<div class=\"item active\"><div><div><img src=".url($e->eventoscorporativos_picone)."></div></div></div>";
			}else{
				$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails\" style=\"background-image:url(".url($e->eventoscorporativos_picone).")\"></li>";
				$items .= "<div class=\"item\"><div><div><img src=".url($e->eventoscorporativos_picone)."></div></div></div>";
			}
			$count++;
		}
		if (strcmp($e->eventoscorporativos_pictwo,"img/eventoscorporativos/default.jpg") !== 0) {
			if($count == 0){
				$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails active\" style=\"background-image:url(".url($e->eventoscorporativos_pictwo).")\"></li>";
				$items .= "<div class=\"item active\"><div><div><img src=".url($e->eventoscorporativos_pictwo)."></div></div></div>";
			}else{
				$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails\" style=\"background-image:url(".url($e->eventoscorporativos_pictwo).")\"></li>";
				$items .= "<div class=\"item\"><div><div><img src=".url($e->eventoscorporativos_pictwo)."></div></div></div>";
			}
			$count++;
		}
		if (strcmp($e->eventoscorporativos_picthrid,"img/eventoscorporativos/default.jpg") !== 0) {
			if($count == 0){
				$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails active\" style=\"background-image:url(".url($e->eventoscorporativos_picthrid).")\"></li>";
				$items .= "<div class=\"item active\"><div><div><img src=".url($e->eventoscorporativos_picthrid)."></div></div></div>";
			}else{
				$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails\" style=\"background-image:url(".url($e->eventoscorporativos_picthrid).")\"></li>";
				$items .= "<div class=\"item\"><div><div><img src=".url($e->eventoscorporativos_picthrid)."></div></div></div>";
			}
			$count++;
		}
		if (strcmp($e->eventoscorporativos_picfour,"img/eventoscorporativos/default.jpg") !== 0) {
			if($count == 0){
				$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails active\" style=\"background-image:url(".url($e->eventoscorporativos_picfour).")\"></li>";
				$items .= "<div class=\"item active\"><div><div><img src=".url($e->eventoscorporativos_picfour)."></div></div></div>";
			}else{
				$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails\" style=\"background-image:url(".url($e->eventoscorporativos_picfour).")\"></li>";
				$items .= "<div class=\"item\"><div><div><img src=".url($e->eventoscorporativos_picfour)."></div></div></div>";
			}
			$count++;
		}
		if($count == 0){
			$indicators = "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"0\" class=\"thumbnails\" style=\"background-image:url(".url('img/eventoscorporativos/default.jpg').")\"></li>";
			$items = "<div class=\"item\"><div><div><img src=".url('img/eventoscorporativos/default.jpg')."></div></div></div>";
		}
		$galeri = array('indicators' => $indicators);
		$galeri = array_add($galeri,'items',$items);
		return $galeri;
	}

	public function instalacionesgalerisajax(){
		$i = json_decode(Bannergaleri::getGaleriinsta());
		$indicators = $items = "";
		$count = 0;
		foreach($i as $clave => $instalacion){
			if (strcmp($instalacion->bannergaleri_rutaimg,"img/default.jpg") !== 0) {
				if($count == 0){
					$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails active\" style=\"background-image:url(".url($instalacion->bannergaleri_rutaimg).")\"></li>";
					$items .= "<div class=\"item active\"><div><div><img src=".url($instalacion->bannergaleri_rutaimg)."></div></div></div>";
				}else{
					$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails\" style=\"background-image:url(".url($instalacion->bannergaleri_rutaimg).")\"></li>";
					$items .= "<div class=\"item\"><div><div><img src=".url($instalacion->bannergaleri_rutaimg)."></div></div></div>";
				}
				$count++;
			}
		}
		if($count == 0){
			$indicators = "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"0\" class=\"thumbnails\" style=\"background-image:url(".url('img/eventoscorporativos/default.jpg').")\"></li>";
			$items = "<div class=\"item\"><div><div><img src=".url('img/eventoscorporativos/default.jpg')."></div></div></div>";
		}
		$galeri = array('indicators' => $indicators);
		$galeri = array_add($galeri,'items',$items);
		return $galeri;
	}

	public function modatgalerihabitad($data){
		$h = json_decode(Bannergaleri::getGaleriahabitacion($data));
		$indicators = $items = "";
		$count = 0;
		foreach($h as $clave => $habitacion){
			if (strcmp($habitacion->bannergaleri_rutaimg,"img/default.jpg") !== 0) {
				if($count == 0){
					$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails active\" style=\"background-image:url(".url($habitacion->bannergaleri_rutaimg).")\"></li>";
					$items .= "<div class=\"item active\"><div><div><img src=".url($habitacion->bannergaleri_rutaimg)."></div></div></div>";
				}else{
					$indicators .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"".$count."\" class=\"thumbnails\" style=\"background-image:url(".url($habitacion->bannergaleri_rutaimg).")\"></li>";
					$items .= "<div class=\"item\"><div><div><img src=".url($habitacion->bannergaleri_rutaimg)."></div></div></div>";
				}
				$count++;
			}
		}
		if($count == 0){
			$indicators = "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"0\" class=\"thumbnails\" style=\"background-image:url(".url('img/eventoscorporativos/default.jpg').")\"></li>";
			$items = "<div class=\"item\"><div><div><img src=".url('img/eventoscorporativos/default.jpg')."></div></div></div>";
		}
		$galeri = array('indicators' => $indicators);
		$galeri = array_add($galeri,'items',$items);
		return $galeri;
	}
	
    public function index(Request $request){


		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$fecha = explode(" ",$fecha_actual)[0];
		$mes_actual = explode("-",$fecha)[1];//mes actual
		$dia_actual = explode("-",$fecha)[2];//dia actual
		$year_actual = explode("-",$fecha)[0];//año actual
		$modal_load = 1;
		
		$res = $indicators = $imets = $promos = $meses = $li_meses = "";
		$key = $li_eventos = $eve_ppal = $cuadro1 = $cuadro2 = $res_ppal = "";
		$promociones = "#promociones";
		$eventos = "#eventos";
		$id = "body";

		$ppal = $countpromo = 0;

		$galeria = [];
		$galeria = $this->generateSlider($indicators,$imets);

		$p = new Promociones();
		$m = new Meses();
		$e = new Eventos();

		if(strcmp($request->target, "promociones") == 0){
			$id = "promociones";
			$modal_load = 0;
		}
		if(strcmp($request->target, "eventos") == 0){
			$id = "eventos";
			$modal_load = 0;
		}
		if(strcmp($request->target, "turismosalud") == 0){
			$id = "turismosalud";
			$modal_load = 0;
		}

		$salud = $this->bloquesalud();
		$bsalud = "<div id=\"turismosalud\" class=\"row\" style=\"background-image:url(".url($salud[0]->salud_path).");\">";
		$bsalud .= "<div class=\"col-xs-12\" style=\"height:100%;\">";
		$bsalud .= "<div id=\"contentsaludbanner\">";
		if(App::getLocale()=="es"){
			$bsalud .= "<p class=\"titulo\" style=\"font-weight:bold;\">".$salud[0]->salud_titulo."</p>";
			$bsalud .= "<p class=\"titulo\">".$salud[0]->salud_situlo."</p>";
			$bsalud .= "<a id=\"imagensalud\" href=\"#modal_salud\" data-toggle=\"modal\" data-salud=\"".url($salud[0]->salud_pathinterna)."\" class=\"btn btn-salud\" style=\"margin-top:10px;\">MÁS INFORMACIÓN</a></div></div></div>";
		}else{
			$bsalud .= "<p class=\"titulo\" style=\"font-weight:bold;\">Health tourism in Paraguaná</p>";
			$bsalud .= "<p class=\"titulo\">We are close to the main clinics for your convenience</p>";
			$bsalud .= "<a id=\"imagensalud\" href=\"#modal_salud\" data-toggle=\"modal\" data-salud=\"".url($salud[0]->salud_pathinterna)."\" class=\"btn btn-salud\" style=\"margin-top:10px;\">MORE INFORMATION</a></div></div></div>";
		}
		
		
		$bloque_salud = array("bloque_salud" => $bsalud);

		$descarga = $salud[0]->salud_pathinterna;
		
		$promociones_rango = Promociones::getPromosactives();
		//FALTATITULOS DE PROMOS EN MODAL
		foreach($promociones_rango as $clave => $promo){
			if($clave == 0){
				$promos .= "<div class=\"item row active\">";
			}else{
				$promos .= "<div class=\"item row\">";
			}
			$promos .= "<div class=\"col-xs-12 col-sm-4 itempromo\">
					<div class=\"row background_img\" style=\"background-image:url(".url('/img/home/promociones')."/".$promociones_rango[$clave]["path"].");\">
					</div>
					<div class=\"row borderpromo\">
						<div class=\"col-xs-12 text-center\" style=\"line-height:14pt;\">
							<p class=\"titulo\">".$promociones_rango[$clave]['title']."</p>
							<div class=\"parrafo\">".$promociones_rango[$clave]['text']."</div>
						</div>
						<div class=\"col-xs-12 text-center\">
							<a data-promo=\"".$promociones_rango[$clave]['id']."\" data-toggle=\"modal\" href=\"#modalPromocionespalmas\" class=\"btn btn-reserva\" style=\"margin-top:10px;margin-bottom:10px;\">ADQUIRIR PROMO</a>
						</div>
					</div>
				</div>
			</div>";//falta href promo
			$countpromo = $clave;
		}

		//meses lanzados
		$mesesact = json_decode($m->getMesactivos()->toJson(),true);
		$clave_mes = "";
		$primero = '0';
		foreach($mesesact as $clave => $mes){
			if($mes['id'] < Carbon::now(-4)->month && $mes['mes_year'] == Carbon::now(-4)->year){
				$clave_mes = $clave;
				$primero = '1';
			}
		}
		unset($mesesact[$clave_mes]);

		$li_meses = ($this->lista_meses($mesesact));
		$key = $li_meses['key'];
		$meses = $li_meses['meses'];

		if($key == 0 && strcmp($meses, "") == 0){//no se lazo ningun mes
			//$meses = "<li><i class=\"fa fa-calendar-times-o\" aria-hidden=\"true\"></i></li>";
			//$meses = 0;
			$li_eventos = "<li><div class=\"col-md-12 col-lg-4\"><div class=\"fijos background_img text-center\" style=\"color:#72a84f;display:table;\"><i style=\"display: table-cell;vertical-align: middle;\" class=\"fa fa-calendar-times-o fa-5x\" aria-hidden=\"true\"></i></div></div><div class=\"col-md-12 col-lg-8\"><!-- descripcion evento --><div class=\"row\"><div class=\"col-xs-12\" style=\"height:270px;\"><table><tbody><tr><td><p class=\"titulo\" style=\"color:#72a84f;\">No hay eventos.</p></td></tr></tbody></table></div></div></div></li>";
			$eve_ppal = "<div class=\"fijos background_img\" style=\"background-image:url(".url('img')."/default.jpg);\"></div>";
			$cuadro1 = $cuadro2 = "<div class=\"fijos background_img\" style=\"background-image:url(".url('img')."/default.jpg);\"></div>";
		}else{
			//cuadro principal
			$date = array('mes'=> $mesesact[$key]['id']);//mes activo
			$date = array_add($date,'year',$mesesact[$key]['mes_year']);//date para buscar eventos
			$res_ppal = ($this->cuadro_principal($date));
			$ppal = $res_ppal['ppal'];
			$eve_ppal = $res_ppal['eve_ppal'];
			//lista eventos
			$li_eventos = ($this->lista_eventos($date,$ppal));
			
			//cuadros peque
			$othermeses = array_filter($mesesact,function($k)use($key){return $k != $key;},ARRAY_FILTER_USE_KEY);
			if(count($othermeses) == 0){//si solo se lanzo un mes
				$date = array('mes'=> $mesesact[$primero]['id']);
				$date = array_add($date,'year',$mesesact[$primero]['mes_year']);//date  para buscar evento
				$cuadro1 = ($this->cuadro_rand($date));
				$cuadro2 = ($this->cuadro_rand($date));
			}
			if(count($othermeses) == 1){//se lanzaron dos meses
				$keys = array_keys($othermeses);
				//primer cuadro primer mes
				$date = array('mes'=> $othermeses[$keys[0]]['id']);
				$date = array_add($date,'year',$othermeses[$keys[0]]['mes_year']);//date  para buscar evento
				$cuadro1 = ($this->cuadro($date));
				$cuadro2 = ($this->cuadro_rand($date));
			}
			if(count($othermeses) == 2){//se lanzaron tres meses
				$keys = array_keys($othermeses);
				//primer cuadro primer mes
				$date = array('mes'=> $othermeses[$keys[0]]['id']);
				$date = array_add($date,'year',$othermeses[$keys[0]]['mes_year']);//date  para buscar evento
				$cuadro1 = ($this->cuadro($date));
				//segundo cuadro segundo mes
				$date = array('mes'=> $othermeses[$keys[1]]['id']);
				$date = array_add($date,'year',$othermeses[$keys[1]]['mes_year']);//date  para buscar evento
				$cuadro2 = ($this->cuadro($date));
			}
		}

		$secundaria = "";
		$galeriasecu = Bannergaleri::where('bannergaleri_section','0')->where('bannergaleri_destacar','1')->whereIn('bannergaleri_id',['1','2','3','4','5','6'])->get();
		$array = array();
		foreach($galeriasecu AS $clave => $galsecu){
			if($clave == 0){
				$secundaria .= "<div class=\"item active\">
					<div class=\"background_img\" style=\"background-image:url(".url($galsecu['bannergaleri_rutaimg']).");\">";
						if((strlen($galsecu['bannergaleri_texto']) >= 20) && strlen($galsecu['bannergaleri_link']) >= 7){
							$habitaciones = url('habitaciones');
							$limit = strlen($habitaciones);

							$posicionLobbyBar = strripos(url('restaurantes/lobbybar'),"restaurantes/lobbybar");
							$posicionBaseLobbyBar = strripos($galsecu['bannergaleri_link'],"restaurantes/lobbybar");
							$baselobby = substr($galsecu['bannergaleri_link'],$posicionBaseLobbyBar,strlen($galsecu['bannergaleri_link']));
							$lobby = substr(url('restaurantes/lobbybar'),$posicionLobbyBar,strlen(url('restaurantes/lobbybar')));

							$posicionHabaneros = strripos(url('restaurantes/habneros'),"restaurantes/habneros");
							$posicionBaseHabaneros = strripos($galsecu['bannergaleri_link'],"restaurantes/habneros");
							$basehabanero = substr($galsecu['bannergaleri_link'],$posicionBaseHabaneros,strlen($galsecu['bannergaleri_link']));
							$habanero = substr(url('restaurantes/habneros'),$posicionHabaneros,strlen(url('restaurantes/habneros')));

							$posicionMaracuya = strripos(url('restaurantes/maracuya'),"restaurantes/maracuya");
							$posicionBaseMaracuya = strripos($galsecu['bannergaleri_link'],"restaurantes/maracuya");
							$baseMaracuya = substr($galsecu['bannergaleri_link'],$posicionBaseMaracuya,strlen($galsecu['bannergaleri_link']));
							$maracuya = substr(url('restaurantes/maracuya'),$posicionMaracuya,strlen(url('restaurantes/maracuya')));

							if($basehabanero == $habanero){
								if(App::getLocale()=="en")
									$galsecu['bannergaleri_texto'] = Lang::get("home.short_habaneros");
								$img = url('img/home/secundaria/logos/Habanerosblanco.png');
							}else if($baseMaracuya == $maracuya){
								if(App::getLocale()=="en")
									$galsecu['bannergaleri_texto'] = Lang::get("home.short_maracuya");
								$img = url('img/home/secundaria/logos/maracuyablanco.png');
							}else if($baselobby == $lobby){
								if(App::getLocale()=="en")
									$galsecu['bannergaleri_texto'] = Lang::get("home.short_lobby");
								$img = url('img/home/secundaria/logos/lobbybarblanco.png');
							}else if(strcmp($galsecu['bannergaleri_link'],url('elhotel')) == 0){
								$img = url('img/home/secundaria/logos/logo.svg');
							}else if(strncasecmp($habitaciones,$galsecu['bannergaleri_link'],$limit)){
								if(App::getLocale()=="en")
									$galsecu['bannergaleri_texto'] = Lang::get("home.short_breakfast");
								$img = url('img/home/secundaria/logos/logo.svg');//Se puede cambiar para habitaciones
							}else{
								$img = url('img/home/secundaria/logos/logo.svg');//Se puede cambiar para link externo
							}
							$secundaria .= "<div class=\"row\">
								<div class=\"col-xs-12 col-sm-4\">
									<a href=\"".$galsecu['bannergaleri_link']."\"><div class=\"logo background_img\" style=\"background-image:url(".$img.");\"></div></a><!-- url logo href -->
								</div>
								<div class=\"col-xs-12 col-sm-8\" style=\"color:#FFF;padding-left: 20px;padding-right: 30px;\"><!-- colocar color #FFF o #707170-->
									<div>
										<div class=\"parrafogaleria\" style=\"background-image: url(".url('img/home/galeria/logos/lineablanca.svg').");\"><!-- Parrafo -->
											".$galsecu['bannergaleri_texto']."
										</div>
										<a href=\"".$galsecu['bannergaleri_link']."\" style=\"color:#FFF;\">".Lang::get("home.seemore").">></a><!-- colocar color #FFF o #707170-->
									</div>
								</div>
							</div>";
						}
				$secundaria .= "</div></div>";
			}else{
				$secundaria .= "<div class=\"item\">
					<div class=\"background_img\" style=\"background-image:url(".url($galsecu['bannergaleri_rutaimg']).");\">";
						if((strlen($galsecu['bannergaleri_texto']) >= 20) && strlen($galsecu['bannergaleri_link']) >= 7){
							$habitaciones = url('habitaciones');
							$limit = strlen($habitaciones);

							$posicionLobbyBar = strripos(url('restaurantes/lobbybar'),"restaurantes/lobbybar");
							$posicionBaseLobbyBar = strripos($galsecu['bannergaleri_link'],"restaurantes/lobbybar");
							$baselobby = substr($galsecu['bannergaleri_link'],$posicionBaseLobbyBar,strlen($galsecu['bannergaleri_link']));
							$lobby = substr(url('restaurantes/lobbybar'),$posicionLobbyBar,strlen(url('restaurantes/lobbybar')));

							$posicionHabaneros = strripos(url('restaurantes/habneros'),"restaurantes/habneros");
							$posicionBaseHabaneros = strripos($galsecu['bannergaleri_link'],"restaurantes/habneros");
							$basehabanero = substr($galsecu['bannergaleri_link'],$posicionBaseHabaneros,strlen($galsecu['bannergaleri_link']));
							$habanero = substr(url('restaurantes/habneros'),$posicionHabaneros,strlen(url('restaurantes/habneros')));

							$posicionMaracuya = strripos(url('restaurantes/maracuya'),"restaurantes/maracuya");
							$posicionBaseMaracuya = strripos($galsecu['bannergaleri_link'],"restaurantes/maracuya");
							$baseMaracuya = substr($galsecu['bannergaleri_link'],$posicionBaseMaracuya,strlen($galsecu['bannergaleri_link']));
							$maracuya = substr(url('restaurantes/maracuya'),$posicionMaracuya,strlen(url('restaurantes/maracuya')));

							if($basehabanero == $habanero){
								if(App::getLocale()=="en")
									$galsecu['bannergaleri_texto'] = Lang::get("home.short_habaneros");
								$img = url('img/home/secundaria/logos/Habanerosblanco.png');
							}else if($baseMaracuya == $maracuya){
								if(App::getLocale()=="en")
									$galsecu['bannergaleri_texto'] = Lang::get("home.short_maracuya");
								$img = url('img/home/secundaria/logos/maracuyablanco.png');
							}else if($baselobby == $lobby){
								if(App::getLocale()=="en")
									$galsecu['bannergaleri_texto'] = Lang::get("home.short_lobby");
								$img = url('img/home/secundaria/logos/lobbybarblanco.png');
							}else if(strcmp($galsecu['bannergaleri_link'],url('elhotel')) == 0){
								$img = url('img/home/secundaria/logos/logo.svg');
							}else if(strncasecmp($habitaciones,$galsecu['bannergaleri_link'],$limit)){
								if(App::getLocale()=="en")
									$galsecu['bannergaleri_texto'] = Lang::get("home.short_breakfast");
								$img = url('img/home/secundaria/logos/logo.svg');//Se puede cambiar para habitaciones
							}else{
								$img = url('img/home/secundaria/logos/logo.svg');//Se puede cambiar para link externo
							}
							$secundaria .= "<div class=\"row\">
								<div class=\"col-xs-12 col-sm-4\">
									<a href=\"".$galsecu['bannergaleri_link']."\"><div class=\"logo background_img\" style=\"background-image:url(".$img.");\"></div></a><!-- url logo href -->
								</div>
								<div class=\"col-xs-12 col-sm-8\" style=\"color:#FFF;padding-left: 20px;padding-right: 30px;\"><!-- colocar color #FFF o #707170-->
									<div>
										<div class=\"parrafogaleria\" style=\"background-image: url(".url('img/home/galeria/logos/lineablanca.svg').");\"><!-- Parrafo -->
											".$galsecu['bannergaleri_texto']."
										</div>
										<a href=\"".$galsecu['bannergaleri_link']."\" style=\"color:#FFF;\">Ver más>></a><!-- colocar color #FFF o #707170-->
									</div>
								</div>
							</div>";
						}
				$secundaria .= "</div></div>";
			}
		}

		$res = array('indicators' => $galeria[0]);
		$res = array_add($res,'imets_ppal',$galeria[1]);
		$res = array_add($res,'promos',$promos);
		$res = array_add($res,'countpromo',$countpromo + 1);
		$res = array_add($res,'meses',$meses);
		$res = array_add($res,'li_eventos',$li_eventos);
		$res = array_add($res,'eve_ppal',$eve_ppal);
		$res = array_add($res,'cuadro1',$cuadro1);
		$res = array_add($res,'cuadro2',$cuadro2);
		$res = array_add($res,'id',$id);
		$res = array_add($res,'promociones',$promociones);
		$res = array_add($res,'eventos',$eventos);
		$res = array_add($res,'modalpromociones',Promociones::getPromosactives());
		$res = array_add($res,'turismo',"#turismosalud");
		$res = array_add($res,'secundaria',$secundaria);
		$res = array_add($res,'descarga',$descarga);
		

		return view("frontend.index")->with([
			"res" => $res,
			"listahabitaciones" => $this->listahabitaciones(),
			"bloque_salud" => $bloque_salud,
			"servicios"  => json_decode(Servicios::obtenerServicios()),
			"modal_load" => $modal_load,
			"ruta" => url("img/home/salud/Cocz710133636f3v7710133144MpuJ75144321saludmodal.jpg")
		]);
	}

	public function elhotel(Request $request){

		$e = json_decode(EventosCorporativos::obtenerEventos());
		$res = $data = "";
		$promociones =  url('/')."/home/promociones";
		$eventos = url('/')."/home/eventos";
		$b = new Bannergaleri();

		$size_inst = count($b->getGaleriinsta());

		$count_ec = 0;
		if (strcmp($e->eventoscorporativos_picone,"img/eventoscorporativos/default.jpg") !== 0) {
			$count_ec++;
		}
		if (strcmp($e->eventoscorporativos_pictwo,"img/eventoscorporativos/default.jpg") !== 0) {
			$count_ec++;
		}
		if (strcmp($e->eventoscorporativos_picthrid,"img/eventoscorporativos/default.jpg") !== 0) {
			$count_ec++;
		}
		if (strcmp($e->eventoscorporativos_picfour,"img/eventoscorporativos/default.jpg") !== 0) {
			$count_ec++;
		}

		$res = array('promociones' => $promociones);
		$res = array_add($res,'eventos',$eventos);
		$res = array_add($res,'banner',$b->getbannersection('1'));
		$res = array_add($res,'comerciales',json_decode(Comerciales::obtenerComerciales()));
		$res = array_add($res,'id',$request->target);
		$modal = array('indicators' => "");
		$modal = array_add($modal,'items',"");
		$res = array_add($res,'turismo',url('/')."/home/turismosalud");

		return view("frontend.elhotel")->with([
			"res" => $res,
			"listahabitaciones" => $this->listahabitaciones(),
			"titulo_eventos" => $e->eventoscorporativos_name,
			"text_one" => $e->eventoscorporativos_textone,
			"text_two" => $e->eventoscorporativos_texttwo,
			"count_ec" => $count_ec,
			"elhotel" => json_decode(ElHotel::obtenerHistoria()),
			"proyectos" => json_decode(Proyectos::obtenerProyectos()),
			"corporativos" => json_decode(DB::table('eventos')->select('id AS evento','event_title AS nombre','event_text AS descripcion')->where('event_ppal','1')->where('event_type','1')->get()),
			"modal" => $modal,
			"size_inst" => $size_inst
		]);
	}
		
	public function habitaciones(Request $request){
		$res = $modal = "";
		$h = new Habitacion();
		$promociones =  url('/')."/home/promociones";
		$eventos = url('/')."/home/eventos";
		$habitaciones = json_decode($h->getAllhabitaciones(),true);
		$items = $titulo = $detalles = $lista = $comodidades = $galeria = $itemsgal = $destacados = $bannerestaurat = "";
		$b = new Bannergaleri();
		$find = "";

		$data = array('seccion' => '2');
		$fija =  json_decode($b->getFijaseccion($data),true);
		if(count($fija) == 1){
			$bannerestaurat .= "<img class=\"col-xs-12 background_img\" src=\"".url($fija[0]['img'])."\" style=\"padding: 0;\"/>";
		}else{
			$bannerestaurat .= "<img class=\"col-xs-12 background_img\" src=\"".url('img/default.jpg')."\" style=\"padding: 0;\"/>";
		}

		$data = array('seccion' => '2');
		$galeria = json_decode($b->getGaleriahabitacionall($data),true);

		/*$resultgaleria = [];
		foreach ($galeria as $item) {
			$resultgaleria[$item['bannergaleri_id']][] = [$item['bannergaleri_destacar'],$item['bannergaleri_rutaimg']];
		}*/

		$res = array('promociones' => $promociones);
		$res = array_add($res,'eventos',$eventos);
		$res = array_add($res,'habitaciones',$habitaciones);
		//$res = array_add($res,'items',$items);
		//$res = array_add($res,'titulo',$titulo);
		//$res = array_add($res,'detalles',$detalles);
		//$res = array_add($res,'lista',$lista);
		//$res = array_add($res,'comodidades',$comodidades);
		//$res = array_add($res,'itemsgal',$itemsgal);
		//$res = array_add($res,'destacados',$destacados);
		$res = array_add($res,'bannerestaurat',$bannerestaurat);
		$res = array_add($res,'banner',$b->getbannersection('2'));
		$res = array_add($res,'id',$request->target);
		$res = array_add($res,'turismo',url('/')."/home/turismosalud");
		return view("frontend.habitaciones")->with([
			"res" => $res,
			"listahabitaciones" => $this->listahabitaciones(),
			"modal" => $modal
		]);
	}

	public function habitacion(Request $request){
		$res = $modal = "";
		$h = new Habitacion();
		$promociones =  url('/')."/home/promociones";
		$eventos = url('/')."/home/eventos";
		$bannerestaurat = "";
		$habitacion = json_decode($h->getHabitacion(['target'=>$request->target]),true);
		$b = new Bannergaleri();
		$data = array('seccion' => '2');
		$fija =  json_decode($b->getFijaseccion($data),true);
		if(count($fija) == 1){
			$bannerestaurat .= "<img class=\"col-xs-12 background_img\" src=\"".url($fija[0]['img'])."\" style=\"padding: 0;\"/>";
		}else{
			$bannerestaurat .= "<img class=\"col-xs-12 background_img\" src=\"".url('img/default.jpg')."\" style=\"padding: 0;\"/>";
		}
		$galeria = json_decode($b->getGaleriaRestID('2',$habitacion['habitacion']),true);
		$res = array('promociones' => $promociones);
		$res = array_add($res,'eventos',$eventos);
		$res = array_add($res,'habitacion',$habitacion);
		$res = array_add($res,'galeria',$galeria);
		$res = array_add($res,'bannerestaurat',$bannerestaurat);
		$res = array_add($res,'banner',$b->getbannersection('2'));
		$res = array_add($res,'id',$request->target);
		$res = array_add($res,'turismo',url('/')."/home/turismosalud");
		return view("frontend.habitacion")->with([
			"res" => $res,
			"listahabitaciones" => $this->listahabitaciones(),
			"modal" => $modal
		]);
	}

	/*private function construirGallery($gallery){
		return "<div class=\"col-xs-12 col-sm-7 p4\">".
				"<div id=\"content_galeresta\" class=\"pa pal pab\">".
					"<div id=\"galeresta\" class=\"carousel slide carousel-fade\" data-ride=\"carousel\" data-interval=\"false\">".
						"<div class=\"carousel-inner\">"
							.$gallery.
						"</div>".
						"<div class=\"controls\">".
							"<button class=\"background_img galchang_left\"></button>".
							"<button  class=\"background_img galchang_right\"></button>".
						"</div>".
					"</div>".
				"</div>".
			"</div>";
	}*/

	//VIEW
	public function view_gallery($codigo,$section){
		$b = new Bannergaleri();
		
		return response()->json([
			"respuesta" => $b->getGaleriaRestID(base64_decode($section),base64_decode($codigo))
		],200);
	}
	
	public function restaurantes(Request $request){
		$res = ""; $galeria_final = "";
		$itembanner = $itemrestau = $galerirest = $galeritem = $formulario = "";
		$promociones =  url('/')."/home/promociones";
		$eventos = url('/')."/home/eventos";
		$j = new Restaurant();
		$b = new Bannergaleri();
		$restaurantes = json_decode($j->getAllrestaurants(),true);
		if(count($restaurantes) > 0){
			$facebook = ""; $tripa = ""; $twitter = "";
			foreach($restaurantes as $clave => $restaurant){
				$data = array('galeria' => base64_encode($restaurant['id']));
				
				$form_img = Bannergaleri::where('bannergaleri_section','3')->where('bannergaleri_destacar','2')->where('bannergaleri_id',($clave + 1))->select('bannergaleri_rutaimg')->first();
				if(is_null($form_img)){
					$form_img = url("img/default.jpg");
				}else{
					$form_img = url($form_img->bannergaleri_rutaimg);
				}
		
				if($clave == 0){
					$active = "<div class=\"item active row\">";
					if(count($restaurantes) == 1){$active = "<div class=\"item active unico row\">";}
					$collapsed = "<a href=\"#restbar_".($clave+1)."\" class=\"restbar\" data-toggle=\"collapse\" data-active=\"#active_".($clave+1)."\" data-desactive=\"#desactive_".($clave+1)."\" data-form_img=\"".$form_img."\">";
					$collapse = "<div id=\"restbar_".($clave+1)."\" class=\"collapse in\">";
				}else{
					$active = "<div class=\"item row\">";
					$collapsed = "<a href=\"#restbar_".($clave+1)."\" class=\"restbar collapsed\" data-toggle=\"collapse\" data-active=\"#active_".($clave+1)."\" data-desactive=\"#desactive_".($clave+1)."\" data-form_img=\"".$form_img."\">";
					$collapse = "<div id=\"restbar_".($clave+1)."\" class=\"collapse\">";
				}
				$itembanner .= $active.
					"<div class=\"col-xs-12 col-sm-4\">
						<div class=\"banner_palmasinn background_img\" style=\"background-image:url(".url($restaurant['restaurant_rest_file3']).");\">
							<div>
								<div>
									<div class=\"a\" id=\"active_".($clave+1)."\">
										<div style=\"background-image:url(".url($restaurant['restaurant_rest_file1']).");\"></div>
									</div>
								</div>
							</div>
							
							<div id=\"desactive_".($clave+1)."\" class=\"desactive\"></div>
							
							".$collapsed."
								<div class=\"background_img\"></div>
							</a>
						</div>
					</div>
				</div>";
				$galeria_final =  json_decode($b->getGaleriaRestID("3",($clave+1)),true);
				$count = 0;
				$count1 = 1;
				$fijas = array();
				foreach($galeria_final AS $item){
					if (strcmp($item['bannergaleri_rutaimg'],"img/default.jpg") !== 0){
						if($count == 0){
							$galeritem .= "<div class=\"item active\">";
							$count++;
						}else{
							$galeritem .= "<div class=\"item\">";
						}
						$galeritem .= "<div>
								<div>
									<div style=\"background-image:url(".url($item['bannergaleri_rutaimg']).");\" class=\"background_img\"></div>
									<!--img src=\"img/restaurantes/galeria/gal3.jpg\"-->
								</div>
							</div>
						</div>";
						if($item['bannergaleri_destacar'] == "1"){
							$fijas = array_add($fijas,$count1,$item['bannergaleri_rutaimg']);
							$count1 ++;
						}

					}
				}
				if(strcmp($galeritem,"") == 0){
					$fijas = array('1' => "/",'2' => "/",'3' => "/",'4' => "/");
				}
				
				if(App::getLocale()=="en"){
					if($restaurant['restaurant_title']=="HABANEROS"){
						$facebook = "https://www.facebook.com/habanerostexmexbar/";
						$tripa = "https://www.tripadvisor.com.ve/Restaurant_Review-g316095-d7063644-Reviews-Habaneros_Tex_Mex-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html?t=316095";
						$twitter = "https://twitter.com/habaneros_tex";
						$instagram = "https://www.instagram.com/habanerostexmex/";
						$restaurant['restaurant_descripcion'] = Lang::get('restaurant.habaneros'); 
					}else if($restaurant['restaurant_title']=="MARACUYA"){
						$tripa = "https://www.tripadvisor.com.ve/Hotel_Review-g316095-d1899060-Reviews-Hotel_Las_Palmas_Inn-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html";
						$twitter = "https://twitter.com/laspalmasinn";
						$facebook = "https://www.facebook.com/maracuyachillout/";
						$instagram = "https://www.instagram.com/maracuyachillout/";
						$restaurant['restaurant_descripcion'] = Lang::get('restaurant.maracuya'); 
					}else if($restaurant['restaurant_title']=="LOBBYBAR"){
						$tripa = "https://www.tripadvisor.com.ve/Hotel_Review-g316095-d1899060-Reviews-Hotel_Las_Palmas_Inn-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html";
						$twitter = "https://twitter.com/laspalmasinn";
						$facebook = "https://www.facebook.com/laspalmsinn/";
						$instagram = "https://www.instagram.com/laspalmasinn/";
						$restaurant['restaurant_descripcion'] = Lang::get('restaurant.lobby');
					}
				}else{
					if($restaurant['restaurant_title']=="HABANEROS"){
						$facebook = "https://www.facebook.com/habanerostexmexbar/";
						$tripa = "https://www.tripadvisor.com.ve/Restaurant_Review-g316095-d7063644-Reviews-Habaneros_Tex_Mex-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html?t=316095";
						$twitter = "https://twitter.com/habaneros_tex";
						$instagram = "https://www.instagram.com/habanerostexmex/";
					}else if($restaurant['restaurant_title']=="MARACUYA"){
						$tripa = "https://www.tripadvisor.com.ve/Hotel_Review-g316095-d1899060-Reviews-Hotel_Las_Palmas_Inn-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html";
						$twitter = "https://twitter.com/laspalmasinn";
						$facebook = "https://www.facebook.com/maracuyachillout/";
						$instagram = "https://www.instagram.com/maracuyachillout/";
					}else if($restaurant['restaurant_title']=="LOBBYBAR"){
						$tripa = "https://www.tripadvisor.com.ve/Hotel_Review-g316095-d1899060-Reviews-Hotel_Las_Palmas_Inn-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html";
						$twitter = "https://twitter.com/laspalmasinn";
						$facebook = "https://www.facebook.com/laspalmsinn/";
						$instagram = "https://www.instagram.com/laspalmasinn/";
					}
				}
				
				$itemrestau .= $collapse."
					<div class=\"row restaurates\">
						<div class=\"col-xs-12\">
							<div>
								<div class=\"row\">
									<div class=\"col-xs-12\">
										<div class=\"background_img fija\" style=\"background-image:url(".url($restaurant['restaurant_rest_file4']).");\"></div>
										<div class=\"background_img fija\" style=\"background-image:url(".url($restaurant['restaurant_rest_file5']).");\"></div>
										<div id=\"dd".($clave+1)."\" class=\"borderes\" style=\"border:4px ".$restaurant['restaurant_color']." solid;\">
											<div class=\"row\">
												<div class=\"col-xs-12\">
													<div class=\"background_img logo\" style=\"background-image:url(".url($restaurant['restaurant_rest_file2']).");\"></div>
												</div>
												<div class=\"col-xs-12 parrafo\">
													<div>
														<div>
															<div>
																".$restaurant['restaurant_descripcion']."
															</div>
														</div>
													</div>
												</div>
												<div class=\"col-xs-12 text-right\" style=\"color:".$restaurant['restaurant_color'].";\">
													<label>".Lang::get("elhotel.contacto").":<a style=\"color:".$restaurant['restaurant_color'].";\" href=\"tel:".str_replace(' ', '', $restaurant['restaurant_contacto1'])."\">".$restaurant['restaurant_contacto1']."</a> / <a style=\"color:".$restaurant['restaurant_color'].";\" href=\"tel:".str_replace(' ', '', $restaurant['restaurant_contacto2'])."\">".$restaurant['restaurant_contacto2']."</a></label>
													<div class='col-xs-5'>
														 <div class=\"dropdown submenu-m\">
														  <button class=\"btn btn-default dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\">
														  <span class=\"caret\"></span></button>
														  <ul class=\"dropdown-menu\">
															<li>
																<a href=\"".$tripa."\" class=\"\" target='_blank'>
																	<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
																		 width=\"26.653px\" height=\"25px\" viewBox=\"0 0 26.653 15.994\" enable-background=\"new 0 0 26.653 15.994\" xml:space=\"preserve\">
																	<g>
																		<path fill=\"".$restaurant['restaurant_color']."\" d=\"M24.875,5.333c1.775-0.443,1.777-1.777,1.777-1.777h-4.441C19.545,0.89,16.879,0,13.327,0
																			C9.773,0,7.106,0.89,4.44,3.555H0c0,0,0.001,1.334,1.777,1.777C1.449,5.785,1.089,6.374,0.779,7.009C0.339,7.914,0,8.92,0,9.775
																			c0,3.664,2.554,6.219,6.22,6.219c2.078,0,4.112-1.169,5.329-2.666l1.778,2.666l1.777-2.666c1.217,1.497,3.252,2.666,5.33,2.666
																			c3.664,0,6.219-2.555,6.219-6.219c0-0.816-0.309-1.768-0.719-2.638C25.612,6.452,25.225,5.815,24.875,5.333z M6.22,14.216
																			c-2.453,0-4.442-1.987-4.442-4.44c0-1.314,0.572-2.491,1.479-3.303c0.786-0.707,1.823-1.14,2.964-1.14
																			c0.693,0,1.346,0.164,1.932,0.447C9.635,6.5,10.66,8.016,10.66,9.775C10.66,12.229,8.672,14.216,6.22,14.216z M13.327,12.441
																			c0-2.557-1.179-5.112-2.856-6.823C9.235,4.357,7.728,3.555,6.22,3.555c0,0,2.663-2.666,7.107-2.666
																			c4.441,0,7.107,2.666,7.107,2.666c-1.516,0-3.031,0.811-4.271,2.083C14.497,7.347,13.327,9.894,13.327,12.441z M20.434,14.216
																			c-2.453,0-4.441-1.987-4.441-4.44c0-1.73,0.99-3.225,2.432-3.959c0.604-0.307,1.287-0.484,2.01-0.484
																			c1.199,0,2.285,0.478,3.082,1.249c0.838,0.808,1.359,1.939,1.359,3.194C24.875,12.229,22.887,14.216,20.434,14.216z\"/>
																		<circle id=\"XMLID_158_\" fill=\"".$restaurant['restaurant_color']."\" cx=\"20.434\" cy=\"9.774\" r=\"0.889\"/>
																		<path fill=\"".$restaurant['restaurant_color']."\" d=\"M20.434,7.108c-1.471,0-2.666,1.195-2.666,2.667s1.195,2.666,2.666,2.666S23.1,11.248,23.1,9.775
																			S21.905,7.108,20.434,7.108z M20.434,11.553c-0.98,0-1.777-0.797-1.777-1.777c0-0.981,0.797-1.778,1.777-1.778
																			s1.777,0.797,1.777,1.778C22.211,10.756,21.415,11.553,20.434,11.553z\"/>
																		<circle id=\"XMLID_155_\" fill=\"".$restaurant['restaurant_color']."\" cx=\"6.22\" cy=\"9.774\" r=\"0.889\"/>
																		<path fill=\"".$restaurant['restaurant_color']."\" d=\"M6.22,7.108c-1.474,0-2.667,1.195-2.667,2.667s1.193,2.666,2.667,2.666c1.471,0,2.665-1.193,2.665-2.666
																			S7.69,7.108,6.22,7.108z M6.22,11.553c-0.982,0-1.779-0.797-1.779-1.777c0-0.981,0.797-1.778,1.779-1.778
																			c0.979,0,1.777,0.797,1.777,1.778C7.997,10.756,7.199,11.553,6.22,11.553z\"/>
																	</g>
																	</svg>
																	<span style='color:".$restaurant['restaurant_color']."'>Tripadvisor</span>
																</a>
															</li>
															<li>
																<a href=\"".$facebook."\" class=\"\" target='_blank'>
																	<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
																		width=\"25px\" height=\"25px\" viewBox=\"0 0 25 25\" enable-background=\"new 0 0 25 25\" xml:space=\"preserve\">
																		<path fill=\"".$restaurant['restaurant_color']."\" d=\"M15.52,9.844L15.5,9.826l-0.041-0.007L15.43,9.821c-0.014,0-0.023,0-0.037,0h-0.785
																		c-0.457,0-0.914,0-1.371,0h-0.004c0-0.001,0-0.003,0-0.006c0-0.487,0-0.973,0.002-1.458c0-0.005-0.002-0.01-0.002-0.014l0.012-0.027
																		l0.004-0.029c0-0.014,0-0.028,0-0.042c-0.002-0.018-0.004-0.034,0-0.04l0.008-0.013l0.004-0.015c0-0.013,0.002-0.023,0.002-0.034
																		c0.008-0.03,0.01-0.059,0.01-0.093c-0.002-0.074,0.033-0.146,0.068-0.219l0.006-0.014c0.031-0.067,0.076-0.121,0.125-0.15
																		c0.055-0.034,0.127-0.071,0.211-0.093c0.094-0.025,0.197-0.048,0.305-0.048l0.051,0.001l0.012-0.014c0.008,0,0.014,0,0.014,0
																		c0.346,0,0.686,0,1.027,0h0.34l0.018-0.002c0.061-0.014,0.1-0.056,0.115-0.119l0.002-2.684l-0.082-0.076h-2.285
																		c-0.021,0-0.039,0.003-0.059,0.007c-0.012,0.002-0.018,0.004-0.033,0.003l-0.041,0c-0.097,0-0.198,0.011-0.291,0.023
																		c-0.035,0.002-0.066,0.008-0.099,0.01c-0.125,0.014-0.249,0.04-0.366,0.064L12.25,4.753c-0.096,0.018-0.188,0.047-0.278,0.075
																		L11.94,4.839c-0.147,0.044-0.291,0.109-0.424,0.173c-0.127,0.058-0.243,0.136-0.348,0.209c-0.144,0.101-0.274,0.222-0.403,0.367
																		c-0.124,0.141-0.237,0.306-0.354,0.516c-0.052,0.094-0.101,0.202-0.144,0.329c-0.006,0.016-0.013,0.031-0.018,0.048
																		c-0.014,0.034-0.028,0.069-0.039,0.107l-0.015,0.061c-0.02,0.071-0.04,0.147-0.063,0.219c-0.009,0.026-0.016,0.053-0.02,0.085
																		c-0.003,0.044-0.012,0.089-0.024,0.136c-0.017,0.071-0.033,0.146-0.028,0.229c0.001,0.014-0.005,0.041-0.015,0.062l-0.007,0.01
																		l-0.003,0.008c-0.013,0.055-0.013,0.107-0.013,0.159c0,0.032,0,0.06-0.005,0.088V7.65c0,0.015,0,0.029,0.001,0.042
																		c0.001,0.01,0.003,0.026,0,0.03l-0.007,0.013l-0.002,0.013C10,7.839,10,7.926,10,8.013c0,0.021,0,0.043,0,0.066l-0.01,0.005
																		L9.987,8.17c0.001,0.551,0.001,1.103,0.001,1.655H9.33H8.583L8.554,9.822L8.489,9.837v0.001L8.26,9.835l0.186,0.12l0.002,2.728
																		l0.059,0.013l0.007,0.001l0.009,0.014h0.584c0.289,0,0.579,0,0.868,0c0.007,0,0.012,0,0.015-0.001
																		c-0.001,0.008-0.001,0.016-0.001,0.026c0,2.271,0,4.544,0,6.815v0.894c0,0.02,0.001,0.04,0.001,0.06l0.001,0.055l0.02,0.021v-0.006
																		l0,0c0.011,0.014,0.034,0.053,0.088,0.053l0.026-0.003c0.016-0.001,0.032-0.001,0.047-0.001h2.894c0.018,0,0.035,0,0.055-0.002
																		h0.074l0.041-0.082l0.002-0.04c0-0.021,0-0.042,0-0.066v-0.948c0.002-2.252,0.002-4.504,0.002-6.756
																		c0-0.008-0.002-0.016-0.004-0.024h1.094h0.84c0.004,0,0.008,0,0.012,0.001l0.031,0.002l0.016-0.008
																		c0.057-0.021,0.068-0.071,0.068-0.092c0-0.002,0.002-0.006,0.002-0.009c0.014-0.042,0.018-0.086,0.02-0.124
																		c0.004-0.031,0.004-0.061,0.014-0.087l0.016-0.195v-0.008c-0.004-0.063,0.008-0.125,0.018-0.193c0.004-0.026,0.01-0.052,0.014-0.079
																		c0.012-0.041,0.012-0.077,0.012-0.11c0.002-0.021,0.002-0.04,0.006-0.057v-0.01v-0.006c0-0.068,0.01-0.134,0.02-0.205
																		c0.006-0.03,0.01-0.059,0.014-0.092l0.012-0.173v-0.006c-0.004-0.054,0.006-0.107,0.016-0.167c0.006-0.039,0.014-0.076,0.016-0.115
																		l0.016-0.147v-0.011c-0.008-0.047,0-0.094,0.008-0.148c0.008-0.041,0.014-0.088,0.014-0.133l0.008-0.013
																		c0.014-0.042,0.014-0.081,0.014-0.115c0-0.018,0.002-0.037,0.004-0.053l0.002-0.007v-0.006c-0.006-0.083,0.01-0.165,0.025-0.254
																		c0.004-0.027,0.01-0.055,0.012-0.083c0.002-0.004,0.002-0.008,0.006-0.013C15.58,9.966,15.596,9.891,15.52,9.844z\"/>
																	</svg>
																	<span style='color:".$restaurant['restaurant_color']."'>Facebook</span>
																</a>
															</li>
															<li>
																<a href=\"".$instagram."\" class=\"\" target='_blank'>
																	<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
																		 width=\"25px\" height=\"25px\" viewBox=\"0 0 25 25\" enable-background=\"new 0 0 25 25\" xml:space=\"preserve\">
																		<g>
																		<path fill=\"".$restaurant['restaurant_color']."\" d=\"M15.408,20.627c-2.191,0-4.381,0-6.573,0c-0.156-0.026-0.313-0.068-0.469-0.078
																			c-2.271-0.118-4.07-1.766-4.154-4.033c-0.097-2.597-0.094-5.202-0.002-7.799c0.076-2.223,1.801-3.944,4.022-4.02
																			c2.583-0.09,5.173-0.077,7.761-0.004c1.969,0.057,3.447,1.374,3.918,3.301c0.105,0.437,0.152,0.886,0.227,1.329
																			c0,2.193,0,4.384,0,6.573c-0.027,0.181-0.068,0.362-0.08,0.546c-0.113,1.939-1.391,3.49-3.279,3.949
																			C16.328,20.498,15.865,20.549,15.408,20.627z M18.736,12.6c-0.02,0-0.039-0.001-0.057-0.001c0-0.574,0.006-1.149-0.002-1.722
																			c-0.012-0.587-0.041-1.173-0.064-1.761c-0.074-1.788-1.209-2.964-2.992-2.995c-2.334-0.039-4.67-0.039-7.004,0
																			C6.834,6.152,5.661,7.33,5.633,9.115c-0.036,2.334-0.039,4.671,0.001,7.004c0.03,1.768,1.192,2.941,2.962,2.973
																			c2.347,0.045,4.697,0.041,7.044,0.002c1.625-0.028,2.697-0.945,2.891-2.542C18.693,15.248,18.674,13.92,18.736,12.6z\"/>
																		<path fill=\"".$restaurant['restaurant_color']."\" d=\"M16.244,12.61c0,2.28-1.844,4.124-4.125,4.12c-2.282-0.002-4.133-1.851-4.128-4.122
																			c0.005-2.28,1.854-4.13,4.128-4.127C14.4,8.482,16.242,10.326,16.244,12.61z M12.132,9.958c-1.476-0.003-2.657,1.161-2.662,2.618
																			c-0.008,1.5,1.156,2.682,2.636,2.684c1.474,0.004,2.662-1.162,2.666-2.618C14.777,11.145,13.613,9.96,12.132,9.958z\"/>
																		<path fill=\"".$restaurant['restaurant_color']."\" d=\"M16.389,7.361c0.533-0.004,0.971,0.42,0.975,0.955c0.012,0.528-0.416,0.978-0.943,0.996
																			c-0.525,0.015-1.006-0.454-1.004-0.983C15.418,7.802,15.857,7.365,16.389,7.361z\"/>
																		</g>
																	</svg>
																	<span style='color:".$restaurant['restaurant_color']."'>Instagram</span>
																</a>
															</li>
															<li>
																<a href=\"".$twitter."\" class=\"\" target='_blank'>
																	<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"25px\" height=\"25px\" viewBox=\"0 0 25 25\" enable-background=\"new 0 0 25 25\" xml:space=\"preserve\">
																		<g>
																		<path fill=\"".$restaurant['restaurant_color']."\" d=\"M22.197,6.527c-0.725,0.321-1.5,0.537-2.32,0.635c0.836-0.5,1.477-1.292,1.775-2.232
																			c-0.779,0.461-1.643,0.798-2.563,0.978c-0.736-0.785-1.787-1.275-2.947-1.275c-2.229,0-4.037,1.809-4.037,4.038
																			c0,0.316,0.037,0.624,0.104,0.92C8.856,9.422,5.881,7.815,3.89,5.373C3.543,5.97,3.343,6.663,3.343,7.403
																			c0,1.399,0.713,2.638,1.797,3.361c-0.661-0.022-1.285-0.203-1.829-0.505c0,0.015,0,0.034,0,0.051c0,1.955,1.392,3.586,3.239,3.958
																			c-0.34,0.091-0.695,0.142-1.065,0.142c-0.259,0-0.514-0.025-0.759-0.072c0.515,1.604,2.004,2.771,3.771,2.804
																			c-1.38,1.082-3.122,1.728-5.012,1.728c-0.327,0-0.647-0.018-0.965-0.057c1.787,1.147,3.908,1.814,6.188,1.814
																			c7.426,0,11.487-6.15,11.487-11.486c0-0.175-0.004-0.349-0.012-0.523C20.971,8.047,21.656,7.337,22.197,6.527L22.197,6.527z\"/>
																		</g>
																	</svg>
																	<span style='color:".$restaurant['restaurant_color']."'>Twitter</span>
																</a>
															</li>
														  </ul>
														</div> 
														<div id=\"navbarpalmasinr\" class='submenu-d'>
															<div>
																<ul class=\"list-inline\">
																	<li>
																		<a href=\"".$tripa."\" class=\"\" target='_blank'>
																			<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
																				 width=\"26.653px\" height=\"25px\" viewBox=\"0 0 26.653 15.994\" enable-background=\"new 0 0 26.653 15.994\" xml:space=\"preserve\">
																			<g>
																				<path fill=\"".$restaurant['restaurant_color']."\" d=\"M24.875,5.333c1.775-0.443,1.777-1.777,1.777-1.777h-4.441C19.545,0.89,16.879,0,13.327,0
																					C9.773,0,7.106,0.89,4.44,3.555H0c0,0,0.001,1.334,1.777,1.777C1.449,5.785,1.089,6.374,0.779,7.009C0.339,7.914,0,8.92,0,9.775
																					c0,3.664,2.554,6.219,6.22,6.219c2.078,0,4.112-1.169,5.329-2.666l1.778,2.666l1.777-2.666c1.217,1.497,3.252,2.666,5.33,2.666
																					c3.664,0,6.219-2.555,6.219-6.219c0-0.816-0.309-1.768-0.719-2.638C25.612,6.452,25.225,5.815,24.875,5.333z M6.22,14.216
																					c-2.453,0-4.442-1.987-4.442-4.44c0-1.314,0.572-2.491,1.479-3.303c0.786-0.707,1.823-1.14,2.964-1.14
																					c0.693,0,1.346,0.164,1.932,0.447C9.635,6.5,10.66,8.016,10.66,9.775C10.66,12.229,8.672,14.216,6.22,14.216z M13.327,12.441
																					c0-2.557-1.179-5.112-2.856-6.823C9.235,4.357,7.728,3.555,6.22,3.555c0,0,2.663-2.666,7.107-2.666
																					c4.441,0,7.107,2.666,7.107,2.666c-1.516,0-3.031,0.811-4.271,2.083C14.497,7.347,13.327,9.894,13.327,12.441z M20.434,14.216
																					c-2.453,0-4.441-1.987-4.441-4.44c0-1.73,0.99-3.225,2.432-3.959c0.604-0.307,1.287-0.484,2.01-0.484
																					c1.199,0,2.285,0.478,3.082,1.249c0.838,0.808,1.359,1.939,1.359,3.194C24.875,12.229,22.887,14.216,20.434,14.216z\"/>
																				<circle id=\"XMLID_158_\" fill=\"".$restaurant['restaurant_color']."\" cx=\"20.434\" cy=\"9.774\" r=\"0.889\"/>
																				<path fill=\"".$restaurant['restaurant_color']."\" d=\"M20.434,7.108c-1.471,0-2.666,1.195-2.666,2.667s1.195,2.666,2.666,2.666S23.1,11.248,23.1,9.775
																					S21.905,7.108,20.434,7.108z M20.434,11.553c-0.98,0-1.777-0.797-1.777-1.777c0-0.981,0.797-1.778,1.777-1.778
																					s1.777,0.797,1.777,1.778C22.211,10.756,21.415,11.553,20.434,11.553z\"/>
																				<circle id=\"XMLID_155_\" fill=\"".$restaurant['restaurant_color']."\" cx=\"6.22\" cy=\"9.774\" r=\"0.889\"/>
																				<path fill=\"".$restaurant['restaurant_color']."\" d=\"M6.22,7.108c-1.474,0-2.667,1.195-2.667,2.667s1.193,2.666,2.667,2.666c1.471,0,2.665-1.193,2.665-2.666
																					S7.69,7.108,6.22,7.108z M6.22,11.553c-0.982,0-1.779-0.797-1.779-1.777c0-0.981,0.797-1.778,1.779-1.778
																					c0.979,0,1.777,0.797,1.777,1.778C7.997,10.756,7.199,11.553,6.22,11.553z\"/>
																			</g>
																			</svg>
																		</a>
																	</li>
																	<li>
																		<a href=\"".$facebook."\" class=\"\"  target='_blank'>
																			<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
																				width=\"25px\" height=\"25px\" viewBox=\"0 0 25 25\" enable-background=\"new 0 0 25 25\" xml:space=\"preserve\">
																				<path fill=\"".$restaurant['restaurant_color']."\" d=\"M15.52,9.844L15.5,9.826l-0.041-0.007L15.43,9.821c-0.014,0-0.023,0-0.037,0h-0.785
																				c-0.457,0-0.914,0-1.371,0h-0.004c0-0.001,0-0.003,0-0.006c0-0.487,0-0.973,0.002-1.458c0-0.005-0.002-0.01-0.002-0.014l0.012-0.027
																				l0.004-0.029c0-0.014,0-0.028,0-0.042c-0.002-0.018-0.004-0.034,0-0.04l0.008-0.013l0.004-0.015c0-0.013,0.002-0.023,0.002-0.034
																				c0.008-0.03,0.01-0.059,0.01-0.093c-0.002-0.074,0.033-0.146,0.068-0.219l0.006-0.014c0.031-0.067,0.076-0.121,0.125-0.15
																				c0.055-0.034,0.127-0.071,0.211-0.093c0.094-0.025,0.197-0.048,0.305-0.048l0.051,0.001l0.012-0.014c0.008,0,0.014,0,0.014,0
																				c0.346,0,0.686,0,1.027,0h0.34l0.018-0.002c0.061-0.014,0.1-0.056,0.115-0.119l0.002-2.684l-0.082-0.076h-2.285
																				c-0.021,0-0.039,0.003-0.059,0.007c-0.012,0.002-0.018,0.004-0.033,0.003l-0.041,0c-0.097,0-0.198,0.011-0.291,0.023
																				c-0.035,0.002-0.066,0.008-0.099,0.01c-0.125,0.014-0.249,0.04-0.366,0.064L12.25,4.753c-0.096,0.018-0.188,0.047-0.278,0.075
																				L11.94,4.839c-0.147,0.044-0.291,0.109-0.424,0.173c-0.127,0.058-0.243,0.136-0.348,0.209c-0.144,0.101-0.274,0.222-0.403,0.367
																				c-0.124,0.141-0.237,0.306-0.354,0.516c-0.052,0.094-0.101,0.202-0.144,0.329c-0.006,0.016-0.013,0.031-0.018,0.048
																				c-0.014,0.034-0.028,0.069-0.039,0.107l-0.015,0.061c-0.02,0.071-0.04,0.147-0.063,0.219c-0.009,0.026-0.016,0.053-0.02,0.085
																				c-0.003,0.044-0.012,0.089-0.024,0.136c-0.017,0.071-0.033,0.146-0.028,0.229c0.001,0.014-0.005,0.041-0.015,0.062l-0.007,0.01
																				l-0.003,0.008c-0.013,0.055-0.013,0.107-0.013,0.159c0,0.032,0,0.06-0.005,0.088V7.65c0,0.015,0,0.029,0.001,0.042
																				c0.001,0.01,0.003,0.026,0,0.03l-0.007,0.013l-0.002,0.013C10,7.839,10,7.926,10,8.013c0,0.021,0,0.043,0,0.066l-0.01,0.005
																				L9.987,8.17c0.001,0.551,0.001,1.103,0.001,1.655H9.33H8.583L8.554,9.822L8.489,9.837v0.001L8.26,9.835l0.186,0.12l0.002,2.728
																				l0.059,0.013l0.007,0.001l0.009,0.014h0.584c0.289,0,0.579,0,0.868,0c0.007,0,0.012,0,0.015-0.001
																				c-0.001,0.008-0.001,0.016-0.001,0.026c0,2.271,0,4.544,0,6.815v0.894c0,0.02,0.001,0.04,0.001,0.06l0.001,0.055l0.02,0.021v-0.006
																				l0,0c0.011,0.014,0.034,0.053,0.088,0.053l0.026-0.003c0.016-0.001,0.032-0.001,0.047-0.001h2.894c0.018,0,0.035,0,0.055-0.002
																				h0.074l0.041-0.082l0.002-0.04c0-0.021,0-0.042,0-0.066v-0.948c0.002-2.252,0.002-4.504,0.002-6.756
																				c0-0.008-0.002-0.016-0.004-0.024h1.094h0.84c0.004,0,0.008,0,0.012,0.001l0.031,0.002l0.016-0.008
																				c0.057-0.021,0.068-0.071,0.068-0.092c0-0.002,0.002-0.006,0.002-0.009c0.014-0.042,0.018-0.086,0.02-0.124
																				c0.004-0.031,0.004-0.061,0.014-0.087l0.016-0.195v-0.008c-0.004-0.063,0.008-0.125,0.018-0.193c0.004-0.026,0.01-0.052,0.014-0.079
																				c0.012-0.041,0.012-0.077,0.012-0.11c0.002-0.021,0.002-0.04,0.006-0.057v-0.01v-0.006c0-0.068,0.01-0.134,0.02-0.205
																				c0.006-0.03,0.01-0.059,0.014-0.092l0.012-0.173v-0.006c-0.004-0.054,0.006-0.107,0.016-0.167c0.006-0.039,0.014-0.076,0.016-0.115
																				l0.016-0.147v-0.011c-0.008-0.047,0-0.094,0.008-0.148c0.008-0.041,0.014-0.088,0.014-0.133l0.008-0.013
																				c0.014-0.042,0.014-0.081,0.014-0.115c0-0.018,0.002-0.037,0.004-0.053l0.002-0.007v-0.006c-0.006-0.083,0.01-0.165,0.025-0.254
																				c0.004-0.027,0.01-0.055,0.012-0.083c0.002-0.004,0.002-0.008,0.006-0.013C15.58,9.966,15.596,9.891,15.52,9.844z\"/>
																			</svg>
																		</a>
																	</li>
																	<li>
																		<a href=\"".$instagram."\" class=\"\" target='_blank'>
																			<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
																				 width=\"25px\" height=\"25px\" viewBox=\"0 0 25 25\" enable-background=\"new 0 0 25 25\" xml:space=\"preserve\">
																				<g>
																				<path fill=\"".$restaurant['restaurant_color']."\" d=\"M15.408,20.627c-2.191,0-4.381,0-6.573,0c-0.156-0.026-0.313-0.068-0.469-0.078
																					c-2.271-0.118-4.07-1.766-4.154-4.033c-0.097-2.597-0.094-5.202-0.002-7.799c0.076-2.223,1.801-3.944,4.022-4.02
																					c2.583-0.09,5.173-0.077,7.761-0.004c1.969,0.057,3.447,1.374,3.918,3.301c0.105,0.437,0.152,0.886,0.227,1.329
																					c0,2.193,0,4.384,0,6.573c-0.027,0.181-0.068,0.362-0.08,0.546c-0.113,1.939-1.391,3.49-3.279,3.949
																					C16.328,20.498,15.865,20.549,15.408,20.627z M18.736,12.6c-0.02,0-0.039-0.001-0.057-0.001c0-0.574,0.006-1.149-0.002-1.722
																					c-0.012-0.587-0.041-1.173-0.064-1.761c-0.074-1.788-1.209-2.964-2.992-2.995c-2.334-0.039-4.67-0.039-7.004,0
																					C6.834,6.152,5.661,7.33,5.633,9.115c-0.036,2.334-0.039,4.671,0.001,7.004c0.03,1.768,1.192,2.941,2.962,2.973
																					c2.347,0.045,4.697,0.041,7.044,0.002c1.625-0.028,2.697-0.945,2.891-2.542C18.693,15.248,18.674,13.92,18.736,12.6z\"/>
																				<path fill=\"".$restaurant['restaurant_color']."\" d=\"M16.244,12.61c0,2.28-1.844,4.124-4.125,4.12c-2.282-0.002-4.133-1.851-4.128-4.122
																					c0.005-2.28,1.854-4.13,4.128-4.127C14.4,8.482,16.242,10.326,16.244,12.61z M12.132,9.958c-1.476-0.003-2.657,1.161-2.662,2.618
																					c-0.008,1.5,1.156,2.682,2.636,2.684c1.474,0.004,2.662-1.162,2.666-2.618C14.777,11.145,13.613,9.96,12.132,9.958z\"/>
																				<path fill=\"".$restaurant['restaurant_color']."\" d=\"M16.389,7.361c0.533-0.004,0.971,0.42,0.975,0.955c0.012,0.528-0.416,0.978-0.943,0.996
																					c-0.525,0.015-1.006-0.454-1.004-0.983C15.418,7.802,15.857,7.365,16.389,7.361z\"/>
																				</g>
																			</svg>
																		</a>
																	</li>
																	<li>
																		<a href=\"".$twitter."\" class=\"\" target='_blank'>
																			<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"25px\" height=\"25px\" viewBox=\"0 0 25 25\" enable-background=\"new 0 0 25 25\" xml:space=\"preserve\">
																				<g>
																				<path fill=\"".$restaurant['restaurant_color']."\" d=\"M22.197,6.527c-0.725,0.321-1.5,0.537-2.32,0.635c0.836-0.5,1.477-1.292,1.775-2.232
																					c-0.779,0.461-1.643,0.798-2.563,0.978c-0.736-0.785-1.787-1.275-2.947-1.275c-2.229,0-4.037,1.809-4.037,4.038
																					c0,0.316,0.037,0.624,0.104,0.92C8.856,9.422,5.881,7.815,3.89,5.373C3.543,5.97,3.343,6.663,3.343,7.403
																					c0,1.399,0.713,2.638,1.797,3.361c-0.661-0.022-1.285-0.203-1.829-0.505c0,0.015,0,0.034,0,0.051c0,1.955,1.392,3.586,3.239,3.958
																					c-0.34,0.091-0.695,0.142-1.065,0.142c-0.259,0-0.514-0.025-0.759-0.072c0.515,1.604,2.004,2.771,3.771,2.804
																					c-1.38,1.082-3.122,1.728-5.012,1.728c-0.327,0-0.647-0.018-0.965-0.057c1.787,1.147,3.908,1.814,6.188,1.814
																					c7.426,0,11.487-6.15,11.487-11.486c0-0.175-0.004-0.349-0.012-0.523C20.971,8.047,21.656,7.337,22.197,6.527L22.197,6.527z\"/>
																				</g>
																			</svg>
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
													<div class='col-xs-7'>
														<a style=\"color:".$restaurant['restaurant_color'].";\" href=\"".$restaurant['restaurant_comercial']."\" class=\"btn logo\">".Lang::get("home.vercomerciales")."
															<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"30px\" height=\"30px\" viewBox=\"0 0 35 34.786\" enable-background=\"new 0 0 35 34.786\" xml:space=\"preserve\">
																<path fill=\"".$restaurant['restaurant_color']."\" d=\"M16.874,0.095c-9.666,0-17.5,7.835-17.5,17.5s7.834,17.5,17.5,17.5c9.664,0,17.5-7.835,17.5-17.5  S26.538,0.095,16.874,0.095z M12.127,27.516V7.673l14.854,9.922L12.127,27.516z\"/>
															</svg>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>";

					if(strcmp($galeritem,"") !== 0){
						$itemrestau .= "<div class=\"row galeria\"  style=\"padding-top:40px;\">
							<div class=\"col-xs-12\">
								<div>
									<div class=\"row\">
										<div class=\"col-xs-12\">
											<div class=\"row\">
												<div class=\"col-xs-0 col-sm-5 p4\" data-clave='".($clave+1)."'>
													<div class=\"pa par pab\" style=\"height:50%;\">
														<div class=\"background_img galerifijarest\" style=\"background-image:url(".url($fijas[1]).");\"></div>
													</div>
													<div class=\"pa pat pab par\" style=\"top:50%;height:50%;\">
														<div class=\"background_img galerifijarest\" style=\"background-image:url(".url($fijas[2]).");\"></div>
													</div>
												</div>
												<div class=\"col-xs-12 col-sm-7 p4\" data-clave='".($clave+1)."'>
													<div id=\"content_galeresta\" class=\"pa pal pab\">
														<div id=\"galeresta".($clave + 1)."\" class=\"galeresta carousel slide carousel-fade\" data-ride=\"carousel\" data-interval=\"false\">
															<div class=\"carousel-inner\">
																".$galeritem."
															</div>
															<div class=\"controls\">
																<button data-carousel=\"#galeresta".($clave + 1)."\" class=\"background_img galchang_left\"></button>
																<button data-carousel=\"#galeresta".($clave + 1)."\" class=\"background_img galchang_right\"></button>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class=\"row\">
											<div class=\"col-xs-0 col-sm-6 p3\" data-clave='".($clave+1)."'>
												<div class=\"pa par pat\">
													<div class=\"background_img galerifijarest\" style=\"background-image:url(".url($fijas[3]).");\"></div>
												</div>
											</div>
											<div class=\"col-xs-0 col-sm-6 p3\" data-clave='".($clave+1)."'>
												<div class=\"pa pal pat\">
													<div class=\"background_img galerifijarest\" style=\"background-image:url(".url($fijas[4]).");\"></div>
												</div>
											</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>";
					}

				$itemrestau .= "</div>";
				$galeritem = "";
				$count = 0;
				$count1 = 1;
				$fijas = array();
			}
		}else{//NO HAY RESTAURANTES
			$itembanner .= "<div class=\"item active row\">
				<div class=\"col-xs-12 col-sm-4\">
					<div class=\"banner_palmasinn background_img\" style=\"background-image:url(".url('img/default.jpg').");\">
						<div>
							<div>
								<div>
									<div style=\"background-image:url(".url('img/logo.svg').");\"></div>
								</div>
							</div>
						</div>
						<a href=\"#restbar_1\" class=\"restbar\" data-toggle=\"collapse\">
							<div class=\"background_img\"></div>
						</a>
					</div>
				</div>
			</div>";
			$itemrestau .= "<div id=\"restbar_1\" class=\"collapse in\">
					<div class=\"row restaurates\">
						<div class=\"col-xs-12\">
							<div>
								<div class=\"row\">
									<div class=\"col-xs-12\">
										<div class=\"background_img fija\" style=\"background-image:url(".url('img/default.jpg').");\"></div>
										<div class=\"background_img fija\" style=\"background-image:url(".url('img/default.jpg').");\"></div>
										<div class=\"borderes\" style=\"border:4px #72a84f solid;\">
											<div class=\"row\">
												<div class=\"col-xs-12\">
													<div class=\"background_img logo\" style=\"background-image:url(".url('img/logo_verde.png').");\"></div>
												</div>
												<div class=\"col-xs-12 parrafo\">
													<div>
														<div>
															<div>
																<p class=\"titulo\" style=\"color:#72a84f;\">No hay Restaurantes.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>";
		}
		/*$data = array('seccion' => '3');
		$fija =  json_decode($b->getFijaseccion($data),true);
		if(count($fija) == 1){
			$formulario .= "<div class=\"background_img\" style=\"background-image:url(".url($fija[0]['img']).");\"></div>";
		}else{
			$formulario .= "<div class=\"background_img\" style=\"background-image:url(".url('img/default.jpg').");\"></div>";
		}*/

		$habaneros = Bannergaleri::where('bannergaleri_section','3')->where('bannergaleri_id','1')->where('bannergaleri_destacar','2')->where('bannergaleri_id','1')->select('bannergaleri_rutaimg')->first();
		if(is_null($habaneros)){
			$habaneros = url("img/default.jpg");
		}else{
			$habaneros = url($habaneros->bannergaleri_rutaimg);
		}
		
		/*$indicadores = ""; $items = "";
		for($i = 0; $i < 3; $i++){
			$galeria = json_decode($b->getGaleriaRestID($i+1),true);
			foreach($galeria as $key => $gal){
				if($key==0){
					$items .= "<div class=\"item active\" style=\"background-image:url(".url($galeria[$i]['bannergaleri_rutaimg']).");\"></div>";
					$indicadores .= "<li data-target=\"#Carousel_palmasinn\" data-slide-to=\"".($i+1)."\" class=\"active\"></li>";
				}else{
					$items .= "<div class=\"item\" style=\"background-image:url(".url($galeria[$i]['bannergaleri_rutaimg']).");\"></div>";
					$indicadores .= "<li data-target=\"#Carousel_palmasinn\" data-slide-to=\"".($i+1)."\"></li>";
				}
			}
		}*/
		
		/*$modal = array('indicators' => $indicadores);
		$modal = array_add($modal,'items',$items);*/
		$res = array('promociones' => $promociones);
		$res = array_add($res,'eventos',$eventos);
		$res = array_add($res,'itembanner',$itembanner);
		$res = array_add($res,'itemrestau',$itemrestau);
		//$res = array_add($res,'galeritem',$galeritem);
		//$res = array_add($res,'formulario',$formulario);
		//$res = array_add($res,'fijas',$fijas);
		$res = array_add($res,'id',$request->target);
		//$res = array_add($res,'modal',$modal);
		$res = array_add($res,'turismo',url('/')."/home/turismosalud");
		$res = array_add($res,'habaneros_form',$habaneros);
		return view("frontend.restaurantes")->with([
			"res" => $res,
			/*"modal" => $modal,*/
			"listahabitaciones" => $this->listahabitaciones(),
		]);
	}
	//DEACARGA
	public function descarga(){
		$pathToFile = json_decode(ISalud::obtenerBannerSalud("0"))[0]->salud_pathinterna;
		$route = explode(".", $pathToFile);
		$name = "Salud.".$route[1];
		return response()->download($pathToFile, $name);
	}
	
}
