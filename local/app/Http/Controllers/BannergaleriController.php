<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bannergaleri;

class BannergaleriController extends Controller
{
	public function galerihabitad(Request $request){
		$res = $estado = $galeria = "";
		$b = new Bannergaleri();
		$habitaciones = $b->getGaleriahabitacion($request->all());
		if(count($habitaciones) == 0){
			$galeria = "<div id=\"info\" class=\"alert alert-info text-center\">
				 <h3><strong>Para comenzar</strong> selecciona una habitación.</h3>
				 * <strong>Recuerda para crear una galeria</strong> debes cargar un minimo de cuatro (4) imagenes y un maximo de ocho (8). *</br>
				 * <strong>Recuerda para crear una galeria</strong> debes seleccionar tres(3) imagenes destacadas. *
			</div>";
			$estado = 1;
		}
		if(count($habitaciones) == 8){
			$images = json_decode($habitaciones->toJson(),true);
			$label = "";
			$galeria = "<div id=\"grid_habitadgaleri\" class=\"row\">";
			foreach($images AS $clave => $valor){	
				if( $valor['bannergaleri_destacar'] == '1'){
					$label = "<label id=\"".($clave + 1)."_label\" class=\"checkbox-inline\">
						<input checked type=\"checkbox\" value=\"1\" id=\"destacados_".($clave + 1)."\" name=\"destacados_".($clave + 1)."\" class=\"checkboxgroup\" data-image=\"#".($clave + 1)."_picture\"><i class=\"fa fa-square-o fa-3x\"></i><i class=\"fa fa-check-square-o fa-3x\"></i>
						<button class=\"delmy\" type=\"button\" data-image='{\"delgaleria\":\"".base64_encode($valor['bannergaleri_id'])."\",\"delimage\":\"".$valor['bannergaleri_rutaimg']."\"}' data-label=\"#".($clave + 1)."_label\" data-picture=\"#".($clave + 1)."_picture\" data-file=\"#file_".($clave + 1)."\"><i class=\"fa fa-trash-o fa-2x\" aria-hidden=\"true\"></i></button>
					</label>";
				}else{
					$label = "<label id=\"".($clave + 1)."_label\" class=\"checkbox-inline\">
						<input type=\"checkbox\" value=\"1\" id=\"destacados_".($clave + 1)."\" name=\"destacados_".($clave + 1)."\" class=\"checkboxgroup\" data-image=\"#".($clave + 1)."_picture\"><i class=\"fa fa-square-o fa-3x\"></i><i class=\"fa fa-check-square-o fa-3x\"></i>
						<button class=\"delmy\" type=\"button\" data-image='{\"delgaleria\":\"".base64_encode($valor['bannergaleri_id'])."\",\"delimage\":\"".$valor['bannergaleri_rutaimg']."\"}' data-label=\"#".($clave + 1)."_label\" data-picture=\"#".($clave + 1)."_picture\" data-file=\"#file_".($clave + 1)."\"><i class=\"fa fa-trash-o fa-2x\" aria-hidden=\"true\"></i></button>
					</label>";
				}
				$galeria .= "<div class=\"col-xs-12 col-sm-6 col-md-3\">
				<div class=\"col-xs-12 box-image\">
					".$label."
					<img class=\"p_picture\" src=\"".url($valor['bannergaleri_rutaimg'])."\" id=\"".($clave + 1)."_picture\">
				</div>
				<div class=\"col-xs-12 box-file\">
					<input type=\"file\" id=\"file_".($clave + 1)."\" name=\"file_".($clave + 1)."\" class=\"file_img\" data-image=\"#".($clave + 1)."_picture\" data-label=\"#".($clave + 1)."_label\"/>
				</div>
				</div>";
			}
			$galeria .= "<div class=\"col-xs-12 col-md-8\">
				<a id=\"btnRegistrar\" class=\"btn btn-app btn-palmasinn\">
					<i class=\"fa fa-save\"></i> REGISTRAR
				</a>
			</div></div>";
			$estado = 1;
		}else{
			$galeria = "<div id=\"info\" class=\"alert alert-info text-center\">
				 <h3><strong>Para comenzar</strong> selecciona una habitación.</h3>
				 * <strong>Recuerda para crear una galeria</strong> debes cargar un minimo de cuatro (4) imagenes y un maximo de ocho (8). *</br>
				 * <strong>Recuerda para crear una galeria</strong> debes seleccionar tres(3) imagenes destacadas. *
			</div>";
			$estado = 0;//algo esta mal		
		}
		$res = array('estado'=> $estado);
		$res = array_add($res,'galeria',$galeria);
		return $res;
	}
	
	public function editgalerihabitad(Request $request){
		$checkboxval = explode(',',$request->checkboxval);
		$data = array('seccion' => '2');
		$data = array_add($data,'galeria',$request->habitadgaleri);
		$b = new Bannergaleri();
		$file = 0;//estado carga
		$galeria = json_decode($b->getGaleriahabitacion($data)->toJson(),true);
		foreach($galeria AS $clave => $value){
			if(array_key_exists("file_".($clave+1),$request->all())){
				$name = "file_".($clave+1);
				$file = $b->updateFilehabitad($request->habitadgaleri,$request->$name,$value['bannergaleri_rutaimg'],$checkboxval[$clave],$clave);
			}else{
				$file = $b->updateDestacarhabitad($request->habitadgaleri,$value['bannergaleri_rutaimg'],$checkboxval[$clave]);
			}
		}
		return $file;
	}
	
	public function erasegalerihabitad(Request $request){
		$res = "";
		$b = new Bannergaleri();
		$res = array('estado' => $b->erasegalerihabitad($request->all()));
		return $res;
	}
	
	public function imagesfija(Request $request){
		$res = "";
		$b = new Bannergaleri();
		$res = array('estado' => $b->imagesfija	($request->all()));
		return $res;
	}

	public function erasefijasection(Request $request){
		$res = "";
		$b = new Bannergaleri();
		$res = array('estado' => $b->erasefijasection($request->all()));
		return $res;
	}
	public function editgalerirestaurat(Request $request){
		$opc = $request->input("idrest");
		$identificador = substr($opc,strlen($opc)-1,strlen($opc));
		
		$checkboxval = explode(',',$request->checkboxval);
		$b = new Bannergaleri();
		$file = 0;//estado carga
		$galeria = json_decode($b->getGaleriaRestID("3",$identificador),true);
		foreach($galeria AS $clave => $value){
			if(array_key_exists("file_".($clave+1),$request->all())){
				$name = "file_".($clave+1);
				$file = $b->updateFilerestaurante($request->$name,$value['bannergaleri_rutaimg'],$checkboxval[$clave],$clave,$identificador);
			}else{
				$file = $b->updateDestacarrestaurante($value['bannergaleri_rutaimg'],$checkboxval[$clave],$identificador);
			}
		}
		return $file;
	}

	public function erasegalerirestaurante(Request $request){
		$res = "";
		$b = new Bannergaleri();
		$res = array('estado' => $b->erasegalerirestaurante($request->all()));
		return $res;
	}
	
	public function bannersection(Request $request){
		$res = "";
		$b = new Bannergaleri();
		$res = array('estado' => $b->bannersection($request->all()));
		return $res;
	}
	public function editgalerinstalaciones(Request $request){
		$res = "";
		$b = new Bannergaleri();
		$estado= 0;
		$galeria = json_decode($b->getGaleriinsta()->toJson(),true);
		foreach($galeria AS $clave => $value){
			if(array_key_exists("file_".($clave+1),$request->all())){
				$name = "file_".($clave+1);
				$estado = $b->editgalerinstalaciones($request->$name,$value['id'],$clave);
			}
		}
		return $estado;
	}
	public function erasegalerinstalaciones(Request $request){
		$res = "";
		$b = new Bannergaleri();
		$res = array('estado' => $b->erasegalerinstalaciones($request->all()));
		return $res;
	}
	public function savegaleriahome(Request $request){
		$estado = Bannergaleri::savegaleriahome($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_galeriasecu" => "Item Registrado con exito."]);
		}else{
			return redirect()->back()->with(["server_galeriasecu" => "Lo sentimos. Algo salio mal, no ne registro item en la galeria"]);
		}
	}
	public function erasegalesecun(Request $request){
		return $res = array('estado' => Bannergaleri::erasegalesecun($request->all()));
	}
}
