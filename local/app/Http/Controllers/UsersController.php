<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Avatares;
use Lang;

class UsersController extends Controller
{
    public function estadoavatares(Request $request){
		$usuario = \Cache::store('database')->get("usuario");
		$estado = Usuario::estadoavatar($request->all(),$usuario->id);
		return response()->json([
			"estado" => $estado,
			"path" => url(Usuario::where("id",$usuario->id)->value("user_avatar"))
		]);
	}
	
	//ELIMINO UN AVATAR
	public function delete_aremover(Request $request){
		$estado = Avatares::deletearemover($request->all());
		return response()->json([
			"estado" => $estado,
		]);
	}
	
	public function delete_users(Request $request){
		$estado = Usuario::deleteusers($request->all());
		return response()->json([
			"estado" => $estado
		]);
	}
	
	public function addusers(Request $request){
		$estado = Usuario::addusers($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_users" => Lang::get("message.yes_register")]);
		}else if($estado==2){
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.login_repetido"));
		}else if($estado==3){
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.email_repetido"));
		}else{
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.not_users"));
		}
	}
	
	public function addavataruser(Request $request){
		$estado = Avatares::addavataruser($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_cuenta" => Lang::get("message.yes_register")]);
		}else{
			return redirect()->back()->with(["server_cuenta" => Lang::get("message.not_register")]);
		}
	}
	
	public function edit_users(Request $request){
		$estado = Usuario::edit_users($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_listusers" => Lang::get("message.yes_modify")]);
		}else if($estado==2){
			return redirect()->back()->with(["server_listusers" => Lang::get("message.not_changeusers")]);
		}else{
			return redirect()->back()->with(["server_listusers" => Lang::get("message.not_modify")]);
		}
	}
	
	public function editperfil(Request $request){
		$usuario = \Cache::store('database')->get("usuario");
		$estado = Usuario::editperfil($request->all(),$usuario->id);
		if($estado==1){
			return redirect()->back()->with(["server_perfil" => Lang::get("message.yes_modify")]);
		}else if($estado==2){
			return redirect()->back()->with(["server_perfil" => Lang::get("message.not_change")]);
		}else{
			return redirect()->back()->with(["server_perfil" => Lang::get("message.not_modify")]);
		}
	}
}
