<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promociones;
use Lang;

class PromocionesController extends Controller
{
    public function addpromocioneshome(Request $request){
		$estado = Promociones::addpromocioneshome($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_promo" => Lang::get("message.yes_register")]);
		}else if($estado==2){
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.is_unique"));
		}else{
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.not_size"));
		}
	}

	public function editpromohome(Request $request){
		$estado = Promociones::editpromohome($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_promo" => Lang::get("message.yes_modify")]);
		}else if($estado==2){
			return redirect()->back()->with(["server_promo" => Lang::get("message.is_unique")]);
		}else{
			return redirect()->back()->with(["server_promo" => Lang::get("message.not_register")]);
		}
	}

	public function estadopromocioneshome(Request $request){
		$estado = Promociones::estadopromocioneshome($request->all());
		return response()->json([
			"estado" => $estado,
			"status" => Promociones::whichstatus(base64_decode($request->input("promo_id")))
		]);
	}

	public function delete_promo(Request $request){
		$estado = Promociones::delete_promo($request->all());
		return response()->json([
			"estado" => $estado,
		]);
	}
}
