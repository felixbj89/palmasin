<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicios;
use Lang;

class ServiciosController extends Controller
{
    public function editservicioshome(Request $request){
		$estado = Servicios::editservicioshome($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_servicios" => Lang::get("message.yes_modify")]);
		}else{
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.not_modify"));
		}
	}
}
