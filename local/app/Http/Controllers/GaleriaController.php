<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Galeria;
use App\ISalud;
use App\Proyectos;
use App\Comerciales;
use Lang;

class GaleriaController extends Controller
{
	public function estadobannerinferior(Request $request){
		$estado = ISalud::estadobannerinferior($request->all());
		return response()->json([
			"estado" => $estado,
			"path" => url(ISalud::where("id",base64_decode($request->input("historial")))->value("salud_path")),
			"pathinterna" => url(ISalud::where("id",base64_decode($request->input("historial")))->value("salud_pathinterna")),
			"id" => base64_decode($request->input("historial"))
		]);
	}
	
    public function addgaleriahome(Request $request){
		$estado = Galeria::addgaleriahome($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_galeria" => Lang::get("message.yes_register")]);
		}else{
			return redirect()->back()->with(["server_galeria" => Lang::get("message.not_register")]);
		}
	}
	
	public function addbannerinferior(Request $request){
		$estado = ISalud::addbannerinferior($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_isalud" => Lang::get("message.yes_register")]);
		}else{
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.not_register"));
		}
	}
	
	public function addproyectoshome(Request $request){
		$estado = Proyectos::addproyectoshome($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_proyectos" => Lang::get("message.yes_register")]);
		}else{
			return redirect()->back()->with(["server_proyectos" => Lang::get("message.not_modify")]);
		}
	}
	
	public function editgaleriahome(Request $request){
		$estado = Galeria::editgaleriahome($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_galeria" => Lang::get("message.yes_modify")]);
		}else{
			return redirect()->back()->with(["server_galeria" => Lang::get("message.yes_modify")]);
		}
	}
	
	public function editbannerinferior(Request $request){
		$estado = ISalud::editbannetinferior($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_galeria" => Lang::get("message.yes_modify")]);
		}else{
			return redirect()->back()->with(["server_galeria" => Lang::get("message.yes_modify")]);
		}
	}
	
	public function editcomercialeshome(Request $request){
		$estado = Comerciales::editcomercialeshome($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_comerciales" => Lang::get("message.yes_modify")]);
		}else{
			return redirect()->back()->with(["server_comerciales" => Lang::get("message.not_modify")]);
		}
	}
	
	public function estadogaleriahome(Request $request){
		$estado = Galeria::estadogaleriahome(base64_decode($request->input("historial_id")));
		return response()->json([
			"estado" => $estado,
			"id" => base64_decode($request->input("historial_id")),
			"imagen" => url(Galeria::where("id",base64_decode($request->input("historial_id")))->value("galeria_path"))
		]);
	}

	public function delete_galeria(Request $request){
		$estado = Galeria::deletegaleria($request->all());
		return response()->json([
			"estado" => $estado
		]);
	}
	
	public function delete_bsalud(Request $request){
		$estado = ISalud::deletebsalud($request->all());
		return response()->json([
			"estado" => $estado
		]);
	}

	public function asociarpromogaleria(Request $request){
		$asociaciones = json_decode($request->input("asociados"));
		$confirmar = 0; $repetido = 0;
		for($i = 0; $i < count($asociaciones); $i++){
			if(Galeria::asociarPromo($asociaciones[$i]->imagen,$asociaciones[$i]->ruta)){
				$confirmar = 1;
			}else{
				$repetido = 1;
			}
		}

		if($confirmar==1 && $repetido==0){
			return redirect()->back()->with(["server_galeria" => Lang::get("message.yes_asociado")]);
		}else{
			return redirect()->back()->with(["server_galeria" => Lang::get("message.not_asociado")]);
		}
	}
	
	public function delete_asociacion(Request $request){
		$estado = Galeria::deleteasociacion($request->all());
		return response()->json([
			"estado" => $estado
		]);
	}
	
	public function editarasociacionpg(Request $request){
		$asociaciones = json_decode($request->input("asociados"));
		$confirmar = 0; $repetido = 0;
		for($i = 0; $i < count($asociaciones); $i++){
			if(Galeria::asociarPromo($asociaciones[$i]->imagen,$asociaciones[$i]->ruta)){
				$confirmar = 1;
			}else{
				$repetido = 1;
			}
		}
		
		if($confirmar==1 && $repetido==0){
			return redirect()->back()->with(["server_asociacionlist" => Lang::get("message.yes_asociado")]);
		}else{
			return redirect()->back()->with(["server_asociacionlist" => Lang::get("message.not_asociado")]);
		}
	}
}
