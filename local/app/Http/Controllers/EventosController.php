<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Eventos;
use App\EventosCorporativos;
use Lang;

class EventosController extends Controller
{
	//PRINCIPAL EVENTO EDIT
	public function ppalevento(Request $request){
		$res = "";
		$e = new Eventos();
		$estado = $e->ppalevento($request->all());
		$evento = $e->get_Evento($request->all());
		$res = array('estado'=> $estado);
		$res = array_add($res,'status',$evento->event_ppal);
		return $res;
	}
	//LISTAEVENTOS DEL MESY AÑO
	public function listmesyeareventohome(Request $request){
		$res = "";
		$e = new Eventos();
		$eventos = $e->get_eventosAll($request->all());
		foreach($eventos as $clave => $valor){
			$valor->id = base64_encode($valor->id);
		}
		return $eventos;
	}
	//ADD
    public function addeventoshome(Request $request){
		$e = new Eventos();
		$estado = $e->addeventoshome($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_eventoshome" => Lang::get("message.yes_register")]);
		}else{
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.evet_not_reg"));
		}
	}
	
	public function addeventoscorporativos(Request $request){
		$estado = EventosCorporativos::addeventoscorporativos($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_eventoscorporativos" => Lang::get("message.yes_register")]);
		}else{
			return redirect()->back()->withInput()->WithErrors(Lang::get("message.evet_not_reg"));
		}
	}
	
	//EDIT EVENTO
	public function editadoeventos_home(Request $request){
		$e = new Eventos();
		$estado = $e->editadoeventos_home($request->all());
		if($estado==1){
			return redirect()->back()->with(["server_eventos" => Lang::get("message.yes_modify")]);
		}else{
			return redirect()->back()->with(["server_eventos" => Lang::get("message.evet_not_reg")]);
		}
	}
	//ERASE
	public function eraseventohome(Request $request){
		$res = "";
		$e = new Eventos();
		$estado = $e->eraseventohome($request->all());
		$res = array('estado'=> $estado);
		return $res;
	}
	//CLONE - COPIAR EVENTO
	public function cloneventohome(Request $request){
		$e = new Eventos();
		return array('estado'=> $e->clonevento_home($request->all()));	
	}
	//EDIT EVENTO CORPORATIVO
	public function editadoeventos_corporativo(Request $request){
		$estado = Eventos::editadoeventos_corporativo($request->all());
		if($estado == 1){
			return redirect()->back()->with(["server_eventos" => Lang::get("message.yes_modify")]);
		}else{
			return redirect()->back()->with(["server_eventos" => Lang::get("message.evet_not_reg")]);
		}
	}
	//ACTIVAR / DESACTIVAR  EVENTO CORPORATIVO
	public function actv_corporativo(Request $request){
		$res = array('estado'=> Eventos::actv_corporativo($request->all()));
		return $res;
	}
}
