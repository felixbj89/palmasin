<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccessOperator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $usuario = \Cache::store('database')->get("usuario");
		if($usuario!=NULL){
			if($usuario->user_role==0 || $usuario->user_role==1 || $usuario->user_role==2)
				return true;
		}return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
