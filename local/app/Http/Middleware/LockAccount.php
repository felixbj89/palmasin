<?php

namespace App\Http\Middleware;

use Closure;

class LockAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$usuario = \Cache::store('database')->get("usuario");
		if($usuario!=NULL)
			return $next($request);
		return redirect()->to("admin/backend");//REGRESO AL INDEX
    }
}
