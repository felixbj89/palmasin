<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bannergaleri;
use Carbon\Carbon;

class Habitacion extends Model
{
    protected $table = "habitaciones";
	
	protected $fillable = [
		"room_title",
		"room_text",
		"room_listdetails",
		"room_comodidades",
		"room_comodidades_list",
		"room_imgportada"
	];
	//STORE
	public static function addhabitad($request){
		$date = Carbon::now(-4);
		$b = new Bannergaleri();
		if(array_key_exists("portada",$request) && array_key_exists("titulo",$request) && array_key_exists("descripcion",$request) && array_key_exists("caracteristicas",$request) && array_key_exists("comodidades",$request)){
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["portada"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$one = (str_replace($search, $replace, $name));
			$comodidades = "";
			foreach (explode(',',$request["comodidades"]) as $clave => $valor){
				$comodidades .= "<li class=\"col-xs-4 col-sm-3 background_img ico ico".$valor."\"></li>";
			}
			\Storage::disk('habitad')->put($one,  \File::get($request["portada"]));
			Habitacion::create([
				"room_title" => $request["titulo"],
				"room_text" => $request["descripcion"],
				"room_listdetails" => $request["caracteristicas"],
				"room_comodidades" => $comodidades,
				"room_comodidades_list" => $request["comodidades"],
				"room_imgportada" => "img/habitaciones/".$one
			]);
			$data = array('seccion' => '2');
			$data = array_add($data,'habitad',json_decode(Habitacion::select('id')->where('room_imgportada',"img/habitaciones/".$one)->get()->toJson(),true)[0]['id']);
			for($i = 0;$i<8;$i++){
				$b->addGalerihabitad($data);//crear galeria para la habitacion
			}
			return 1;
		}else{
			return 0;
		}
	}
	//UPDATE HABITACIÓN
	public static function edit_habitad($request){
		$date = Carbon::now(-4);
		$habitacion = Habitacion::where("id",base64_decode($request['habitad']))->first();
		if(count($habitacion)==1){
			if(array_key_exists("portada",$request)){//cambio la imagen
				$comodidades = "";
				foreach (explode(',',$request["comodidades"]) as $clave => $valor){
					$comodidades .= "<li class=\"col-xs-4 col-sm-3 background_img ico ico".$valor."\"></li>";
				}
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["portada"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$one = (str_replace($search, $replace, $name));
				\Storage::disk('habitad')->put($one,  \File::get($request["portada"]));//nueva imagen
				if (strcmp($habitacion->room_imgportada,"img/default.jpg") !== 0) {
					$del_file = explode('/',$habitacion->room_imgportada);
					\Storage::disk('habitad')->delete($del_file[2]);//borro imagen vieja
				}
				$habitacion->update([
					"room_title" => $request["titulo"],
					"room_text" => $request["descripcion"],
					"room_listdetails" => $request["caracteristicas"],
					"room_comodidades" => $comodidades,
					"room_comodidades_list" => $request["comodidades"],
					"room_imgportada" => "img/habitaciones/".$one
				]);
			}else{
				$comodidades = "";
				foreach (explode(',',$request["comodidades"]) as $clave => $valor){
					$comodidades .= "<li class=\"col-xs-4 col-sm-3 background_img ico ico".$valor."\"></li>";
				}
				$habitacion->update([
					"room_title" => $request["titulo"],
					"room_text" => $request["descripcion"],
					"room_listdetails" => $request["caracteristicas"],
					"room_comodidades" => $comodidades,
					"room_comodidades_list" => $request["comodidades"]
				]);
			}
			return 1;
		}else{
			return 0;
		}
	}
	//ERASE HABITACIÓN
	public static function erasehabitad($request){
		$habitacion = Habitacion::where("id",base64_decode($request['target']))->first();
		if(count($habitacion)==1){
			$b = new Bannergaleri();
			$b->eraseallgalerihabitad($request);
			if (strcmp($habitacion->room_imgportada,"img/default.jpg") !== 0) {
				$del_file = explode('/',$habitacion->room_imgportada);
				\Storage::disk('habitad')->delete($del_file[2]);//borro imagen vieja
			}
			$habitacion->delete();
			return 1;
		}			
		return 0;		
	}
	//OBTENER ALL HABITACIONES
	public static function getAllhabitaciones(){
		return Habitacion::select("id AS habitacion","room_title AS nombre","room_text AS descripcion","room_listdetails AS detalles","room_comodidades AS comididades","room_imgportada AS img")->get()->toJson();
	}
	//OBTENER ALL HABITACIONES SOLO LOS NOMBRES Y IDs
	public static function getAllhabitacionesname(){
		return Habitacion::select("id AS habitacion","room_title AS nombre")->get()->toJson();
	}
	//OBTENER HABITACION POR ID
	public static function getHabitacion($request){
		return Habitacion::select("id AS habitacion","room_title AS nombre","room_text AS descripcion","room_listdetails AS detalles","room_comodidades AS comodidades","room_comodidades_list AS list_comodidades","room_imgportada AS img")->where('id',base64_decode($request['target']))->first();
	}	
}
