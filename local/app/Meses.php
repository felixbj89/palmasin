<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Eventos;
use Carbon\Carbon;

class Meses extends Model
{
    protected $table = "meses";

	protected $fillable = [
		"mes_nombre",
		"mes_titulo",
		"mes_year",
		"mes_status"
	];
	
	//OBTENER MES
	public static function get_mes($mes){
		return  DB::table('meses')->where('id',$mes)->first();
	}	
	//OBTENER TODOS LOS MESES
	public static function get_Allmeses(){
		return  DB::table('meses')->skip(0)->take(12)->get();
	}
	//OBTENER TODOS LOS MESES CON EL NUMERO DE EVENTOS DEL MES
	public static function get_Allmesescounteventos(){
		$array = array();
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$fecha = explode(" ",$fecha_actual)[0];
		$year = explode("-",$fecha)[0];//año actual
		$mes_actual = explode("-",$fecha)[1];//mes actual
		$e = new Eventos();
		$meses = json_decode(DB::table('meses')->skip(0)->take(12)->get()->toJson(),true);
		foreach($meses as $clave => $mes){
			$date = array('mes'=> $mes['id']);
			if($mes_actual > $mes['id']){
				$date = array_add($date,'year',$year+1);
			}else{
				$date = array_add($date,'year',$year);
			}
			$mes = array_add($mes,'counteventos',$e->get_eventosAll($date)->count());
			$array = array_add($array,$clave,$mes);
		}
		return $array;
	}
	//OBTENER MESES LANZADOS
	public static function getMesactivos(){
		return DB::table('meses')->where('mes_status','1')->take(3)->get();
	}
	//ACTIVAR MES
	public static function lanzarmesevetoshome($request){
		$fecha_actual = Carbon::now(-4,"America/Asuncion");
		$fecha = explode(" ",$fecha_actual)[0];
		$year_actual = explode("-",$fecha)[0];//año actual
		$mes_actual = explode("-",$fecha)[1];//mes actual
		$mes = DB::table('meses')->where('meses.id',base64_decode($request['evento']))->first();
		$activos = DB::table('meses')->where('meses.mes_status',"1")->count();
		if(count($mes) == 1 && 3 > $activos){
			if($mes->mes_status == "1"){
				DB::table('meses')->where('meses.id',base64_decode($request['evento']))->update(["mes_status" => "0","mes_year" => "0"]);
			}else{
				if($mes->id >= $mes_actual){
					DB::table('meses')->where('meses.id',base64_decode($request['evento']))->update(["mes_status" => "1","mes_year" => $year_actual]);
				}else{
					DB::table('meses')->where('meses.id',base64_decode($request['evento']))->update(["mes_status" => "1","mes_year" => ($year_actual + 1)]);
				}
			}
			return 1;
		}
		if(3 >= $activos){
			if(3 == $activos && $mes->mes_status == "1" && count($mes) == 1){
				DB::table('meses')->where('meses.id',base64_decode($request['evento']))->update(["mes_status" => "0"]);
				return 1;
			}
			return 2;
		}
		return 0;
	}	
	
}
