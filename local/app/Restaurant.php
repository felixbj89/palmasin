<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Restaurant extends Model
{
    protected $table = "restaurant";
	
	protected $fillable = [
		"restaurant_title",
		"restaurant_descripcion",
		"restaurant_contacto1",
		"restaurant_contacto2",
		"restaurant_comercial",
		"restaurant_rest_file1",//logoB
		"restaurant_rest_file2",//logoC
		"restaurant_rest_file3",//banner
		"restaurant_rest_file4",//fijaI
		"restaurant_rest_file5",//fijaII
		"restaurant_color"
	];
	
	//OBTENER TODOS LOS LOGOS
	public static function getLogos(){
		return Restaurant::select("restaurant_rest_file2","restaurant_title")->get()->toJson();
	}
	
	//OBTENER TODOS LOS RESTAURANTES
	public static function getAllrestaurants(){
		return Restaurant::get();
	}
	//OBTENER RESTAURANTE POR ID
	public static function getRestaurant($request){
		return Restaurant::where("id",base64_decode($request['target']))->first();
	}
	
	//UPDATE HABITACIÓN
	public static function edit_restaurat($request){
		$date = Carbon::now(-4);
		$restaurant = Restaurant::where("id",base64_decode($request['restaurant']))->first();
		if(count($restaurant)==1){
			$titulo = "";
			if(array_key_exists("titulo",$request) && $request["titulo"]!=""){
				$titulo = $request["titulo"];
			}else{
				$titulo = $restaurant->restaurant_title;
			}
			
			$descripcion = "";
			if(array_key_exists("descripcion",$request) && $request["descripcion"]!=""){
				$descripcion = $request["descripcion"];
			}else{
				$descripcion = $restaurant->restaurant_descripcion;
			}
			
			$contactouno = "";
			if(array_key_exists("contactoI",$request) && $request["contactoI"]!=""){
				$contactouno = $request["contactoI"];
			}else{
				$contactouno = $restaurant->restaurant_contacto1;
			}
			
			$contactotwo = "";
			if(array_key_exists("contactoII",$request) && $request["contactoII"]!=""){
				$contactotwo = $request["contactoII"];
			}else{
				$contactotwo = $restaurant->restaurant_contacto2;
			}
			
			$comercial = "";
			if(array_key_exists("comercial",$request) && $request["comercial"]!=""){
				$comercial = $request["comercial"];
			}else{
				$comercial = $restaurant->restaurant_comercial;
			}
			
			$restaurant->update([
				"restaurant_title" => $titulo,
				"restaurant_descripcion" => $descripcion,
				"restaurant_contacto1" => $contactouno,
				"restaurant_contacto2" => $contactotwo,
				"restaurant_comercial" => $comercial
			]);
			
			for($i = 1;$i <= 5;$i++){
				$img_name = "restaurant_rest_file".$i;
				if(array_key_exists("rest_file".$i,$request) && $request["rest_file".$i]!=""){
					$file = $request["rest_file".$i];
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$i.$request["rest_file".$i]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$one = (str_replace($search, $replace, $name));
					\Storage::disk('restaurante')->put($one, \File::get($file));
					if (strcmp($restaurant->$img_name,"img/default.jpg") !== 0) {
						//$del_file = explode('/',$restaurant->$img_name);
						\Storage::disk('restaurante')->delete($restaurant->$img_name);//borro imagen vieja
					}
					$restaurant->update([
						"restaurant_rest_file".$i => "img/restaurantes/".$one
					]);
				}
			}
		
			return 1;
		}else{
			return 0;
		}
	}	
}
