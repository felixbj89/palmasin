<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Historial;
class ISalud extends Model
{
    protected $table = "salud";

	protected $fillable = [
		"salud_titulo",
		"salud_situlo",
		"salud_path",
		"salud_pathinterna",
		"salud_estado",
		"user_id"
	];

	protected $hidden = [
      'remember_token',
	];
	
	
	public function usuario(){
        return $this->hasMany('App\Usuario',"id","user_id");
    }
	
	public static function deletebsalud($inputs){
		$finder = ISalud::where("salud_estado","1")->where("id",base64_decode($inputs["historial"]))->first();
		if(count($finder)==1){
			//COMPRUEBO QUE NO EXISTA LA IMAGEN A BORRAR EN LA TABLA DE MI BD
			if(file_exists($finder->salud_path)){//EXISTE LA RUTA ACTIVA
				unlink($finder->salud_path);
			}
			
			if(file_exists($finder->salud_pathinterna)){//EXISTE LA RUTA ACTIVA
				unlink($finder->salud_pathinterna);
			}
			
			$finder->delete();
			return 1;
		}return 0;
	}
	
	//OBTENGO EL BLOQUE DE SALUD
	public static function obtenerBannerSalud($estado){
		return ISalud::where("salud_estado",$estado)->get()->toJson();
	}
	
	//ACTIVO UNA IMAGEN DE SALUD DEL Historial
	public static function estadobannerinferior($inputs){
		$finder = ISalud::where("salud_estado","1")->where("id",base64_decode($inputs["historial"]))->first();
		if(count($finder)==1){
			//ACTIVO->HISTORIAL------------------EXISTE LA IMAGEN EXTERNA
			$rutaone = "img/historial/saludhome/";
			$activa = ISalud::where("salud_estado","0")->where("id","1")->first();
			$imagen_activa = substr($activa->salud_path,strripos($activa->salud_path,"/")+1);
			$imagen_historial = substr($finder->salud_path,strripos($finder->salud_path,"/")+1);
			if($imagen_historial==$imagen_activa){//NO SE PUEDE ACTIVAR UNA IMAGEN QUE ESTE ACTIVA Y EN EL HISTORIAL A LA VEZ
				return 2;
			}else{			
				if(file_exists($activa->salud_path)){//EXISTE LA RUTA ACTIVA
					$imagen = substr($activa->salud_path,strripos($activa->salud_path,"/")+1);
					$rutaone .= $imagen;
					if(copy($activa->salud_path,$rutaone)){
						unlink($activa->salud_path);
					}
				}
				
				//EXISTE LA IMAGEN INTERNA
				$rutatwo = "img/historial/saludhome/";
				if(file_exists($activa->salud_pathinterna)){//EXISTE LA RUTA ACTIVA
					$imagen = substr($activa->salud_pathinterna,strripos($activa->salud_pathinterna,"/")+1);
					$rutatwo .= $imagen;
					if(copy($activa->salud_pathinterna,$rutatwo)){
						unlink($activa->salud_pathinterna);
					}
				}
				
				//HISTORIAL
				ISalud::where("id",base64_decode($inputs["historial"]))->where("salud_estado","1")->update([
					"salud_path" => $rutaone,
					"salud_pathinterna" => $rutatwo,
				]);
				
				//HISTORIAL->ACTIVO-----------------EXISTE LA IMAGEN EXTERNA
				$rutathrid = "img/home/salud/";
				if(file_exists($finder->salud_path)){//EXISTE LA RUTA ACTIVA
					$imagen = substr($finder->salud_path,strripos($finder->salud_path,"/")+1);
					$rutathrid .= $imagen;
					if(copy($finder->salud_path,$rutathrid)){
						unlink($finder->salud_path);
					}
				}
				
				//EXISTE LA IMAGEN INTERNA
				$rutafour = "img/home/salud/";
				if(file_exists($finder->salud_pathinterna)){//EXISTE LA RUTA ACTIVA
					$imagen = substr($finder->salud_pathinterna,strripos($finder->salud_pathinterna,"/")+1);
					$rutafour .= $imagen;
					if(copy($finder->salud_pathinterna,$rutafour)){
						unlink($finder->salud_pathinterna);
					}
				}
				
				//ACTIVA
				ISalud::where("id","1")->where("salud_estado","0")->update([
					"salud_path" => $rutathrid,
					"salud_pathinterna" => $rutafour,
				]);
							
				return 1;
			}
		}return 0;
	}
	
	//EDITO EL BLOQUE DE SALUD
	public static function editbannetinferior($inputs){
		$date = Carbon::now(-4);
		$isalud = ISalud::all();
		$rutaone = ""; $rutatwo = "";
		$titulo = ""; $subtitlo = "";
		$historialone = 0; $historialtwo = 0;
		
		if(array_key_exists("titulo_salud",$inputs)){//CONTROLO LA EDICIÓN DEL TÍTULO 
			$titulo = $inputs["titulo_salud"];
		}else{
			$titulo = $isalud[0]->salud_titulo;
		}
		
		if(array_key_exists("subtitulo_salud",$inputs)){//CONTROLO LA EDICIÓN DEL SUB-TÍTULO
			$subtitulo = $inputs["subtitulo_salud"];
		}else{
			$subtitulo = $isalud[0]->salud_situlo;
		}
		
		if(array_key_exists("file_one",$inputs)){//BANNER DE SALUD
			$name = str_random(4).$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$uno = (str_replace($search, $replace, $name));
			
			if($isalud[0]->salud_path=="img/home/salud/default.jpg"){//SI USO EL EDITAR COMO UN INSERTAR/ORIGINALMENTE
				\Storage::disk('saludhome')->put($uno,  \File::get($inputs["file_one"]));
				$rutaone = ("img/home/salud/".$uno);
			}else{
				$rutaone = "img/historial/saludhome/";
				if(file_exists($isalud[0]->salud_path)){
					$imagen = substr($isalud[0]->salud_path,strripos($isalud[0]->salud_path,"/")+1);
					$rutaone .= $imagen;
					if(copy($isalud[0]->salud_path,$rutaone)){
						unlink($isalud[0]->salud_path);
					}
					
					$historialone = 1;
				}

				\Storage::disk('saludhome')->put($uno,  \File::get($inputs["file_one"]));
				$uno = ("img/home/salud/".$uno);
			}
		}else{
			$rutaone = "img/historial/saludhome/";
			$imagen = substr($isalud[0]->salud_path,strripos($isalud[0]->salud_path,"/")+1);
			$rutaone .= $imagen; copy($isalud[0]->salud_path,$rutaone);
			
			$uno = $isalud[0]->salud_path;
		}
		
		if(array_key_exists("file_two",$inputs)){//BANNER DE SALUD
			$name = str_random(4).$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_two"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$dos = (str_replace($search, $replace, $name));
			if($isalud[0]->salud_pathinterna=="img/home/salud/default.jpg"){
				\Storage::disk('saludhome')->put($dos,  \File::get($inputs["file_two"]));
				$dos = ("img/home/salud/".$dos);
			}else{
				$rutatwo = "img/historial/saludhome/";
				if(file_exists($isalud[0]->salud_pathinterna)){
					$imagen = substr($isalud[0]->salud_pathinterna,strripos($isalud[0]->salud_pathinterna,"/")+1);
					$rutatwo .= $imagen;
					if(copy($isalud[0]->salud_pathinterna,$rutatwo)){
						unlink($isalud[0]->salud_pathinterna);
					}
					
					$historialtwo = 1;
				}

				\Storage::disk('saludhome')->put($dos,  \File::get($inputs["file_two"]));
				$dos = ("img/home/salud/".$dos);
			}
		}else{
			$rutatwo = "img/historial/saludhome/";
			$imagen = substr($isalud[0]->salud_pathinterna,strripos($isalud[0]->salud_pathinterna,"/")+1);
			$rutatwo .= $imagen; copy($isalud[0]->salud_pathinterna,$rutatwo);
			
			$dos = $isalud[0]->salud_pathinterna;
		}
				
		if($historialone || $historialtwo){
			ISalud::where("id","1")->update([
				"salud_titulo" => $titulo,
				"salud_situlo" => $subtitulo,
				"salud_path" => $uno,
				"salud_pathinterna" => $dos,
				"salud_estado" => "0",
			]);
			
			ISalud::create([
				"salud_titulo" => $titulo,
				"salud_situlo" => $subtitulo,
				"salud_path" => $rutaone,
				"salud_pathinterna" => $rutatwo,
				"salud_estado" => "1",
				"user_id" => "1"
			]);
			
			return 1;
		}else if(!$historialone && !$historialtwo){
			ISalud::where("id","1")->update([
				"salud_titulo" => $titulo,
				"salud_situlo" => $subtitulo,
				"salud_path" => $uno,
				"salud_pathinterna" => $dos,
				"salud_estado" => "0",
			]);
			
			return 1;
		}return 0;
	}
	
	//AÑADO EL BLOQUE DE SALUD
	public static function addbannerinferior($inputs){
		$date = Carbon::now(-4);
		$isalud = ISalud::all();
		$rutaone = ""; $rutatwo = "";
		if(array_key_exists("file_one",$inputs) && array_key_exists("file_two",$inputs)){
			if(array_key_exists("titulo_salud",$inputs) && array_key_exists("subtitulo_salud",$inputs)){
				if(array_key_exists("file_one",$inputs)){//BANNER DE SALUD
					$name = str_random(4).$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$uno = (str_replace($search, $replace, $name));
					if($isalud[0]->salud_path=="img/home/salud/default.jpg"){
						\Storage::disk('saludhome')->put($uno,  \File::get($inputs["file_one"]));
						$rutaone = ("img/home/salud/".$uno);
					}
					
					ISalud::where("id","1")->update([
						"salud_titulo" => $inputs["titulo_salud"],
						"salud_situlo" => $inputs["subtitulo_salud"],
						"salud_path" => $rutaone,
						"salud_estado" => "0"
					]);
					
					$uno = "1";
				}
				
				if(array_key_exists("file_two",$inputs)){//IMAGEN EN LA MODAL
					$name = str_random(4).$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_two"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$dos = (str_replace($search, $replace, $name));
					if($isalud[0]->salud_pathinterna=="img/home/salud/default.jpg"){
						\Storage::disk('saludhome')->put($dos,  \File::get($inputs["file_two"]));
						$rutatwo = ("img/home/salud/".$dos);
					}
					
					ISalud::where("id","1")->update([
						"salud_titulo" => $inputs["titulo_salud"],
						"salud_situlo" => $inputs["subtitulo_salud"],
						"salud_pathinterna" => $rutatwo,
						"salud_estado" => "0"
					]);
					
					$dos = "1";
				}
				
				if($uno=="1" && $dos=="1"){
					return 1;
				}
			}
		}return 0;
	}
}
