<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Eventos extends Model
{
    protected $table = "eventos";

	protected $fillable = [
		"event_title",
		"event_text",
		"event_ppal",
		"event_type",
		"event_dateini",
		"event_horaini",
		"event_datefin",
		"event_href",
		"event_path"
	];
	//OBTENER TODOS LOS EVENTOS CORPORATIVOS
	public static function get_corporativosAll(){
		return  DB::table('eventos')->where("event_type","1")->get();
	}
	//OBTENER TODOS DEL MES INDICADO Y AÑO ACTUAL
	public static function get_eventosAll($date){
		return  DB::table('eventos')->where("event_type","0")->whereMonth('event_dateini',$date['mes'])->whereYear('event_dateini',$date['year'])->orderBy('event_dateini','asc')->get();
	}
	//OBTENER EVENTO ID
	public static function get_Evento($request){
		return Eventos::where("id",base64_decode($request['evento']))->first();
	}
	//EDIT PRINCIPAL EVENTO
	public static function ppalevento($request){
		$evento = Eventos::where("id",base64_decode($request['evento']))->first();
		if(count($evento)==1){
			$date = Carbon::parse($evento['event_dateini']);
			if($evento->event_ppal == "0"){
				$ppal = Eventos::where("event_type","0")->whereMonth('event_dateini',$date->month)->whereYear('event_dateini',$date->year)->where('event_ppal','1')->first();
				if(count($ppal)==1){
					$ppal->update([
						"event_ppal" => "0"
					]);
				}
				$evento->update([
					"event_ppal" => "1"
				]);
				return 1;
			}else if($evento->event_ppal == "1"){
				$evento->update([
					"event_ppal" => "0"
				]);
				return 0;
			}
			return 2;
		}else{
			return 2;
		}
	}	
	
	private static function construirHour($hora,$minutos){
		$isTime = "";
		switch($hora){
			case "01":
				$isTime = "01".":".$minutos." A.M.";
			break;
			case "02":
				$isTime = "02".":".$minutos." A.M.";
			break;
			case "03":
				$isTime = "03".":".$minutos." A.M.";
			break;
			case "04":
				$isTime = "04".":".$minutos." A.M.";
			break;
			case "05":
				$isTime = "05".":".$minutos." A.M.";
			break;
			case "06":
				$isTime = "06".":".$minutos." A.M.";
			break;
			case "07":
				$isTime = "07".":".$minutos." A.M.";
			break;
			case "08":
				$isTime = "08".":".$minutos." A.M.";
			break;
			case "09":
				$isTime = "09".":".$minutos." A.M.";
			break;
			case "10":
				$isTime = "10".":".$minutos." A.M.";
			break;
			case "11":
				$isTime = "11".":".$minutos." A.M.";
			break;
			case "12":
				$isTime = "12".":".$minutos." M.";
			break;
			case "13":
				$isTime = "01".":".$minutos." P.M.";
			break;
			case "14":
				$isTime = "02".":".$minutos." P.M.";
			break;
			case "15":
				$isTime = "03".":".$minutos." P.M.";
			break;
			case "16":
				$isTime = "04".":".$minutos." P.M.";
			break;
			case "17":
				$isTime = "05".":".$minutos." P.M.";
			break;
			case "18":
				$isTime = "06".":".$minutos." P.M.";
			break;
			case "19":
				$isTime = "07".":".$minutos." P.M.";
			break;
			case "20":
				$isTime = "08".":".$minutos." P.M.";
			break;
			case "21":
				$isTime = "09".":".$minutos." P.M.";
			break;
			case "22":
				$isTime = "10".":".$minutos." P.M.";
			break;
			case "23":
				$isTime = "11".":".$minutos." P.M.";
			break;
			case "00":
				$isTime = "12".":".$minutos." A.M.";
			break;
		}
		
		return $isTime;
	}
	
	//STORE
	public static function addeventoshome($request){
		$date = Carbon::now(-4);
		$urleven = $hora = $one = "";
		if(array_key_exists("tipoevento",$request) && array_key_exists("titulo_evento",$request) && array_key_exists("descripcion_evento",$request) && array_key_exists("file_one",$request) && array_key_exists("inicio_evento",$request) && array_key_exists("fin_evento",$request)){
			if($request["tipoevento"] == "0"){	
				$hora = Eventos::construirHour(explode(":",explode(" ",$request["inicio_evento"])[1])[0],explode(":",explode(" ",$request["inicio_evento"])[1])[1]);
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["file_one"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$one = (str_replace($search, $replace, $name));
				if(array_key_exists('link',$request) && !is_null($request['link'])){
					$urleven = $request['link'];
				}else{
					if(array_key_exists('habitaciones',$request)){
						$urleven = $request['habitaciones'];
					}
					if(array_key_exists('restaurante',$request)){
						$urleven = $request['restaurante'];
					}
				}
				\Storage::disk('eventoshome')->put($one,  \File::get($request["file_one"]));
				Eventos::create([
					"event_title" => $request["titulo_evento"],
					"event_text" => $request["descripcion_evento"],
					"event_ppal" => "0",
					"event_type" => $request["tipoevento"],
					"event_dateini" => $request["inicio_evento"],
					"event_horaini" => $hora,
					"event_datefin" => $request["fin_evento"],
					"event_href" => $urleven,
					"event_path" => $one
				]);
				return 1;
			}else{
				return 0;
			}
		}else{
			if(array_key_exists("tipoevento",$request) && $request["tipoevento"] == "1"){
				Eventos::create([
					"event_title" => $request["titulo_evento"],
					"event_text" => $request["descripcion_evento"],
					"event_ppal" => "1",//activo
					"event_type" => $request["tipoevento"],
					"event_dateini" => "",
					"event_horaini" => "",
					"event_datefin" => "",
					"event_href" => "",
					"event_path" => ""
				]);
				return 1;
			}
			return 0;
		}
	}
	//EDIT
	public static function editadoeventos_home($request){
		$date = Carbon::now(-4);
		$urleven = "";
		if(array_key_exists("titulo_evento",$request) && array_key_exists("descripcion_evento",$request) && array_key_exists("inicio_evento",$request) && array_key_exists("fin_evento",$request)){
			$evento = Eventos::where("id",base64_decode($request['evento']))->first();
			$hora = Eventos::construirHour(explode(":",explode(" ",$request["inicio_evento"])[1])[0],explode(":",explode(" ",$request["inicio_evento"])[1])[1]);
			if(count($evento)==1){
				$urleven = $evento->event_href;
				if(array_key_exists("file_one",$request)){//cambio la imagen
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["file_one"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$one = (str_replace($search, $replace, $name));
					if(array_key_exists('link',$request) && !is_null($request['link'])){
						$urleven = $request['link'];
					}else{
						if(array_key_exists('habitaciones',$request)){
							$urleven = $request['habitaciones'];
						}
						if(array_key_exists('restaurante',$request)){
							$urleven = $request['restaurante'];
						}
					}
					\Storage::disk('eventoshome')->put($one,  \File::get($request["file_one"]));//nueva imagen
					\Storage::disk('eventoshome')->delete($evento->event_path);//borro imagen vieja
					$evento->update([
						"event_title" => $request["titulo_evento"],
						"event_text" => $request["descripcion_evento"],
						"event_ppal" => "0",
						"event_dateini" => $request["inicio_evento"],
						"event_horaini" => $hora,
						"event_datefin" => $request["fin_evento"],
						"event_href" => $urleven,
						"event_path" => $one
					]);
				}else{
					if(array_key_exists('link',$request) && !is_null($request['link'])){
						$urleven = $request['link'];
					}else{
						if(array_key_exists('habitaciones',$request)){
							$urleven = $request['habitaciones'];
						}
						if(array_key_exists('restaurante',$request)){
							$urleven = $request['restaurante'];
						}
					}
					$evento->update([
						"event_title" => $request["titulo_evento"],
						"event_text" => $request["descripcion_evento"],
						"event_ppal" => "0",
						"event_dateini" => $request["inicio_evento"],
						"event_horaini" => $hora,
						"event_href" => $urleven,
						"event_datefin" => $request["fin_evento"]
					]);
				}
				return 1;
			}
			return 0;
		}else{
			return 0;
		}
	}
	
	//ERASE
	public static function eraseventohome($request){
		$evento = Eventos::where("id",base64_decode($request['evento']))->first();
		if(count($evento)==1){
			\Storage::disk('eventoshome')->delete($evento->event_path);//borro imagen vieja
			$evento->delete();
			return 1;
		}			
		return 0;
	}
	//CLONE
	public static function clonevento_home($request){
		$date = Carbon::now(-4);
		if(array_key_exists("evento",$request) && array_key_exists("inicio",$request) && array_key_exists("fin",$request)){
			$base = Eventos::get_Evento(array('evento' => $request["evento"]));
			$hora = Eventos::construirHour(explode(":",explode(" ",$request["inicio"])[1])[0],explode(":",explode(" ",$request["inicio"])[1])[1]);
			$clon_img = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second."clon".$base['event_path'];
			\Storage::disk('eventoshome')->copy($base['event_path'],$clon_img);
			Eventos::create([
				"event_title" => $base['event_title'],
				"event_text" => $base['event_text'],
				"event_ppal" => "0",
				"event_type" => $base['event_type'],
				"event_dateini" => $request["inicio"],
				"event_horaini" => $hora,
				"event_datefin" => $request["fin"],
				"event_href" => $base['event_href'],
				"event_path" => $clon_img
			]);
			return 1;
		}else{
			return 0;
		}		
	}
	//EDIT CORPORATIVO
	public static function editadoeventos_corporativo ($request){
		if(array_key_exists("evento",$request) && array_key_exists("titulo_evento",$request) && array_key_exists("descripcion_evento",$request)){
			$evento = Eventos::where("id",base64_decode($request['evento']))->first();
			if(count($evento)==1){
				$evento->update([
					"event_title" => $request["titulo_evento"],
					"event_text" => $request["descripcion_evento"],
				]);
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	//ACTIVAR / DESACTIVAR  EVENTO CORPORATIVO
	public static function actv_corporativo($request){
		$evento = Eventos::where("id",base64_decode($request['evento']))->first();
		if(count($evento)==1){
			if($evento->event_ppal == "0"){
				$evento->update([
					"event_ppal" => "1"
				]);
				return 1;
			}else if($evento->event_ppal == "1"){
				$evento->update([
					"event_ppal" => "0"
				]);
				return 0;
			}
			return 2;
		}else{
			return 2;
		}
	}
}
