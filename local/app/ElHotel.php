<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ElHotel extends Model
{
    protected $table = "elhotel";
	
	protected $fillable = [
		'elhotel_title',
		'elhotel_texto',
		'elhotel_picone',
		'elhotel_pictwo',
		'elhotel_picthrid'
	];

	protected $hidden = [
      'remember_token',
	];
	
	public static function obtenerHistoria(){
		return ElHotel::first()->toJson();
	}
	
	public static function addelhotel($inputs){
		$finder = json_decode(ElHotel::obtenerHistoria());
		$date = Carbon::now(-4);
		
		$titulo = ""; $d1 = ""; $d2 = ""; $uno = ""; $dos = ""; $tres = "";
		if(array_key_exists("titulo_corporativos",$inputs) && $inputs["titulo_corporativos"]!=""){
			$titulo = $inputs["titulo_corporativos"];
		}else{
			$titulo = $finder->elhotel_title;
		}
		
		if(array_key_exists("descripcion_evento1",$inputs) && $inputs["descripcion_evento1"]!=""){
			$d1 = $inputs["descripcion_evento1"];
		}else{
			$d1 = $finder->elhotel_texto;
		}
			
		if(array_key_exists("file_one",$inputs) && $inputs["file_one"]!=""){//1
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_one"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$uno = (str_replace($search, $replace, $name));
			if($finder->elhotel_picone!="'img/elhotel/historia/hotel_proyectos.jpg")
				\Storage::disk('eventoscorporativos')->delete($finder->elhotel_picone);
			\Storage::disk('historia')->put($uno,  \File::get($inputs["file_one"]));
			$uno = ('img/elhotel/historia/'.$uno);
		}else{
			$uno = $finder->elhotel_picone;
		}
		
		if(array_key_exists("file_two",$inputs) && $inputs["file_two"]!=""){//2
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_two"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$dos = (str_replace($search, $replace, $name));
			if($finder->elhotel_pictwo!="'img/elhotel/historia/hotel_galeriainstalaciones.jpg")
				\Storage::disk('eventoscorporativos')->delete($finder->elhotel_pictwo);
			\Storage::disk('historia')->put($dos,  \File::get($inputs["file_two"]));
			$dos = ('img/elhotel/historia/'.$dos);
		}else{
			$dos = $finder->elhotel_pictwo;
		}
		
		if(array_key_exists("file_thrid",$inputs) && $inputs["file_thrid"]!=""){//3
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_thrid"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$tres = (str_replace($search, $replace, $name));
			if($finder->elhotel_picthrid!="'img/elhotel/historia/hotel_galeriaeventos.jpg")
				\Storage::disk('historia')->delete($finder->elhotel_picthrid);
			\Storage::disk('historia')->put($tres,  \File::get($inputs["file_thrid"]));
			$tres = ('img/elhotel/historia/'.$tres);
		}else{
			$tres = $finder->elhotel_picthrid;
		}
		
		ElHotel::where("id","1")->update([
			'elhotel_title' =>$titulo,
			'elhotel_texto' => $d1,
			'elhotel_picone' => $uno,
			'elhotel_pictwo' => $dos,
			'elhotel_picthrid' => $tres
		]);
		
		return 1;
	}
}
