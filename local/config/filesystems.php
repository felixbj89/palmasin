<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
		
		'galeriahome' => [
            'driver' => 'local',
            'root' => 'img/home/galeria/',
            'visibility' => 'public',
        ],
		
		'proyectos' => [
            'driver' => 'local',
            'root' => 'img/elhotel/centroempresarial/',
            'visibility' => 'public',
        ],
		
		'historia' => [
            'driver' => 'local',
            'root' => 'img/elhotel/historia/',
            'visibility' => 'public',
        ],
		
		'eventoscorporativos' => [
            'driver' => 'local',
            'root' => 'img/eventoscorporativos/',
            'visibility' => 'public',
        ],
		
		'comercialhome' => [
            'driver' => 'local',
            'root' => 'img/elhotel/comerciales/',
            'visibility' => 'public',
        ],
		
		'saludhome' => [
            'driver' => 'local',
            'root' => 'img/home/salud/',
            'visibility' => 'public',
        ],
		
		'avatarhome' => [
            'driver' => 'local',
            'root' => 'img/avatares/',
            'visibility' => 'public',
        ],
		
		'promohome' => [
            'driver' => 'local',
            'root' => 'img/home/promociones/',
            'visibility' => 'public',
        ],
		
		'historialhome' => [
            'driver' => 'local',
            'root' => 'img/',
            'visibility' => 'public',
        ],
		
        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],
		
		'eventoshome' => [
            'driver' => 'local',
            'root' => 'img/home/eventos/',
            'visibility' => 'public',
        ],

		'habitad' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/',
            'visibility' => 'public',
        ],

		'habitadgaleri' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/galeria',
            'visibility' => 'public',
        ],
		
		'restaurante' => [
            'driver' => 'local',
            'root' => 'img/restaurantes/',
            'visibility' => 'public',
        ],
		
		'restauratgaleri' => [
            'driver' => 'local',
            'root' => 'img/restaurantes/galeria/',
            'visibility' => 'public',
        ],
		
		'elhotel' => [
            'driver' => 'local',
            'root' => 'img/elhotel/',
            'visibility' => 'public',
        ],
		
		'instalaciones' => [
            'driver' => 'local',
            'root' => 'img/elhotel/instalaciones/',
            'visibility' => 'public',
        ],
		
		'secundaria' => [
            'driver' => 'local',
            'root' => 'img/home/secundaria/',
            'visibility' => 'public',
        ],
    ],

];
