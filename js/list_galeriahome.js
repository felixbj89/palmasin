$(document).ready(function(){
	var tablaHistorial = $("#historial_list").DataTable({
	"searching": true,
	"bInfo": false,
	"Info":false,
	"bLengthChange": false,
	"iDisplayLength": 1,
	"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
	},

	"fixedHeader": {
		"header": true,
		"footer": true
	},
	"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	if(tablaHistorial.rows().data().length==0){
		$("#modal_empty").modal("show");
	}
	
	if($("#server_asociacionlist").val()!=undefined){
		$("#asociaciongaleriaadvertencia").text($("#server_asociacionlist").val());
		$("#modal_alerta").modal("show");
	}
	
	var seleccion = undefined; var historial = undefined;
	$(document).on("click","#btnActivar",function(){//ACTIVA UNA IMAGEN DEL HISTORIAL
		var historial = $(this).parents("tr").data("historial");
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$.ajax({
			url: url + "admin/estadogaleriahome",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"historial_id":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
					$("#imagen" + respuesta.id).attr("src",respuesta.imagen);
				}else{
					$("#promoadvertencia").text("No habido cambios en su galería.");
					$("#modal_alerta").modal("show");
				}
			}
		});
	});
	
	$("#btnAceptar").on("click",function(){//BORRA UNA IMAGEN DEL HISTORIAL DE LA GALERÍA
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$("#modal_confirmarremover").modal("hide");
		$.ajax({
			url: url + "admin/delete_galeria",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"historial":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				tablaHistorial.row(seleccion).remove().draw();
				if(tablaHistorial.rows().data().length==0){
					$("#modal_empty").modal("show");
				}
			}
		});
	});
	
	$("#btnAceptarRemover").on("click",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$("#modal_confirmarremoverasoacion").modal("hide");
		$.ajax({
			url: url + "admin/delete_asociacion",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"historial":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
					tablaHistorial.row(seleccion).remove().draw();
					if(tablaHistorial.rows().data().length==0){
						$("#modal_empty").modal("show");
					}
				}else if(respuesta.estado==2){
					$("#asociaciongaleriaadvertencia").text("La promoción no puede estar activa, intente nuevamente.");
					$("#modal_alerta").modal("show");
				}
			}
		});
	});
	
	//LOS REMOVE, PREGUNTO SI EN VERDAD SE QUIERE ELIMINAR
	$(document).on("click","#btnRemover",function(){
		$("#confirmarmensajeadvertencia").text("¿Esta usted seguro de remover la imagen seleccionada?");
		$("#modal_confirmarremover").modal("show");
		seleccion = $(this).parents("tr");
		historial = $(this).parents("tr").data("historial");
	});
	
	$(document).on("click","#btnRemoverAsociacionesRealizadas",function(){
		$("#confirmarmensajeremoverasociacion").text("¿Esta usted seguro de remover la asociación seleccionada?");
		$("#modal_confirmarremoverasoacion").modal("show");
		seleccion = $(this).parents("tr");
		historial = $(this).parents("tr").data("historial");
	});
	
	$(document).on("click","#btnAsociadosHechas",function(){//VEO MI IMAGEN Y LAS ASOCIACIONES POSIBLES
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var asociar = $(this).parents("tr").data("historial");
		$("#modal_confirmarremover").modal("hide");
		$.ajax({
			url: url + "admin/viewasociacion",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"historial":asociar["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);
			}
		});
	});

	$('html, body').animate({scrollTop: (0)}, 1500);
});