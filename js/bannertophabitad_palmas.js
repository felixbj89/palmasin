var pic1 = undefined;
function openFile1(event){
  pic1 = event;
}
$(document).ready(function(){
	var _URL = window.URL || window.webkitURL;
	$("#file_one").on("change",function(){//CARGO LA PRIMERA IMAGEN
		if(pic1!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#roomsmessageadvertencia").text(text);
					$("#modal_alerta").modal("show");
				};
				img.src = _URL.createObjectURL(file);
				$("#banner_bottom").attr("src",img.src);
			}
		}
	});
	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#roomsmessageadvertencia").text(text);
				$("#modal_alerta").modal("show");
				
				$("#banner_bottom").attr("src",$("#url1").val());
			},
			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				pic1 = undefined;
				$("#btnRemoverPicture").on("click",function(){
					callback();
					pic1 = undefined;
					//$("#banner_bottom").attr("src",$("#url1").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});	
});
$('#btnGuardar').on('click',function(){
	if(comprobar_longitud($('#file_one').val())){
		$("#roomsmessageadvertencia").text("El campo es obligatorio, ingrese una imagen válida.");
		$("#modal_alerta").modal("show");
	}else{
		var fd = new FormData();
		var url = window.location.href.substr(0,window.location.href.search("admin"));		
		fd.append('file',$( '#file_one' )[0].files[0]);
		fd.append('section','2');
		$.ajax({
			url: url + "admin/bannersection",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},			
			data: fd,
			processData: false,
			contentType: false,
			type: 'POST',
			beforeSend:function(res){
				$("#modal_wait").modal("show");
			},				
			success: function(res){
				$("#modal_wait").modal("hide");
				if(res.estado == 1){
					$('#form-habitad input[type=file]').prop('jFiler').reset();
					pic1 = undefined;	
					$("#roomsmessageadvertencia").text("Los cambios han sido guardados satisfactoriamente.");
					$("#modal_alerta").modal("show");
				}else{
					$("#roomsmessageadvertencia").text("Algo salio mal no se pudo reguistrar el cambio.");
					$("#modal_alerta").modal("show");
				}
			}
		});
	}
});