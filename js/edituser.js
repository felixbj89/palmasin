$(document).ready(function(){
	
	count_chart($("#nombreusers"),"input","#cantidad-nombre");
	load_count_limite($("#nombreusers"),"#cantidad-nombre","20");
	limit_chart($("#nombreusers"),"keypress",19);
	
	count_chart_limite($("#emailusers"),"input","#cantidad-email","30");
	load_count_limite($("#emailusers"),"#cantidad-email","30");
	limit_chart($("#emailusers"),"keypress",29);
	
	count_chart($("#loginusers"),"input","#cantidad-login");
	load_count_limite($("#loginusers"),"#cantidad-login","20");
	limit_chart($("#loginusers"),"keypress",19);
	
	count_chart($("#passwordusers"),"input","#cantidad-password");
	load_count_limite($("#passwordusers"),"#cantidad-password","20");
	limit_chart($("#passwordusers"),"keypress",19);
	
	count_chart($("#confirmarusers"),"input","#cantidad-confirmar");
	load_count_limite($("#confirmarusers"),"#cantidad-confirmar","20");
	limit_chart($("#confirmarusers"),"keypress",19);
	
	$("#btnModificarUsers").on("click",function(){
		if($("#passwordusers").val()!=$("#confirmarusers").val()){
			$("#listusersadvertencia").text("La campos de contraseña no coinciden.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-editusers").submit();
		}
	});
	
	$(document).on("click","#btnVerpassword",function(){
		$("#passwordusers").attr("type","text");
		$("#confirmarusers").attr("type","text");
		$("#icobtn").attr("class","fa fa-eye");
		$("#btnVerpassword").attr("id","btnOcultarPassword");
	}); 
	
	
	$(document).on("click","#btnOcultarPassword",function(){
		$("#passwordusers").attr("type","password");
		$("#confirmarusers").attr("type","password");
		$("#icobtn").attr("class","fa fa-eye-slash");
		$("#btnOcultarPassword").attr("id","btnVerpassword");
	}); 
});