$(document).ready(function(){
	var tablaHistorial = $("#roomscreados_list").DataTable({
		"searching": true,
		"bInfo": false,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 1,
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
			"header": true,
			"footer": true
		},
		"bPaginate": true,
			"responsive":true,
			"bAutoWidth": false,
	});
	
	if(tablaHistorial.rows().data().length==0){
		$("#modal_empty").modal("show");
	}
	
	$(document).on('click','.btnModificar',function(){
		var elemen = this;
		var habitad = $(elemen).closest('tr').data('habitad')
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$.ajax({
			url: url + "admin/edithabitad",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"target":habitad["habitacion"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);				
			},error:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#mensajeadvertencia").text("Algo salio mal. No se pude modificar la habitación.");
				$("#modal_alerta").modal("show")
			}
		});
	});
	
	$(document).on('click','.btnRemover',function(){
		var elemen = this;
		var habitad = $(elemen).closest('tr').data('habitad')
		var table = $('#roomscreados_list').DataTable();
		var tr = $(elemen).closest('tr');
		var seleccion = $(this).parents("tr");
		
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$("#confirmarmensajeadvertencia").text('Quieres borrar esta Habitación? ');
		$("#modal_confirmarremover").modal("show");
		$("#btnAceptar").on("click",function(){
			$("#modal_confirmarremover").modal("hide");
			$.ajax({
				url: url + "admin/erasehabitad",
				headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:"post",
				data:{
					"target":habitad["habitacion"],
				},
				beforeSend:function(respuesta){
					$("#modal_wait").modal("show");
				},
				success:function(respuesta){
					if(respuesta.estado==1){
						table.row(seleccion).remove().draw();
						if(table.rows().data().length==0){
							$("#modal_empty").modal("show");
						}
						
						$("#modal_wait").modal("hide");
					}else{
						$("#modal_wait").modal("hide");
						$("#mensajeadvertencia").text("Algo salio mal. No se pudo borrar la habitación.");
						$("#modal_alerta").modal("show")
					}
				},error:function(respuesta){
					$("#modal_wait").modal("hide");
					$("#mensajeadvertencia").text("Algo salio mal. No se pudo borrar la habitación.");
					$("#modal_alerta").modal("show")
				}
			});
		});
	});
});