$(document).ready(function(){
	var tablaHistorial = $("#event_list").DataTable({
	"searching": true,
	"bInfo": false,
	"Info":false,
	"bLengthChange": false,
	"iDisplayLength": true,
	"order": false,
	"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
	},

	"fixedHeader": {
	"header": true,
	"footer": true
	},
	"bPaginate": false,
	"responsive":true,
	"bAutoWidth": false,
	});
	
	if(tablaHistorial.rows().data().length==0){
		$("#modal_empty").modal("show");
	}
	if($("#server").val()!=undefined){
		$("#mesesadvertencia").text($("#server").val());
		$("#modal_alerta").modal("show");
	}

	$('html, body').animate({scrollTop: (0)}, 1500);
});
$('table').on('click','.btnActivar',function(){
	var elemen = this;
	var elementos = $(".btnActivar i.fa-toggle-on").size();
	var historial = $(elemen).closest('tr').data('historial');
	var event = $(elemen).closest('tr').data('evemes');
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	if(event > 0){
		$.ajax({
			url: url + "admin/lanzarmesevetoshome",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"evento":historial["evento"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				if(respuesta.estado==1){
					if($(elemen).children('i').hasClass('fa-toggle-on')){
						$(elemen).children('i').removeClass('fa-toggle-on');
						$(elemen).children('i').addClass('fa-toggle-off');
						$("#modal_wait").modal("hide");
					}else{
						$(elemen).children('i').removeClass('fa-toggle-off');
						$(elemen).children('i').addClass('fa-toggle-on');
						$("#modal_wait").modal("hide");							
					}
				}
				if(respuesta.estado==0){
					$("#modal_wait").modal("hide");
					$("#mesesadvertencia").text("Algo salio mal. No se pudo registrar el cambio.");
					$("#modal_alerta").modal("show");
				}
				if(respuesta.estado==2){
					$("#modal_wait").modal("hide");
					$("#mesesadvertencia").text("No puedes lanzar mas de tres (3) mes.");
					$("#modal_alerta").modal("show");
				}
			},error:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#mesesadvertencia").text("Algo salio mal. No se pudo registrar el cambio.");
				$("#modal_alerta").modal("show");
			}
		});
	}else{
		$("#mesesadvertencia").text("No puedes lanzar este mes. Este mes no tiene eventos registrados");
		$("#modal_alerta").modal("show");
	}
});