jQuery(function ($) {
	$(document).ready(function (){
		if(target == "instalaciones" || target == "eventoscorporativos" || target == "proyectos" || target == "comerciales" || target == "centroempresarial" || target == "ubicacion" || target == "contacto"){
			$('html, body').animate({scrollTop: ($('#'+target).offset().top)-10}, 1500);
		}	
		$('#carousel-example-generic.fdi-Carousel .item').each(function () {
			var next = $(this).next();
			if (!next.length) {
				next = $(this).siblings(':first');
			}
			next.children(':first-child').clone().appendTo($(this));

			if (next.next().length > 0) {
				next.next().children(':first-child').clone().appendTo($(this));
			}
			else {
				$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
			}
		});

		$('.galeriahistoria_').hover(
			function(){
				var element = $(this).find('.galeriahistoria_over');
				$(element).animate({'top': '0px'},0);
				$(element).animate({'opacity':'1','filter':'alpha(opacity=1)'},400);
			},
			function(){
				$('.galeriahistoria_over').animate({'opacity':'0','filter':'alpha(opacity=0)'},400);
				$('.galeriahistoria_over').animate({'top': '100%'},0);
			}
		);
		
	});	
	$('#galevent').on('click',function(){
		$('#modalEventospalmas').modal('hide');
		//ajax galeria de eventos
			var url = window.location.href.substr(0,window.location.href.search("elhotel"));
			url += 'elhotel/corporativos';
			$.ajax({
				type: "POST",
				url: url,
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(res){
					$('#LightBox-indicators').html(res.indicators);
					$('#BootstrapCarouse-innerlLightBox').html(res.items);
					$('#modalGaleriapalmas').modal('show');
				},
				error: function(res){
				}
			})
			return false;
	});
	$('#galinstalaciones').on('click',function(){
		//ajax galeria instalaciones
		var url = window.location.href.substr(0,window.location.href.search("elhotel"));
		url += 'elhotel/instalaciones';
		$.ajax({
			type: "POST",
			url: url,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function(res){
				$('#LightBox-indicators').html(res.indicators);
				$('#BootstrapCarouse-innerlLightBox').html(res.items);
				$('#modalGaleriapalmas').modal('show');
			},
			error: function(res){
			}
		})
		return false;
		$('#modalGaleriapalmas').modal('show');
	});
	//Validar formulariomodal eventos reservar corporarivos
	$("#modalPromocionespalmas").on('hidden.bs.modal',function(){
		$('#form-contactuspromo input,#form-contactuspromo textarea').val('');
		$('span.text-danger.pull-right').hide();
	});
	$("#promo-telefono,#telefono").mask("+99-999-9999999");
	$("#promo-nombre,#promo-apellido,#nomre,#apellido").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			if (!regex.test(key) || ($(this).val()).length > 50) {
				event.preventDefault();
				return false;
			}
		}
	});
	$("#promo-telefono,#telefono").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[0-9]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			if (!regex.test(key) || ($(this).val()).length > 14) {
				event.preventDefault();
				return false;
			}
		}
	});
	$("#promo-correo,#email").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-z0-9@_.-]$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			if (!regex.test(key) || ($(this).val()).length > 100) {
				event.preventDefault();
				return false;
			}
		}
	});
	
	$('#form-contactuspromo input,#form-contactuspromo textarea,#contactoelhotel input,#contactoelhotel textarea').bind('focusin focusout keyup',function(eventObject){
		if(!$(this).val() || ($(this).val().length<2)){
			$(this).addClass('invalid');
			$(this).removeClass('valid');
			$(this).siblings('.requerido').show();
		}else{
			$(this).removeClass('invalid');
			$(this).addClass('valid');
			$(this).siblings('.requerido').hide();
		}
	});
	$("#promo-telefono,#telefono").bind('focusout keyup',function(eventObject){
		var expresion = /^(\+[0-9]{2})+(\-[0-9]{3})+(\-[0-9]{7})+$/i;				
		if(!expresion.test($(this).val())){
			$(this).addClass('invalid');
			$(this).removeClass('valid');
			if($(this).val().length == 0){
				$(this).siblings('.requerido').show();
				$(this).siblings('.invalido').hide();
			}else{
				$(this).siblings('.requerido').hide();
				$(this).siblings('.invalido').show();						
			}
		}else{
			$(this).removeClass('invalid');
			$(this).addClass('valid');
			$(this).siblings('.invalido').hide();
		}
	});
	$("#promo-correo,#email").bind('focusout keyup',function(eventObject){
		var expresion = /^[a-z0-9_\.\-]+(.[a-z0-9_\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
		if(!expresion.test($(this).val())){
			$(this).addClass('invalid');
			$(this).removeClass('valid');
			if($(this).val().length == 0){
				$(this).siblings('.requerido').show();
				$(this).siblings('.invalido').hide();
			}else{
				$(this).siblings('.requerido').hide();
				$(this).siblings('.invalido').show();						
			}
		}else{
			$(this).removeClass('invalid');
			$(this).addClass('valid');
			$(this).siblings('.invalido').hide();
		}
	});
	$('#form-contactuspromo textarea,#contactoelhotel textarea').bind('focusout keyup',function(eventObject){
		if($(this).val().length < $(this).attr('minlength')) {
			$(this).addClass('invalid');
			$(this).removeClass('valid');
			if($(this).val().length == 0){
				$(this).siblings('.requerido').show();
				$(this).siblings('.minlength').hide();
			}else{
				$(this).siblings('.requerido').hide();
				$(this).siblings('.minlength').show();						
			}
		} else {
			$(this).removeClass('invalid');
			$(this).siblings('.minlength').hide();
			$(this).addClass('valid');
		}					
	});	
	
	$('#form-contactuspromo input,#form-contactuspromo textarea').on('keyup blur', function (){
		if ($('#promo-nombre').hasClass('valid') && $('#promo-apellido').hasClass('valid') && $('#promo-correo').hasClass('valid') && $('#promo-telefono').hasClass('valid') && $('#promo-Mensaje').hasClass('valid')){
			$('#form-contactuspromo #btn-envpromo').prop('disabled', false);        
		} else {
			$('#form-contactuspromo #btn-envpromo').prop('disabled', 'disabled');   
		}
	});

	/*
	$('#form-contactuspromo #btn-envpromo').click(function() {
		var fd = new FormData();
		fd.append('nombre',$('#promo-nombre').val());
		fd.append('apellido',$('#promo-apellido').val());
		fd.append('email',$('#promo-correo').val());
		fd.append('telefono',$('#promo-telefono').val());
		fd.append('mensaje',$('#promo-Mensaje').val());
		fd.append('promo',$('input[name="promoradio"]:checked').val());
		
		//var url = window.location.href;
		//var url = window.location.href.substr(0,window.location.href.search("admin"));
		//var url = window.location.href;
		//var url1 = window.location.href.substr(0,window.location.href.search("contacto"));		
		$.ajax({
			type: "POST",
			url:(url + "contacto/sendemails"),
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: fd,
			processData: false,
			contentType: false,
			beforeSend:function(res){
			},
			success: function(res){
				console.log(res);
				if(res.resultado == 1){
				}else{
				}
			},
			error: function(res){
			}
		})
		return false;
	});	*/
	
	$('#contactoelhotel input,#contactoelhotel textarea').on('keyup blur', function (){
		if ($('#nombre').hasClass('valid') && $('#apellido').hasClass('valid') && $('#email').hasClass('valid') && $('#telefono').hasClass('valid') && $('#mensaje').hasClass('valid')){
			$('#contactoelhotel #btn-enviar').prop('disabled', false);        
		} else {
			$('#contactoelhotel #btn-enviar').prop('disabled', 'disabled');   
		}
	});
	/*
	$('#contactoelhotel #btn-enviar').click(function() {
		var fd = new FormData();
		fd.append('nombre',$('#nombre').val());
		fd.append('apellido',$('#apellido').val());
		fd.append('email',$('#email').val());
		fd.append('telefono',$('#telefono').val());
		fd.append('mensaje',$('#mensaje').val());
		fd.append('promo',$('input[name="everadio"]:checked').val());
		
		//var url = window.location.href;
		//var url = window.location.href.substr(0,window.location.href.search("admin"));
		//var url = window.location.href;
		//var url1 = window.location.href.substr(0,window.location.href.search("contacto"));		
		$.ajax({
			type: "POST",
			url:(url + "contacto/sendemails"),
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: fd,
			processData: false,
			contentType: false,
			beforeSend:function(res){
			},
			success: function(res){
				console.log(res);
				if(res.resultado == 1){
				}else{
				}
			},
			error: function(res){
			}
		})
		return false;
	});	*/
	$(document).on('click', 'a.btn-scroll', function(event){
		event.preventDefault();
		$('html, body').animate({scrollTop: ($($.attr(this, 'href')).offset().top)-10}, 1500);
	});	
	$(document).on('change click','#contactoelhotel input[type="radio"]',function(){
		var mensaje = "Requiero / Solicitola evento: "+$('#contactoelhotel input[type="radio"]:checked').data('nombre')+". Descripcion: "+$('#contactoelhotel input[type="radio"]:checked').data('texto')+".";
		$('#contactoelhotel #mensaje').text(mensaje);
	});
	$('#reserevet').on('click',function(){
		$('#modalEventospalmas').modal('hide');
		setTimeout("$('html, body').animate({scrollTop: ($('#contacto').offset().top)-10}, 1500);",500);
	});
});