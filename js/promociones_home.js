var pic1 = undefined;

function openFile1(event){
  pic1 = event;
}

$(document).ready(function(){
	$("#inicio_promociones").flatpickr({
		"minDate":"today",
		"maxDate":new Date().fp_incr(90),
		enableTime: true,
		enableSeconds:true,
		time_24hr: false,
		noCalendar: false,
	});
	
	$("#fin_promociones").flatpickr({
		"minDate":"today",
		"maxDate":new Date().fp_incr(90),
		enableTime: true,
		enableSeconds:true,
		time_24hr: false,
		noCalendar: false,
	});
	
	$('.summernote').summernote({
	    lang: 'es-ES',
		toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['para', ['paragraph']],
			['height', ['height']]
		],
		callbacks: {
			onImageUpload: function(files) {
				// upload image to server and create imgNode...
				$summernote.summernote('insertNode', "");
			},
			onPaste: function (e) {
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
				e.preventDefault();
				document.execCommand('insertText', false, bufferText);
				
				var cleanText = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(cleanText.length > 0 && cleanText.length <= 300){
				  $("#cantidad-descripcion").text(cleanText.length + "/300");
				}else if(cleanText.length==0){
				  $("#cantidad-descripcion").text("0/330");
				}else if(cleanText.length > 300){
					$(".summernote").summernote("code",cleanText.substring(0,299));
				}				
				
			},
			onKeydown:function(e){
				var cleanText = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(e.currentTarget.innerText.length > 300){
					$("#promocionesadvertencia").text("Ha excedido el límite válido, en el campo descripción.");
					$("#cantidad-descripcion").attr("style","color:red !important");
					$("#cantidad-descripcion").text( cleanText.trim().length + "/300");
					
					$("#modal_alerta").modal("show");
				}else{
					$("#cantidad-descripcion").attr("style","color:white !important");
					$("#modal_alerta").modal("hide");
					$("#cantidad-descripcion").text( cleanText.trim().length + "/300");
				}
			}
		},
		placeholder: 'Ingrese la descripción de su promoción',
		height: 300,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
		focus: false 
	});
		
	if($("#server_promo").val()!=undefined){
		$("#promocionesadvertencia").text($("#server_promo").val());
		$("#modal_alerta").modal("show");
	}else if($("#error_promo").val()!=undefined){
		$("#promocionesadvertencia").text($("#error_promo").val());
		$("#modal_alerta").modal("show");
	}
	
	//INPUT-TITULO
	count_chart($("#titulo_promociones"),"input","#cantidad-titulo");
	load_count($("#titulo_promociones"),"#cantidad-titulo");
	
	//KEYPRESS-TITULO
	limit_chart($("#titulo_promociones"),"keypress",29);

	$('#titulo_promociones').on('change keypress paste',function(){
		var cleanText = $('#titulo_promociones').val();
		if(cleanText.length > 30){
			$("#titulo_promociones").val(cleanText.substring(0,29));
			$('#cantidad-titulo').text($("#titulo_promociones").val().length+" / 30");
		}
	});	
	
	//INPUT-DESCRIPCIÓN
	$('.summernote').on("summernote.change",function(){
		var cleanText = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
		if(cleanText.trim().length > 0 && cleanText.trim().length <= 300){
			$("#cantidad-descripcion").attr("style","color:white !important");
			$("#cantidad-descripcion").text(cleanText.trim().length + "/300");
		}else if(cleanText.trim().length==0){
			$("#cantidad-descripcion").text("0/300");
		}
	});

	if($('.summernote')[0].value.length > 0){		
		var cleanText = $('.summernote')[0].value.replace(/<\/?[^>]+(>|$)/g, "");
		if(cleanText.trim().length > 0 && cleanText.trim().length <= 300){
			$("#cantidad-descripcion").attr("style","color:white !important");
			$("#cantidad-descripcion").text(cleanText.trim().length + "/300");
		}else if(cleanText.trim().length==0){
			$("#cantidad-descripcion").text("0/300");
		}else{
			$("#promocionesadvertencia").text("Ha excedido el límite válido, en el campo descripción.");
			$("#cantidad-descripcion").attr("style","color:red !important");
			$("#cantidad-descripcion").text( cleanText.trim().length + "/300");
			
			$("#modal_alerta").modal("show");
		}
	}
	
	$("#btnRegistrar").on("click",function(){
		var descripcion = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
		if(descripcion.trim().length==0 && pic1==undefined && comprobar_longitud($("#fin_promociones").val()) && comprobar_longitud($("#inicio_promociones").val()) && $("#estado_promociones").val()=="NULL" && comprobar_longitud($("#titulo_promociones").val())){
			$("#promocionesadvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#titulo_promociones").val())){
			$("#promocionesadvertencia").text("El campo es obligatorio, ingrese un título válido.");
			$("#modal_alerta").modal("show");
		}else if($("#estado_promociones").val()=="NULL"){
			$("#promocionesadvertencia").text("El campo es obligatorio, ingrese un estado válido.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#inicio_promociones").val())){
			$("#promocionesadvertencia").text("El campo es obligatorio, ingrese una fecha de inicio válido.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#fin_promociones").val())){
			$("#promocionesadvertencia").text("El campo es obligatorio, ingrese una fecha de fin válido.");
			$("#modal_alerta").modal("show");
		}else if(pic1==undefined){
			$("#promocionesadvertencia").text("El campo es obligatorio, ingrese una imagen válida.");
			$("#modal_alerta").modal("show");
		}else if(descripcion.trim().length==0){
			$("#promocionesadvertencia").text("El campo es obligatorio, ingrese una descripción válida.");
			$("#modal_alerta").modal("show");
		}else if(descripcion.trim().length > 300){
			$("#promocionesadvertencia").text("El campo es obligatorio, ha excedido el límite válido.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-promociones").submit();
		}
	})
		
	$("#btnModificarPromo").on("click",function(){
		$("#form-promociones").submit();
	});
	
	$("#btnEditar").on("click",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$.ajax({
			url: url + "admin/viewgaleriahome",
			headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);
			}
		});
	});
	
	var _URL = window.URL || window.webkitURL;
	$("#file_one").on("change",function(){//CARGO LA PRIMERA IMAGEN
		if(pic1!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#promocionesadvertencia").text(text);
					$("#modal_alerta").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#uno_picture").attr("src",img.src);
			}
		}
	});
	
	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#promocionesadvertencia").text(text);
				$("#modal_alerta").modal("show");
				$("#uno_picture").attr("src",$("#url1").val());
			},
		
			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				pic1 = undefined;
				$("#btnRemoverPicture").on("click",function(){
					callback();
					pic1 =undefined;
					$("#uno_picture").attr("src",$("#url1").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
});