var pic1 = undefined;
var pic2 = undefined;

function openFile1(event){
  pic1 = event;
}

function openFile2(event){
  pic2 = event;
}

$(document).ready(function(){
	if($("#server_isalud").val()!=undefined){
		$("#saludadvertencia").text($("#server_isalud").val());
		$("#modal_alerta").modal("show");
	}else if($("#error_isalud").val()!=undefined){
		$("#saludadvertencia").text($("#error_isalud").val());
		$("#modal_alerta").modal("show");
	}

	$("#btnRegistrar").on("click",function(){
		if($("#titulo_salud").val().length==0 && $("#subtitulo_salud").val().length==0 && pic1==undefined && pic2==undefined){
			$("#saludadvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else if($("#titulo_salud").val().length==0){
			$("#saludadvertencia").text("El campo es obligatorio, ingrese un título válido.");
			$("#modal_alerta").modal("show");
		}else if($("#subtitulo_salud").val().length==0){
			$("#saludadvertencia").text("El campo es obligatorio, ingrese un sub-título válido.");
			$("#modal_alerta").modal("show");
		}else if(pic1==undefined || pic2==undefined){
			$("#saludadvertencia").text("Debe ingresar las imágenes de su salud al sistema.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-banner").submit();
		}
	})
	
	$("#btnEditarImagenSalud").on("click",function(){
		$("#form-banner").submit();
	});
	
	$("#btnEditar").on("click",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$.ajax({
			url: url + "admin/viewimagensalud",
			headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);
			}
		});
	});
	
	//INPUT-TITULO
	count_chart_limite($("#titulo_salud"),"input","#cantidad-tsalud","29");
	load_count_limite($("#titulo_salud"),"#cantidad-tsalud","29");
	
	//KEYPRESS-TITULO
	limit_chart($("#titulo_salud"),"keypress",28);
	
	//INPUT-SUB_TITULO
	count_chart_limite($("#subtitulo_salud"),"input","#cantidad-ssalud","80");
	load_count_limite($("#subtitulo_salud"),"#cantidad-ssalud","80");
	
	//KEYPRESS-SUB_TITULO
	limit_chart($("#subtitulo_salud"),"keypress",79);

	var _URL = window.URL || window.webkitURL;
	$("#file_one").on("change",function(){//CARGO LA PRIMERA IMAGEN
		console.log(pic1);
		if(pic1!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#saludadvertencia").text("Extensiones permitidas jpg y png.");
					$("#modal_alerta").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#uno_picture").attr("src",img.src);
			}
		}
	});

	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#saludadvertencia").text(text);
				$("#modal_alerta").modal("show");
				
				$("#uno_picture").attr("src",$("#url1").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback(); pic1 = undefined;
					$("#uno_picture").attr("src",$("#url1").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_two").on("change",function(){//CARGO LA SEGUNDA IMAGEN
		if(pic2!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#saludadvertencia").text("Extensiones permitidas jpg y png.");
					$("#modal_alerta").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#dos_picture").attr("src",img.src);
			}
		}
	});

	$("#file_two").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#saludadvertencia").text(text);
				$("#modal_alerta").modal("show");
				
				$("#dos_picture").attr("src",$("#url2").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback();  pic2 = undefined;
					$("#dos_picture").attr("src",$("#url2").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
});
