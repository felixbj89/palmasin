$(document).ready(function(){
	var _URL = window.URL || window.webkitURL;
	$("#file_one").on("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onerror = function() {
				$("#restadvertencia").text("Extensiones permitidas jpg, png");
				$("#modal_alerta").modal("show");
			};
			img.src = _URL.createObjectURL(file);
			$("#1_picture").attr("src",img.src);
		}
	});
	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#restadvertencia").text(text);
				$("#modal_alerta").modal("show");
			},
			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback();
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});	
	$('html, body').animate({scrollTop: (0)}, 0);
});

$('#btnGuardar').on('click',function(){
	if(comprobar_longitud($('#file_one').val())){
		$("#restadvertencia").text("El campo es obligatorio, ingrese una imagen válida.");
		$("#modal_alerta").modal("show");
	}else{
		var fd = new FormData();
		var url = window.location.href.substr(0,window.location.href.search("admin"));		
		fd.append('file',$( '#file_one' )[0].files[0]);
		fd.append('section','3');
		fd.append('id_',$('.selres.active').data('form'));
		$.ajax({
			url: url + "admin/imagesfija",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},			
			data: fd,
			processData: false,
			contentType: false,
			type: 'POST',
			beforeSend:function(res){
				$("#modal_wait").modal("show");
			},				
			success: function(res){
				$("#modal_wait").modal("hide");
				if(res.estado == 1){
					$('#form-habitad input[type=file]').prop('jFiler').reset();	
					$("#restadvertencia").text("Los cambios han sido guardados satisfactoriamente.");
					$("#modal_alerta").modal("show");
				}else{
					$("#restadvertencia").text("Algo salio mal no se pudo reguistrar el cambio.");
					$("#modal_alerta").modal("show");
				}
			}
		});
	}
});
$('#btnRemover').on('click',function(){
	var fd = new FormData();
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	fd.append('section','3');
	fd.append('id_',$('.selres.active').data('form'));
	$.ajax({
		url: url + "admin/erasefijasection",
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},			
		data: fd,
		processData: false,
		contentType: false,
		type: 'POST',
		beforeSend:function(res){
			$("#modal_wait").modal("show");
		},				
		success: function(res){
			$("#modal_wait").modal("hide");
			if(res.estado == 1){
				$('#form-habitad input[type=file]').prop('jFiler').reset();
				pic1 = undefined;
				$("#1_picture").attr('src',url+"/img/default.jpg");
				$('#form-habitad input[type=file]').prop('jFiler').reset();	
				$("#restadvertencia").text("Los cambios han sido guardados satisfactoriamente.");
				$("#modal_alerta").modal("show");
			}else{
				$("#restadvertencia").text("Algo salio mal no se pudo borrar el cambio.");
				$("#modal_alerta").modal("show");
			}
		}
	});
});
$('.selres').on('click',function(){
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	$('.selres').removeClass('active');
	$(this).addClass('active');
	$('#form-habitad input[type=file]').prop('jFiler').reset();
	$("#1_picture").attr('src',url+$(this).data('rest'));
});