var pdf = undefined;
var pic1 = undefined;
var pic2 = undefined;
var pic3 = undefined;
var pic4 = undefined;

function openFile1(event){
  pic1 = event;
}

function openPROYECTOS(event){
	pdf = event;
}

function openFile2(event){
  pic2 = event;
}

function openFile3(event){
  pic3 = event;
}

function openFile4(event){
  pic4 = event;
}

$(document).ready(function(){
	if($("#server_proyectos").val()!=undefined){
		$("#hoteladvertencia").text($("#server_proyectos").val());
		$("#modal_alerta").modal("show");
	}

	$("#btnRegistrar").on("click",function(){
		if(pic1==undefined && pic2==undefined && pic3==undefined && pic4==undefined && pdf==undefined){
			$("#hoteladvertencia").text("Debe ingresar una galería y/o un proyecto en curso.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-proyectoshome").submit();
		}
	})

	var _URL = window.URL || window.webkitURL;
	$("#file_one").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onerror = function() {
				$("#hoteladvertencia").text("Extensiones permitidas jpg, png");
				$("#modal_alerta").modal("show");
			};

			img.src = _URL.createObjectURL(file);
			$("#uno_picture").attr("src",img.src);
		}
	});

	$("#proyecto_curso").filer({
		limit:1,
		extensions:['pdf'],
		maxSize: 5,
    fileMaxSize:5,
		showThumbs:false,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#hoteladvertencia").text(text);
				$("#modal_alerta").modal("show");
			},

			// confirm dialog
			confirm: function(text, callback) {
				console.log(text);
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback();
					pic1 = undefined;
					$("#modal_confirmar").modal("hide");
					$("#uno_picture").attr("src",$("#url1").val());
				});
			}
		},
		captions:{
			button: "Subir Proyecto",
			feedback: "Suba su P.D.F",
			feedback2: "Proyecto seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover su proyecto?",
			errors: {
				filesLimit: "Solo es permitido subir un proyecto.",
				filesType: "Solo se permiten la siguiente extensión: .{{fi-extensions}}",
				filesSize: "{{fi-name}} Intente con otro P.D.F., Peso máximo 5 MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#hoteladvertencia").text(text);
				$("#modal_alerta").modal("show");
				$("#uno_picture").attr("src",$("#url1").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				console.log(text);
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback();
					pic1 = undefined;
					$("#modal_confirmar").modal("hide");
					$("#uno_picture").attr("src",$("#url1").val());
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_two").on("change",function(){//CARGO LA SEGUNDA IMAGEN
		if(pic2!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#hoteladvertencia").text("Extensiones permitidas jpg, png");
					$("#modal_alerta").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#dos_picture").attr("src",img.src);
			}
		}
	});

	$("#file_two").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#hoteladvertencia").text(text);
				$("#modal_alerta").modal("show");
				$("#dos_picture").attr("src",$("#url2").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic2 = undefined;
					$("#dos_picture").attr("src",$("#url2").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_thrid").on("change",function(){//CARGO LA TERCERA IMAGEN
		if(pic3!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#hoteladvertencia").text("Extensiones permitidas jpg, png");
					$("#modal_alerta").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#tres_picture").attr("src",img.src);
			}
		}
	});

	$("#file_thrid").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#hoteladvertencia").text(text);
				$("#modal_alerta").modal("show");
				$("#tres_picture").attr("src",$("#url3").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic3 = undefined;
					$("#tres_picture").attr("src",$("#url3").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_four").on("change",function(){//CARGO LA CUARTA IMAGEN
		if(pic4!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#hoteladvertencia").text("Extensiones permitidas jpg, png");
					$("#modal_alerta").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#cuatro_picture").attr("src",img.src);
			}
		}
	});

	$("#file_four").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#hoteladvertencia").text(text);
				$("#modal_alerta").modal("show");
				$("#cuatro_picture").attr("src",$("#url4").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic4 = undefined;
					$("#cuatro_picture").attr("src",$("#url4").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
});
