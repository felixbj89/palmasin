$(document).ready(function(){
	//INPUT-TITULO
	count_chart_limite($("#nombre_restaurant"),"input","#cantidad_nombre","33");
	load_count_limite($("#nombre_restaurant"),"#cantidad_nombre","33");
	
	//KEYPRESS-TITULO
	limit_chart($("#nombre_restaurant"),"keypress",32);
	
	//INPUT-COMERCIAL
	count_chart_limite($("#comercal_restaurant"),"input","#cantidad_comercial","200");
	load_count_limite($("#comercal_restaurant"),"#cantidad_comercial","200");
	
	//KEYPRESS-COMERCIAL
	limit_chart($("#comercal_restaurant"),"keypress",199);
	
	$("#contacto1_restaurant").mask("+99 999-9999999");
	$("#contacto2_restaurant").mask("+99 999-9999999");

	//INPUT-TITULO
	load_count_limite($("#contacto1_restaurant"),"#cantidad_contacto1","12");
	
	
	
	if($('#editor_restaurat.summernote').text().length > 0){		
		var cleanText = $('#editor_restaurat.summernote').text();
		
		if(cleanText.trim().length > 0 && cleanText.trim().length <= 898){
			$("#cantidad_desrest").attr("style","color:white !important");
			$("#cantidad_desrest").text(cleanText.trim().length  + "/898");
		}else if(cleanText.trim().length==0){
			$("#cantidad_desrest").text("0/898");
		}else{
			$("#restadvertencia").text("Ha excedido el límite válido, en el campo descripción.");
			$("#cantidad_desrest").attr("style","color:red !important");
			$("#cantidad_desrest").text( cleanText.trim().length + "/898");
			
			$("#modal_alerta").modal("show");
		}
	}
	
	$('#editor_restaurat.summernote').summernote({
		lang: 'ca-ES',
		toolbar: [
			//[groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['para', ['paragraph','ul','ol']],
			//['height', ['height']]
			//['color', ['color']],
			//['para', ['ul', 'ol']],
			//['link', ['linkDialogShow']],
			//['help', ['codeview']]
		],
		callbacks: {
			onImageUpload: function(files) {
			// upload image to server and create imgNode...
			$summernote.summernote('insertNode', "");
		},
		  onPaste: function (e) {
			var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
			e.preventDefault();
			document.execCommand('insertText', false, bufferText);

			var cleanText = $("#editor_restaurat.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
			if(cleanText.length > 0 && cleanText.length <= 898){
			  $("#cantidad_desrest").text(cleanText.length + "/898");
			}else if(cleanText.length==0){
			  $("#cantidad_desrest").text("0/898");
			}else if(cleanText.length > 898){
				$("#editor_restaurat.summernote").summernote("code",cleanText.substring(0,329));
			}
		  },onKeydown:function(e){
			var cleanText = $("#editor_restaurat.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
			if(e.currentTarget.innerText.length > 898){
				$("#cantidad_desrest").attr("style","color:red !important");
				$("#cantidad_desrest").text( cleanText.trim().length + "/898");
			}else{
				$("#cantidad_desrest").text( cleanText.trim().length + "/898");
			}
			},onKeyup:function(e){
				$("#cantidad_desrest").attr("style","color:#FFF!important");
				var cleanText = $("#editor_restaurat.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(cleanText.length > 898){
					$("#editor_restaurat.summernote").summernote("code",cleanText.substring(0,897));
				}
		  }
		},
		height: 300,
		minHeight: 300,
		maxHeight: 300,
	});	
	
	$('#nombre_restaurant').on('change keypress',function(){
		if(cleanText.length > 33){
			$("#nombre_restaurant").val($("#nombre_restaurant").val().substring(0,32));
			$('#cantidad_nombre').text($("#nombre_restaurant").val().length+" / 33");
		}
	});	
	$('#comercal_restaurant').on('change keypress',function(){
		if(cleanText.length > 200){
			$("#comercal_restaurant").val($("#comercal_restaurant").val().substring(0,199));
			$('#cantidad_comercial').text($("#comercal_restaurant").val().length+" / 200");
		}
	});
	
	
	
	$('.file_pic').filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#restadvertencia").text(text);
				$("#modal_alerta").modal("show");
			},
			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				pic1 = undefined;
				$("#btnRemoverPicture").on("click",function(){
					callback();
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
});

$(document).on('click','#btnGuardar',function(){
		var code = $('#editor_restaurat.summernote').summernote('code').replace(/<\/?[^>]+(>|$)/g, "");
		
		if(comprobar_longitud($("#nombre_restaurant").val())&&comprobar_longitud(code)&&comprobar_longitud($("#contacto1_restaurant").val())&&comprobar_longitud($("#contacto2_restaurant").val())&&comprobar_longitud($("#comercal_restaurant").val())){
			$("#restadvertencia").text("Los campos son obligatorio.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#nombre_restaurant").val())){
			$("#restadvertencia").text("El campo es obligatorio, ingrese un nombre para el restaurante");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud(code)){
			$("#restadvertencia").text("El campo es obligatorio, ingrese una descripción para el restaurante");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#comercal_restaurant").val())){
			$("#restadvertencia").text("El campo es obligatorio, ingrese link valido");
			$("#modal_alerta").modal("show");
		}else{
			var fd = new FormData();
			var url = window.location.href.substr(0,window.location.href.search("admin"));
			fd.append('restaurant',$(this).data('restaurant'));
			fd.append('titulo',$("#nombre_restaurant").val());
			fd.append('descripcion',$('#editor_restaurat.summernote').summernote('code'));
			fd.append('contactoI',$("#contacto1_restaurant").val());
			fd.append('contactoII',$("#contacto2_restaurant").val());
			fd.append('comercial',$("#comercal_restaurant").val());
			for(var i = 1;i <= 5;i++){
				if($('#rest_file'+i).val() != ''){
					fd.append('rest_file'+i,$('#rest_file'+i)[0].files[0]);
				}
			}
			$.ajax({
				url: url + "admin/edit_restaurat",
				headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},			
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				beforeSend:function(res){
					$("#modal_wait").modal("show");
				},				
				success: function(res){
					$("#modal_wait").modal("hide");
					if(res == 1){
						$('html, body').animate({scrollTop: (0)}, 0);					
						$("#restadvertencia").text("Los cambios han sido guardados satisfactoriamente.");
						$("#modal_alerta").modal("show");
						$('#btnAcceder').on('click',function(){window.location.href = url+"admin/listrestaurantesinn"});		
					}else{
						$("#restadvertencia").text("Algo salio mal no se pudo reguistrar la habitación.");
						$("#modal_alerta").modal("show");
					}
				}
			});
		}
});