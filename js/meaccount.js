$(document).ready(function(){
	$("#btnModificarPerfil").on("click",function(){
		if(comprobar_longitud($("#loginuser").val()) && comprobar_longitud($("#passworduser").val()) && comprobar_longitud($("#confirmaruser").val())){
			$("#perfiladvertencia").text("Los campos no pueden permanecer vacíos.");
			$("#modal_alerta").modal("show");
		}else if(isequal_password($("#passworduser").val(),$("#confirmaruser").val())){
			$("#perfiladvertencia").text("Las contraseñas deben coincidir.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-perfil").submit();
		}
	});
		
	load_count($("#roluser"),"#cantidad-role");
	
	count_chart($("#loginuser"),"input","#cantidad-login");
	load_count($("#loginuser"),"#cantidad-login");
	
	count_chart($("#passworduser"),"input","#cantidad-password");
	load_count($("#passworduser"),"#cantidad-password");
	
	count_chart($("#confirmaruser"),"input","#cantidad-confirmar");
	load_count($("#confirmaruser"),"#cantidad-confirmar");
	
	limit_chart($("#loginuser"),"keypress",19);
	limit_chart($("#passworduser"),"keypress",19);
	limit_chart($("#confirmaruser"),"keypress",19);
});