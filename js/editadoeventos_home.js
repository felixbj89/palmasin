$('#btnGuardar').on('click',function(){
	if(pic1==undefined && comprobar_longitud($("#fin_evento").val()) && comprobar_longitud($("#inicio_evento").val()) && comprobar_longitud($("#titulo_evento").val()) && comprobar_longitud($("#descripcion_evento").val())){
		$("#mensajeadvertencia").text("Todos los campos son obligatorios.");
		$("#modal_alerta").modal("show");
	}else if(comprobar_longitud($("#titulo_evento").val())){
		$("#mensajeadvertencia").text("El campo es obligatorio, ingrese un título válido.");
		$("#modal_alerta").modal("show");
	}else if(comprobar_longitud($("#descripcion_evento").val())){
		$("#mensajeadvertencia").text("El campo es obligatorio, ingrese una descripción válida.");
		$("#modal_alerta").modal("show");
	}else if(comprobar_longitud($("#inicio_evento").val())){
		$("#mensajeadvertencia").text("El campo es obligatorio, ingrese una fecha de inicio válido.");
		$("#modal_alerta").modal("show");
	}else if(comprobar_longitud($("#fin_evento").val())){
		$("#mensajeadvertencia").text("El campo es obligatorio, ingrese una fecha de fin válido.");
		$("#modal_alerta").modal("show");
	}else if($("#link").val().length > 0){			   
		var urlRegex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
		if($("#link").val().match(urlRegex)){
			$("#form-eventos").submit();
		}else{
			$("#mensajeadvertencia").text("Ingrese un link válido para el evento.");
			$("#modal_alerta").modal("show");
		}
	}else{
		$("#form-eventos").submit();
	}
});