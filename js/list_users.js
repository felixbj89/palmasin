$(document).ready(function(){
	var tablaHistorial = $("#usersinner_list").DataTable({
	"searching": true,
	"bInfo": false,
	"Info":false,
	"bLengthChange": false,
	"iDisplayLength": 3,
	"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
	},

	"fixedHeader": {
		"header": true,
		"footer": true
	},
	"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	if(tablaHistorial.rows().data().length==0){
		$("#modal_empty").modal("show");
	}
	
	if($("#server_listusers").val()!=undefined){
		$("#listusersadvertencia").text($("#server_listusers").val());
		$("#modal_alerta").modal("show");
	}else if($("#error_listusuario").val()!=undefined){
		$("#listusersadvertencia").text($("#error_listusuario").val());
		$("#modal_alerta").modal("show");
	}
	
	var seleccion = undefined; var historial = undefined;
	$("#btnAceptar").on("click",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$("#modal_confirmarremover").modal("hide");
		$.ajax({
			url: url + "admin/delete_users",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"user_id":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado=="1"){
					tablaHistorial.row(seleccion).remove().draw();
					if(tablaHistorial.rows().data().length==0){
						window.location.reload();
					}
				}else{
					$("#listusersadvertencia").text("No se pudo remover a su usuario.");
					$("#modal_alerta").modal("show");
				}
			}
		});
	});
	
	$(document).on("click","#btnRemoverUsuario",function(){
		$("#confirmarmensajeadvertencia").text("¿Esta usted seguro de remover el usuario seleccionado?");
		$("#modal_confirmarremover").modal("show");
		seleccion = $(this).parents("tr");
		historial = $(this).parents("tr").data("historial");
	});
		
	$(document).on("click","#btnModificarUsuario",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var historial = $(this).parents("tr").data("historial");
		$.ajax({
			url: url + "admin/editusers",
			headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"user_id":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);
			}
		});
	});
	
	$('html, body').animate({scrollTop: (0)}, 1500);
});