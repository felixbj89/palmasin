jQuery(function ($) {
	$(document).ready(function (){
		if(target == "contacto" ){
			$('html, body').animate({scrollTop: ($('#'+target).offset().top)-10}, 1500);
		}
		if(target == "habneros" || target == "" ){
			$('.a').removeClass('activeres');
			$('#active_1').addClass('activeres');
			$('.desactive').css('display','block');
			$('#desactive_1').css('display','none');
			$('#img_form').css('background-image','url('+$('.restbar[href="#restbar_1"]').data("form_img")+')');
			if(target == "habneros"){
				$('html, body').animate({scrollTop: ($('#collapse').offset().top)-15}, 1500);
			}else{
				$('.collapse').collapse('hide');
				$('.a').removeClass('activeres');
				$('.desactive').css('display','block');
				$('#contacto').hide();
				$('#BotonRes').hide();
			}
		}	
		if(target == "maracuya" ){
			$('.collapse').collapse('hide');
			$('#restbar_2').collapse('show');
			$('.a').removeClass('activeres');
			$('#active_2').addClass('activeres');
			$('.desactive').css('display','block');
			$('#desactive_2').css('display','none');
			$('#img_form').css('background-image','url('+$('.restbar[href="#restbar_2"]').data("form_img")+')');
			$('html, body').animate({scrollTop: ($('#collapse').offset().top)-15}, 1500);
		}	
		if(target == "lobbybar" ){
			$('.collapse').collapse('hide');
			$('#restbar_3').collapse('show');
			$('.a').removeClass('activeres');
			$('#active_3').addClass('activeres');
			$('.desactive').css('display','block');
			$('#desactive_3').css('display','none');
			$('#img_form').css('background-image','url('+$('.restbar[href="#restbar_3"]').data("form_img")+')');
			$('html, body').animate({scrollTop: ($('#collapse').offset().top)-15}, 1500);
		}
		var items = $('#restbarCarousel>.carousel-inner>.item').length;
		//console.log(items)
		if(items == 1){
			$('#restbarCarousel>.carousel-inner>.item.active').addClass('unico');
		}
		if(items >= 4){
			$('#restbarCarousel>.conrtosBanner').show();
		}
		$('.fdi-Carousel .item').each(function (){
			var next = $(this).next();
			if (!next.length) {
				next = $(this).siblings(':first');
			}
			next.children(':first-child').clone().appendTo($(this));
			
			if (next.next().length > 0) {
				next.next().children(':first-child').clone().appendTo($(this));
			}
			else {
				$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
			}
		});	
	});
	
	$(document).on('click','.restbar',function(){
		var active = $(this).data('active');
		var desactive = $(this).data('desactive');
		$('.restbar').addClass('collapsed');
		$(this).removeClass('collapsed');
		$('.a').removeClass('activeres');
		$(active).addClass('activeres');
		$('.desactive').css('display','block');
		$(desactive).css('display','none');
		$('.collapse').collapse('hide');
		$('#contacto').show();
		$('#BotonRes').show();
		$('#img_form').css('background-image','url('+$(this).data("form_img")+')');
		$('html, body').animate({scrollTop: ($('#collapse').offset().top)-15}, 1500);
	});
	
	$('.galchang_left').on('click',function(){
		var carousel = $(this).data('carousel');
		$(carousel).carousel('next');
	});
	$('.galchang_right').on('click',function(){
		var carousel = $(this).data('carousel');
		$(carousel).carousel('prev');
	});
	$(document).on('click','.galerifijarest,.row.galeria #galeresta .item',function(){
		var url = window.location.href.substr(0,window.location.href.search("restaurantes"))
		var element = this;
		$.ajax({
			type:'GET',
			url: url+'view/gallery/' + btoa($(this).parents("div").parents("div").data("clave")) + "/" + btoa('3'),
			headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
			beforeSend:function(respuesta){
				//$('#eventos #loading').css('display','table');
			},
			success:function(res){
				var imagenes = JSON.parse(res.respuesta);
				$("#LightBox-indicators").empty();
				$("#BootstrapCarouse-innerlLightBox").empty();
				$.each(imagenes,function($key,$galeria){		
					if($galeria.bannergaleri_rutaimg!="img/default.jpg"){
						if($key==0){
							$("#BootstrapCarouse-innerlLightBox").append("<div class=\"item active\"><div><div><img src=\"" + url + $galeria.bannergaleri_rutaimg + "\"></div></div></div>");
							$("#LightBox-indicators").append("<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"" + $key  + "\" class=\"thumbnails active\" style=\"background-image:url('" + url + $galeria.bannergaleri_rutaimg + "')\"></li>");
						}else{
							$("#BootstrapCarouse-innerlLightBox").append("<div class=\"item\"><div><div><img src=\"" + url + $galeria.bannergaleri_rutaimg + "\"></div></div></div>");
							$("#LightBox-indicators").append("<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"" + $key  + "\" class=\"thumbnails\" style=\"background-image:url('" + url + $galeria.bannergaleri_rutaimg + "')\"></li>");
						}
					}
				});
				
				$('#modalGaleriapalmas').modal('show');
			},
			error:function(xhr,status){
				/*alert('Algo salio mal. Estamos trabajando en solucionarlo.');
				$('#eventos #loading').css('display','none');*/
			},
		});
	});
});