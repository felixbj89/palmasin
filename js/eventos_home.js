var pic1 = undefined;

function openFile1(event){
  pic1 = event;
}

$(document).ready(function(){
	var value = $.trim($('#server_eventoshome').val());
	if(value.length>0){
		$("#eventosmensajeadvertencia").text($('#server_eventoshome').val());
		$("#modal_alerta").modal("show");
	}else if($("#server_eventoshome").val()!=undefined){
		$("#eventosmensajeadvertencia").text($("#server_eventoshome").val());
		$("#modal_alerta").modal("show");
	}
	
	$("#inicio_evento").flatpickr({
		"minDate":$("#mindate").val(),
		enableTime: true,
		enableSeconds:false,
		time_24hr: false,
		noCalendar: false,
	});
	
	$("#fin_evento").flatpickr({
		"minDate":$("#mindate").val(),
		enableTime: true,
		enableSeconds:false,
		time_24hr: false,
		noCalendar: false,
	});
	
	var _URL = window.URL || window.webkitURL;
	$("#file_one").on("change",function(){//CARGO LA PRIMERA IMAGEN
		if(pic1!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#eventosmensajeadvertencia").text("Extensiones permitidas jpg, png");
					$("#modal_alerta").modal("show");
				};
				img.src = _URL.createObjectURL(file);
				$("#uno_picture").attr("src",img.src);
			}
		}
	});
	
	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#eventosmensajeadvertencia").text(text);
				$("#modal_alerta").modal("show");
				$("#uno_picture").attr("src",$("#url1").val());
			},
			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				pic1 = undefined;
				$("#btnRemoverPicture").on("click",function(){
					callback();
					pic1 =undefined;
					$("#uno_picture").attr("src",$("#url1").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
});

$("#titulo_evento").bind("keypress", function (event) {
	if (event.charCode!=0) {
		var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ 0-9]+$");
		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		if (!regex.test(key) || (($(this).val()).length+1) > 30){
			event.preventDefault();
			return false;
		}else{
			$('#cantidad-titulo').text(''+($(this).val().length + 1)+'/30');
		}
	}
});
/*$("#titulo_evento").bind("paste", function(e){
    var pastedData = e.originalEvent.clipboardData.getData('text');
	var res = pastedData.substring(0,19);
	$(this).val(res);
    console.log(res);
});*/
$('#titulo_evento').on('paste', function (){
	var element = this;
	setTimeout(function (){
		var text = $(element).val();
		text = text.replace(/[^ a-z A-Z ñ á-ú Á-Ú Ñ 0-9]+/ig,"");//elimina todo aquel caracter que no sea letra o espacio
		var res = text.substring(0,30);
		$(element).val(res);
		$('#cantidad-titulo').text(''+res.length+'/30');
	}, 100);
});
$("#descripcion_evento").bind("keypress", function (event) {
	if (event.charCode!=0) {
		var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ 0-9 ,.]+$");
		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		if (!regex.test(key) || (($(this).val()).length+1) > 250){
			event.preventDefault();
			return false;
		}else{
			$('#cantidad-descripcion').text(''+($(this).val().length + 1)+'/250');
		}
	}
});
$('#descripcion_evento').on('paste', function (){
	var element = this;
	setTimeout(function (){
		var text = $(element).val();
		text = text.replace(/[^ a-z A-Z ñ á-ú Á-Ú Ñ 0-9 ,.]+/ig,"");//elimina todo aquel caracter que no sea letra o espacio
		var res = text.substring(0,249);
		$(element).val(res);
		$('#cantidad-descripcion').text(''+res.length+'/250');
	}, 100);
});
$("#inicio_evento").on('change', function (){//cambio de fecha
	if ((Date.parse($("#inicio_evento").val()) > Date.parse($("#fin_evento").val())) || $("#fin_evento").val().length == 0){
		$("#fin_evento").val($("#inicio_evento").val());	
    }
});
$("#fin_evento").on('change', function (){//cambio de fecha
	if ((Date.parse($("#inicio_evento").val()) > Date.parse($("#fin_evento").val())) || $("#fin_evento").val().length == 0){
		$("#fin_evento").val($("#inicio_evento").val());	
    }
});
$('.collapse').on('show.bs.collapse',function (){
	$('.collapse').each(function(clave,valor){
		if($(valor).hasClass('in')){
			$(valor).collapse('hide');
		}
	});
});
$('.btnlink').on('click',function(){
	$('.btnlink').removeClass('active');
	$(this).addClass('active');
	$('select.selectlink').val('');
});
$('select[name="tipoevento"]').on('change',function(){
	if($(this).val() == '1'){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$('.corporativo').prop('disabled', 'disabled');
		$('.corporativo').val('');
		$('#uno_picture').attr("src",url+"img/default.jpg");
		$('#file_one').prop('jFiler').reset();
	}else{
		$('.corporativo').prop('disabled', false);
	}
});
//corporativo
$("#btnRegistrar").on("click",function(){
	if($('select[name="tipoevento"]').val()== 1){
		if(comprobar_longitud($("#titulo_evento").val()) && comprobar_longitud($("#descripcion_evento").val())){
			$("#eventosmensajeadvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#titulo_evento").val())){
			$("#eventosmensajeadvertencia").text("El campo es obligatorio, ingrese un título válido.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#descripcion_evento").val())){
			$("#eventosmensajeadvertencia").text("El campo es obligatorio, ingrese una descripción válida.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-eventos").submit();
		}
	}else if($('select[name="tipoevento"]').val()== 0){
		if(pic1==undefined && comprobar_longitud($("#fin_evento").val()) && comprobar_longitud($("#inicio_evento").val()) && comprobar_longitud($("#titulo_evento").val()) && comprobar_longitud($("#descripcion_evento").val())){
			$("#eventosmensajeadvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#titulo_evento").val())){
			$("#eventosmensajeadvertencia").text("El campo es obligatorio, ingrese un título válido.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#descripcion_evento").val())){
			$("#eventosmensajeadvertencia").text("El campo es obligatorio, ingrese una descripción válida.");
			$("#modal_alerta").modal("show");
		}else if($('#file_one').val()==''){
			$("#eventosmensajeadvertencia").text("El campo es obligatorio, ingrese una imagen válida.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#inicio_evento").val())){
			$("#eventosmensajeadvertencia").text("El campo es obligatorio, ingrese una fecha de inicio válido.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#fin_evento").val())){
			$("#eventosmensajeadvertencia").text("El campo es obligatorio, ingrese una fecha de fin válido.");
			$("#modal_alerta").modal("show");
		}else if($("#link").val().length > 0){			   
			var urlRegex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if($("#link").val().match(urlRegex) || $("#link").val() == 'elhotel'){
				$("#form-eventos").submit();
			}else{
				$("#eventosmensajeadvertencia").text("Ingrese un link válido para el evento.");
				$("#modal_alerta").modal("show");
			}
		}else{
			$("#form-eventos").submit();
		}
	}else{
		$("#eventosmensajeadvertencia").text("Indica un tipo de evento Corporativo / Promocional.");
		$("#modal_alerta").modal("show");
	}
});
$(document).on('click','#btnGuardarcorporativo',function(){
	if(comprobar_longitud($("#titulo_evento").val()) && comprobar_longitud($("#descripcion_evento").val())){
		$("#editeventosadvertencia").text("Todos los campos son obligatorios.");
		$("#modal_alerta").modal("show");
	}else if(comprobar_longitud($("#titulo_evento").val())){
		$("#editeventosadvertencia").text("El campo es obligatorio, ingrese un título válido.");
		$("#modal_alerta").modal("show");
	}else if(comprobar_longitud($("#descripcion_evento").val())){
		$("#editeventosadvertencia").text("El campo es obligatorio, ingrese una descripción válida.");
		$("#modal_alerta").modal("show");
	}else{
		$("#form-eventos").submit();
	}	
});
$('.btnlink[href="#linkexterno"]').on('click',function(){
	$('#link').val('elhotel');
});