$(document).ready(function(){
	
	var tablaHistorial = $("#restaurant_list").DataTable({
	"searching": false,
	"bInfo": false,
	"bSort":false,
	"Info":false,
	"bLengthChange": false,
	"iDisplayLength": true,
	"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
	},

	"fixedHeader": {
		"header": true,
		"footer": true
	},
	"bPaginate": false,
	"responsive":true,
	"bAutoWidth": true,
	});	
});
$(document).on('click','.btnModificar',function(){
	var elemen = this;
	var restaurat = $(this).data('restauranteid')
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	$.ajax({
			url: url + "admin/editrestaurante",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"target":restaurat["restaurat"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);
			},error:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#restadvertencia").text("Algo salio mal. No se pude modificar el restaurante.");
				$("#modal_alerta").modal("show")
			}
		});	
});
var _URL = window.URL || window.webkitURL;
$(document).on('change','.file_pic',function(){
	if ((file = this.files[0])) {
		img = new Image();
		img.onerror = function() {
			alert( "not a valid file: " + file.type);
		};
		img.src = _URL.createObjectURL(file);
		$($(this).data('img')).attr("src",img.src);
	}
});