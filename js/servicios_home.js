$(document).ready(function(){	
	
	
	$('.summernote').summernote({
	    lang: 'es-ES',
		toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['para', ['paragraph']],
			['height', ['height']]
		],
		callbacks: {
			onImageUpload: function(files) {
				// upload image to server and create imgNode...
				$summernote.summernote('insertNode', "");
			},
			onPaste: function (e) {
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
				e.preventDefault();
				document.execCommand('insertText', false, bufferText);
				
				var textopuro = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(textopuro.trim().length > 898){
					$("#serviciosadvertencia").text("Ha excedido el límite válido, en el campo descripción.");
					$("#cantidad-descripcion").attr("style","color:red !important");
					$("#cantidad-descripcion").text( textopuro.trim().length + "/898");
					var temporal = $(".summernote").summernote("code");
					$(".summernote").summernote("code",temporal);
					$("#modal_alerta").modal("show");
					excedido = 1;
				}else{
					$("#cantidad-descripcion").attr("style","color:white !important");
					$("#cantidad-descripcion").text( textopuro.trim().length + "/898");
					$("#modal_alerta").modal("hide");
				}
			},
			onKeydown:function(e){
				var textopuro = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(textopuro.trim().length > 898){
					$("#serviciosadvertencia").text("Ha excedido el límite válido, en el campo descripción.");
					$("#cantidad-descripcion").attr("style","color:red !important");
					$("#cantidad-descripcion").text( textopuro.trim().length + "/898");
					$("#modal_alerta").modal("show");
					excedido = 1;
				}else{
					$("#cantidad-descripcion").attr("style","color:white !important");
					$("#cantidad-descripcion").text( textopuro.trim().length + "/898");
					$("#modal_alerta").modal("hide");
				}
			}
		},
		placeholder: 'Ingrese la descripción de su servicio',
		height: 300,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
		focus: false 
	});
		
	if($("#server_servicios").val()!=undefined){
		$("#serviciosadvertencia").text($("#server_servicios").val());
		$("#modal_alerta").modal("show");
	}else if($("#error_servicios").val()!=undefined){
		$("#serviciosadvertencia").text($("#error_servicios").val());
		$("#modal_alerta").modal("show");
	}
	
	//INPUT-TITULO
	count_chart_limite($("#titulo"),"input","#cantidad-titulo","40");
	load_count_limite($("#titulo"),"#cantidad-titulo","40");
	
	//KEYPRESS-TITULO
	limit_chart($("#titulo"),"keypress",39);
	
	//INPUT-SUBTITULO
	count_chart_limite($("#subtitulo"),"input","#cantidad-subtitulo","40");
	load_count_limite($("#subtitulo"),"#cantidad-subtitulo","40");
	
	//KEYPRESS-SUBTITULO
	limit_chart($("#subtitulo"),"keypress",39);
	
	$('#titulo').on('change keypress paste',function(){
		var cleanText = $('#titulo').val();
		if(cleanText.length > 40){
			$("#titulo").val(cleanText.substring(0,39));
			$('#cantidad-titulo').text($("#titulo").val().length+" / 40");
		}
	});		
	$('#subtitulo').on('change keypress paste',function(){
		var cleanText = $('#subtitulo').val();
		if(cleanText.length > 40){
			$("#subtitulo").val(cleanText.substring(0,39));
			$('#cantidad-subtitulo').text($("#subtitulo").val().length+" / 40");
		}
	});		
	
	//INPUT-DESCRIPCIÓN
	$('.summernote').on("summernote.change",function(){
		var cleanText = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
		if(cleanText.trim().length > 0 && cleanText.trim().length <= 898){
			$("#cantidad-descripcion").attr("style","color:white !important");
			$("#cantidad-descripcion").text(cleanText.trim().length + "/898");
		}else if(cleanText.trim().length==0){
			$("#cantidad-descripcion").text("0/898");
		}
	});

	if($('.summernote')[0].value.length > 0){		
		var cleanText = $('.summernote')[0].value.replace(/<\/?[^>]+(>|$)/g, "");
		if(cleanText.trim().length > 0 && cleanText.trim().length <= 898){
			$("#cantidad-descripcion").attr("style","color:white !important");
			$("#cantidad-descripcion").text(cleanText.trim().length + "/898");
		}else if(cleanText.trim().length==0){
			$("#cantidad-descripcion").text("0/898");
		}else{
			$("#serviciosadvertencia").text("Ha excedido el límite válido, en el campo descripción.");
			$("#cantidad-descripcion").attr("style","color:red !important");
			$("#cantidad-descripcion").text( cleanText.trim().length + "/898");
			
			$("#modal_alerta").modal("show");
		}
	}
	
	$("#btnModificar").on("click",function(){
		var descripcion = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
		if(descripcion.trim().length==0 && comprobar_longitud($("#titulo").val()) && comprobar_longitud($("#subtitulo").val())){
			$("#serviciosadvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#titulo").val())){
			$("#serviciosadvertencia").text("El campo es obligatorio, ingrese un título válido.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#subtitulo").val())){
			$("#serviciosadvertencia").text("El campo es obligatorio, ingrese un subtítulo válido.");
			$("#modal_alerta").modal("show");
		}else if(descripcion.trim().length==0){
			$("#serviciosadvertencia").text("El campo es obligatorio, ingrese una descripción válida.");
			$("#modal_alerta").modal("show");
		}else if(descripcion.trim().length > 898){
			$("#serviciosadvertencia").text("El campo es obligatorio, ha excedido el límite válido.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-servicios").submit();
		}
	})
});