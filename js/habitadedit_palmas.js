var pic1 = undefined;
function openFile1(event){
  pic1 = event;
}
jQuery(function ($){
	$(document).ready(function(){
	$('#editor_habitad.summernote').summernote({
		lang: 'ca-ES',
		toolbar: [
			//[groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['para', ['paragraph','ul','ol']],
			//['height', ['height']]
			//['color', ['color']],
			//['para', ['ul', 'ol']],
			//['link', ['linkDialogShow']],
			//['help', ['codeview']]
		],
		callbacks: {
			onImageUpload: function(files) {
			// upload image to server and create imgNode...
			$summernote.summernote('insertNode', "");
		},
		  onPaste: function (e) {
			var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
			e.preventDefault();
			document.execCommand('insertText', false, bufferText);

			var cleanText = $("#editor_habitad.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
			if(cleanText.length > 0 && cleanText.length <= 329){
			  $("#cantidad_descripcion").text(cleanText.length + "/330");
			}else if(cleanText.length==0){
			  $("#cantidad_descripcion").text("0/330");
			}else if(cleanText.length > 330){
				$("#editor_habitad.summernote").summernote("code",cleanText.substring(0,329));
			}
		  },onKeydown:function(e){
			var cleanText = $("#editor_habitad.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
			if(e.currentTarget.innerText.length > 329){
				$("#cantidad_descripcion").attr("style","color:red !important");
				$("#cantidad_descripcion").text( cleanText.trim().length + "/330");
			}else{
				$("#cantidad_descripcion").text( cleanText.trim().length + "/330");
			}
			},onKeyup:function(e){
				$("#cantidad_descripcion").attr("style","color:#FFF!important");
				var cleanText = $("#editor_habitad.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(cleanText.length > 330){
					$("#editor_habitad.summernote").summernote("code",cleanText.substring(0,329));
				}
		  }
		},
		height: 300,
		minHeight: 300,
		maxHeight: 300,
	});
	$('#caracteristicas_habitad.summernote').summernote({
		lang: 'ca-ES',
		disableDragAndDrop: true,
		toolbar: [
			['para', ['ul', 'ol']]
		],
		callbacks: {
			onImageUpload: function(files) {
				// upload image to server and create imgNode...
				$summernote.summernote('insertNode', "");
			},
		  onPaste: function (e) {
			var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
			e.preventDefault();
			document.execCommand('insertText', false, bufferText);

			var cleanText = $("#caracteristicas_habitad.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
			if(cleanText.length > 0 && cleanText.length <= 329){
			  $("#cantidad_caracteristicas").text(cleanText.length + "/330");
			}else if(cleanText.length==0){
			  $("#cantidad_caracteristicas").text("0/330");
			}else if(cleanText.length > 330){
				$("#caracteristicas_habitad.summernote").summernote("code",cleanText.substring(0,329));
			}
		  },onKeydown:function(e){
			var cleanText = $("#caracteristicas_habitad.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
			if(e.currentTarget.innerText.length > 329){
				$("#cantidad_caracteristicas").attr("style","color:red !important");
				$("#cantidad_caracteristicas").text( cleanText.trim().length + "/330");
			}else{
				$("#cantidad_caracteristicas").text( cleanText.trim().length + "/330");
			}
			},onKeyup:function(e){
				$("#cantidad_caracteristicas").attr("style","color:#FFF!important");
				var cleanText = $("#caracteristicas_habitad.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(cleanText.length > 330){
					$("#caracteristicas_habitad.summernote").summernote("code",cleanText.substring(0,329));
				}
		  }
		},
		height: 300,
		minHeight: 300,
		maxHeight: 300,
	});
		$("#file_one").filer({
			limit:1,
			extensions:['jpg','png'],
			showThumbs:false,
			dialogs: {
				// alert dialog
				alert: function(text) {
					$("#mensajeadvertencia").text(text);
					$("#modal_alerta").modal("show");
					$("#uno_picture").attr("src",$("#url1").val());
				},
				// confirm dialog
				confirm: function(text, callback) {
					$("#confirmarimageadvertencia").text(text);
					$("#modal_confirmar").modal("show");
					pic1 = undefined;
					$("#btnRemoverPicture").on("click",function(){
						callback();
						pic1 =undefined;
						$("#uno_picture").attr("src",$("#url1").val());
						$("#modal_confirmar").modal("hide");
					});
				}
			},
			captions:{
				button: "Subir Imagen",
				feedback: "Suba una Imagen",
				feedback2: "Imagen sea seleccionado",
				drop: "Drop file here to Upload",
				removeConfirmation: "Está usted seguro de remover la imagen?",
				errors: {
					filesLimit: "Solo es permitido subir una imagen.",
					filesType: "Extensiones permitidas {{fi-extensions}}",
					filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
					filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
					folderUpload: "You are not allowed to upload folders."
				}
			}
		});
		$('#cantidad-titulo').text($('#titulo_habitad').val().length+'/30');
		$('#cantidad_descripcion').text($('#editor_habitad.summernote').summernote('code').replace(/<\/?[^>]+(>|$)/g,"").length+'/330');		
		$('#cantidad_caracteristicas').text($('#caracteristicas_habitad.summernote').summernote('code').replace(/<\/?[^>]+(>|$)/g,"").length+'/330');		
		
		
		//$('#cantidad-titulo').text($('#titulo_habitad').val()+'/30');
		//$('#editor_habitad').summernote('code',code1);
		//$('#caracteristicas_habitad').summernote('code',code2);	
	});
	var _URL = window.URL || window.webkitURL;
	$("#file_one").on("change",function(){//CARGO LA PRIMERA IMAGEN
		if(pic1!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					alert( "not a valid file: " + file.type);
				};
				img.src = _URL.createObjectURL(file);
				$("#uno_picture").attr("src",img.src);
			}
		}
	});

	$('#titulo_habitad').on('change keypress',function(){
		if($('#titulo_habitad').val().length > 30){
			$("#titulo_habitad").val($("#titulo_habitad").val().substring(0,29));
			$('#cantidad-titulo').text($("#titulo_habitad").val().length+" / 30");
		}else{
			$('#cantidad-titulo').text($("#titulo_habitad").val().length+" / 30")
		}
	});		
	
	$(document).on('click','#btnGuardar',function(){
		var codeI = $('#editor_habitad.summernote').summernote('code').replace(/<\/?[^>]+(>|$)/g, "");
		var codeII = $('#caracteristicas_habitad.summernote').summernote('code').replace(/<\/?[^>]+(>|$)/g, "");
		var selected = '';    
		$('#form-habitad label.ico>input[type=checkbox]').each(function(){
			if (this.checked) {
				selected += $(this).val()+',';
			}
		});
		if(pic1==undefined && comprobar_longitud($("#titulo_habitad").val())){
			$("#mensajeadvertencia").text("Los campos son obligatorio.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#titulo_habitad").val())){
			$("#mensajeadvertencia").text("El campo es obligatorio, ingrese un título para la habitación.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud(codeI)){
			$("#mensajeadvertencia").text("El campo es obligatorio, ingrese una descripción para la habitación.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud(codeII)){
			$("#mensajeadvertencia").text("El campo es obligatorio, ingrese lista de caracteristicas para la habitación.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud(selected)){
			$("#mensajeadvertencia").text("El campo es obligatorio, debes seleccionar al menos una opción.");
			$("#modal_alerta").modal("show");
		}else{
			var fd = new FormData();
			var url = window.location.href.substr(0,window.location.href.search("admin"));
			var caracteristicas = $('#caracteristicas_habitad.summernote').summernote('code');
			$(caracteristicas).addClass('list-unstyled');
			
			var parrafos = lista = "";
			var dom = $('<div></div>');
			dom.html($('#editor_habitad.summernote').summernote('code'));
			$.each($(dom[0]).children("p,div"),function(clave,valor){
				if($(valor).html() != "<br>"){
					parrafos +="<p>"+$(valor).html()+"</p>";
				}
			});
			var dom = $('<div></div>');
			dom.html($('#caracteristicas_habitad.summernote').summernote('code'));
			$.each($(dom[0]).children("p,div"),function(clave,valor){
				if($(valor).html() != "<br>"){
					lista += "<p>"+$(valor).html()+"</p>";
				}
			});
			lista += "<ul>";
			$.each($(dom[0]).children("ul, ol"),function(clave,valor){
				if($(valor).html() != "<br>"){
					lista += $(valor).html();
				}
			});
			lista += "</ul>";
			
			fd.append('habitad',$(this).data('habitad'));
			fd.append('titulo',$("#titulo_habitad").val());
			fd.append('descripcion',parrafos);
			fd.append('caracteristicas',lista);
			fd.append('comodidades',selected);
			
			if($( '#file_one' ).val() != ''){
				fd.append('portada',$( '#file_one' )[0].files[0]);
			}
			$.ajax({
				url: url + "admin/edit_habitad",
				headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},			
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				beforeSend:function(res){
					$("#modal_wait").modal("show");
				},				
				success: function(res){
					$("#modal_wait").modal("hide");
					if(res == 1){
						$('html, body').animate({scrollTop: (0)}, 0);					
						$("#mensajeadvertencia").text("Los cambios han sido guardados satisfactoriamente.");
						$("#modal_alerta").modal("show");
						$(document).on('click','#btnAcceder',function(){window.location.href = url+"admin/listar_habitacion"});		
					}else{
						$("#mensajeadvertencia").text("Algo salio mal no se pudo reguistrar la habitación.");
						$("#modal_alerta").modal("show");
					}
				}
			});
		}
	});	
});
