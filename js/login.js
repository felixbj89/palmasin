$(document).ready(function(){
	$("#loginuser").focus();
	
	$("#loginuser, #passworduser").bind("keypress", function (event) {
		if (event.charCode!=0) {
		   var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
		   var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

		   if (!regex.test(key) || ($(this).val()).length > 29) {
			  event.preventDefault();
			  return false;
		   }
		}
	});
	
	$("#loginuser, #passworduser").bind("keydown, keyup", function (event) {
		if(event.which!=0 && event.which==13){
			if(comprobar_longitud($("#loginuser").val()) && comprobar_longitud($("#passworduser").val())){
				$("#mensajeadvertencia").text($("#camposvacios").val());
				$("#modal_alerta").modal("show");
			}else{
				$("#form-login").submit();
			}
		}
	});
	
	$("#btnAcceder").on("click",function(){
		if(comprobar_longitud($("#loginuser").val()) && comprobar_longitud($("#passworduser").val())){
			$("#mensajeadvertencia").text($("#camposvacios").val());
			$("#modal_alerta").modal("show");
		}else{
			$("#form-login").submit();
		}
	});
	
	if($("#server_error").val()!=undefined){
		$("#mensajeadvertencia").text($("#server_error").val());
		$("#modal_alerta").modal("show");
	}
});