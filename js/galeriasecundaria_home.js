$(document).ready(function(){
		  var tablaHistorial = $("#galeria_list").DataTable({
				"searching": true,
				"bInfo": false,
				"bSort":true,
				"Info":false,
				"bLengthChange": false,
				"iDisplayLength": 1,
				"language": {
				  "sProcessing":     "Procesando...",
				  "sLengthMenu":     "Mostrar _MENU_ registros",
				  "sZeroRecords":    "No se encontraron resultados",
				  "sEmptyTable":     "Ningún dato disponible en esta tabla",
				  "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
				  "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
				  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				  "sInfoPostFix":    "",
				  "sSearch":         "Buscar:",
				  "sUrl":            "",
				  "sInfoThousands":  ",",
				  "sLoadingRecords": "Cargando...",
				  "oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				  },
				  "oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				  }
				},

				"fixedHeader": {
				   "header": true,
				   "footer": true
				 },
				"bPaginate": true,
				"responsive":false,
				"bAutoWidth": false,
			});
		$('#editor.summernote').summernote({
		lang: 'ca-ES',
		toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['color', ['color']],
			['para', ['paragraph']],
			//['height', ['height']]
		],
		callbacks: {
			onImageUpload: function(files) {
			$summernote.summernote('insertNode', "");
		},
		  onPaste: function (e) {
			var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
			e.preventDefault();
			document.execCommand('insertText', false, bufferText);
			var cleanText = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
			if(cleanText.length > 0 && cleanText.length <= 329){
			  $("#cantidad_descripcion").text(cleanText.length + "/330");
			}else if(cleanText.length==0){
			  $("#cantidad_descripcion").text("0/330");
			}else if(cleanText.length > 330){
				$("#editor_habitad.summernote").summernote("code",cleanText.substring(0,329));
			}
		  },onKeydown:function(e){
			var cleanText = $(".summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
			if(e.currentTarget.innerText.length > 329){
				$("#cantidad_descripcion").attr("style","color:red !important");
				$("#cantidad_descripcion").text( cleanText.trim().length + "/330");
			}else{
				$("#cantidad_descripcion").text( cleanText.trim().length + "/330");
			}
			
			},onKeyup:function(e){
				$("#cantidad_descripcion").attr("style","color:#FFF!important");
				var cleanText = $("#editor.summernote").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(cleanText.length > 330){
					$("#editor_habitad.summernote").summernote("code",cleanText.substring(0,329));
				}
		  }
		},
		height: 300,
		minHeight: 300,
		maxHeight: 300,
	});
	
	$('.file_pic').filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#restadvertencia").text(text);
				$("#modal_alerta").modal("show");
			},
			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				pic1 = undefined;
				$("#btnRemoverPicture").on("click",function(){
					callback();
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
	if($("#server_galeriasecu").val()!=undefined){
		$("#editeventosadvertencia").text($("#server_galeriasecu").val());
		$("#modal_alerta").modal("show");
	}
});
var _URL = window.URL || window.webkitURL;
$(document).on('change','.file_pic',function(){
	if ((file = this.files[0])) {
		img = new Image();
		img.onerror = function() {
			alert( "not a valid file: " + file.type);
		};
		img.src = _URL.createObjectURL(file);
		$($(this).data('img')).attr("src",img.src);
	}
});
$('.collapse').on('show.bs.collapse',function (){
	$('.collapse').each(function(clave,valor){
		if($(valor).hasClass('in')){
			$(valor).collapse('hide');
		}
	});
});
$('.btnlink').on('click',function(){
	$('.btnlink').removeClass('active');
	$(this).addClass('active');
	$('select.selectlink').val('');
});
$('table').on('click','.btnModificar',function(){
	var elemen = this;
	var historial = $(elemen).closest('tr').data('historial');
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	$.ajax({
		url: url + "admin/editadogaleriahome",
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:{
			"galeria":historial["evento"],
		},
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#row_edit").empty();
			$("#row_edit").html(respuesta);				
		},error:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#editeventosadvertencia").text("Algo salio mal. No se pude modificar el evento.");
			$("#modal_alerta").modal("show")
		}
	});	
});
$('#btn-guardar').on('click',function(){
	var link = parrafo = 0;
	cleanText1 = $("#editor").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
	if($('#galeri_file1').val()=='' && $('#galeri_file1').data('status') == '0'){
		$("#editeventosadvertencia").text("El campo es obligatorio, ingrese una imagen válida.");
		$("#modal_alerta").modal("show");
	}else{
		if($("#link").val().length > 0 && $("#link").val()){			   
			var urlRegex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if($("#link").val().match(urlRegex) || $("#link").val() == 'elhotel'){
				link = 1;//es valido
			}else{
				$("#editeventosadvertencia").text("Ingrese un link válido para el evento.");
				$("#modal_alerta").modal("show");
			}
		}
		if($('select[name="habitaciones"]').val() != null || $('select[name="restaurante"]').val() != null){
			link = 1;//habitaciones o restaurante
		}
		if($("#editor").summernote("code").replace(/<\/?[^>]+(>|$)/g, "").length > 20){
			parrafo = 1;
		}
		console.log(parrafo+"    "+link);
		if(parrafo == 1 && link == 1){
			var lengthtext = $("#editor").summernote("code").replace(/<\/?[^>]+(>|$)/g, "").length;
			if(lengthtext < 20){
			  $("#editor").summernote("reset");
			  $("#editeventosadvertencia").text("La descripción es de maciada corta.");
				$("#modal_alerta").modal("show");
				return false;
			}
			console.log('PARRAFO Y LINK + IMAGEN');
			$("#form-galeri").submit();
		}else if(parrafo == 0 && link == 0){
			console.log('IMAGEN');
			$("#form-galeri").submit();
		}else{
			$("#editeventosadvertencia").text("Debe cargar un parrafo y un link.");
			$("#modal_alerta").modal("show");	
		}
	}
});
$(document).on('click','#elhotel',function(){
	$('#link').val('elhotel');
});
$('table').on('click','.btnRemover',function(){
	var elemen = this;
	var historial = $(elemen).closest('tr').data('historial');
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	$("#confirmarmensajeadvertencia").text('Quieres borrar este Item? ');
	$("#modal_confirmarremover").modal("show");
	$("#btnAceptar").on("click",function(){
		$("#modal_confirmarremover").modal("hide");
		$.ajax({
			url: url + "admin/erasegalesecun",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"galeria":historial["evento"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				if(respuesta.estado=='1'){
					$("#modal_wait").modal("hide");
					$("#editeventosadvertencia").text("Item borrado con exito.");
					$("#modal_alerta").modal("show");
					$(document).on('click','#btnAcceder',function(){window.location.href = url+"admin/listar_galerinferior"});
				}else{
					$("#modal_wait").modal("hide");
					$("#editeventosadvertencia").text("Algo salio mal. No se pudo borrar el Item cero.");
					$("#modal_alerta").modal("show")
				}				
			},error:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#editeventosadvertencia").text("Algo salio mal. No se pudo borrar el Item error.");
				$("#modal_alerta").modal("show")
			}
		});
	});
});