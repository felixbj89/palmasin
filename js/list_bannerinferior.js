$(document).ready(function(){
	var tablaHistorial = $("#historial_list").DataTable({
	"searching": true,
	"bInfo": false,
	"Info":false,
	"bLengthChange": false,
	"iDisplayLength": 1,
	"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
	},

	"fixedHeader": {
		"header": true,
		"footer": true
	},
	"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	$(document).on("click","#btnUsarSalud",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var historial = $(this).parents("tr").data("historial");;
		$.ajax({
			url: url + "admin/estadobannerinferior",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"historial":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
					$("#imagenexterna" + respuesta.id).attr("src",respuesta.path);
					$("#imageninterna" + respuesta.id).attr("src",respuesta.pathinterna);
				}else if(respuesta.estado==2){
					console.log(2);
					$("#saludadvertencia").text("La imagen de salud no pudo ser activada.");
					$("#modal_alerta").modal("show");
				}
			}
		});
	});
	
	if(tablaHistorial.rows().data().length==0){
		$("#modal_empty").modal("show");
	}
	
	var seleccion = undefined; var historial = undefined;
	$("#btnAceptar").on("click",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$("#modal_confirmarremover").modal("hide");
		$.ajax({
			url: url + "admin/delete_bsalud",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"historial":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				tablaHistorial.row(seleccion).remove().draw();
				if(tablaHistorial.rows().data().length==0){
					$("#modal_empty").modal("show");
				}
			}
		});
	});
		
	$(document).on("click","#btnRemoverSalud",function(){
		$("#confirmarmensajeadvertencia").text("¿Esta usted seguro de remover las imágenes seleccionada?");
		$("#modal_confirmarremover").modal("show");
		seleccion = $(this).parents("tr");
		historial = $(this).parents("tr").data("historial");
	});
});