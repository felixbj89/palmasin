jQuery(function ($){
	if($('#sel_habitad')[0].length==1){
		$("#modal_empty").modal("show");
	}
	
	$('#sel_habitad').change(function(){
		var table = $('#galeri_list').DataTable();
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var galeria = $(this).val();
		$.ajax({
			url: url + "admin/galerihabitad",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"seccion":"2",
				"galeria":galeria
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#conten_galeria").html(respuesta.galeria);
				$(".file_img").filer({
					limit:1,
					extensions:['jpg','png'],
					showThumbs:false,
					maxSize:5,
					dialogs:{
						// alert dialog
						alert: function(text) {
							$("#galeriahabadvertencia").text(text);
							$("#modal_alerta").modal("show");
						},
						// confirm dialog
						confirm: function(text, callback) {
							$("#confirmarimageadvertencia").text(text);
							$("#modal_confirmar").modal("show");
							$("#btnRemoverPicture").on("click",function(){
								callback();
								$("#modal_confirmar").modal("hide");
							});
						}
					},
					captions:{
						button: "Subir Imagen",
						feedback: "Suba una Imagen",
						feedback2: "Imagen sea seleccionado",
						drop: "Drop file here to Upload",
						removeConfirmation: "Está usted seguro de remover la imagen?",
						errors: {
							filesLimit: "Solo es permitido subir una imagen.",
							filesType: "Extensiones permitidas {{fi-extensions}}",
							filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
							filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
							folderUpload: "You are not allowed to upload folders."
						}
					}
				});
				var checkbox = $('input:checkbox:checked').length;
				if(checkbox >= 3){
					$('input:checkbox:not(":checked")').prop('disabled',true);
				}else{
					$('input:checkbox').prop('disabled',false);
				}
			},error:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#galeriahabadvertencia").text("Algo salio mal. No se pudo encontrar una galeria.");
				$("#modal_alerta").modal("show")
			}
		});
	});
	var _URL = window.URL || window.webkitURL;
	$(document).on('change','.file_img',function(){
		var element = $(this).data('image');
		var label = $(this).data('label');
		var file = this.files[0];
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		if (file){
			img = new Image();
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			img.src = _URL.createObjectURL(file);
			$(element).attr("src",img.src);
			$(label+'>input[type=checkbox]').prop('disabled',false);
			//$(label).show();
			var checkbox = $('input:checkbox:checked').length;
			if(checkbox >= 3){
				$('input:checkbox:not(":checked")').prop('disabled',true);
			}else{
				$('input:checkbox').prop('disabled',false);
			}
			$('input:checkbox').each(function (clave,valor){
				var image = $(this).data('image');
				if($(image).attr('src') == url+'img/default.jpg'){
					$(this).prop('disabled',true);
				}
			});			
		}else{
			if($(label+'>input[type=checkbox]').is(':checked')){
				$('input:checkbox').prop('disabled',false);
			}else{
				var checkbox = $('input:checkbox:checked').length;
				if(checkbox >= 3){
					$('input:checkbox:not(":checked")').prop('disabled',true);
				}else{
					$('input:checkbox').prop('disabled',false);
				}		
			}
			$('input:checkbox').each(function (clave,valor){
				var image = $(this).data('image');
				if($(image).attr('src') == url+'img/default.jpg'){
					$(this).prop('disabled',true);
				}
			});
			$(label+'>input[type=checkbox]').prop('checked',false);
			$(label+'>input[type=checkbox]').prop('disabled',true);
			//$(label).hide();
			$(element).attr("src",url+"img/default.jpg");
		}
	});
	$(document).on('change','.checkboxgroup',function(){
		var checkbox = $('input:checkbox:checked').length;
		if(checkbox >= 3){
			$('input:checkbox:not(":checked")').prop('disabled',true);
		}else{
			$('input:checkbox').prop('disabled',false);
		}
	});
	$(document).on('click','.checkboxgroup',function(){
		var image = $(this).data('image');
		var url = window.location.href.substr(0,window.location.href.search("admin"));	
		if($(image).attr('src') == url+'img/default.jpg'){
			$(this).prop('checked',false);
			$(this).prop('disabled',true);
		}
	});
	$(document).on('click','#btnRegistrar',function(){
		var fd = new FormData();
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var images = checkbox = 0;
		for(var i = 1;i<=8;i++){
			if($('#file_'+i)[0].files[0]){
				fd.append('image'+i,$('#file_'+i)[0].files[0]);
				images++;
			}else{
				if($('#file_'+i).attr('src') !== url+'img/default.jpg'){
					images++;
				}
			}
		}
		checkbox = $('input:checkbox:checked').length;
		if(images >= 4 && checkbox == 3){
			var fd = new FormData();
			var checkboxval = new Array();
			var habitadgaleri = $( "select option:selected" ).val();
			$('input:checkbox').each(function (clave,valor){
				if($(this).is(':checked')){
					checkboxval.push("1");
				}else{
					checkboxval.push("0");
				}
			});
			fd.append('habitadgaleri',habitadgaleri);//galeriahabitad
			fd.append('checkboxval',checkboxval);//destacados
			$('.file_img').each(function (clave,valor){
				if($(this).val().length != 0){
					fd.append($(this).attr('id'),$(this)[0].files[0]);//agrego
				}
			});
			$.ajax({
				url: url + "admin/editgalerihabitad",
				headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},			
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				beforeSend:function(res){
					$("#modal_wait").modal("show");
				},				
				success: function(res){
					$("#modal_wait").modal("hide");
					if(res == 1){				
						$("#galeriahabadvertencia").text("Los cambios han sido guardados satisfactoriamente.");
						$("#modal_alerta").modal("show");
						$(document).on('click','#btnAcceder',function(){location.reload();})
					}else{
						$("#galeriahabadvertencia").text("Algo salio mal no se pudo reguistrar la habitación.");
						$("#modal_alerta").modal("show");
					}
				}
			});
		}else{
			if(images < 4){
				$("#galeriahabadvertencia").text("Para registrar los cambios debe cargar al menos cuatro (4) imagenes.");
				$("#modal_alerta").modal("show");
				return false;
			}
			if(checkbox != 3){
				$("#galeriahabadvertencia").text("Para registrar los cambios debe selecionar tres (3) imagenes destacadas.");
				$("#modal_alerta").modal("show");
				return false;				
			}
		}
	});
	$(document).on('click','.delmy',function(){
		var element = $(this).data('label');
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var dato = $(this).data('image');
		var img = $(this).data('picture');
		var file = $(this).data('file');
		var images = 0;
		
		if($(img).attr("src") != (url+dato.delimage)){
			console.log('primero');
			if((url+dato.delimage) == (url+"img/default.jpg")){
				$(img).attr("src",url+"img/default.jpg");
				$(file).prop('jFiler').reset();
				return false;
			}else{
				console.log('segundo');
				$(img).attr("src",url+dato.delimage);
				$(file).prop('jFiler').reset();
				return false;	
			}
		}
		
		$('.p_picture').each(function (clave,valor){
			if($(this).attr('src') !== url+'img/default.jpg'){
				images++;
			}
		});		
		if($(img).attr('src') == url+'img/default.jpg'){
			return false;
		}
		if(images <= 4){
			$("#galeriahabadvertencia").text("No puedes borra esta imagen. Debe haber un mínimo de cuatro (4) imagenes en la galeria.");
			$("#modal_alerta").modal("show");
			return false;
		}
		
		if(dato.delimage == 'img/default.jpg'){
			$(img).attr("src",url+"img/default.jpg");
			$(file).prop('jFiler').reset();
			return false;
		}
		
		if($(element+'>input[type=checkbox]').is(':not(":checked")')){
			if (confirm('Quieres borrar esta imagen? ')){
				console.log();
				$.ajax({
					url: url + "admin/erasegalerihabitad",
					headers:{
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					method:"post",
					data: dato,
					beforeSend:function(respuesta){
						$("#modal_wait").modal("show");
					},
					success:function(respuesta){
						if(respuesta.estado==1){
							$("#modal_wait").modal("hide");
							$("#galeriahabadvertencia").text("Imagen borrada con exito.");
							$("#modal_alerta").modal("show")
							$(img).attr("src",url+"img/default.jpg");
							dato.delimage = "img/default.jpg";
							$(element).data('image',dato);
							$(file).prop('jFiler').reset();
						}else{
							$("#modal_wait").modal("hide");
							$("#galeriahabadvertencia").text("Algo salio mal. No se pude borrar la imagen.");
							$("#modal_alerta").modal("show")
						}		
					},error:function(respuesta){
						$("#modal_wait").modal("hide");
						$("#galeriahabadvertencia").text("Algo salio mal. No se pude borrar la imagen.");
						$("#modal_alerta").modal("show")
					}
				});
			}else{
				return false;
			}
		}else{
			$("#galeriahabadvertencia").text("No puedes borra esta imagen. Es una imagen destacada.");
			$("#modal_alerta").modal("show");
		}
	});
});