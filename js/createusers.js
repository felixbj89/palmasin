$(document).ready(function(){
	
	count_chart($("#nombreusers"),"input","#cantidad-nombre");
	load_count_limite($("#nombreusers"),"#cantidad-nombre","20");
	limit_chart($("#nombreusers"),"keypress",19);
	
	count_chart_limite($("#emailusers"),"input","#cantidad-email","30");
	load_count_limite($("#emailusers"),"#cantidad-email","30");
	limit_chart($("#emailusers"),"keypress",29);
	
	count_chart($("#loginusers"),"input","#cantidad-login");
	load_count_limite($("#loginusers"),"#cantidad-login","20");
	limit_chart($("#loginusers"),"keypress",19);
	
	count_chart($("#passwordusers"),"input","#cantidad-password");
	load_count_limite($("#passwordusers"),"#cantidad-password","20");
	limit_chart($("#passwordusers"),"keypress",19);
	
	count_chart($("#confirmarusers"),"input","#cantidad-confirmar");
	load_count_limite($("#confirmarusers"),"#cantidad-confirmar","20");
	limit_chart($("#confirmarusers"),"keypress",19);
	
	$("#btnModificarUsers").on("click",function(){
		if($("#passwordusers").val()!=$("#confirmarusers").val()){
			$("#usuariosadvertencia").text("La campos de contraseña no coinciden.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-editusers").submit();
		}
	});
	
	$("#btnRegistrarUsers").on("click",function(){
		if($("#nombreusers").val().length==0 && $("#emailusers").val().length==0 && $("#roleusers").val()=="NULL" && $("#loginusers").val().length==0 && $("#passwordusers").val().length==0 && $("#confirmarusers").val().length==0){
			$("#usuariosadvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else if($("#nombreusers").val().length==0){
			$("#usuariosadvertencia").text("El campo nombre es obligatorio.");
			$("#modal_alerta").modal("show");
		}else if($("#emailusers").val().length==0){
			$("#usuariosadvertencia").text("El campo e-mail es obligatorio.");
			$("#modal_alerta").modal("show");
		}else if(!check_email($("#emailusers"))){
			$("#usuariosadvertencia").text("El formato del e-mail es incorrecto.");
			$("#modal_alerta").modal("show");
		}else if($("#roleusers").val()=="NULL"){
			$("#usuariosadvertencia").text("El rol seleccionado es incorrecto.");
			$("#modal_alerta").modal("show");
		}else if($("#loginusers").val().length==0){
			$("#usuariosadvertencia").text("El campo login es obligatorio.");
			$("#modal_alerta").modal("show");
		}else if($("#passwordusers").val().length==0){
			$("#usuariosadvertencia").text("El campo password es obligatorio.");
			$("#modal_alerta").modal("show");
		}else if($("#confirmarusers").val().length==0){
			$("#usuariosadvertencia").text("El campo confirmar es obligatorio.");
			$("#modal_alerta").modal("show");
		}else if($("#passwordusers").val()!=$("#confirmarusers").val()){
			$("#usuariosadvertencia").text("La campos de contraseña no coinciden.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-crearusers").submit();
		}
	});
	
	$(document).on("click","#btnVerpassword",function(){
		$("#passwordusers").attr("type","text");
		$("#confirmarusers").attr("type","text");
		$("#icobtn").attr("class","fa fa-eye");
		$("#btnVerpassword").attr("id","btnOcultarPassword");
	}); 
	
	
	$(document).on("click","#btnOcultarPassword",function(){
		$("#passwordusers").attr("type","password");
		$("#confirmarusers").attr("type","password");
		$("#icobtn").attr("class","fa fa-eye-slash");
		$("#btnOcultarPassword").attr("id","btnVerpassword");
	}); 
	
	if($("#server_usuario").val()!=undefined){
		$("#usuariosadvertencia").text($("#server_usuario").val());
		$("#modal_alerta").modal("show");
	}else if($("#error_usuario").val()!=undefined){
		$("#usuariosadvertencia").text($("#error_usuario").val());
		$("#modal_alerta").modal("show");
	}
});