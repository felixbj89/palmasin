function comprobar_longitud(campo){
	if(campo.length==0){
		return true;
	}return false;
}

function count_chart(campo, evento, idcount){
	campo.bind(evento,function(){
		if(campo.val().length >= 0){
			$(idcount).text(campo.val().length + "/30");
		}
	});
}

function count_chart_limite(campo, evento, idcount, limite){
	campo.bind(evento,function(){
		if(campo.val().length >= 0){
			$(idcount).text(campo.val().length + "/" + limite);
		}
	});
}

function load_count(campo, idcount){
	if(campo.val().length >= 0){
		$(idcount).text(campo.val().length + "/30");
	}
}

function load_count_limite(campo, idcount, limit){
	if(campo.val().length >= 0){
		$(idcount).text(campo.val().length + "/" + limit);
	}
}

function isequal_password(password, confirmar){
	if(password !="" && confirmar !="" && password!=confirmar){
		return false;
	}else if(password.length > 0 && confirmar.length > 0){
		return true;
	}
}

function check_email(campo){
	var expresion = /^[a-z0-9_\.\-]+(.[a-z0-9_\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
	if(!expresion.test(campo.val())){
		return false;
	}return true;
}

function limit_chart(campo, evento, limite){
	campo.bind(evento,function(event){
		if (event.charCode!=0) {
		   //var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9.@ ]+$");
		   var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ 0-9 @ _ , . ¡ !]+$");
		   var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		   if (!regex.test(key) || ($(this).val()).length > limite) {
			  event.preventDefault();
			  return false;
		   }
		}
	});
}