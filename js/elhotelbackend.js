var pic1 = undefined;
var pic2 = undefined;
var pic3 = undefined;

function openFile1(event){
  pic1 = event;
}

function openFile2(event){
  pic2 = event;
}

function openFile3(event){
  pic3 = event;
}


$(document).ready(function(){
	var _URL = window.URL || window.webkitURL;
	count_chart_limite($("#titulo_corporativos"),"input","#cantidad-titulo","29");
	load_count_limite($("#titulo_corporativos"),"#cantidad-titulo","29");
	limit_chart($("#titulo_corporativos"),"keypress",29);
	
	$('#titulo_corporativos').on('change keypress paste',function(){
		var cleanText = $('#titulo_corporativos').val();
		if(cleanText.length > 30){
			$("#titulo_corporativos").val(cleanText.val().substring(0,29));
			$('#cantidad-titulo').text($("#titulo_corporativos").val().length+" / 30");
		}
	});	
	
	if($("#server_hotel").val()!=undefined){
		$("#historiahoteladvertencia").text($("#server_hotel").val());
		$("#modal_alerta").modal("show");
	}else if($("#error_hotel").val()!=undefined){
		$("#historiahoteladvertencia").text($("#error_hotel").val());
		$("#modal_alerta").modal("show");
	}
	
	$("#btnGuardar").on("click",function(){
		var descripcion1 = $("#descripcion_evento1").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
		if(descripcion1.trim().length==0 && comprobar_longitud($("#titulo_corporativos").val())){
			$("#historiahoteladvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else if(comprobar_longitud($("#titulo_corporativos").val())){
			$("#historiahoteladvertencia").text("El campo es obligatorio, ingrese un título válido.");
			$("#modal_alerta").modal("show");
		}else if(descripcion1.trim().length==0){
			$("#historiahoteladvertencia").text("El campo es obligatorio, ingrese una descripción válida.");
			$("#modal_alerta").modal("show");
		}else if(descripcion1.trim().length > 922){
			$("#historiahoteladvertencia").text("El campo es obligatorio, ha excedido el límite válido.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-elhotel").submit();
		}
	});
	
	if($('#descripcion_evento1')[0].value.length > 0){		
		var cleanText = $('#descripcion_evento1')[0].value.replace(/<\/?[^>]+(>|$)/g, "");
		if(cleanText.trim().length > 0 && cleanText.trim().length <= 922){
			$("#cantidad-descripcion1").attr("style","color:white !important");
			$("#cantidad-descripcion1").text(cleanText.trim().length + "/922");
		}else if(cleanText.trim().length==0){
			$("#cantidad-descripcion1").text("0/922");
		}else{
			$("#historiahoteladvertencia").text("Ha excedido el límite válido, en el campo descripción.");
			$("#cantidad-descripcion1").attr("style","color:red !important");
			$("#cantidad-descripcion1").text( cleanText.trim().length + "/922");
			
			$("#modal_alerta").modal("show");
		}
	}
	
	$('#descripcion_evento1').summernote({
	    lang: 'es-ES',
		toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['para', ['paragraph']],
			['height', ['height']]
		],
		callbacks: {
			onImageUpload: function(files) {
				// upload image to server and create imgNode...
				$summernote.summernote('insertNode', "");
			},
			onPaste: function (e) {
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
				e.preventDefault();
				document.execCommand('insertText', false, bufferText);
				
				var textopuro = $("#descripcion_evento1").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(textopuro.trim().length > 922){
					$("#historiahoteladvertencia").text("Ha excedido el límite válido, en el campo descripción.");
					$("#cantidad-descripcion1").attr("style","color:red !important");
					$("#cantidad-descripcion1").text( textopuro.trim().length + "/922");
					var temporal = $("#descripcion_evento1").summernote("code");
					$("#descripcion_evento1").summernote("code",temporal);
					$("#modal_alerta").modal("show");
					excedido = 1;
				}else{
					$("#cantidad-descripcion1").attr("style","color:white !important");
					$("#cantidad-descripcion1").text( textopuro.trim().length + "/922");
					$("#modal_alerta").modal("hide");
				}
			},
			onKeydown:function(e){
				var textopuro = $("#descripcion_evento1").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
				if(textopuro.trim().length > 922){
					$("#historiahoteladvertencia").text("Ha excedido el límite válido, en el campo descripción.");
					$("#cantidad-descripcion1").attr("style","color:red !important");
					$("#cantidad-descripcion1").text( textopuro.trim().length + "/922");
					$("#modal_alerta").modal("show");
					excedido = 1;
				}else{
					$("#cantidad-descripcion1").attr("style","color:white !important");
					$("#cantidad-descripcion1").text( textopuro.trim().length + "/922");
					$("#modal_alerta").modal("hide");
				}
			}
		},
		placeholder: 'Ingrese la descripción de su hotel',
		height: 300,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
		focus: false 
	});
	
	$("#file_one").on("change",function(){//CARGO LA SEGUNDA IMAGEN
		if(pic1!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#historiahoteladvertencia").text("Extensiones permitidas jpg, png");
					$("#modal_alerta").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#uno_picture").attr("src",img.src);
			}
		}
	});
	
	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#historiahoteladvertencia").text(text);
				$("#modal_alerta").modal("show");
				$("#uno_picture").attr("src",$("#url1").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				console.log(text);
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback();
					pic1 = undefined;
					$("#modal_confirmar").modal("hide");
					$("#uno_picture").attr("src",$("#url1").val());
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_two").on("change",function(){//CARGO LA SEGUNDA IMAGEN
		if(pic2!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#historiahoteladvertencia").text("Extensiones permitidas jpg, png");
					$("#modal_alerta").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#dos_picture").attr("src",img.src);
			}
		}
	});

	$("#file_two").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#historiahoteladvertencia").text(text);
				$("#modal_alerta").modal("show");
				$("#dos_picture").attr("src",$("#url2").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic2 = undefined;
					$("#dos_picture").attr("src",$("#url2").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_thrid").on("change",function(){//CARGO LA TERCERA IMAGEN
		if(pic3!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#historiahoteladvertencia").text("Extensiones permitidas jpg, png");
					$("#modal_alerta").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#tres_picture").attr("src",img.src);
			}
		}
	});

	$("#file_thrid").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#historiahoteladvertencia").text(text);
				$("#modal_alerta").modal("show");
				$("#tres_picture").attr("src",$("#url3").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic3 = undefined;
					$("#tres_picture").attr("src",$("#url3").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
});