var pic1 = undefined;
var pic2 = undefined;

function openFile1(event){
  pic1 = event;
}

function openFile2(event){
  pic2 = event;
}

$(document).ready(function(){
	if($("#server_comerciales").val()!=undefined){
		$("#comercialesadvertencia").text($("#server_comerciales").val());
		$("#modal_alerta").modal("show");
	}

	$("#btnModificar").on("click",function(){
		if($("#titulo_comercialone").val().length==0 && $("#link_comercialone").val().length==0 && $("#titulo_comercialtwo").val().length==0 && $("#link_comercialdos").val().length==0 && pic1==undefined && pic2==undefined){
			$("#comercialesadvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else{
			$("#form-comerciales").submit();
		}
	})
		
	var _URL = window.URL || window.webkitURL;
	$("#file_one").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};

			img.src = _URL.createObjectURL(file);
			$("#uno_picture").attr("src",img.src);
		}
	});

	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		dialogs: {
			// alert dialog
			alert: function(text) {
				return alert(text);
			},

			// confirm dialog
			confirm: function(text, callback) {
				console.log(text);
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback();
					pic1 = undefined;
					$("#modal_confirmar").modal("hide");
					$("#uno_picture").attr("src",$("#url1").val());
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_two").on("change",function(){//CARGO LA SEGUNDA IMAGEN
		if(pic2!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					alert( "not a valid file: " + file.type);
				};

				img.src = _URL.createObjectURL(file);
				$("#dos_picture").attr("src",img.src);
			}
		}
	});

	$("#file_two").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		dialogs: {
			// alert dialog
			alert: function(text) {
				return alert(text);
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic2 = undefined;
					$("#dos_picture").attr("src",$("#url2").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
	
	$('#titulo_comercialone').on('change keypress paste',function(){
		var cleanText = $('#titulo_comercialone').val();
		if(cleanText.length > 50){
			$("#titulo_comercialone").val($("#titulo_comercialone").val().substring(0,49));
			$('#cantidad-titulo').text($("#titulo_comercialone").val().length+" / 50");
		}
	});	
	$('#titulo_comercialtwo').on('change keypress paste',function(){
		var cleanText = $('#titulo_comercialtwo').val();
		if(cleanText.length > 50){
			$("#titulo_comercialtwo").val($("#titulo_comercialtwo").val().substring(0,49));
			$('#cantidad-titulotwo').text($("#titulo_comercialtwo").val().length+" / 50");
		}
	});	
	
	//INPUT-TITULO
	count_chart_limite($("#titulo_comercialone"),"input","#cantidad-titulo","50");
	load_count_limite($("#titulo_comercialone"),"#cantidad-titulo","50");
	//KEYPRESS-TITULO
	limit_chart($("#titulo_comercialone"),"keypress",49);
	
	//INPUT-TITULO
	count_chart_limite($("#link_comercialone"),"input","#cantidad-linksone","200");
	load_count_limite($("#link_comercialone"),"#cantidad-linksone","200");
	//KEYPRESS-TITULO
	limit_chart($("#link_comercialone"),"keypress",199);
	
	//INPUT-TITULO
	count_chart_limite($("#titulo_comercialtwo"),"input","#cantidad-titulotwo","50");
	load_count_limite($("#titulo_comercialtwo"),"#cantidad-titulotwo","50");
	//KEYPRESS-TITULO
	limit_chart($("#titulo_comercialtwo"),"keypress",49);
	
	//INPUT-TITULO
	count_chart_limite($("#link_comercialdos"),"input","#cantidad-linkdos","200");
	load_count_limite($("#link_comercialdos"),"#cantidad-linkdos","200");
	//KEYPRESS-TITULO
	limit_chart($("#link_comercialdos"),"keypress",199);
	
	$('html, body').animate({scrollTop: (0)}, 1500);
});
