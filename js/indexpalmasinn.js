jQuery(function ($) {	
	$(document).ready(function (){
		//var modal_load = localStorage.getItem('load');
		//if(!modal_load){
			/*setTimeout(function() {
				Shadowbox.open({
					content:    '<img src="' + window.location.href  + '/img/aviso.jpg" style="width:auto;height:100%;">',
					player:     "html",
					title:      "",
					width:      500,
					height:     500
				});
			}, 50);*/
		//	$('#modal_load').modal('show');
		//	localStorage.setItem('load', '1');
		//}
		
		$('.compartir').popover({trigger:"focus",html:true,placement:"top",content:function(){
			return "<ul class=\"list-inline\"><li><a href=\"http://www.facebook.com/sharer.php?u="+$(this).data('url')+"/\" target=\"_blank\" class=\"share-facebook\"></a></li><li><a href=\"http://www.twitter.com/home?status="+$(this).data('url')+"/\" target=\"_blank\" class=\"share-twitter\"></a></li></ul>";
		}});
		
		if(target == "promociones" || target == "eventos" || target == "turismosalud"){
			$('html, body').animate({scrollTop: ($('#'+target).offset().top)-10}, 1500);
		}
		var promos = $('#carouselPromo>.carousel-inner>.item').length;
		var galeria = $('#carouselGaleria>.carousel-inner>.item').length;
		$('#carouselPromo').carousel({
			interval: false
		});
		$('#carouselGaleria').carousel('prev');
		$("#imagensalud").on("click",function(){
			$(".modal-dimensiones").attr("style","background:url(" + $(this).data("salud") + ");background-size:800px;background-repeat: no-repeat;");
		});
		$('.carousel.fdi-Carousel .item').each(function () {
			var next = $(this).next();
			if (!next.length) {
				next = $(this).siblings(':first');
			}
			next.children(':first-child').clone().appendTo($(this));

			if (next.next().length > 0) {
				next.next().children(':first-child').clone().appendTo($(this));
			}
			else {
				$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
			}
		});
		if(promos == 1){
			$('#carouselPromo .item>div').addClass('punico');
			$('.ocultarpromo').hide();
		}else{
			$('#carouselPromo').carousel('next');
		}
		if(galeria == 1){
			$('#galeria .item>div').addClass('unico');
		}
	});
	$(document).on('click', 'a.btn-scroll', function(event){
		event.preventDefault();
		$('html, body').animate({scrollTop: ($($.attr(this, 'href')).offset().top)-10}, 1500);
	});	
	
	$('#palmasinnHabitaciones,#palmasinnInstalaciones,#palmasinnRestaurantes').hover(
		function(){$('#'+$(this).attr('id')+' .name_ad').animate({'bottom': '0px'},400);},
		function(){$('.name_ad').animate({'bottom': -100},400);}
	);

	$(document).on('click','#eventos>div[class*=col-]:nth-child(2)>ul>li>button',function(){
		var pathname = window.location.pathname;
		var url = window.location.href.substr(0,window.location.href.search(pathname))
		url += pathname;
		var element = this;
		$.ajax({
			type:'POST',
			url: url+'index_eventos',
			headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
			data:{mes:$(element).data('mes'),year:$(element).data('year'),key:$(element).data('key')},
			beforeSend:function(respuesta){
				$('#eventos #loading').css('display','table');
			},
			success:function(res){
				$('#eventos>div[class*=col-]:nth-child(2)>ul>li>button').removeClass('active');
				$(element).addClass('active');
				$('#li_eventos').html(res.li_eventos);
				$('#eve_ppal').html(res.eve_ppal);
				$('#cuadro1').html(res.cuadro1);
				$('#cuadro2').html(res.cuadro2);
				$('#eventos #loading').css('display','none');
			},
			error:function(xhr,status){
				alert('Algo salio mal. Estamos trabajando en solucionarlo.');
				$('#eventos #loading').css('display','none');
			},
		});
	});

	$(document).on('click','a[data-toggle="modal"][href="#modalPromocionespalmas"]',function(){
		$('#modalPromocionespalmas input[type="radio"]').attr('checked',false);
		$('#modalPromocionespalmas input[type="radio"][id="'+$(this).data('promo')+'"]').attr('checked',true);
	});	
	
	$('#modalPromocionespalmas').on('shown.bs.modal',function(){
		var mensaje = "Requiero / Solicitola promoción: "+$('#modalPromocionespalmas input[type="radio"]:checked').data('name')+" valida desde: "+$('#modalPromocionespalmas input[type="radio"]:checked').data('ini')+" hasta: "+$('#modalPromocionespalmas input[type="radio"]:checked').data('fin')+".";
		$('#modalPromocionespalmas #promo-Mensaje').text(mensaje);
	});
	
	$(document).on('change click','#modalPromocionespalmas input[type="radio"]',function(){
		var mensaje = "Requiero / Solicitola promoción: "+$('#modalPromocionespalmas input[type="radio"]:checked').data('name')+" valida desde: "+$('#modalPromocionespalmas input[type="radio"]:checked').data('ini')+" hasta: "+$('#modalPromocionespalmas input[type="radio"]:checked').data('fin')+".";
		$('#modalPromocionespalmas #promo-Mensaje').text(mensaje);
	});
	
	$(document).on('click','#cuadro1>div>.datevent',function(){
		var mes = $(this).data('mes');
		$('.btn-mesactivo[data-mes="'+mes+'"]').click();
	});
	$(document).on('click','#cuadro2>div>.datevent',function(){
		var mes = $(this).data('mes');
		$('.btn-mesactivo[data-mes="'+mes+'"]').click();
	});
	
	$(document).on('click','a.btn.btn-reserevent',function(){
		var mensaje = "Requiero / Solicitola evento: "+$(this).data('nombre')+".Que comienza: "+$(this).data('timein')+" y finaleza"+$(this).data('timefin')+". Descripcion: "+$(this).data('texto')+".";
		$('#form-contactusevep #evep-Mensaje').text(mensaje);
	});
	
	//Validar formulariomodalpromo
	$("#modalPromocionespalmas,#modalEvepromocionalpalmas").on('hidden.bs.modal',function(){
		$('#form-contactuspromo input[type="text"],#modalEvepromocionalpalmas input[type="text"]').val('');
		$('#form-contactuspromo input[type="email"],#modalEvepromocionalpalmas input[type="email"]').val('');
		$('span.text-danger.pull-right').hide();
	});
	$("#promo-telefono,#evep-telefono").mask("+99-999-9999999");
	$("#promo-nombre,#promo-apellido,#evep-nombre,#evep-apellido").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			if (!regex.test(key) || ($(this).val()).length > 50) {
				event.preventDefault();
				return false;
			}
		}
	});
	$("#promo-telefono,#evep-telefono").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[0-9]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			if (!regex.test(key) || ($(this).val()).length > 14) {
				event.preventDefault();
				return false;
			}
		}
	});
	$("#promo-correo,#evep-correo").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-z0-9@_.-]$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			if (!regex.test(key) || ($(this).val()).length > 100) {
				event.preventDefault();
				return false;
			}
		}
	});
	
	$('#form-contactuspromo input,#form-contactuspromo textarea,#form-contactusevep input,#form-contactusevep textarea').bind('focusin focusout keyup',function(eventObject){
		if(!$(this).val() || ($(this).val().length<2)){
			$(this).addClass('invalid');
			$(this).removeClass('valid');
			$(this).siblings('.requerido').show();
		}else{
			$(this).removeClass('invalid');
			$(this).addClass('valid');
			$(this).siblings('.requerido').hide();
		}
	});
	$("#promo-telefono,#evep-telefono").bind('focusout keyup',function(eventObject){
		var expresion = /^(\+[0-9]{2})+(\-[0-9]{3})+(\-[0-9]{7})+$/i;				
		if(!expresion.test($(this).val())){
			$(this).addClass('invalid');
			$(this).removeClass('valid');
			if($(this).val().length == 0){
				$(this).siblings('.requerido').show();
				$(this).siblings('.invalido').hide();
			}else{
				$(this).siblings('.requerido').hide();
				$(this).siblings('.invalido').show();						
			}
		}else{
			$(this).removeClass('invalid');
			$(this).addClass('valid');
			$(this).siblings('.invalido').hide();
		}
	});
	$("#promo-correo,#evep-correo").bind('focusout keyup',function(eventObject){
		var expresion = /^[a-z0-9_\.\-]+(.[a-z0-9_\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
		if(!expresion.test($(this).val())){
			$(this).addClass('invalid');
			$(this).removeClass('valid');
			if($(this).val().length == 0){
				$(this).siblings('.requerido').show();
				$(this).siblings('.invalido').hide();
			}else{
				$(this).siblings('.requerido').hide();
				$(this).siblings('.invalido').show();						
			}
		}else{
			$(this).removeClass('invalid');
			$(this).addClass('valid');
			$(this).siblings('.invalido').hide();
		}
	});
	$('#form-contactuspromo textarea,#form-contactusevep textarea').bind('focusout keyup',function(eventObject){
		if($(this).val().length < $(this).attr('minlength')) {
			$(this).addClass('invalid');
			$(this).removeClass('valid');
			if($(this).val().length == 0){
				$(this).siblings('.requerido').show();
				$(this).siblings('.minlength').hide();
			}else{
				$(this).siblings('.requerido').hide();
				$(this).siblings('.minlength').show();						
			}
		} else {
			$(this).removeClass('invalid');
			$(this).siblings('.minlength').hide();
			$(this).addClass('valid');
		}					
	});	
	
	$('#form-contactuspromo input,#form-contactuspromo textarea').on('keyup blur', function (){
		if ($('#promo-nombre').hasClass('valid') && $('#promo-apellido').hasClass('valid') && $('#promo-correo').hasClass('valid') && $('#promo-telefono').hasClass('valid') && $('#promo-Mensaje').hasClass('valid')){
			$('#form-contactuspromo #btn-envpromo').prop('disabled', false);        
		} else {
			$('#form-contactuspromo #btn-envpromo').prop('disabled', 'disabled');   
		}
	});
	
	$('#form-contactusevep input,#form-contactusevep textarea').on('keyup blur', function (){
		if ($('#evep-nombre').hasClass('valid') && $('#evep-apellido').hasClass('valid') && $('#evep-correo').hasClass('valid') && $('#evep-telefono').hasClass('valid') && $('#evep-Mensaje').hasClass('valid')){
			$('#form-contactusevep #btn-evep').prop('disabled', false);        
		} else {
			$('#form-contactusevep #btn-evep').prop('disabled', 'disabled');   
		}
	});

	/*
	$('#form-contactuspromo #btn-envpromo').click(function() {
		var fd = new FormData();
		fd.append('nombre',$('#promo-nombre').val());
		fd.append('apellido',$('#promo-apellido').val());
		fd.append('email',$('#promo-correo').val());
		fd.append('telefono',$('#promo-telefono').val());
		fd.append('mensaje',$('#promo-Mensaje').val());
		fd.append('promo',$('input[name="promoradio"]:checked').val());
		
		//var url = window.location.href;
		//var url = window.location.href.substr(0,window.location.href.search("admin"));
		//var url = window.location.href;
		//var url1 = window.location.href.substr(0,window.location.href.search("contacto"));		
		$.ajax({
			type: "POST",
			url:(url + "contacto/sendemails"),
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: fd,
			processData: false,
			contentType: false,
			beforeSend:function(res){
			},
			success: function(res){
				console.log(res);
				if(res.resultado == 1){
				}else{
				}
			},
			error: function(res){
			}
		})
		return false;
	});	*/
	
	$('#descarga').on('click',function(){
		var rote = $('#descarga').data('route');
		$.get(rote,function() {
		})
	});
});