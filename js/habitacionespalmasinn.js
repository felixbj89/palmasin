jQuery(function ($) {
	$(document).ready(function (){
		if(target == "banner_restaurates" ){
			$('html, body').animate({scrollTop: ($('#'+target).offset().top)-10}, 1500);
		}
		var habitaciones = $('#habitypes>.carousel-inner>.item').length;
		$('.fdi-Carousel .item').each(function () {
			var next = $(this).next();
			if (!next.length) {
				next = $(this).siblings(':first');
			}
			next.children(':first-child').clone().appendTo($(this));

			if (next.next().length > 0) {
				next.next().children(':first-child').clone().appendTo($(this));
			}
			else {
				$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
			}
		});
		if(habitaciones == 1){
			$('#habitypes .item>div').addClass('unico');
			$('#habitypes .controls,#habitypes .carousel-control').addClass('unico');
		}

        /*$.each( galeria, function( key, val ) {
            localforage.setItem(key,val);
            $.each( val.labels.split('/'), function( i,label ) {
                labels += '<h5 class="d-inline-block mb-1 mr-1"><span class="delsaytag">'+label.toUpperCase()+'</span></h5>';
            });
            $("#"+key+">div[class*='col']:nth-child(1)>div").html(labels);
            $("#"+key+">div[class*='col']:nth-child(2)>p").text(val.littletitle.toUpperCase());
            $("#"+key+">div[class*='col']:nth-child(3)>h5").text(val.bigtitle.toUpperCase());
            $("#"+key+">div[class*='col']:nth-child(4)>p").text(val.date.toUpperCase());
            labels = "";
        });	*/	

	});
	$('#habitypes .left.carousel-control,#habitypes .habitype_right').on('click',function(){
		$('#habitypes').carousel('prev');
	});	
	$('#habitypes .right.carousel-control,#habitypes .habitype_left').on('click',function(){
		$('#habitypes').carousel('next');
	});
    $('#habitypes').on('slid.bs.carousel', function () {
		$('#habitypes>.onebyone-carosel .item>div[class*="col-"]').removeClass('select');
		if($(window).width()>767){
			$('#habitypes>.onebyone-carosel .item>div[class*="col-"]:nth-child(2),#accesos_habitaciones>div[class*="col-"]').addClass('select');
		}else{
			$('#habitypes>.onebyone-carosel .item>div[class*="col-"]:nth-child(1),#accesos_habitaciones>div[class*="col-"]').addClass('select');
		}		
		if($('#habitaciones').is( ":hidden" )){
			$("#habitaciones").habitajax();
		}else{
			//$('#habitaciones').slideUp('slow');
			$('#habitaciones').slideUp({duration: 1000, queue: false});
			//$('#habitaciones').collapse('hide');
			$("#habitaciones").habitajax();
		}
    });
	$('#habitaciones').on('show.bs.collapse',function(){
		$('#habitypes>.onebyone-carosel .item>div[class*="col-"]').removeClass('select');
		if($(window).width()>767){
			$('#habitypes>.onebyone-carosel .item>div[class*="col-"]:nth-child(2),#accesos_habitaciones>div[class*="col-"]').addClass('select');
		}else{
			$('#habitypes>.onebyone-carosel .item>div[class*="col-"]:nth-child(1),#accesos_habitaciones>div[class*="col-"]').addClass('select');
		}
    });
	$(document).on('click','#galeriabi .galchang_up',function(){
		$('#galeriabi').carousel('prev');
	});
	$(document).on('click','#galeriabi .galchang_bown',function(){
		$('#galeriabi').carousel('next');
	});	
	$(document).on('click','#habitaciones .row.row_gal:nth-child(2) div.p3 div.background_img,#habitaciones .row.row_gal #galeriabi .item',function(){
		//falta pasar slide
		var url = window.location.href.substr(0,window.location.href.search("habitaciones"))
		var habitacion = $('#habitypes .item.active>div[class*="col-"].select>a').data('item');
		var element = this;
		$.ajax({
			type:'GET',
			url: url+'/view/gallery/' + habitacion + "/" + btoa('2'),
			headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
			beforeSend:function(respuesta){
				//$('#eventos #loading').css('display','table');
			},
			success:function(res){
				var imagenes = JSON.parse(res.respuesta);
				$("#LightBox-indicators").empty();
				$("#BootstrapCarouse-innerlLightBox").empty();
				$.each(imagenes,function($key,$galeria){		
					if($galeria.bannergaleri_rutaimg!="img/default.jpg"){
						if($key==0){
							$("#BootstrapCarouse-innerlLightBox").append("<div class=\"item active\"><div><div><img src=\"" + url + $galeria.bannergaleri_rutaimg + "\"></div></div></div>");
							$("#LightBox-indicators").append("<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"" + $key  + "\" class=\"thumbnails active\" style=\"background-image:url('" + url + $galeria.bannergaleri_rutaimg + "')\"></li>");
						}else{
							$("#BootstrapCarouse-innerlLightBox").append("<div class=\"item\"><div><div><img src=\"" + url + $galeria.bannergaleri_rutaimg + "\"></div></div></div>");
							$("#LightBox-indicators").append("<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"" + $key  + "\" class=\"thumbnails\" style=\"background-image:url('" + url + $galeria.bannergaleri_rutaimg + "')\"></li>");
						}
					}
				});
				
				$('#modalGaleriapalmas').modal('show');
			},
			error:function(xhr,status){
				/*alert('Algo salio mal. Estamos trabajando en solucionarlo.');
				$('#eventos #loading').css('display','none');*/
			},
		});
		
		//$('#modalGaleriapalmas').modal('show');
	});
	
	$(document).on("click","#viewgale",function(){		
		var url = window.location.href.substr(0,window.location.href.search("habitaciones"))
		var habitacion = $('#habitypes .item.active>div[class*="col-"].select>a').data('item');
		var element = this;
		$.ajax({
			type:'GET',
			url: url+'/view/gallery/' + habitacion + "/" + btoa('2'),
			headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
			beforeSend:function(respuesta){
				//$('#eventos #loading').css('display','table');
			},
			success:function(res){
				var imagenes = JSON.parse(res.respuesta);
				$("#LightBox-indicators").empty();
				$("#BootstrapCarouse-innerlLightBox").empty();
				$.each(imagenes,function($key,$galeria){		
					if($galeria.bannergaleri_rutaimg!="img/default.jpg"){
						if($key==0){
							$("#BootstrapCarouse-innerlLightBox").append("<div class=\"item active\"><div><div><img src=\"" + url + $galeria.bannergaleri_rutaimg + "\"></div></div></div>");
							$("#LightBox-indicators").append("<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"" + $key  + "\" class=\"thumbnails active\" style=\"background-image:url('" + url + $galeria.bannergaleri_rutaimg + "')\"></li>");
						}else{
							$("#BootstrapCarouse-innerlLightBox").append("<div class=\"item\"><div><div><img src=\"" + url + $galeria.bannergaleri_rutaimg + "\"></div></div></div>");
							$("#LightBox-indicators").append("<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"" + $key  + "\" class=\"thumbnails\" style=\"background-image:url('" + url + $galeria.bannergaleri_rutaimg + "')\"></li>");
						}
					}
				});
				
				$('#modalGaleriapalmas').modal('show');
			},
			error:function(xhr,status){
				/*alert('Algo salio mal. Estamos trabajando en solucionarlo.');
				$('#eventos #loading').css('display','none');*/
			},
		});
	});
	
	$(document).on('click', 'a.btn-scroll', function(event){
		event.preventDefault();
		$('#habitypes>.onebyone-carosel .item>div[class*="col-"]').removeClass('select');
		if($(window).width()>767){
			$('#habitypes>.onebyone-carosel .item>div[class*="col-"]:nth-child(2),#accesos_habitaciones>div[class*="col-"]').addClass('select');
		}else{
			$('#habitypes>.onebyone-carosel .item>div[class*="col-"]:nth-child(1),#accesos_habitaciones>div[class*="col-"]').addClass('select');
		}
		$('#habitaciones').slideDown({duration: 2000, queue: false});
		$('html, body').animate({scrollTop: ($($.attr(this,'href')).offset().top)-10}, 1500);
	});
});
jQuery.fn.habitajax = function(){ 
		var url = window.location.href.substr(0,window.location.href.search("habitaciones"));
		url += 'habitaciones/habitajax';
		var element = this;
		var habitacion = $('#habitypes .item.active>div[class*="col-"].select>a').data('item');
		var loading = $('#habitypes .item.active>div[class*="col-"].select .loading');
		loading.show();
		$.ajax({
			type:'POST',
			url: url,
			headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
			data:{target:habitacion},
			success:function(res){
			    if(res.habitacion['nombre']=="Matrimonial Ejecutiva"){
			        if(res.locale=="en"){
			            $('#habitaciones #nombre').text("Matrimonial Executive");
			            $('#habitaciones #detalles').html('<ul><li>1 King Size bed</li><li>Breackfast included ( up to 2 pax max. )</li><li>Capacity of 1 to 2 pax max.</li><li>TV LCD 32"</li><li>Cable TV (80 Channels) </li><li>Executive Fridge</li><li>Hot Water</li><li>A/C with adjustable temperature</li<li>Wi-Fi (free)</li><li>1gbyte x min Lan Point<br><br></li></ul>');
			        }else{
			            $('#habitaciones #nombre').text(res.habitacion['nombre']);
			            $('#habitaciones #detalles').html(res.habitacion['detalles']);
			        }
			    }else if(res.habitacion['nombre']=="Doble Empresarial"){
			        if(res.locale=="en"){
			            $('#habitaciones #nombre').text("Business Double");
			            $('#habitaciones #detalles').html('<ul><li>2 Bed King Size</li><li>Breackfast included (up to 6 pax max)</li><li>Capacity of 1 to 6 pax max.</li><li>TV LCD 32"</li><li>Cable TV (80 Channels)</li><li>Executive Fridge</li><li>Hot Water</li><li>A/C with adjustable temperature</li><li>Wi-Fi (free)</li><li>1gbyte x min Lan Point<br><br></li></ul>');
			        }else{
			            $('#habitaciones #nombre').text(res.habitacion['nombre']);
			            $('#habitaciones #detalles').html(res.habitacion['detalles']);
			        }
			    }else if(res.habitacion['nombre']=="Matrimonial Estándar"){
			        if(res.locale=="en"){
			            $('#habitaciones #nombre').text("Standar Matrimonial");
			            $('#habitaciones #detalles').html('<ul><li>1 King Size Bed</li><li>Breackfast included ( up to 2 pax max. )</li><li>Capacity of 1 to 2 pax max.</li><li>TV LCD 32"</li><li>Cable TV (80 Channels)</li><li>Executive Fridge</li><li>Hot Water</li><li>A/C with adjustable temperature</li><li>Wi-Fi (free)</li><li>1gbyte x min Lan Point<br><br></li></ul>');
			        }else{
			            $('#habitaciones #nombre').text(res.habitacion['nombre']);
			            $('#habitaciones #detalles').html(res.habitacion['detalles']);
			        }
			    }else if(res.habitacion['nombre']=="Doble Estándar"){
			        if(res.locale=="en"){
			            $('#habitaciones #nombre').text("Double Standar");
			            $('#habitaciones #detalles').html('<ul><li>2 Double Beds</li><li>Breackfast included ( up to 4 pax max. )</li><li>Capacity of 1 to 4 pax max.</li><li>TV LCD 32"</li><li>Cable TV (80 Channels)</li><li>Executive Fridge</li><li>Hot Water</li><li>A/C with adjustable temperature</li><li>Wi-Fi (free)</li><li>1gbyte x min Lan Point<br><br></li></ul>');
			        }else{
			            $('#habitaciones #nombre').text(res.habitacion['nombre']);
			            $('#habitaciones #detalles').html(res.habitacion['detalles']);
			        }
			    }else if(res.habitacion['nombre']=="Suite Familiar"){
			        if(res.locale=="en"){
			            $('#habitaciones #nombre').text("Suite Familiar");
			            $('#habitaciones #detalles').html('<ul><li>1 King Size bed + 1 Sofa bed</li><li>Breackfast included ( up to 8 pax max. )</li><li>Capacity of 1 to 8 pax max.</li><li>TV LCD 32"</li><li>Cable TV (80 Channels)</li><li>Executive Fridge</li><li>Hot Water</li><li>A/C with adjustable temperature</li><li>Wi-Fi (free)</li><li>1gbyte x min Lan Point<br><br></li></ul>');
			        }else{
			            $('#habitaciones #nombre').text(res.habitacion['nombre']);
			            $('#habitaciones #detalles').html(res.habitacion['detalles']);
			        }
			    }else if(res.habitacion['nombre']=="Empresarial Sencilla"){
			        if(res.locale=="en"){
			            $('#habitaciones #nombre').text("Simple Business");
			            $('#habitaciones #detalles').html('<ul><li>2 King Size Beds</li><li>Breackfast included ( up to 6 pax max. )</li><li>Capacity of 1 to 6 pax max.</li><li>TV LCD 32"</li><li>Cable TV (80 Channels)</li><li>Executive Fridge</li><li>Hot Water</li><li>A/C with adjustable temperature</li><li>Wi-Fi (free)</li><li>1gbyte x min Lan Point<br><br></li></ul>');
			        }else{
			            $('#habitaciones #nombre').text(res.habitacion['nombre']);
			            $('#habitaciones #detalles').html(res.habitacion['detalles']);
			        }
			    }else if(res.habitacion['nombre']=="Ejecutiva Sencilla"){
			        if(res.locale=="en"){
			            $('#habitaciones #nombre').text("Simple Executive");
			            $('#habitaciones #detalles').html('<ul><li>2 Double Beds</li><li>Breackfast included ( up to 4 pax max. )</li><li>Capacity of 1 to 4 pax max.</li><li>TV LCD 32"</li><li>Cable TV (80 Channels)</li><li>Executive Fridge</li><li>Hot Water</li><li>A/C with adjustable temperature</li><li>Wi-Fi (free)</li><li>1gbyte x min Lan Point<br><br></li></ul>');
			        }else{
			            $('#habitaciones #nombre').text(res.habitacion['nombre']);
			            $('#habitaciones #detalles').html(res.habitacion['detalles']);
			        }
			    }else if(res.habitacion['nombre']=="Suite Estándar"){
			        if(res.locale=="en"){
			            $('#habitaciones #nombre').text("Standar Suite");
			            $('#habitaciones #detalles').html('<ul><li>1 King Size bed + 1 Sofa bed</li><li>Breackfast included ( up to 5 pax max. )</li><li>Capacity of 1 to 5 pax max.</li><li>TV LCD 32"</li><li>Cable TV (80 Channels)</li><li>Executive Fridge</li><li>Hot Water</li><li>A/C with adjustable temperature</li><li>Wi-Fi (free)</li><li>1gbyte x min Lan Point<br><br></li></ul>');
			        }else{
			            $('#habitaciones #nombre').text(res.habitacion['nombre']);
			            $('#habitaciones #detalles').html(res.habitacion['detalles']);
			        }
			    }
			    
			        if(res.locale=="en"){
			          $('#habitaciones #descripcion').html("Comfortable room for the enjoyment of all types of guests"); 
			        }else{
				  $('#habitaciones #descripcion').html(res.habitacion['descripcion']);
				}
				
				$('#habitaciones #comodidades').html(res.habitacion['comodidades']);
				$("#habitaciones").attr("style","position: relative;margin-bottom: 1em;");
				if(res.itemsgal == ""){
					$('#habitaciones #galeriabi>div.carousel-inner').html("<div class=\"text-center item active\"><i class=\"fa fa-picture-o fa-4x\" aria-hidden=\"true\"></i><p class=\"titulo text-center vacio\">NO HAY GALERIA PARA MOSTRAR</p></div>");
					$('#destacados').html("");
					$('#galeriabi .controls').hide();
					$('#viewgale').prop('disabled', 'disabled');
				}else{
					$('#habitaciones #galeriabi>div.carousel-inner').html(res.itemsgal);
					$('#destacados').html(res.destacados);
					$('#galeriabi .controls').show();
					$('#viewgale').prop('disabled', false);
					$('#LightBox-indicators').html(res.modal.indicators);
					$('#BootstrapCarouse-innerlLightBox').html(res.modal.items);
				}
				loading.hide();
				$('#habitaciones').slideDown({duration: 2000, queue: false});
				//$('#habitaciones').collapse('show');
				//setTimeout(function(){ $('.collapse').collapse('show'); }, 3000);
			},
			error:function(xhr,status){
			},
		});
};