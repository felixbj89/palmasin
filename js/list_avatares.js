var pic1 = undefined;

function openFile1(event){
  pic1 = event;
}

$(document).ready(function(){
	var tablaHistorial = $("#historial_list").DataTable({
	"searching": true,
	"bInfo": false,
	"Info":false,
	"bLengthChange": false,
	"iDisplayLength": 1,
	"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
	},

	"fixedHeader": {
		"header": true,
		"footer": true
	},
	"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});

	if(tablaHistorial.rows().data().length==0){
		$("#modal_empty").modal("show");
	}

	if($("#server_perfil").val()!=undefined){
		$("#perfiladvertencia").text($("#server_perfil").val());
		$("#modal_alerta").modal("show");
	}

	if($("#server").val()!=undefined){
		$("#avataradvertencia").text($("#server").val());
		$("#alerta_avatar").modal("show");
	}

	$("#btnAgregar").on("click",function(){
		if(pic1==undefined){
			$("#avataradvertencia").text("El campo es obligatorio, ingrese una imagen.");
			$("#alerta_avatar").modal("show");
		}else{
			$("#form-avatar").submit();
		}
	});

	var seleccion = undefined; var historial = undefined;
	$("#btnAceptar").on("click",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$("#modal_confirmarremover").modal("hide");
		$.ajax({
			url: url + "admin/delete_aremover",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"historial":historial["avatar_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
					tablaHistorial.row(seleccion).remove().draw();
					if(tablaHistorial.rows().data().length==0){
						$("#modal_empty").modal("show");
					}
				}else{
					$("#avataradvertencia").text("El avatar se encuentra en uso.");
					$("#alerta_avatar").modal("show");
				}
			}
		});
	});

	$(document).on("click","#btnUsarAvatar",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var historial = $(this).parents("tr").data("historial");;
		$.ajax({
			url: url + "admin/estadoavatares",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"historial":historial["avatar_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
					$("#useravatar").attr("src",respuesta.path);
					$("#usersubavatar").attr("src",respuesta.path);
					$("#sidebaravatar").attr("src",respuesta.path);
					$("#avataractive").attr("src",respuesta.path);
				}else if(respuesta.estado==2){
					$("#avataradvertencia").text("El avatar ya se encuentra en uso.");
					$("#alerta_avatar").modal("show");
				}
			}
		});
	});

	$(document).on("click","#btnRemoverAvatar",function(){
		$("#confirmarmensajeadvertencia").text("¿Esta usted seguro de remover el avatar seleccionado?");
		$("#modal_confirmarremover").modal("show");
		seleccion = $(this).parents("tr");
		historial = $(this).parents("tr").data("historial");
	});

	var _URL = window.URL || window.webkitURL;
	$("#file_one").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
      img.onload = function(){
          if((img.width!=160 && img.width!=256) && (img.height!=160 && img.height!=256)){
            $("#avataradvertencia").text("No cumple con las medidas recomendadas.");
    				$("#alerta_avatar").modal("show");
            pic1 = undefined;
          }
      }

			img.onerror = function() {
				$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				$("#alerta_avatar").modal("show");
			};

			img.src = _URL.createObjectURL(file);
			$("#uno_picture").attr("src",img.src);
		}
	});

	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
    fileMaxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#avataradvertencia").text(text);
				$("#alerta_avatar").modal("show");
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback();
					pic1 = undefined;
					$("#modal_confirmar").modal("hide");
					$("#uno_picture").attr("src",$("#url1").val());
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
});
