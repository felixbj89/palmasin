var pic1 = undefined;
var pic2 = undefined;
var pic3 = undefined;
var pic4 = undefined;
var pic5 = undefined;
var pic6 = undefined;

function openFile1(event){
  pic1 = event;
}

function openFile2(event){
  pic2 = event;
}

function openFile3(event){
  pic3 = event;
}

function openFile4(event){
  pic4 = event;
}

function openFile5(event){
  pic5 = event;
}

function openFile6(event){
  pic6 = event;
}

$(document).ready(function(){
	if($("#server_galeria").val()!=undefined){
		$("#galeriaadvertencia").text($("#server_galeria").val());
		$("#alerta_galeria").modal("show");
	}

	$("#btnRegistrar").on("click",function(){
		if(pic1==undefined && pic2==undefined && pic3==undefined && pic4==undefined && pic5==undefined && pic6==undefined){
			$("#galeriaadvertencia").text("Debe ingresar una galería al sistema.");
			$("#alerta_galeria").modal("show");
		}else{
			$("#form-galeriahome").submit();
		}
	})
		
	$("#btnAsociar").on("click",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var asociar = $(this).data("promociones");
		$.ajax({
			url: url + "admin/asociargaleriahome",
			headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"promociones":asociar,
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);
			}
		});
	});

	$("#btnEditar").on("click",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$.ajax({
			url: url + "admin/viewgaleriahome",
			headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);
			}
		});
	});

	var _URL = window.URL || window.webkitURL;
	$("#file_one").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onerror = function() {
				$("#galeriaadvertencia").text(text);
				$("#alerta_galeria").modal("show");
			};

			img.src = _URL.createObjectURL(file);
			$("#uno_picture").attr("src",img.src);
		}
	});

	$("#file_one").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#galeriaadvertencia").text(text);
				$("#alerta_galeria").modal("show");
				
				$("#uno_picture").attr("src",$("#url1").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				console.log(text);
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback();
					pic1 = undefined;
					$("#modal_confirmar").modal("hide");
					$("#uno_picture").attr("src",$("#url1").val());
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_two").on("change",function(){//CARGO LA SEGUNDA IMAGEN
		if(pic2!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#galeriaadvertencia").text(text);
					$("#alerta_galeria").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#dos_picture").attr("src",img.src);
			}
		}
	});

	$("#file_two").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#galeriaadvertencia").text(text);
				$("#alerta_galeria").modal("show");
				
				$("#dos_picture").attr("src",$("#url2").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic2 = undefined;
					$("#dos_picture").attr("src",$("#url2").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_thrid").on("change",function(){//CARGO LA TERCERA IMAGEN
		if(pic3!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#galeriaadvertencia").text(text);
					$("#alerta_galeria").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#tres_picture").attr("src",img.src);
			}
		}
	});

	$("#file_thrid").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#galeriaadvertencia").text(text);
				$("#alerta_galeria").modal("show");
				
				$("#tres_picture").attr("src",$("#url3").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic3 = undefined;
					$("#tres_picture").attr("src",$("#url3").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_four").on("change",function(){//CARGO LA CUARTA IMAGEN
		if(pic4!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#galeriaadvertencia").text(text);
					$("#alerta_galeria").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#cuatro_picture").attr("src",img.src);
			}
		}
	});

	$("#file_four").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#galeriaadvertencia").text(text);
				$("#alerta_galeria").modal("show");
				
				$("#cuatro_picture").attr("src",$("#url4").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic4 = undefined;
					$("#cuatro_picture").attr("src",$("#url4").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_five").on("change",function(){//CARGO LA QUINTA IMAGEN
		if(pic5!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#galeriaadvertencia").text(text);
					$("#alerta_galeria").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#cinco_picture").attr("src",img.src);
			}
		}
	});

	$("#file_five").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#galeriaadvertencia").text(text);
				$("#alerta_galeria").modal("show");
				
				$("#cinco_picture").attr("src",$("#url5").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic5 = undefined;
					$("#cinco_picture").attr("src",$("#url5").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});

	$("#file_six").on("change",function(){//CARGO LA QUINTA IMAGEN
		if(pic6!=undefined){
			var confirmar = 0;
			if ((file = this.files[0])) {
				img = new Image();
				img.onerror = function() {
					$("#galeriaadvertencia").text("Extensiones permitidas jpg, png");
					$("#alerta_galeria").modal("show");
				};

				img.src = _URL.createObjectURL(file);
				$("#seis_picture").attr("src",img.src);
			}
		}
	});

	$("#file_six").filer({
		limit:1,
		extensions:['jpg','png'],
		showThumbs:false,
		maxSize:5,
		dialogs: {
			// alert dialog
			alert: function(text) {
				$("#galeriaadvertencia").text(text);
				$("#alerta_galeria").modal("show");
				
				$("#seis_picture").attr("src",$("#url6").val());
			},

			// confirm dialog
			confirm: function(text, callback) {
				$("#confirmarimageadvertencia").text(text);
				$("#modal_confirmar").modal("show");
				$("#btnRemoverPicture").on("click",function(){
					callback()
					pic6 = undefined;
					$("#seis_picture").attr("src",$("#url6").val());
					$("#modal_confirmar").modal("hide");
				});
			}
		},
		captions:{
			button: "Subir Imagen",
			feedback: "Suba una Imagen",
			feedback2: "Imagen sea seleccionado",
			drop: "Drop file here to Upload",
			removeConfirmation: "Está usted seguro de remover la imagen?",
			errors: {
				filesLimit: "Solo es permitido subir una imagen.",
				filesType: "Extensiones permitidas {{fi-extensions}}",
				filesSize: "{{fi-name}} Por favor intente con otra imagen, Peso máximo {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		}
	});
});
