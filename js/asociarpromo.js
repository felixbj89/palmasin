var PromosAsociadas = function(path,slider){
	this.ruta = path;
	this.imagen = slider;
}

$(document).ready(function(){
	var path = undefined; var slider = undefined;
	var asociaciones = [];
	$("#promocion_seleccionada").on("change, click",function(){
		if($(this).val()!="NULL"){
			path = $(this).val();
			$("#uno_picture").attr("src",$("#urlpath").val() + "/img/home/promociones/" + path);
		}
	});

	$(document).on("click","#btnAsociarSlider",function(){
		slider = $(this).parents("div").data("chooseslider");
		if(path==undefined){
			$("#asociaciongaleriaadvertencia").text("Debe seleccionar una promoción antes de poder asociar.");
			$("#modal_alerta").modal("show");
		}else{
			$("#asociaciongaleriaadvertencia").text("Asociación Satisfactoria.");
			$("#modal_alerta").modal("show");
			if($("#promocion_seleccionada option").length==1){
				$("#asociaciongaleriaadvertencia").text("Todas las promociones ya han sido asociadas.");
				$("#modal_alerta").modal("show");
			}else{
				asociaciones.push(new PromosAsociadas(path,slider));
				$("#promocion_seleccionada option[value='" + path + "']").remove();
				if($("#promocion_seleccionada option").length==1){
					$("#promocion_seleccionada").prop("disabled",true);
				}
			}
		}
	});
	
	if($("#promocion_seleccionada")[0].length==1){
		$("#empty_asohechas").modal("show");
	}
	
	$("#btnModificarGaleria").on("click",function(){
		if(path==undefined && slider==undefined){
			$("#asociaciongaleriaadvertencia").text("Debe seleccionar una asociación antes de poder guardar.");
			$("#modal_alerta").modal("show");
		}else if(path==undefined){
			$("#asociaciongaleriaadvertencia").text("Debe seleccionar una promoción antes de poder guardar.");
			$("#modal_alerta").modal("show");
		}else if(slider==undefined){
			$("#asociaciongaleriaadvertencia").text("Debe seleccionar una imagen antes de poder guardar.");
			$("#modal_alerta").modal("show");
		}else{//SUBMIT GENERAL
			$("#asociados").val(JSON.stringify(asociaciones));
			$("#form-asociaciones").submit();
			asociaciones.length = 0;
		}
	});
	
	$("#btnGuardarAsociación").on("click",function(){
		if(path==undefined && slider==undefined){
			$("#asociaciongaleriaadvertencia").text("Debe seleccionar una asociación antes de poder guardar.");
			$("#modal_alerta").modal("show");
		}else if(path==undefined){
			$("#asociaciongaleriaadvertencia").text("Debe seleccionar una promoción antes de poder guardar.");
			$("#modal_alerta").modal("show");
		}else if(slider==undefined){
			$("#asociaciongaleriaadvertencia").text("Debe seleccionar una imagen antes de poder guardar.");
			$("#modal_alerta").modal("show");
		}else{//SUBMIT GENERAL
			$("#asociados").val(JSON.stringify(asociaciones));
			$("#form-asociaciones").submit();
			asociaciones.length = 0;
		}
	});
});
