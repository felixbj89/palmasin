$(document).ready(function(){
	var dispositivos = JSON.parse($("#dispositivosusados").val());
	var navegadores = JSON.parse($("#browser").val());
	var secciones = JSON.parse($("#secciones").val());
	var meses = JSON.parse($("#meses").val());
	var visitas = JSON.parse($("#visitas").val());
	var paises = JSON.parse($("#paises").val());
	
	//--------------
	//- VISITAS REALIZADAS -
	//--------------

	// Get context with jQuery - using jQuery's .get() method.
	var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
	// This will get the first returned node in the jQuery collection.
	var areaChart = new Chart(areaChartCanvas);

	var areaChartData = {
	  labels: meses,
	  datasets: [
		{
		  label: "Digital Goods",
		  fillColor: "rgba(60,141,188,0.9)",
		  strokeColor: "rgba(60,141,188,0.8)",
		  pointColor: "#3b8bba",
		  pointStrokeColor: "rgba(60,141,188,1)",
		  pointHighlightFill: "#fff",
		  pointHighlightStroke: "rgba(60,141,188,1)",
		  data: visitas
		}
	  ]
	};

	var areaChartOptions = {
	  //Boolean - If we should show the scale at all
	  showScale: true,
	  //Boolean - Whether grid lines are shown across the chart
	  scaleShowGridLines: false,
	  //String - Colour of the grid lines
	  scaleGridLineColor: "rgba(0,0,0,.05)",
	  //Number - Width of the grid lines
	  scaleGridLineWidth: 1,
	  //Boolean - Whether to show horizontal lines (except X axis)
	  scaleShowHorizontalLines: true,
	  //Boolean - Whether to show vertical lines (except Y axis)
	  scaleShowVerticalLines: true,
	  //Boolean - Whether the line is curved between points
	  bezierCurve: true,
	  //Number - Tension of the bezier curve between points
	  bezierCurveTension: 0.3,
	  //Boolean - Whether to show a dot for each point
	  pointDot: false,
	  //Number - Radius of each point dot in pixels
	  pointDotRadius: 4,
	  //Number - Pixel width of point dot stroke
	  pointDotStrokeWidth: 1,
	  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
	  pointHitDetectionRadius: 20,
	  //Boolean - Whether to show a stroke for datasets
	  datasetStroke: true,
	  //Number - Pixel width of dataset stroke
	  datasetStrokeWidth: 2,
	  //Boolean - Whether to fill the dataset with a color
	  datasetFill: true,
	  //String - A legend template
	  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
	  //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	  maintainAspectRatio: true,
	  //Boolean - whether to make the chart responsive to window resizing
	  responsive: true
	};

	//Create the line chart
	areaChart.Line(areaChartData, areaChartOptions);
	
	//DISPOSITIVOS
	var areaChartData = {
      labels: ["Secciones Visitadas"],
      datasets: [
        {
          label: "Inicio/Home",
          fillColor: "#ff0969",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [secciones[0]]
        },
        {
          label: "Habitaciones",
          fillColor: "#2840ab",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#308bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [secciones[1]]
        },
		{
          label: "El Hotel",
          fillColor: "#92A000",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#308bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [secciones[2]]
        },
		{
          label: "Restaurante",
          fillColor: "#9FF800",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#308bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [secciones[3]]
        },
		{
          label: "Booking",
          fillColor: "#F88000",
          strokeColor: "rgba(60,141,18,0.8)",
          pointColor: "#308bba",
          pointStrokeColor: "rgba(60,141,18,0.8)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,18,0.8)",
          data: [secciones[4]]
        }
      ]
    };
	
	//- BAR CHART -
    //-------------
    var barChartCanvas = $("#seccionesvistas").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
	
	//DISPOSITIVOS
	var areaChartData = {
      labels: ["Dispositivos Usados"],
      datasets: [
        {
          label: "Computador",
          fillColor: "#ff0969",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [dispositivos[0]]
        },
        {
          label: "Móvil",
          fillColor: "#2840ab",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#308bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [dispositivos[1]]
        },
		{
          label: "Tablet",
          fillColor: "#9FF000",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#308bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [dispositivos[2]]
        },
      ]
    };
	
	//- BAR CHART -
    //-------------
    var barChartCanvas = $("#devices").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
	
	var areaChartData = {
      labels: ["Navegadores Usados"],
      datasets: [
        {
          label: "Firefox",
          fillColor: "#f56954",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [navegadores[0]]
        },
        {
          label: "Chrome",
          fillColor: "#f39c12",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [navegadores[1]]
        },
		{
          label: "IE",
          fillColor: "#AB00ED",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [navegadores[2]]
        },
        {
          label: "Safari",
          fillColor: "#ABCAED",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [navegadores[3]]
        }
      ]
    };
	
	//- BAR CHART -
    //-------------
    var barChartCanvas = $("#navegadores").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
	
	$(document).on("click","#irpromocion",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var promociones_id = $(this).data("id");
		$.ajax({
			url: url + "admin/viewpromocioneshome",
			headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"promociones":promociones_id,
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);
			}
		});
	});
	
	//PAISES
	//-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
	if(paises[0]!=0 || paises[1]!=0){
		var areaChartData = {
		labels: ["Paises registrados"],
			datasets: [
				{
				  label: "Venezuela",
				  fillColor: "#ff0969",
				  strokeColor: "rgba(210, 214, 222, 1)",
				  pointColor: "rgba(210, 214, 222, 1)",
				  pointStrokeColor: "#c1c7d1",
				  pointHighlightFill: "#fff",
				  pointHighlightStroke: "rgba(220,220,220,1)",
				  data: [paises[0]]
				},
				{
				  label: "Alemania",
				  fillColor: "#2840ab",
				  strokeColor: "rgba(60,141,188,0.8)",
				  pointColor: "#308bba",
				  pointStrokeColor: "rgba(60,141,188,1)",
				  pointHighlightFill: "#fff",
				  pointHighlightStroke: "rgba(60,141,188,1)",
				  data: [paises[1]]
				},
				{
				  label: "EstadoS Unidos",
				  fillColor: "#2840ab",
				  strokeColor: "rgba(60,141,188,0.8)",
				  pointColor: "#308bba",
				  pointStrokeColor: "rgba(60,141,188,1)",
				  pointHighlightFill: "#fff",
				  pointHighlightStroke: "rgba(60,141,188,1)",
				  data: [paises[2]]
				}
			]
		};

		//- BAR CHART -
		//-------------
		var barChartCanvas = $("#pieChartpaises").get(0).getContext("2d");
		var barChart = new Chart(barChartCanvas);
		var barChartData = areaChartData;
		barChartData.datasets[1].fillColor = "#00a65a";
		barChartData.datasets[1].strokeColor = "#00a65a";
		barChartData.datasets[1].pointColor = "#00a65a";
		var barChartOptions = {
		//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		scaleBeginAtZero: true,
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines: true,
		//String - Colour of the grid lines
		scaleGridLineColor: "rgba(0,0,0,.05)",
		//Number - Width of the grid lines
		scaleGridLineWidth: 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,
		//Boolean - If there is a stroke on each bar
		barShowStroke: true,
		//Number - Pixel width of the bar stroke
		barStrokeWidth: 2,
		//Number - Spacing between each of the X value sets
		barValueSpacing: 5,
		//Number - Spacing between data sets within X values
		barDatasetSpacing: 1,
		//String - A legend template
		legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
		//Boolean - whether to make the chart responsive
		responsive: true,
		maintainAspectRatio: true
		};

		barChartOptions.datasetFill = false;
		barChart.Bar(barChartData, barChartOptions);
	}
	
	if($("#server").val()!=undefined){
		$("#avataradvertencia").text($("#server").val());
		$("#alerta_avatar").modal("show");
	}
	
	var tablaUsersHistorial = $("#users_list").DataTable({
		"searching": true,
		"bInfo": false,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 3,
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
			"header": true,
			"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	var tablaUsersHistorial = $("#promociondash_list").DataTable({
		"searching": true,
		"bInfo": false,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 1,
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
			"header": true,
			"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	var tablaHistorial = $("#avatares_list").DataTable({
		"searching": false,
		"bInfo": false,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 1,
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
			"header": true,
			"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	$(document).on("click","#btnUsarAvatar",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var historial = $(this).parents("tr").data("historial");;
		$.ajax({
			url: url + "admin/estadoavatares",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"historial":historial["avatar_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
					$("#useravatar").attr("src",respuesta.path);
					$("#usersubavatar").attr("src",respuesta.path);
					$("#sidebaravatar").attr("src",respuesta.path);
					$("#avataractive").attr("src",respuesta.path);
				}else if(respuesta.estado==2){
					$("#avataradvertencia").text("El avatar ya se encuentra en uso.");
					$("#alerta_avatar").modal("show");
				}
			}
		});
	});
});