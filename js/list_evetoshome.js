$(document).ready(function(){
	$("#inicio_evento").flatpickr({
		"minDate":$("#mindate").val(),
		enableTime: true,
		enableSeconds:false,
		time_24hr: false,
		noCalendar: false,
	});
	
	$("#fin_evento").flatpickr({
		"minDate":$("#mindate").val(),
		enableTime: true,
		enableSeconds:false,
		time_24hr: false,
		noCalendar: false,
	});
	$("#inicio_evento").on('change', function (){//cambio de fecha
		if ((Date.parse($("#inicio_evento").val()) > Date.parse($("#fin_evento").val())) || $("#fin_evento").val().length == 0){
			$("#fin_evento").val($("#inicio_evento").val());	
		}
	});
	$("#fin_evento").on('change', function (){//cambio de fecha
		if ((Date.parse($("#inicio_evento").val()) > Date.parse($("#fin_evento").val())) || $("#fin_evento").val().length == 0){
			$("#fin_evento").val($("#inicio_evento").val());	
		}
	});
	
	var tablaHistorial = $("#event_list").DataTable({
	"searching": true,
	"bInfo": false,
	"Info":false,
	"bLengthChange": false,
	"iDisplayLength": true,
	"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
	},

	"fixedHeader": {
		"header": true,
		"footer": true
	},
	"bPaginate": false,
	"responsive":true,
	"bAutoWidth": false,
	});
	
	if(tablaHistorial.rows().data().length==0){
		$("#modal_empty").modal("show");
	}
	if($("#server_eventos").val()!=undefined){
		$("#editeventosadvertencia").text($("#server_eventos").val());
		$("#modal_alerta").modal("show");
	}
});
$('table').on('click','.btnActivar',function(){
	var elemen = this;
	var historial = $(elemen).closest('tr').data('historial')
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	var i = $(this).children('i');
	$.ajax({
		url: url + "admin/ppalevento",
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:{
			"evento":historial["evento"],
		},
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.estado==0){
				$(i).removeClass('fa-toggle-on');
				$(i).addClass('fa-toggle-off');
			}else if(respuesta.estado==1){
				$('.btnActivar i').removeClass('fa-toggle-on');
				$('.btnActivar i').addClass('fa-toggle-off');
				$(i).removeClass('fa-toggle-off');
				$(i).addClass('fa-toggle-on');
			}else if(respuesta.estado==2){
				$("#editeventosadvertencia").text("Algo salio mal. No se pudo registrar el evento.");
				$("#modal_alerta").modal("show");
			}
		},error:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#editeventosadvertencia").text("Algo salio mal. No se pudo registrar el evento.");
			$("#modal_alerta").modal("show");
		}
	});
});

$('table').on('click','.btnModificar',function(){
	var elemen = this;
	var historial = $(elemen).closest('tr').data('historial');
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	$.ajax({
		url: url + "admin/editadoeventohome",
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:{
			"evento":historial["evento"],
		},
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#row_edit").empty();
			$("#row_edit").html(respuesta);				
		},error:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#editeventosadvertencia").text("Algo salio mal. No se pude modificar el evento.");
			$("#modal_alerta").modal("show")
		}
	});	
});

$('table').on('click','.btnModificarCorporativos',function(){
	var elemen = this;
	var historial = $(elemen).closest('tr').data('historial');
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	$.ajax({
		url: url + "admin/editadoeventocorporativo",
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:{
			"evento":historial["evento"],
		},
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#row_edit").empty();
			$("#row_edit").html(respuesta);				
		},error:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#editeventosadvertencia").text("Algo salio mal. No se pude modificar el evento.");
			$("#modal_alerta").modal("show")
		}
	});	
});

$('table').on('click','.btnRemover',function(){
	var elemen = this;
	var historial = $(elemen).closest('tr').data('historial')
	var table = $('#event_list').DataTable();
	var tr = $(elemen).closest('tr');
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	$("#confirmarmensajeadvertencia").text('Quieres borrar este Evento? ');
	$("#modal_confirmarremover").modal("show");
	$("#btnAceptar").on("click",function(){
		$("#modal_confirmarremover").modal("hide");
		$.ajax({
			url: url + "admin/eraseventohome",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"evento":historial["evento"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				if(respuesta.estado==1){
					table.row(tr).remove().draw();
					if(table.rows().data().length==0){
						$("#modal_empty").modal("show");
					}
				
					$("#modal_wait").modal("hide");
					$("#editeventosadvertencia").text("Evento borrado con exito.");
					$("#modal_alerta").modal("show")
				}else{
					$("#modal_wait").modal("hide");
					$("#editeventosadvertencia").text("Algo salio mal. No se pude borrar el evento.");
					$("#modal_alerta").modal("show")
				}		
			},error:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#editeventosadvertencia").text("Algo salio mal. No se pude borrar el evento.");
				$("#modal_alerta").modal("show")
			}
		});
	});
});
$('table').on('click','.btnclone',function(){
	var elemen = this;
	var historial = $(elemen).closest('tr').data('historial');
	$('#modal_clonarevento').modal('show');
	$('#evento_target').val(historial["evento"]);
});
$('ul.list-year>li>button').on('click',function(){
	$('ul.list-year>li>button').removeClass('active');
	$(this).addClass('active');
	$.fn.Ajaxlistmesyear();
});
$('ul.list-mes>li>button').on('click',function(){
	$('ul.list-mes>li>button').removeClass('active');
	$(this).addClass('active');
	$.fn.Ajaxlistmesyear();
});
$(document).on('click','#btnclonar',function(){
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	if($('#evento_target').val().length > 0){
		if($('#inicio_evento').val().length > 0 && $('#fin_evento').val().length > 0){
			$("#modal_clonarevento").modal("hide");
			$.ajax({
				url: url + "admin/cloneventohome",
				headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:"post",
				data:{
					"evento":$('#evento_target').val(),
					"inicio":$('#inicio_evento').val(),
					"fin":$('#fin_evento').val()
				},
				beforeSend:function(respuesta){
					$("#modal_wait").modal("show");
				},
				success:function(respuesta){
					if(respuesta.estado==1){
						$("#modal_wait").modal("hide");
						$("#editeventosadvertencia").text("Evento copiado con exito.");
						$("#modal_alerta").modal("show");
						$('#evento_target').val('');
					}else{
						$("#modal_wait").modal("hide");
						$("#editeventosadvertencia").text("Algo salio mal. No se pudo copiar el evento.");
						$("#modal_alerta").modal("show");
						$('#evento_target').val('');
					}		
				},error:function(respuesta){
					$("#modal_wait").modal("hide");
					$("#editeventosadvertencia").text("Algo salio mal. No se pude copiar el evento.");
					$("#modal_alerta").modal("show");
					$('#evento_target').val('');
				}
			});
		}else{
			$("#modal_clonarevento").modal("hide");
			$("#editeventosadvertencia").text("Los campos son obligatorios, coloca una fecha valida.");
			$("#modal_alerta").modal("show");
			$('#evento_target').val('');
		}
	}else{
		$("#modal_clonarevento").modal("hide");
		$("#editeventosadvertencia").text("Algo salio mal. Intentalo de nuevo.");
		$("#modal_alerta").modal("show");
		$('#evento_target').val('');
	}	
});
$.fn.Ajaxlistmesyear = (function(){
	var table = $('#event_list').DataTable();
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	$.ajax({
		url: url + "admin/listmesyeareventohome",
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:{
			"mes":$('ul.list-mes>li>button.active').data('mes'),
			"year":$('ul.list-year>li>button.active').data('year'),
		},
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");			
			table.clear().draw();
			$.each(respuesta, function (clave,valor){
				if(valor['event_ppal'] == 0){
					table.row.add($("<tr class=\"fondo-tr odd\" data-historial='{\"evento\":\""+valor['id']+"\"}' role=\"row\"><td tabindex=\"0\" class=\"sorting_1\">"+valor['event_title']+"</td><td>"+valor['event_dateini']+"</td><td><div class=\"row\"><div class=\"col-xs-12\"><a class=\"btnActivar\"><i class=\"fa fa-toggle-off fa-2x\"></i></a></div></div></td><td><div class=\"row form-group position-right\"><div class=\"col-xs-12 col-sm-12 col-md-4\"><a class=\"btn btn-app btn-palmasinn btnModificar\"><i class=\"fa fa-pencil-square-o\"></i> MODIFICAR</a></div><div class=\"col-xs-12 col-sm-12 col-md-4\"><a class=\"btn btn-app btn-palmasinn btnRemover\"><i class=\"fa fa-trash\"></i> REMOVER</a></div><div class=\"col-xs-12 col-sm-12 col-md-4\"><a class=\"btn btn-app btn-palmasinn btnclone center\"><i class=\"fa fa-clone\"></i>COPIAR</a></div></div></td></tr>")[0] ).draw();
				}else{
					table.row.add($("<tr class=\"fondo-tr odd\" data-historial='{\"evento\":\""+valor['id']+"\"}' role=\"row\"><td tabindex=\"0\" class=\"sorting_1\">"+valor['event_title']+"</td><td>"+valor['event_dateini']+"</td><td><div class=\"row\"><div class=\"col-xs-12\"><a class=\"btnActivar\"><i class=\"fa fa-toggle-on fa-2x\"></i></a></div></div></td><td><div class=\"row form-group position-right\"><div class=\"col-xs-12 col-sm-12 col-md-4\"><a class=\"btn btn-app btn-palmasinn btnModificar\"><i class=\"fa fa-pencil-square-o\"></i> MODIFICAR</a></div><div class=\"col-xs-12 col-sm-12 col-md-4\"><a class=\"btn btn-app btn-palmasinn btnRemover\"><i class=\"fa fa-trash\"></i> REMOVER</a></div><div class=\"col-xs-12 col-sm-12 col-md-4\"><a class=\"btn btn-app btn-palmasinn btnclone center\"><i class=\"fa fa-clone\"></i>COPIAR</a></div></div></td></tr>")[0] ).draw();
				}
			});
		},error:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#editeventosadvertencia").text("Algo salio mal. No se pudo encontrar lista de eventos.");
			$("#modal_alerta").modal("show")
		}
	});
});

$('table').on('click','.btnActivarcorporativo',function(){
	var elemen = this;
	var historial = $(elemen).closest('tr').data('historial')
	var url = window.location.href.substr(0,window.location.href.search("admin"));
	var i = $(this).children('i');
	$.ajax({
		url: url + "admin/actv_corporativo",
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:{
			"evento":historial["evento"],
		},
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.estado==0){
				$(i).removeClass('fa-toggle-on');
				$(i).addClass('fa-toggle-off');
			}else if(respuesta.estado==1){
				$(i).removeClass('fa-toggle-off');
				$(i).addClass('fa-toggle-on');
			}else if(respuesta.estado==2){
				$("#editeventosadvertencia").text("Algo salio mal. No se pudo registrar el evento.");
				$("#modal_alerta").modal("show");
			}
		},error:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#editeventosadvertencia").text("Algo salio mal. No se pudo registrar el evento.");
			$("#modal_alerta").modal("show");
		}
	});
});
