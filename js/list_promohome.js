$(document).ready(function(){
	var tablaHistorial = $("#promo_list").DataTable({
	"searching": true,
	"bInfo": false,
	"Info":false,
	"bLengthChange": false,
	"iDisplayLength": 1,
	"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
	},

	"fixedHeader": {
		"header": true,
		"footer": true
	},
	"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	if(tablaHistorial.rows().data().length==0){
		$("#modal_empty").modal("show");
	}
	
	var seleccion = undefined; var historial = undefined;
	$("#btnAceptar").on("click",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$("#modal_confirmarremover").modal("hide");
		$.ajax({
			url: url + "admin/delete_promo",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"promo_id":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado=="1"){
					tablaHistorial.row(seleccion).remove().draw();
					if(tablaHistorial.rows().data().length==0){
						$("#modal_empty").modal("show");
					}
				}else if(respuesta.estado=="2"){
					$("#promoadvertencia").text("No puede remover una promoción que se encuentre activa.");
					$("#modal_alerta").modal("show");
				}else if(respuesta.estado=="3"){
					$("#promoadvertencia").text("No puede remover una promoción que se encuentre asociada.");
					$("#modal_alerta").modal("show");
				}
			}
		});
	});
	
	$(document).on("click","#btnRemover",function(){
		$("#confirmarmensajeadvertencia").text("¿Esta usted seguro de remover la promoción seleccionada?");
		$("#modal_confirmarremover").modal("show");
		seleccion = $(this).parents("tr");
		historial = $(this).parents("tr").data("historial");
	});
	
	if($("#server_promo").val()!=undefined){
		$("#editpromoadvertencia").text($("#server_promo").val());
		$("#modal_alerta").modal("show");
	}
		
	$(document).on("click","#btnModificar",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var historial = $(this).parents("tr").data("historial");
		$.ajax({
			url: url + "admin/viewpromocioneshome",
			headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"promociones":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#row_edit").empty();
				$("#row_edit").html(respuesta);
			}
		});
	});
	
	$(document).on("click","#btnActivar",function(){
		var historial = $(this).parents("tr").data("historial");
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$.ajax({
			url: url + "admin/estadopromocioneshome",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			data:{
				"promo_id":historial["historial_id"],
			},
			beforeSend:function(respuesta){
				$("#modal_wait").modal("show");
			},
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#btnActivar").empty();
				if(respuesta.estado==1){
					console.log(respuesta.status["promo_status"]);
					
					if(respuesta.status["promo_status"]=="0"){
						$("#btnActivar").append("<i class=\"fa fa-power-off\"></i> ACTIVAR");
					}else{
						$("#btnActivar").append("<i class=\"fa fa-power-off\"></i> DESACTIVAR");
					}
				}else if(respuesta.estado==2){
					$("#promoadvertencia").text("Solo puede existir 3 promociones activas a la vez.");
					$("#modal_alerta").modal("show");
				}else{
					$("#promoadvertencia").text("No habido cambios en la promoción.");
					$("#modal_alerta").modal("show");
				}
			}
		});
	});

	$('html, body').animate({scrollTop: (0)}, 1500);
});